package com.procrea8.sexyafrikan.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.adapter.CommentAdapter;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.api.CommentApiInterface;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.response.CommentResponse;

import java.io.File;
import java.util.ArrayList;

import me.nereo.multi_image_selector.MultiImageSelector;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentDialog implements Contants {
    Context context;
    String type;
    String typeId;
    String entityType;
    String entityTypeId;
    CommentsChangedListener commentsChangedListener;
    Dialog dialog;
    CommentAdapter commentAdapter;
    ArrayList<CommentResponse> comments = new ArrayList<>();
    ListView commentListsView;
    Boolean isReply = false;
    ImageView imageSelector;
    EditText commentText;
    ImageView commentButton;
    TextView dialogTitle;

    static String photoPath;
    ProgressDialog progressDialog;

    public CommentDialog(Context c, Boolean reply) {
        this.context = c;
        isReply = reply;
        LayoutInflater layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.comment_dialog, null);
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context, R.style.MYDIALOG);
        alertBuilder.setView(convertView);
        //alertBuilder.setTitle("Comments");
        ImageView closePopup = (ImageView) convertView.findViewById(R.id.close_popup);
        dialog = alertBuilder.create();
        progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading..");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        commentListsView = (ListView) convertView.findViewById(R.id.comment_lists);
        commentButton = (ImageView) convertView.findViewById(R.id.comment_button);
        imageSelector = (ImageView) convertView.findViewById(R.id.select_photo);
        commentText = (EditText) convertView.findViewById(R.id.comment_text);
        commentAdapter = new CommentAdapter(context, comments, isReply);
        dialogTitle = (TextView) convertView.findViewById(R.id.dialog_title);
        commentListsView.setAdapter(commentAdapter);

        if (isReply) {
            dialogTitle.setText("Replies");
        }
        closePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) convertView.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadComments();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        imageSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ActivityBase activity = (ActivityBase) context;
                activity.setPermissionGranted(new ActivityBase.OnPermissionGranted() {
                    @Override
                    public void onGranted() {
                        MultiImageSelector.create()
                                .showCamera(true)
                                .count(1)
                                .single()
                                .start(activity, COMMENT_PHOTO_SELECT);
                    }
                });

            }
        });
        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = commentText.getText().toString();
                if (text.isEmpty() && photoPath.isEmpty()) {
                    commentText.setError("Please write something");
                    return;
                }
                progressDialog.show();
                MultipartBody.Part body = null;
                if (photoPath != null && !photoPath.isEmpty()) {
                    File file = new File(photoPath);
                    RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    body = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
                }
                Call<CommentResponse> call = null;
                if (photoPath == null) {
                    call = Api.getRetrofit().create(CommentApiInterface.class).add(
                            App.getInstance().getSession().getUserid(),
                            type,
                            typeId,
                            text,
                            entityType,
                            entityTypeId
                    );
                } else {
                    call = Api.getRetrofit().create(CommentApiInterface.class).add(
                            App.getInstance().getSession().getUserid(),
                            type,
                            typeId,
                            text,
                            entityType,
                            entityTypeId,
                            body
                    );
                }
                call.enqueue(new Callback<CommentResponse>() {
                    @Override
                    public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
                        comments.add(response.body());
                        commentAdapter.notifyDataSetChanged();
                        photoPath = null;
                        commentText.setText("");
                        progressDialog.dismiss();
                        //commentsChangedListener.onChange();
                        scrollToBottom();
                    }

                    @Override
                    public void onFailure(Call<CommentResponse> call, Throwable t) {
                        MyLog.e(t.getMessage());
                        Toast.makeText(context, "Failed to add reply", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                });

            }
        });
    }

    public static void setPhotoPath(String photo) {
        photoPath = photo;
    }

    public void show() {

        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.copyFrom(dialog.getWindow().getAttributes());
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;

        dialog.show();
        dialog.getWindow().setAttributes(params);
        //dialog.getWindow().setBackgroundDrawable(null);

        loadComments();
    }

    public void loadComments() {
        Call<ArrayList<CommentResponse>> call = Api.getRetrofit().create(CommentApiInterface.class).load(
                App.getInstance().getSession().getUserid(),
                this.type,
                this.typeId,
                "20",
                "0"
        );
        call.enqueue(new Callback<ArrayList<CommentResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<CommentResponse>> call, Response<ArrayList<CommentResponse>> response) {
                comments.clear();
                comments.addAll(response.body());
                commentAdapter.notifyDataSetChanged();
                //MyLog.e("Comments loaded - " + response.body().size());
                scrollToBottom();
            }

            @Override
            public void onFailure(Call<ArrayList<CommentResponse>> call, Throwable t) {
                MyLog.e("Comment load error -" + t.getMessage());
                Toast.makeText(context, "Failed to load comments, try again - ", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void scrollToBottom() {
        commentListsView.smoothScrollToPosition(comments.size() - 1);
    }

    public void setIsReply(Boolean reply) {
        this.isReply = reply;
    }
    public void setType(String type) {
        this.type = type;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public void setEntityTypeId(String entityTypeId) {
        this.entityTypeId = entityTypeId;
    }

    public void setOnCommentsChangeListener(CommentsChangedListener listener) {
        this.commentsChangedListener = listener;
    }

    public static class CommentsChangedListener {
        public void onChange(Integer commentsCount) {

        }
    }
}
