package com.procrea8.sexyafrikan;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.liulishuo.filedownloader.FileDownloader;
import com.procrea8.sexyafrikan.helper.Session;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.blog.BlogModule;
import com.procrea8.sexyafrikan.modules.chat.ChatModule;
import com.procrea8.sexyafrikan.modules.event.EventModule;
import com.procrea8.sexyafrikan.modules.feed.FeedModule;
import com.procrea8.sexyafrikan.modules.forum.ForumModule;
import com.procrea8.sexyafrikan.modules.group.GroupModule;
import com.procrea8.sexyafrikan.modules.hashtag.HashtagModule;
import com.procrea8.sexyafrikan.modules.home.HomeModule;
import com.procrea8.sexyafrikan.modules.lastvisitor.LastvisitorModule;
import com.procrea8.sexyafrikan.modules.marketplace.MarketplaceModule;
import com.procrea8.sexyafrikan.modules.music.MusicModule;
import com.procrea8.sexyafrikan.modules.notification.NotificationModule;
import com.procrea8.sexyafrikan.modules.page.PageModule;
import com.procrea8.sexyafrikan.modules.photo.PhotoModule;
import com.procrea8.sexyafrikan.modules.profile.ProfileModule;
import com.procrea8.sexyafrikan.modules.relationship.RelationshipModule;
import com.procrea8.sexyafrikan.modules.video.VideoModule;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class App extends Application implements Contants{
    private static App instance;
    Context context;
    Session session;
    ArrayList<ModuleInterface> modules = new ArrayList<ModuleInterface>();

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        context = getApplicationContext();
        session = Session.getInstance(context);
        TwitterAuthConfig authConfig =  new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new TwitterCore(authConfig));

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        addModule(new HomeModule()); //to prevent overriding
        addModule(new ProfileModule());
        addModule(new FeedModule());
        addModule(new RelationshipModule());
        addModule(new NotificationModule());
        addModule(new ChatModule());
        addModule(new HashtagModule());
        addModule(new ForumModule());
        addModule(new MusicModule());
        addModule(new BlogModule());
        addModule(new VideoModule());
        addModule(new EventModule());
        addModule(new GroupModule());
        addModule(new PageModule());
        addModule(new PhotoModule());
//        addModule(new MarketplaceModule());
        addModule(new LastvisitorModule());
        //NON Core modules can go between here


        //Non core modules ends here
        FileDownloader.init(context);
        init();
    }

    public Session getSession() {
        return session;
    }

    public synchronized static App getInstance() {
        return instance;
    }

    private void init() {

    }

    private void addModule(ModuleInterface moduleInterface) {
        modules.add(moduleInterface);
    }

    public ArrayList<ModuleInterface> getModules() {
        return modules;
    }

    public Boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public Boolean isNetworkAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }
}
