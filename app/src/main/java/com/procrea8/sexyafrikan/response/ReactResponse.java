package com.procrea8.sexyafrikan.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class ReactResponse extends Response {
    public ArrayList<ArrayList<String>> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<ArrayList<String>> members) {
        this.members = members;
    }

    @SerializedName("members")
    ArrayList<ArrayList<String>> members;
}
