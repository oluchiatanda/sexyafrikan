package com.procrea8.sexyafrikan.response;

import com.google.gson.annotations.SerializedName;

public class GettingStartedResponse extends Response{
    @SerializedName("textview")
    public  String textview;
    public String getTxt(){
        return textview;
    }
    public void setTextview(){this.setTextview();}

    @SerializedName("image")
    public String image;
    public String getImage(){
        return image;
    }
    public void setImage(){this.setImage();}

    @SerializedName("button")
    public String buttn;
    public String getButtn(){
        return buttn;
    }
    public void setButtn(){
        this.setButtn();
    }


}
