package com.procrea8.sexyafrikan.response;


import com.google.gson.annotations.SerializedName;

public class LikeResponse extends Response {
    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getDislikes() {
        return dislikes;
    }

    public void setDislikes(Integer dislikes) {
        this.dislikes = dislikes;
    }

    public Boolean getHas_like() {
        return has_like;
    }

    public void setHas_like(Boolean has_like) {
        this.has_like = has_like;
    }

    public Boolean getHas_dislike() {
        return has_dislike;
    }

    public void setHas_dislike(Boolean has_dislike) {
        this.has_dislike = has_dislike;
    }

    @SerializedName("likes")
    Integer likes;
    @SerializedName("dislikes")
    Integer dislikes;
    @SerializedName("has_like")
    Boolean has_like;

    @SerializedName("has_dislike")
    Boolean has_dislike;
}
