package com.procrea8.sexyafrikan.response;

import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;


public class Response implements Contants {
    @SerializedName("status")
    public Integer status;

    @SerializedName("data_one")
    public String dataOne;

    @SerializedName("data_two")
    public String dataTwo;

    @SerializedName("data_three")
    String dataThree;

    @SerializedName("data_four")
    String dataFour;

    @SerializedName("data_five")
    String dataFive;

    public String getDataFour() {
        return dataFour;
    }

    public void setDataFour(String dataFour) {
        this.dataFour = dataFour;
    }

    public String getDataFive() {
        return dataFive;
    }

    public void setDataFive(String dataFive) {
        this.dataFive = dataFive;
    }

    public String getDataThree() {
        return dataThree;
    }

    public void setDataThree(String dataThree) {
        this.dataThree = dataThree;
    }

    @SerializedName("message")
    public String message;

    @SerializedName("error_message")
    public String errorMessage;
//    public Response(){

//    }
//    Response(Integer status, String dataOne, String dataTwo, String message, String errorMessage) {
//        this.status = status;
//        this.dataOne = dataOne;
//        this.dataTwo = dataTwo;
//        this.message = message;
//        this.errorMessage = message;
//    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setDataOne(String dataOne) {
        this.dataOne = dataOne;
    }

    public String getDataOne() {
        return this.dataOne;
    }

    public void  setDataTwo(String dataTwo) {
        this.dataTwo = dataTwo;
    }

    public String getDataTwo() {
        return this.dataTwo;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {

        return this.message;
    }



}
