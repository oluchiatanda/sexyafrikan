package com.procrea8.sexyafrikan.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class GetStartedSuggestionResponse extends Response {
    public ArrayList<ArrayList<String>> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<ArrayList<String>> members) {
        this.members = members;
    }

    @SerializedName("members")
    ArrayList<ArrayList<String>> members;

    @SerializedName("userid")
    public String userid;

    @SerializedName("firstname")
    public String firstName;

    @SerializedName("lastname")
    public String lastName;

    @SerializedName("email")
    public String email;

    @SerializedName("gender")
    public String gender;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}

