package com.procrea8.sexyafrikan.response;


import com.google.gson.annotations.SerializedName;

public class FCMResponse {
    @SerializedName("type")
    String type;
    @SerializedName("message")
    String message;

    @SerializedName("userid")
    String userid;
    @SerializedName("connection_id")
    String connectionId;
    @SerializedName("enable_video")
    Boolean enableVideo;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    public Boolean getEnableVideo() {
        return enableVideo;
    }

    public void setEnableVideo(Boolean enableVideo) {
        this.enableVideo = enableVideo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
