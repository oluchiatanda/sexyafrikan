package com.procrea8.sexyafrikan.model;

import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

public class User extends Response {
    @SerializedName("id")
    String id = "";
    @SerializedName("name")
    String name = "";

    @SerializedName("activated")
    Boolean activated;

    @SerializedName("active")
    Boolean active;

    @SerializedName("avatar")
    String avatar = "";

    @SerializedName("friend_status")
    String friendStatus = "";

    @SerializedName("cover")
    String cover;

    @SerializedName("recent_photos")
    ArrayList<String> recentPhotos;

    @SerializedName("profile_info")
    ArrayList<Info> profileInfo;

    @SerializedName("city")
    String city;
    @SerializedName("state")
    String state;
    @SerializedName("notifications")
    Integer notificationCount;
    @SerializedName("friend_requests")
    Integer friendRequestsCount;
    @SerializedName("messages")
    Integer messagesCount;
    @SerializedName("password")
    String password;
    @SerializedName("bio")
    String bio;
    @SerializedName("country")
    String country;

    @SerializedName("first_name")
    String firstName;
    @SerializedName("last_name")
    String lastName;

    @SerializedName("verified")
    Boolean verified;
    @SerializedName("email_address")
    String email;
    @SerializedName("gender")
    String gender;

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public void setGender(String gender){this.gender = gender;}

    public String getGender(){return gender;}

    public String getCountry(){
        return country;
    }

    public void setCountry(String country){
        this.country = country;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    @SerializedName("can_post_timeline")
    Boolean canPostTimeline;

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getCanPostTimeline() {
        return canPostTimeline;
    }

    public void setCanPostTimeline(Boolean canPostTimeline) {
        this.canPostTimeline = canPostTimeline;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(Integer notificationCount) {
        this.notificationCount = notificationCount;
    }

    public Integer getFriendRequestsCount() {
        return friendRequestsCount;
    }

    public void setFriendRequestsCount(Integer friendRequestsCount) {
        this.friendRequestsCount = friendRequestsCount;
    }

    public Integer getMessagesCount() {
        return messagesCount;
    }

    public void setMessagesCount(Integer messagesCount) {
        this.messagesCount = messagesCount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public ArrayList<String> getRecentPhotos() {
        return recentPhotos;
    }

    public ArrayList<Info> getProfileInfo() {
        return profileInfo;
    }

    public void setProfileInfo(ArrayList<Info> profileInfo) {
        this.profileInfo = profileInfo;
    }

    public void setRecentPhotos(ArrayList<String> recentPhotos) {
        this.recentPhotos = recentPhotos;
    }

    public Boolean getFollowing() {
        return following;
    }

    public void setFollowing(Boolean following) {
        this.following = following;
    }

    @SerializedName("follow_status")
    Boolean following;


    public void setCover(String cover) {
        this.cover = cover;
    }
    public String getCover() {
        return cover;
    }

    public String getFriendStatus() {
        return friendStatus;
    }

    public void setFriendStatus(String friendStatus) {
        this.friendStatus = friendStatus;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @SerializedName("can_edit")
    Boolean canEdit;

    public Boolean getCanEdit() {
        return canEdit;
    }

    public void setCanEdit(Boolean canEdit) {
        this.canEdit = canEdit;
    }


}
