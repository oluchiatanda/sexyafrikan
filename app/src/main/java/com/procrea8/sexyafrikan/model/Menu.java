package com.procrea8.sexyafrikan.model;


import com.google.gson.annotations.SerializedName;

public class Menu {
    @SerializedName("title")
    String title;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @SerializedName("link")
    String link;
}
