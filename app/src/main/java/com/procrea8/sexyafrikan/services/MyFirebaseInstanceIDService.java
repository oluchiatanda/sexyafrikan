package com.procrea8.sexyafrikan.services;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.api.ApiInterface;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.response.Response;

import retrofit2.Call;
import retrofit2.Callback;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String refreshToken = FirebaseInstanceId.getInstance().getToken();
        if (App.getInstance().getSession().isLogin()) {
            //lets send the token to server and update the user
            Call<Response> call = Api.getRetrofit().create(ApiInterface.class).setFCM(
                    App.getInstance().getSession().getUserid(),
                    refreshToken
            );
            call.enqueue(new Callback<Response>() {
                @Override
                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                    MyLog.e("FCM Token saved on Server");
                }

                @Override
                public void onFailure(Call<Response> call, Throwable t) {
                    MyLog.e("FCM Token failed to saved on Server");
                }
            });
        }

    }
}
