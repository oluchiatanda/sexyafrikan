package com.procrea8.sexyafrikan.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.AppActivity;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.api.ApiInterface;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.mediachat.CallApiInterface;
import com.procrea8.sexyafrikan.modules.mediachat.activity.IncommingCallActivity;
import com.procrea8.sexyafrikan.modules.notification.NotificationApiInterface;
import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;
import com.procrea8.sexyafrikan.response.FCMResponse;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyFirebaseMessagingService extends FirebaseMessagingService implements Contants{
    Integer mId = 10;
    Integer fId = 20;
    Integer nId = 30;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {
            MyLog.e("Message arrived");
            Gson gson = new Gson();
            FCMResponse response = gson.fromJson(remoteMessage.getData().get("message"), FCMResponse.class);
            if (response != null) {
                if (response.getType().equals("new-event")) {
                    EventBus.getDefault().post(response);
                    //we need to show notification
                    if (App.getInstance().getSession().pushEnabled()) {
                        //MyLog.e("new Messages arrive push");
                        Call<User> call = Api.getRetrofit().create(ApiInterface.class).checkEvents(App.getInstance().getSession().getUserid());
                        call.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                Integer requestsCount = response.body().getFriendRequestsCount();
                                Integer messagesCount = response.body().getMessagesCount();
                                Integer notificationCount = response.body().getNotificationCount();

                                Boolean playSound = true;
                                if (!messagesCount.equals(0) || requestsCount > 0 || notificationCount > 0) {
                                    notifyNotifications(notificationCount, requestsCount, messagesCount, playSound);
                                    playSound = false;
                                }

                                //MyLog.e("new Messages arrive push2");
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {

                            }
                        });
                    }
                } else if(response.getType().equals("push-notification")) {
                    showPushNotification(response);
                } else if(response.getType().equals("new-call")) {
                    Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(CallApiInterface.class).getPending(App.getInstance().getSession().getUserid());
                    call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                        @Override
                        public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, Response<com.procrea8.sexyafrikan.response.Response> response) {
                            if (response.body().getStatus()==1) {
                                String identity = response.body().getDataOne();
                                String name = response.body().getDataTwo();
                                String avatar = response.body().getDataThree();
                                String type = response.body().getDataFour();
                                String connectionId = response.body().getDataFive();
                                IncommingCallActivity.launch(MyFirebaseMessagingService.this, identity, connectionId, name, avatar, type);
                            }
                        }

                        @Override
                        public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {

                        }
                    });
                }
            }
        }
    }

    public void showPushNotification(FCMResponse response) {
        if (!App.getInstance().getSession().pushEnabled()) return;
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        final Context context = this;
        final NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(largeIcon)
                        .setAutoCancel(true)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setContentText(response.getMessage());
        if (App.getInstance().getSession().pushSound()) {
            //Define sound URI
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSound(soundUri, AudioManager.STREAM_NOTIFICATION);
        }
        Intent resultIntent = new Intent(context, AppActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        stackBuilder.addParentStack(AppActivity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(mId, mBuilder.build());
    }

    public void notifyNotifications(Integer notificationCount, Integer requestCount, Integer messageCount, Boolean playSound) {
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        Integer totalCount = notificationCount + requestCount + messageCount;
        final Context context = this;
        final NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(largeIcon)
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setContentTitle(getResources().getString(R.string.notifications))
                        .setContentText(totalCount.toString() + " " + getResources().getString(R.string.new_notifications));
        if (App.getInstance().getSession().pushSound() && playSound) {
            //Define sound URI
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSound(soundUri, AudioManager.STREAM_NOTIFICATION);
        }
        final NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        //inboxStyle.setBigContentTitle(totalCount.toString() + " " + getResources().getString(R.string.new_notifications));

        if (requestCount > 0) {
            String text = requestCount.toString() + " " + getResources().getString(R.string.new_friend_requests);
            inboxStyle.addLine(text);
        }

        if (messageCount > 0) {
            String text2 = messageCount.toString() + " " + getResources().getString(R.string.new_messages);
            inboxStyle.addLine(text2);
        }
        Call<ArrayList<NotificationModel>> call = Api.getRetrofit().create(NotificationApiInterface.class).getUnreadNotifications(
                App.getInstance().getSession().getUserid(),
                "5"
        );
        call.enqueue(new Callback<ArrayList<NotificationModel>>() {
            @Override
            public void onResponse(Call<ArrayList<NotificationModel>> call, Response<ArrayList<NotificationModel>> response) {
                if (response.body().size() > 0) {
                    for (int i = 0; i < response.body().size(); i++) {
                        CharSequence text = getNotificationTitle(context, response.body().get(i));
                        inboxStyle.addLine(text);
                    }
                }

                mBuilder.setStyle(inboxStyle);
                Intent resultIntent = new Intent(context, AppActivity.class);
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

                stackBuilder.addParentStack(AppActivity.class);

                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(
                                0,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );
                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                mNotificationManager.notify(nId, mBuilder.build());
            }

            @Override
            public void onFailure(Call<ArrayList<NotificationModel>> call, Throwable t) {
                mBuilder.setStyle(inboxStyle);
                Intent resultIntent = new Intent(context, AppActivity.class);
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

                stackBuilder.addParentStack(AppActivity.class);

                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(
                                0,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );
                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                mNotificationManager.notify(nId, mBuilder.build());
            }
        });
    }

    public CharSequence getNotificationTitle(Context context, NotificationModel notificationModel) {
        ArrayList<ModuleInterface> modules = App.getInstance().getModules();
        for (int i = 0; i < modules.size(); i++) {
            ModuleInterface module = modules.get(i);
            CharSequence str = (CharSequence) module.invoke(context, "notification-own-title", notificationModel);
            if (str != null) return str;
        }
        CharSequence title = TextUtils.concat("", "", "");
        SpannableStringBuilder spanName = new SpannableStringBuilder(notificationModel.getName());
        spanName.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, notificationModel.getName().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        title = TextUtils.concat(title, "",spanName);
        CharSequence aTitle = null;
        for (int i = 0; i < modules.size(); i++) {
            ModuleInterface module = modules.get(i);
            CharSequence str = (CharSequence) module.invoke(context, "notification-title", notificationModel);
            if (str != null) aTitle = str;
        }

        if (aTitle != null) title = TextUtils.concat(title, " ", aTitle);
        return title;
    }


}
