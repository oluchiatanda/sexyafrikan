package com.procrea8.sexyafrikan.helper;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.procrea8.sexyafrikan.Contants;

public class Session implements Contants{
    private SharedPreferences preferences;

    private static Session instance;

    private String userid;
    private String firstName;
    private String lastName;
    private String email;
    private String gender;
    private String country;
    private String dateofbirth;


    public Session(Context c) {
        preferences = PreferenceManager.getDefaultSharedPreferences(c);
    }

    public static Session getInstance(Context c) {
        if (instance != null) return instance;
        instance = new Session(c);
        return instance;
    }

    public boolean isLogin() {
        String userid = this.getUserid();
        if (userid != null) return true;
        return false;
    }

    public boolean logout() {
        preferences.edit().remove("userid").apply();
        preferences.edit().remove("firstname").apply();

        return true;
    }

    public String getUserid() {
        return preferences.getString("userid", null);
    }

    public Boolean pushEnabled() {
        return preferences.getBoolean("push-notification", true);
    }

    public Boolean pushSound() {
        return preferences.getBoolean("notification-sound", true);
    }

    public void setUserid(String userid) {
        preferences.edit().putString("userid", userid).apply();
    }

    public String getAvatar() {
        return preferences.getString("avatar", null);
    }

    public void setAvatar(String avatar) {
        preferences.edit().putString("avatar", avatar).apply();
    }

    public String getPassword() {
        return preferences.getString("password", null);
    }

    public void setPassword(String password) {
        preferences.edit().putString("password", password).apply();
    }

    public String getState() {
        return preferences.getString("state", null);
    }

    public void setState(String state) {
        preferences.edit().putString("state", state).apply();
    }

    public String getCover() {
        return preferences.getString("cover", null);
    }

    public void setCover(String cover) {
        preferences.edit().putString("cover", cover).apply();
    }
    public String getFirstName() {
        return preferences.getString("firstname", null);
    }

    public void setFirstName(String firstName) {
        preferences.edit().putString("firstname", firstName).apply();
    }

    public String getLastName() {
        return preferences.getString("lastname", null);
    }

    public void setLastName(String lastName) {
        preferences.edit().putString("lastname", lastName).apply();
    }

    public String getEmail() {
        return preferences.getString("email", null);
    }

    public void setEmail(String email) {
        preferences.edit().putString("email",email).apply();
    }

    public String getGender() {
        return preferences.getString("gender", null);
    }

    public void setGender(String gender) {
        preferences.edit().putString("gender", gender).apply();
    }

    public String getCountry(){
        return preferences.getString("country",null);
    }
    public void setCountry(String country){
        preferences.edit().putString("country",country).apply();
    }

    public String getDateofbirth(){return preferences.getString("Age",null);}
    public void setDateofbirth(String dateofbirth){
        preferences.edit().putString("Date",country).apply();
    }


    public String getLikeType() {
        return preferences.getString("like_type", "reaction");
    }

    public void setLikeType(String likeType) {
        preferences.edit().putString("like_type", likeType).apply();
    }

    public Boolean isDislikeEnabled() {
        return preferences.getBoolean("dislike_enable", true);
    }

    public void setDislikeEnabled(Boolean b) {
        preferences.edit().putBoolean("dislike_enable", b).apply();
    }
    public void setTextview(String textview){
        preferences.edit().putString("textview",textview).apply();
    }
    public void setImage(String image){
        preferences.edit().putString("image", image).apply();
    }
    public void setButtn(String buttn){
        preferences.edit().putString("buttn", buttn).apply();
    }

    public void setBio(String bio) {
        preferences.edit().putString("bio", bio).apply();
    }
    public String getBio() {
        return preferences.getString("bio", "");
    }
    public void setCity(String city) {
        preferences.edit().putString("city", city).apply();
    }
    public String getCity() {
        return preferences.getString("bio", "");
    }
}
