package com.procrea8.sexyafrikan.helper;


import android.content.Context;
import android.content.res.Resources;

import com.procrea8.sexyafrikan.R;

public class TimeAgo {
    Context context;

    public TimeAgo(Context c) {

    }

    public static String format(Context context, String time) {
        String result = "";
        Resources resource = context.getResources();
        String[] split = time.split(":");
        String numbr = split[0];
        String format = split[1];
        switch (format) {
            case "seconds-ago":
                if (numbr.equals("0")) {
                    return resource.getString(R.string.just_now);
                } else {
                    return numbr + " " + resource.getString(R.string.seconds_ago);
                }

            case "minutes-ago":
                if (numbr.equals("1")) {
                    return resource.getString(R.string.a_minute_ago);
                } else {
                    return numbr + " " + resource.getString(R.string.minutes_ago);
                }

            case "hours-ago":
                if (numbr.equals("1")) {
                    return resource.getString(R.string.an_hour_ago);
                } else {
                    return numbr + " " + resource.getString(R.string.hours_ago);
                }
            case "days-ago":
                if (numbr.equals("1")) {
                    return resource.getString(R.string.a_day_ago);
                } else {
                    return numbr + " " + resource.getString(R.string.days_ago);
                }
            case "weeks-ago":
                if (numbr.equals("1")) {
                    return resource.getString(R.string.a_week_ago);
                } else {
                    return numbr + " " + resource.getString(R.string.weeks_ago);
                }
            case "months-ago":
                if (numbr.equals("1")) {
                    return resource.getString(R.string.a_month_ago);
                } else {
                    return numbr + " " + resource.getString(R.string.months_ago);
                }
            case "years-ago":

                if (numbr.equals("1")) {
                    return resource.getString(R.string.a_year_ago);
                } else {
                    return numbr + " " + resource.getString(R.string.years_ago);
                }
        }
        return result;
    }
}
