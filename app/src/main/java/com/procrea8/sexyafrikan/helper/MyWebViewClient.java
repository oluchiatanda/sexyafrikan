package com.procrea8.sexyafrikan.helper;


import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.procrea8.sexyafrikan.modules.feed.activity.FeedActivity;
import com.procrea8.sexyafrikan.modules.hashtag.activity.HashtagActivity;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;

public class MyWebViewClient extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith("hashtag:")) {
            String[] str = url.split(":");
            String hashtag = str[1];
            HashtagActivity.launch(view.getContext(), hashtag);
        } else if(url.startsWith("mention:")) {
            String[] str = url.split(":");
            String userid = str[1];
            ProfileActivity.load(view.getContext(), userid);
        } else if (url.startsWith("readmore:")) {
            String[] str = url.split(":");
            String type  = str[1];
            String typeId = str[2];
            if (type.equals("feed")) {
                FeedActivity.load(view.getContext(), typeId);
            }
            return true;
        }
        else {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            view.getContext().startActivity(intent);
        }
        return true;
    }

}
