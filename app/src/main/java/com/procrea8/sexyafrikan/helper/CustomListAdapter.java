//package com.procrea8.crea8.helper;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.bumptech.glide.Glide;
//import com.procrea8.crea8.R;
//import com.procrea8.crea8.model.User;
//import com.procrea8.crea8.modules.event.model.Event;
//import com.procrea8.crea8.modules.group.model.Group;
//import com.procrea8.crea8.modules.marketplace.model.Listing;
//import com.procrea8.crea8.modules.music.model.Music;
//import com.procrea8.crea8.modules.page.model.Page;
//import com.procrea8.crea8.modules.video.model.Video;
//
//import java.util.ArrayList;
//
//public class CustomListAdapter extends BaseAdapter {
//    private ArrayList itemname;
//    private LayoutInflater layoutInflater;
////    ArrayList<User> usersList = new ArrayList<>();
////    ArrayList<Music> songs = new ArrayList<>();
////    ArrayList<Page> pages = new ArrayList<>();
////    ArrayList<Group> groups = new ArrayList<>();
////    ArrayList<Video> video = new ArrayList<>();
////    ArrayList<Listing> market = new ArrayList<>();
////    ArrayList<Event> events = new ArrayList<>();
//    Context context;
//
//    public CustomListAdapter(Context context, ArrayList itemname) {
//        this.context = context;
//        this.itemname = itemname;
//        layoutInflater = LayoutInflater.from(context);
//    }
//
//    @Override
//    public int getCount() {
//        return itemname.size();
//    }
//
////        public void addSection(String caption, Context context, ArrayList itemname) {
////
////            itemname.add(new CustomListAdapter(caption, context, itemname));
////        }
//
//    @Override
//    public Object getItem(int position) {
//        return itemname.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHolder holder;
//        if (convertView == null) {
//            convertView = layoutInflater.inflate(R.layout.list_item, null);
//            holder = new ViewHolder();
//
//            holder.text = (TextView) convertView.findViewById(R.id.text);
//            holder.image = (ImageView) convertView.findViewById(R.id.image);
//
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//
//        try {
//            for (int i = 0; i < usersList.size(); i++) {
//                User userList = (User) itemname.get(position);
//                holder.text.setText(userList.getName());
//                if (holder.image != null) {
//                    Glide.with(context).load(userList.getAvatar()).crossFade().into(holder.image);
//                }
//            }
//        } catch (Exception e) {
////                MyLog.e("user" + e.getMessage());
//        }
//
//        try {
//            for (int i = 0; i < songs.size(); i++) {
//                Music musicList = (Music) itemname.get(position);
//                holder.text.setText(musicList.getTitle());
//                if (holder.image != null) {
//                    Glide.with(context).load(musicList.getCover()).crossFade().into(holder.image);
//                }
//            }
//        } catch (Exception e) {
////                MyLog.e("music" + e.getMessage());
//        }
//
//        try {
//
//            for (int i = 0; i < pages.size(); i++) {
//                Page pageList = (Page) itemname.get(position);
//                holder.text.setText(pageList.getTitle());
//                if (holder.image != null) {
//                    Glide.with(context).load(pageList.getCover()).crossFade().into(holder.image);
//                }
//            }
//        } catch (Exception e) {
////                MyLog.e("page" + e.getMessage());
//        }
//        try {
//
//            for (int i = 0; i < groups.size(); i++) {
//                Group groupList = (Group) itemname.get(position);
//                holder.text.setText(groupList.getTitle());
//                if (holder.image != null) {
//                    Glide.with(context).load(groupList.getCover()).crossFade().into(holder.image);
//                }
//            }
//        } catch (Exception e) {
////                MyLog.e("group" + e.getMessage());
//        }
////
//        try {
//            for (int i = 0; i < video.size(); i++) {
//                Video videoList = (Video) itemname.get(position);
//                holder.text.setText(videoList.getTitle());
//                if (holder.image != null) {
//                    Glide.with(context).load(videoList.getPhoto()).crossFade().into(holder.image);
//                }
//            }
////                }
//        } catch (Exception e) {
////                MyLog.e("video" + e.getMessage());
//        }
//
//        try {
//            for (int i = 0; i < market.size(); i++) {
//                Listing marketList = (Listing) itemname.get(position);
//                holder.text.setText(marketList.getTitle());
//                if (holder.image != null) {
//                    Glide.with(context).load(marketList.getImage()).crossFade().into(holder.image);
//                }
//            }
//        } catch (Exception e) {
////
//        }
//
//        try {
//            for (int i = 0; i < events.size(); i++) {
//                Event eventList = (Event) itemname.get(position);
//                holder.text.setText(eventList.getTitle());
//                if (holder.image != null) {
//                    Glide.with(context).load(eventList.getImage()).crossFade().into(holder.image);
//                }
//            }
//        } catch (Exception e) {
////
//        }
//
//        return convertView;
//    }
//
//    class ViewHolder {
//        TextView text;
//        ImageView image;
//
//    }
//}