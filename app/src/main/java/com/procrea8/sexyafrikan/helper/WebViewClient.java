package com.procrea8.sexyafrikan.helper;

import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;

import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.modules.hashtag.activity.HashtagActivity;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;


public class WebViewClient extends android.webkit.WebViewClient implements Contants{
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith("hashtag:")) {
            String[] str = url.split(":");
            String hashtag = str[1];
            HashtagActivity.launch(view.getContext(), hashtag);
        } else if(url.startsWith("mention:")) {
            String[] str = url.split(":");
            String userid = str[1];
            ProfileActivity.load(view.getContext(), userid);
        } else {
            if (Uri.parse(url).getHost().endsWith(DOMAIN)) {
                if (url.contains("&")) {
                    url = url + "&webview=true";
                } else {
                    url = url + "?webview=true";
                }
                //view.loadUrl(url);
                MyLog.e("WebView Url - " + url);
                return false;
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                view.getContext().startActivity(intent);
            }

        }
        return true;
    }

}
