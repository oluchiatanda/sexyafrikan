package com.procrea8.sexyafrikan;


import com.google.gson.annotations.SerializedName;

public class Users implements Contants {


    public Users[] newArray(int i){return new Users[i];}

    @SerializedName("userid")
    public String getUserid;

    @SerializedName("bio")
    public String getBio;

    @SerializedName("avatar")
    public String avatar;

    public String getGetUserid() {
        return getUserid;
    }

    public void setGetUserid(String getUserid) {
        this.getUserid = getUserid;
    }

    public String getGetBio() {
        return getBio;
    }

    public void setGetBio(String getBio) {
        this.getBio = getBio;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}


