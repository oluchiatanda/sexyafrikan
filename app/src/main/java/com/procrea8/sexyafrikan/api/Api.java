package com.procrea8.sexyafrikan.api;


import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.helper.MyLog;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api implements Contants{

    public static Retrofit retrofit;
    public static Retrofit retrofit2;
    public static ApiInterface service;

    public static Retrofit getRetrofit() {
        return getRetrofit(false);
    }
    public static Retrofit getRetrofit(Boolean cache) {
        OkHttpClient okHttpClient = null;
        if (cache) {
            okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(300, TimeUnit.SECONDS)
                    .connectTimeout(300, TimeUnit.SECONDS)
                    .cache(new Cache(App.getInstance().getCacheDir(), 10*1024*1024))
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request request = chain.request();
                            if (App.getInstance().isNetworkAvailable()) {
                                request = request.newBuilder().header("Cache-Control", "public, max-age=" + 60).build();
                            } else {
                                MyLog.e("No network");
                                request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build();
                            }
                            return chain.proceed(request);
                        }
                    })
                    .build();
        } else {
//            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
//                @Override
//                public void log(String message) {
//                    MyLog.e("Body - content - " + message);
//                }
//            });
//            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(300, TimeUnit.SECONDS)
                    .connectTimeout(300, TimeUnit.SECONDS)
//                    .addInterceptor(interceptor)
                    .build();
        }

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit;

    }

    public static ApiInterface getService() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(300, TimeUnit.SECONDS)
                .connectTimeout(300, TimeUnit.SECONDS)
                .build();

        retrofit2=new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        return service;
    }

    public static void setService(ApiInterface service) {
        Api.service = service;
    }
}
