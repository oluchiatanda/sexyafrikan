package com.procrea8.sexyafrikan.api;


import com.procrea8.sexyafrikan.model.Menu;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.response.ForgotpasswordResponse;
import com.procrea8.sexyafrikan.response.GetStartedSuggestionResponse;
import com.procrea8.sexyafrikan.response.GettingStartedResponse;
import com.procrea8.sexyafrikan.response.LoginResponse;
import com.procrea8.sexyafrikan.response.Response;
import com.procrea8.sexyafrikan.response.SignUpResponse;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;


public interface ApiInterface {
    @POST("login")
    Call<LoginResponse> loginUser(@Query("username") String username, @Query("password") String password);

    @GET("get/menu")
    Call<ArrayList<Menu>> getMenus();

    @POST("signup")
    Call<SignUpResponse> signupUser(@Query("firstname") String firstname, @Query("lastname")String lastname,
                                    @Query("username") String username, @Query("email_address") String email,
                                    @Query("password")String password, @Query("gender") String spinner,
                                    @Query("country") String spinnerCountry,@Query("age") String dateofbirth
                                    );

    @POST("forgotpassword")
    Call<ForgotpasswordResponse> forgotpassword(@Query("email") String email);
    @Multipart
    @POST("getstarted/upload")
    Call<GettingStartedResponse> gettingStarted(@Part MultipartBody.Part avatar,
                                                @Query ("userid") String userId);

    @Multipart
    @POST("getstarted/suggestion")
    Call<GetStartedSuggestionResponse> getStartedSuggestion(@Part MultipartBody.Part avatar,
                                                           @Query("userid")String userid,
                                                            @Query("firstname") String firstname,
                                                            @Query("lastname") String lastname);


    @GET("check/login")
    Call<User> checkLogin(@Query("userid") String userid);

    @GET("check/events")
    Call<User> checkEvents(@Query("userid") String userid);

    @GET("set/fcm/token")
    Call<Response> setFCM(@Query("userid") String userid, @Query("token") String token);

    @GET("register/social")
    Call<LoginResponse> socialLogin(@Query("id") String id, @Query("firstname") String firstname, @Query("lastname")String lastname,
                                     @Query("username") String username, @Query("email") String email,
                                     @Query("gender") String gender, @Query("image") String image, @Query("type") String type);

}
