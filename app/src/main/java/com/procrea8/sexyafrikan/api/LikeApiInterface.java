package com.procrea8.sexyafrikan.api;

import com.procrea8.sexyafrikan.response.LikeResponse;
import com.procrea8.sexyafrikan.response.ReactResponse;
import com.procrea8.sexyafrikan.response.Response;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface LikeApiInterface {
    @GET("like/item")
    Call<LikeResponse> likeItem(@Query("userid") String userid, @Query("type") String type, @Query("type_id") String typeId);

    @GET("dislike/item")
    Call<LikeResponse> dislikeItem(@Query("userid") String userid, @Query("type") String type, @Query("type_id") String typeId);

    @GET("react/item")
    Call<Response> reactItem(@Query("userid") String userid, @Query("type") String type, @Query("type_id") String typeId, @Query("code") String code);

    @GET("react/load")
    Call<ReactResponse> getReacts(@Query("type") String type, @Query("type_id") String typeId);

    @GET("react/load")
    Call<ReactResponse> getReacts(@Query("type") String type, @Query("type_id") String typeId, @Query("limit") String limit);
}
