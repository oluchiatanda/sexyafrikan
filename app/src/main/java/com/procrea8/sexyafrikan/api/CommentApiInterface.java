package com.procrea8.sexyafrikan.api;

import com.procrea8.sexyafrikan.response.CommentResponse;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface CommentApiInterface {
    @GET("comment/load")
    Call<ArrayList<CommentResponse>> load(@Query("userid") String userid, @Query("type") String type, @Query("type_id") String type_id, @Query("limit") String limit, @Query("offset") String offset);

    @Multipart
    @POST("comment/add")
    Call<CommentResponse> add(@Query("userid") String userid, @Query("type") String type, @Query("type_id") String typeId, @Query("text") String text
    ,@Query("entity_type") String entityType, @Query("entity_id") String entityId, @Part MultipartBody.Part image);


    @FormUrlEncoded
    @POST("comment/add")
    Call<CommentResponse> add(@Field("userid") String userid, @Field("type") String type, @Field("type_id") String typeId, @Field("text") String text
            ,@Field("entity_type") String entityType, @Field("entity_id") String entityId);

    @GET("comment/remove")
    Call<Response> remove(@Query("userid") String userid, @Query("id") String id);
}
