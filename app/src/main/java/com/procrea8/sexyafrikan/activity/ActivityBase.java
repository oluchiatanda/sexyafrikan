package com.procrea8.sexyafrikan.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.adapter.ReactorsAdapter;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.api.LikeApiInterface;
import com.procrea8.sexyafrikan.dialogs.CommentDialog;
import com.procrea8.sexyafrikan.modules.feed.dialogs.EditorDialog;
import com.procrea8.sexyafrikan.modules.marketplace.dialog.MarketplaceCreateDialog;
import com.procrea8.sexyafrikan.modules.music.dialog.MusicCreateDialog;
import com.procrea8.sexyafrikan.modules.video.dialog.VideoCreateDialog;
import com.procrea8.sexyafrikan.response.LikeResponse;
import com.procrea8.sexyafrikan.response.ReactResponse;
import com.procrea8.sexyafrikan.response.Response;

import net.alhazmy13.mediapicker.Video.VideoPicker;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;
import retrofit2.Call;
import retrofit2.Callback;

public abstract class ActivityBase extends AppCompatActivity implements Contants {
    ProgressDialog progressDialog;

    //reacts and like system
    LinearLayout likeButton;
    LinearLayout replyButton;
    LinearLayout dislikeButton;
    ImageView likeButtonIcon;
    ImageView dislikeButtonIcon;
    ImageView reactLike;
    ImageView reactLove;
    ImageView reactHaha;
    ImageView react_sad;
    ImageView reactWow;
    ImageView reactYay;
    ImageView reactAngry;
    LinearLayout reactIcons;
    TextView commentCountView;
    TextView likeCountView;
    TextView dislikeCountView;
    LinearLayout shareButton;
    LinearLayout feedReacts;
    CircleImageView reactorsUser1;
    CircleImageView reactorsUser2;
    CircleImageView reactorsUser3;
    CircleImageView reactorsUser4;
    CircleImageView reactorsMore;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO
    };
    public static OnPermissionGranted permissionGranted;

    @Override
    protected void onCreate(Bundle saveInstance) {
        super.onCreate(saveInstance);
        progressDialog = new ProgressDialog(this);
    }

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        } else {
            if (permissionGranted != null) {
                permissionGranted.onGranted();
                permissionGranted = null;
            }
        }
    }

    public void setLoading() {
        setLoading(null);
    }

    public void setLoading(String message) {
        progressDialog.setIndeterminate(true);
        if (message == null) {
            progressDialog.setMessage("Loading..");
        } else {
            progressDialog.setMessage(message);
        }
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    public void hideLoading() {
        progressDialog.hide();
    }

    /**
     * Converting dp to pixel
     */
    public int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public String getAudioMediaPath(Uri fileUri) {
        String[] filePathColumn = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getContentResolver().query(fileUri, filePathColumn, null, null, null);
        assert cursor != null;
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        return cursor.getString(columnIndex);
    }

    public void setPermissionGranted(OnPermissionGranted p) {
        permissionGranted = p;
        verifyStoragePermissions(this);
    }




    public static class OnPermissionGranted {
        public void onGranted() {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (permissionGranted != null) {
                        permissionGranted.onGranted();
                        permissionGranted = null;
                    }
                } else {
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == COMMENT_PHOTO_SELECT && resultCode == RESULT_OK) {
            List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
            final String photoPath = path.get(0);
            setPermissionGranted(new OnPermissionGranted(){
                @Override
                public void onGranted() {
                    CommentDialog.setPhotoPath(photoPath);
                }
            });
        } else if(requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            final String mPath = data.getStringExtra(VideoPicker.EXTRA_VIDEO_PATH);
            setPermissionGranted(new OnPermissionGranted() {
                @Override
                public void onGranted() {
                    try{
                        EditorDialog.setVideoPath(mPath);
                    } catch (Exception e){}

                    try{
                        VideoCreateDialog.setVideoPath(mPath);
                    } catch (Exception e){}
                }
            });

        } else if(requestCode == FEED_PHOTO_SELECT && resultCode == RESULT_OK) {
            final List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);

            setPermissionGranted(new OnPermissionGranted() {
                @Override
                public void onGranted() {
                    EditorDialog.setImagePath(path);
                }
            });
        } else if(requestCode == FEED_VIDEO_SELECT && resultCode == RESULT_OK && data != null && data.getData() != null) {


        } else if(requestCode == MUSIC_SELECT_COVER && resultCode == RESULT_OK) {
            List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
            final String photoPath = path.get(0);
            setPermissionGranted(new OnPermissionGranted() {
                @Override
                public void onGranted() {
                    MusicCreateDialog.setArtFile(photoPath);
                }
            });
        }else if(requestCode == MUSIC_SELECT_SONG && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri fileUri = data.getData();
            final String musicPAth = getAudioMediaPath(fileUri);

            setPermissionGranted(new OnPermissionGranted() {
                @Override
                public void onGranted() {
                    if (musicPAth != null) MusicCreateDialog.setMusicFile(musicPAth);
                }
            });
        } else if (requestCode == MARKETPLACE_SELECT_COVER && resultCode == RESULT_OK) {
            List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
            final String photoPath = path.get(0);
            setPermissionGranted(new OnPermissionGranted() {
                @Override
                public void onGranted() {
                    MarketplaceCreateDialog.setArtFile(photoPath);
                }
            });
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initGlobalFooter(final Context context, final String type, final String typeId, final Boolean hasReact, final Boolean hasLike,
                                 Boolean hasDislike,
                                 Integer likeCount, Integer dislikeCount, Integer commentCount) {
        initGlobalFooter(context, type, typeId, hasReact, hasLike,
                hasDislike,
                likeCount, dislikeCount, commentCount, false);
    }

    public void initGlobalFooterComment(final Context context, final String type, final String typeId, Integer commentCount) {
        initGlobalFooter(context, type, typeId, false, false,
                false,
                0, 0, commentCount, true);
    }

    public void initGlobalFooter(final Context context, final String type, final String typeId, final Boolean hasReact, final Boolean hasLike,
                                 Boolean hasDislike,
                                 Integer likeCount, Integer dislikeCount, Integer commentCount, Boolean commentOnly) {
        likeButton = (LinearLayout) findViewById(R.id.like_button);
        dislikeButton = (LinearLayout) findViewById(R.id.dislike_button);
        likeButtonIcon = (ImageView) findViewById(R.id.like_button_icon);
        dislikeButtonIcon = (ImageView) findViewById(R.id.dislike_button_icon);
        replyButton = (LinearLayout) findViewById(R.id.reply_button);
        reactIcons = (LinearLayout) findViewById(R.id.react_icons);
        react_sad = (ImageView) findViewById(R.id.react_sad);
        reactHaha = (ImageView) findViewById(R.id.react_haha);
        reactLike = (ImageView) findViewById(R.id.react_like);
        reactLove = (ImageView) findViewById(R.id.react_love);
        reactWow = (ImageView) findViewById(R.id.react_wow);
        reactAngry = (ImageView) findViewById(R.id.react_angry);
        reactYay = (ImageView) findViewById(R.id.react_yay);
        commentCountView = (TextView) findViewById(R.id.comment_count);
        likeCountView = (TextView) findViewById(R.id.like_count);
        dislikeCountView = (TextView) findViewById(R.id.dislike_count);
        feedReacts = (LinearLayout) findViewById(R.id.feed_reacts);
        reactorsUser1 = (CircleImageView) findViewById(R.id.reactors_user1);
        reactorsUser2 = (CircleImageView) findViewById(R.id.reactors_user2);
        reactorsUser3 = (CircleImageView) findViewById(R.id.reactors_user3);
        reactorsUser4 = (CircleImageView) findViewById(R.id.reactors_user4);
        reactorsMore = (CircleImageView) findViewById(R.id.reactors_more);
        reactIcons.setVisibility(View.GONE);

        if (!App.getInstance().getSession().isDislikeEnabled()) {
            dislikeButton.setVisibility(View.GONE);
            dislikeCountView.setVisibility(View.GONE);
        }

        if (App.getInstance().getSession().getLikeType().equals("reaction")) {
            //viewHolder.reactIcons.setVisibility(View.VISIBLE);
            feedReacts.setVisibility(View.VISIBLE);
            dislikeButton.setVisibility(View.GONE);
            dislikeCountView.setVisibility(View.GONE);
            likeCountView.setVisibility(View.GONE);

            //load reactions
            load_reactions(context, type, typeId);
            if (hasReact) {
                likeButtonIcon.setImageResource(R.drawable.ic_thumb_up_done);
            } else {
                likeButtonIcon.setImageResource(R.drawable.ic_thumb_up);
            }
        } else {
            feedReacts.setVisibility(View.GONE);
            likeCountView.setText(likeCount.toString());
            dislikeCountView.setText(dislikeCount.toString());
            if (hasLike) {
                likeButtonIcon.setImageResource(R.drawable.ic_thumb_up_done);
            } else {
                likeButtonIcon.setImageResource(R.drawable.ic_thumb_up);
            }

            if (hasDislike) {
                dislikeButtonIcon.setImageResource(R.drawable.ic_thumb_down_done);
            } else {
                dislikeButtonIcon.setImageResource(R.drawable.ic_thumb_down);
            }
        }
        feedReacts.setVisibility(View.GONE);
        likeButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (App.getInstance().getSession().getLikeType().equals("reaction")) {
                    reactIcons.setVisibility(View.VISIBLE);
                }
                return true;
            }
        });
        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (v.getResources())
                if (App.getInstance().getSession().getLikeType().equals("reaction")) {
                    //viewHolder.reactIcons.setVisibility(View.VISIBLE);

                    //likeButtonIcon.setImageResource(R.drawable.ic_thumb_up);
                    if (!hasReact) {
                        processReact(context, type, typeId, "1"); //remove reaction
                        likeButtonIcon.setImageResource(R.drawable.ic_thumb_up_done);
                    } else {
                        processReact(context, type, typeId, "0"); //remove reaction
                        likeButtonIcon.setImageResource(R.drawable.ic_thumb_up);
                    }
                } else {
                    likeButtonIcon.setImageResource(R.drawable.ic_thumb_up_done);
                    String userid = App.getInstance().getSession().getUserid();

                    Call<LikeResponse> call = Api.getRetrofit().create(LikeApiInterface.class).likeItem(userid, type, typeId);
                    call.enqueue(new Callback<LikeResponse>() {
                        @Override
                        public void onResponse(Call<LikeResponse> call, retrofit2.Response<LikeResponse> response) {
                            //processLike("like", viewHolder, i, response.body());
                        }

                        @Override
                        public void onFailure(Call<LikeResponse> call, Throwable t) {
                            Toast.makeText(context, "Failed to complete that" + t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        dislikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dislikeButtonIcon.setImageResource(R.drawable.ic_thumb_down_done);
                String userid = App.getInstance().getSession().getUserid();

                Call<LikeResponse> call = Api.getRetrofit().create(LikeApiInterface.class).dislikeItem(userid, type, typeId);
                call.enqueue(new Callback<LikeResponse>() {
                    @Override
                    public void onResponse(Call<LikeResponse> call, retrofit2.Response<LikeResponse> response) {
                        //processLike("dislike", viewHolder, i, response.body());
                    }

                    @Override
                    public void onFailure(Call<LikeResponse> call, Throwable t) {
                        Toast.makeText(context, "Failed to complete that" + t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        reactLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processReact(context, type, typeId, "1");
            }
        });
        reactLove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processReact(context, type, typeId, "4");
            }
        });
        reactWow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processReact(context, type, typeId, "7");
            }
        });
        reactHaha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processReact(context, type, typeId, "5");
            }
        });
        react_sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                processReact(context, type, typeId, "8");
            }
        });
        reactAngry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processReact(context, type, typeId, "9");
            }
        });
        reactYay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processReact(context, type, typeId, "6");
            }
        });
        reactorsMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int position = (int) v.getTag();
                showReactorsDialog(context, type, typeId);
            }
        });

        replyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showComments(context, type, typeId);
            }
        });

        if (commentOnly) {
            likeButton.setVisibility(View.GONE);
            dislikeButton.setVisibility(View.GONE);
        }

        commentCountView.setText(commentCount.toString());

    }

    public void showComments(Context context, String type, String typeId) {

        CommentDialog dialog = new CommentDialog(context, false);
        dialog.setType(type);
        dialog.setTypeId(typeId);
        dialog.setEntityType("user");
        dialog.setEntityTypeId(App.getInstance().getSession().getUserid());
        dialog.setOnCommentsChangeListener(new CommentDialog.CommentsChangedListener() {
            @Override
            public void onChange(Integer count) {

            }
        });

        dialog.show();
    }

    public void processReact(final Context context, final String type, final String typeId, String code) {
        likeButtonIcon.setImageResource(R.drawable.ic_thumb_up_done);
        reactIcons.setVisibility(View.GONE);

        String userid = App.getInstance().getSession().getUserid();

        Call<Response> call = Api.getRetrofit().create(LikeApiInterface.class).reactItem(userid, type, typeId, code);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                //we can load the react stats now
                load_reactions(context, type, typeId);
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Toast.makeText(context, "Failed to react to feed, check your network connection", Toast.LENGTH_LONG).show();
                likeButtonIcon.setImageResource(R.drawable.ic_thumb_up);
            }
        });
    }

    public void showReactorsDialog(Context context, String type, String typeId) {

        AlertDialog.Builder alertBuider = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.reactors_dialog, null);
        alertBuider.setView(convertView);
        alertBuider.setTitle("People who react to this");
        ListView reactorsListView = (ListView) convertView.findViewById(R.id.reactorLists);
        ArrayList<ArrayList<String>> members = new ArrayList<>();
        final ReactorsAdapter reactorsAdapter = new ReactorsAdapter(context, members);
        reactorsListView.setAdapter(reactorsAdapter);
        Call<ReactResponse> call = Api.getRetrofit().create(LikeApiInterface.class).getReacts(type, typeId, "50");
        call.enqueue(new Callback<ReactResponse>() {
            @Override
            public void onResponse(Call<ReactResponse> call, retrofit2.Response<ReactResponse> response) {
                reactorsAdapter.addAll(response.body().getMembers());
            }

            @Override
            public void onFailure(Call<ReactResponse> call, Throwable t) {

            }
        });
        alertBuider.create().show();

    }

    public void load_reactions(final Context context, String type, String typeId) {
        Call<ReactResponse> call = Api.getRetrofit().create(LikeApiInterface.class).getReacts(type, typeId);
        call.enqueue(new Callback<ReactResponse>() {
            @Override
            public void onResponse(Call<ReactResponse> call, retrofit2.Response<ReactResponse> response) {


                ArrayList<ArrayList<String>> members = response.body().getMembers();

                if (members.size() > 0) {
                    feedReacts.setVisibility(View.VISIBLE);
                    show_react_members(context, members);
                } else {
                    feedReacts.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ReactResponse> call, Throwable t) {

            }
        });
    }

    public void show_react_members(Context context, ArrayList<ArrayList<String>> members) {
        reactorsUser1.setVisibility(View.VISIBLE);
        reactorsMore.setVisibility(View.VISIBLE);
        if (context == null) return;
        Glide.with(context).load(members.get(0).get(0)).crossFade().into(reactorsUser1);
        if (members.size() > 1) {
            reactorsUser2.setVisibility(View.VISIBLE);
            Glide.with(context).load(members.get(1).get(0)).crossFade().into(reactorsUser2);
        } else {
            reactorsUser2.setVisibility(View.GONE);
        }

        if (members.size() > 2) {
            reactorsUser3.setVisibility(View.VISIBLE);
            Glide.with(context).load(members.get(2).get(0)).crossFade().into(reactorsUser3);
        } else {
            reactorsUser3.setVisibility(View.GONE);
        }

        if (members.size() > 3) {
            reactorsUser4.setVisibility(View.VISIBLE);
            Glide.with(context).load(members.get(3).get(0)).crossFade().into(reactorsUser4);
        } else {
            reactorsUser4.setVisibility(View.GONE);
        }
    }


    /**
     * Function to convert milliseconds time to
     * Timer Format
     * Hours:Minutes:Seconds
     */
    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

}