package com.procrea8.sexyafrikan.activity;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.api.ApiInterface;
import com.procrea8.sexyafrikan.helper.Helper;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.helper.Session;
import com.procrea8.sexyafrikan.response.SignUpResponse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends ActivityBase implements View.OnClickListener {
    EditText firstname, lastname, username, email, password, confirmpassword;
    ImageView joinusbutton, backkey;
    Spinner gender, selcountry;
    TextView selectcountry,selectgender;

    public EditText dateofbirth;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat simpleDateFormat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        firstname = (EditText) findViewById(R.id.firstname);
        lastname = (EditText) findViewById(R.id.lastname);
        username = (EditText) findViewById(R.id.username);
        email = (EditText) findViewById(R.id.email);
        joinusbutton = (ImageView) findViewById(R.id.joinus);
        password = (EditText) findViewById(R.id.password);
        confirmpassword = (EditText) findViewById(R.id.confirmpassword);
        gender = (Spinner) findViewById(R.id.gender);
        selcountry = (Spinner) findViewById(R.id.country);
        selectcountry = (TextView) findViewById(R.id.selectcountry);
        selectgender=(TextView)findViewById(R.id.genderselection);

        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        findViewById();
        setDateTimeField();
        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(this, R.array.spinner, android.R.layout.select_dialog_item);
        gender.setAdapter(adapter);

        Locale[] locales = Locale.getAvailableLocales();
        final ArrayList<String> countries = new ArrayList<>();
        String country;

        for (Locale locale : locales) {
            country = locale.getDisplayCountry();
            if (country.length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }

        }
        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, countries);
        selcountry.setAdapter(arrayAdapter);


        joinusbutton.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                String Username = username.getText().toString();
                String Firstname = firstname.getText().toString();
                String Lastname = lastname.getText().toString();
                String Email = email.getText().toString();
                String Password = password.getText().toString();
                String Confirm = confirmpassword.getText().toString();
                String Gender = gender.getSelectedItem().toString();
                String Country = selcountry.getSelectedItem().toString();
                String Dateofbirth = dateofbirth.getText().toString();


                if (Firstname.isEmpty()) {
                    firstname.setError(getResources().getString(R.string.field_cannot_empty));
                    return;
                }
                if (Lastname.isEmpty()) {
                    lastname.setError(getResources().getString(R.string.field_cannot_empty));
                    return;
                }

                if (Password.matches(Confirm)) {
                } else {
                    confirmpassword.setError(getResources().getString(R.string.password_not_match));
                    return;
                }
                if (Username.isEmpty()) {
                    username.setError(getResources().getString(R.string.field_cannot_empty));
                    return;
                }

                if (Email.isEmpty()) {
                    email.setError(getResources().getString(R.string.field_cannot_empty));
                    return;
                }

                if (!Helper.isValidEmail(Email)) {
                    email.setError(getResources().getString(R.string.a_valid_email_required));
                    return;
                }
                setLoading(getResources().getString(R.string.registering));
                Call<SignUpResponse> call = Api.getRetrofit().create(ApiInterface.class).signupUser(Firstname, Lastname, Username, Email,
                        Password, Gender, Country, Dateofbirth);
                call.enqueue(new Callback<SignUpResponse>() {
                    @Override
                    public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                        hideLoading();
                        if (response.body().getStatus() == 1) {
                            if (!response.body().getActivated()) {
                                Toast.makeText(getApplicationContext(), R.string.check_your_mail_for_confirmation, Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                                finish();
                                return;
                            }
                            Session session = App.getInstance().getSession();
                            session.setFirstName(response.body().getFirstName());
                            session.setEmail(response.body().getEmail());
                            session.setGender(response.body().getGender());
                            session.setDateofbirth(response.body().getDateofbirth());
                            session.setLastName(response.body().getLastName());
                            session.setUserid(response.body().getId());
                            session.setCountry(response.body().getSpinnerCountry());
                            session.setPassword(response.body().getPassword());
                            session.setBio(response.body().getBio());
                            session.setCity(response.body().getCity());
                            session.setState(response.body().getState());
                            session.setAvatar(response.body().getAvatar());
                            session.setCover(response.body().getCover());

                            Intent intent = new Intent(getApplicationContext(), GetStartedActivity.class);
                            //Toast.makeText(getApplicationContext(), "Successfully Signed in!", Toast.LENGTH_LONG).show();
                            startActivity(intent);
                            finish();


                        } else {
                            MyLog.e(response.body().getMessage());
                            Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<SignUpResponse> call, Throwable t) {
                        hideLoading();
                        Toast.makeText(getApplicationContext(), "Please review your internet connection",
                                Toast.LENGTH_LONG).show();

                    }
                });


            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.

    }


    private void findViewById() {
        dateofbirth = (EditText) findViewById(R.id.dateofbirth);
        dateofbirth.setInputType(InputType.TYPE_NULL);
        dateofbirth.requestFocus();
    }

    private void setDateTimeField() {
        dateofbirth.setOnClickListener(this);

        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int month, int day) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, day);
                dateofbirth.setText(simpleDateFormat.format(newDate.getTime()));

            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)
        );

    }

    public void onClick(View view) {
        if (view == dateofbirth) {
            datePickerDialog.show();
        } else if (view == dateofbirth) {
            datePickerDialog.show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {
        switch (menu.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menu);
        }

    }

    @Override
    public void onStart() {
        super.onStart();

        //  AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

    }
}
