package com.procrea8.sexyafrikan.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.procrea8.sexyafrikan.R;

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread timer = new Thread(){
            public void run(){
                try{
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    Intent intent=new Intent(SplashActivity.this,AppActivity.class);
                    startActivity(intent);
                    finish();

                }
            }
        };
        timer.start();
    }
    /**@Override
    protected void onPause(){
        super.onPause();
        finish();
    }**/

}
