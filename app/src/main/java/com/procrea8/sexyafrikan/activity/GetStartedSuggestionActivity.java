package com.procrea8.sexyafrikan.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.relationship.fragments.FriendsSuggestionFragment;

public class GetStartedSuggestionActivity extends ActivityBase {
    ImageView btn,backkey;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_get_started_suggestion);
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btn=(ImageView)findViewById(R.id.nextbtn);

        FriendsSuggestionFragment friends=new FriendsSuggestionFragment();
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container,friends).commit();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),AppActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }



}
