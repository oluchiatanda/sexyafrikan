package com.procrea8.sexyafrikan.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.api.GoogleApiClient;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.api.ApiInterface;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.helper.Session;
import com.procrea8.sexyafrikan.response.GettingStartedResponse;

import java.io.File;
import java.util.List;

import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetStartedActivity extends ActivityBase {
    TextView textView;
    Button imageButton;
    ImageView button;
    TextView nextbtn;
    String image;
    ImageView imageView;
    final  Integer MY_PHOTO_SELECT = 250;
    final Integer MY_PHOTO_CAPTURE = 200;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);
        imageView=(ImageView)findViewById(R.id.imageview);
        textView = (TextView) findViewById(R.id.gettstdtext);
        imageButton = (Button) findViewById(R.id.imageupld);
        button = (ImageView) findViewById(R.id.uploadbtn);
        nextbtn = (TextView) findViewById(R.id.nextbtn);

        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userid = App.getInstance().getSession().getUserid();
                  Intent intent = new Intent(getApplicationContext(), GetStartedSuggestionActivity.class);
                  startActivity(intent);
                finish();
            }
        });

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog.show();
                MultipartBody.Part body = null;
                //MyLog.e("uploading image");
                if (image != null && !image.isEmpty()) {
                    File file = new File(image);
                    //MyLog.e(image);
                    RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/"), file);
                    body = MultipartBody.Part.createFormData("avatar", file.getName(), requestBody);
                }

                setLoading(getResources().getString(R.string.uploading));
                String userid = App.getInstance().getSession().getUserid();
                //MultipartBody.Part avatar=App.getInstance().getSession().setImage("avatar");

                Call<GettingStartedResponse> call = Api.getRetrofit().create(ApiInterface.class).gettingStarted(body, userid);
                call.enqueue(new Callback<GettingStartedResponse>() {
                    @Override
                    public void onResponse(Call<GettingStartedResponse> call, Response<GettingStartedResponse> response) {
                        //MyLog.e("message coming" +response.body().getMessage());
                        if (response.body().getStatus()==1) {
                            Session session = App.getInstance().getSession();
                            //session.setTextview(response.body().getTxt());
                            session.setAvatar(response.body().getImage());
                            //session.setButtn(response.body().getButtn());

                            Intent intent = new Intent(getApplicationContext(), GetStartedSuggestionActivity.class);
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.photo_upload_successful),
                                    Toast.LENGTH_LONG).show();
                            startActivity(intent);
                            finish();

                        } else {
                            hideLoading();
                            MyLog.e(response.body().getMessage());
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.upload_failed), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<GettingStartedResponse> call, Throwable t) {
                        hideLoading();
                        MyLog.e("Upload avatar" + t.getMessage());
                        Toast.makeText(getApplicationContext(), R.string.failed_internet, Toast.LENGTH_LONG).show();

                    }
                });

            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
      //  client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void selectImage() {
        setPermissionGranted(new ActivityBase.OnPermissionGranted() {
            @Override
            public void onGranted() {
                MultiImageSelector.create()
                        .showCamera(true)
                        .count(1)
                        .single()
                        .start(GetStartedActivity.this, MY_PHOTO_SELECT);
            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_PHOTO_SELECT && resultCode == RESULT_OK) {
            List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
            final String photoPath = path.get(0);
            image = photoPath;
            //imageView.setImageURI(Uri.);
            setPermissionGranted(new OnPermissionGranted());
        }


    }
}