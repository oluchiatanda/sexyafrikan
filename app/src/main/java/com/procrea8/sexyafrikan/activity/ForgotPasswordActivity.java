package com.procrea8.sexyafrikan.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.api.ApiInterface;
import com.procrea8.sexyafrikan.helper.Session;
import com.procrea8.sexyafrikan.response.ForgotpasswordResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends ActivityBase {
    EditText email;
    ImageView button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        email=(EditText)findViewById(R.id.forgotEmail);
        button=(ImageView)findViewById(R.id.retrievebutton);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailText=email.getText().toString();
                if (emailText.isEmpty()){
                    email.setError(getResources().getString(R.string.field_cannot_empty));
                    return;
                }
                setLoading();
                Call<ForgotpasswordResponse> call=Api.getRetrofit().create(ApiInterface.class).forgotpassword(emailText);
                call.enqueue(new Callback<ForgotpasswordResponse>() {
                    @Override
                    public void onResponse(Call<ForgotpasswordResponse> call, Response<ForgotpasswordResponse> response) {
                        hideLoading();
                        if (response.body().getStatus()==1){
                            Session session = App.getInstance().getSession();
                            //session.setEmail(response.body().getEmail());



                            Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_your_mail_for_forgot),
                                    Toast.LENGTH_LONG).show();
                            startActivity(intent);
                            finish();
                        }else{
                            Toast.makeText(getApplicationContext(),"E-mail not recognised,please sign up!",Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ForgotpasswordResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),"Please review your internet connection!",
                            Toast.LENGTH_LONG).show();
                    }
                });
            }
        });




    }

}
