package com.procrea8.sexyafrikan.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.api.ApiInterface;
import com.procrea8.sexyafrikan.helper.Session;
import com.procrea8.sexyafrikan.response.LoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends ActivityBase {
    EditText username;
    EditText password;
    ImageView loginButton;
    TextView forgotButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        loginButton = (ImageView) findViewById(R.id.login_button);
        forgotButton = (TextView) findViewById(R.id.forgot_button);

        forgotButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginUser();
            }
        });

    }

    public void loginUser() {
        String usernameText = username.getText().toString();
        String passwordText = password.getText().toString();

        if (usernameText.isEmpty()) {
            username.setError("Enter your username");
            return;
        }

        if (passwordText.isEmpty()) {
            password.setError("Enter your password");
            return;
        }

        setLoading(getResources().getString(R.string.authenticating));
        Call<LoginResponse> call = Api.getRetrofit().create(ApiInterface.class).loginUser(usernameText, passwordText);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideLoading();
                if (response.body().getStatus() == 1) {
                    Session session = App.getInstance().getSession();
                    session.setUserid(response.body().getId());

                    session.setFirstName(response.body().getFirstName());
                    session.setLastName(response.body().getLastName());
                    session.setPassword(response.body().getPassword());
                    session.setBio(response.body().getBio());
                    session.setCity(response.body().getCity());
                    session.setState(response.body().getState());
                    session.setAvatar(response.body().getAvatar());
                    session.setCover(response.body().getCover());


                    Intent intent = new Intent(getApplicationContext(), AppActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.failed_to_login, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                hideLoading();
                Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem menu) {
        switch (menu.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menu);
        }

    }
}
