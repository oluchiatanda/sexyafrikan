package com.procrea8.sexyafrikan.activity;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.api.ApiInterface;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.helper.Session;
import com.procrea8.sexyafrikan.model.Menu;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.home.fragments.HomeFragment;

import com.procrea8.sexyafrikan.modules.search.SearchFragment;
import com.procrea8.sexyafrikan.modules.setting.activity.SettingsActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppActivity extends ActivityBase {
    Drawer drawer;
    Toolbar toolbar;
    ArrayList<ModuleInterface> modules;
    FrameLayout containerBody;

    static Fragment fragment;
    boolean restore = false;
    int page = 0;
    static TextView toolbarTitle;
    ImageView settingsAction, searchAction;
    EditText editSearch;

    static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        containerBody = (FrameLayout) findViewById(R.id.container_body);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        searchAction = (ImageView)findViewById(R.id.action_search);
        setSupportActionBar(toolbar);
        modules = App.getInstance().getModules();
        if (!App.getInstance().getSession().isLogin()) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        if (savedInstanceState != null) {
            //fragment = getSupportFragmentManager().getFragment(savedInstanceState, "current_fragment");
            restore = savedInstanceState.getBoolean("restore");
            page = savedInstanceState.getInt("page");
        }


        fragmentManager = getSupportFragmentManager();
        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.container_body, fragment).commit();
        }

        settingsAction = (ImageView) findViewById(R.id.action_settings);
        setCurrentTitle();

        setupDrawerMenu();

        if (!restore) {
            fragment = new HomeFragment();
            Bundle bundle = new Bundle();
            //bundle.putString("type");
            //fragment.setArguments();
            fragmentManager.beginTransaction().replace(R.id.container_body, fragment).commit();
        }
        searchAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSearch();
            }
        });

        settingsAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadSettings();
            }
        });

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(10);
        mNotificationManager.cancel(30);
        final String fcmToken = FirebaseInstanceId.getInstance().getToken();
        if (fcmToken != null) {
            //send to server again for update
            Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(ApiInterface.class).setFCM(
                    App.getInstance().getSession().getUserid(),
                    fcmToken
            );
            call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                @Override
                public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, retrofit2.Response<com.procrea8.sexyafrikan.response.Response> response) {
                    MyLog.e("FCM Token saved on Server - " + fcmToken);
                }

                @Override
                public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                    MyLog.e("FCM Token failed to saved on Server");
                }
            });
        }

    }


    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putBoolean("restore", true);
        state.putInt("page", page);
       // getSupportFragmentManager().putFragment(state, "current_fragment", fragment);
    }

    private void setCurrentTitle() {
        toolbarTitle.setText(modules.get(page).getTitle(this));
    }

    public static void setCurrentTitle(String title) {
        toolbarTitle.setText(title);
    }
    public void setupDrawerMenu() {
        AccountHeader accountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.menu_header)
                        /**
                         * .addProfiles(new ProfileDrawerItem()
                         .withName(App.getInstance()
                         .getSession()
                         .getFirstName())
                         .withIcon(Uri.parse(App.getInstance().getSession().getAvatar()))
                         .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                        @Override
                        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Intent intent = new Intent(AppActivity.this, ProfileActivity.class);
                        startActivity(intent);
                        drawer.closeDrawer();
                        return false;
                        }
                        }))
                         */
                .build();
        ImageView headerView = accountHeader.getHeaderBackgroundView();
        Glide.with(this).load(App.getInstance().getSession().getCover()).into(headerView);
        DrawerImageLoader.init(new AbstractDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                super.set(imageView, uri, placeholder);
                Glide.with(imageView.getContext()).load(uri).into(imageView);
            }

            @Override
            public void cancel(ImageView imageView) {
                super.cancel(imageView);
            }
        });
        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withDisplayBelowStatusBar(true)
                .withTranslucentStatusBar(true)
                .withAccountHeader(accountHeader)
                .build();
        //add items
        for (int i = 0; i < modules.size(); i++) {
            ModuleInterface module = modules.get(i);
            module.registerMenu(this, drawer, i);
        }

        Call<ArrayList<Menu>> call = Api.getRetrofit().create(ApiInterface.class).getMenus();
        call.enqueue(new Callback<ArrayList<Menu>>() {
            @Override
            public void onResponse(Call<ArrayList<Menu>> call, Response<ArrayList<Menu>> response) {
                ArrayList<Menu> menus = response.body();
                for (int i = 0; i < menus.size(); i++) {
                    final Menu menu = menus.get(i);
                    drawer.addItem(new PrimaryDrawerItem().withIcon(R.drawable.ic_content_paste_grey)
                            .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                                @Override
                                public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                    Intent intent = new Intent(getApplicationContext(), WebViewActivity.class);
                                    intent.putExtra("url", menu.getLink());
                                    startActivity(intent);
                                    return false;
                                }
                            })
                            .withName(menu.getTitle()));
                }

                addRemainMenus();
            }

            @Override
            public void onFailure(Call<ArrayList<Menu>> call, Throwable t) {
                addRemainMenus();
            }
        });

    }

    public void addRemainMenus() {
        //add footer settings and footer menus
        drawer.addItem(new PrimaryDrawerItem().withIcon(R.drawable.ic_settings_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        loadSettings();
                        return false;
                    }
                })
                .withName(getApplicationContext().getResources().getString(R.string.settings)));

        drawer.addItem(new PrimaryDrawerItem().withIcon(R.drawable.ic_reply)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        logoutAccount();
                        return false;
                    }
                })
                .withName(getApplicationContext().getResources().getString(R.string.logout)));
    }
    public void loadSettings() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void doSearch() {
        Fragment fragment = new SearchFragment();
        AppActivity.setFragment(fragment);
        AppActivity.setCurrentTitle("Search");
    }


    public void logoutAccount() {
        App.getInstance().getSession().logout(); //delete save details
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public static void setFragment(Fragment f) {
        fragment = f;
        fragmentManager.beginTransaction().replace(R.id.container_body, fragment).addToBackStack(null).commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        } else {
            getSupportFragmentManager().popBackStack();

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (App.getInstance().getSession().isLogin()) {
            Call<User> call = Api.getRetrofit().create(ApiInterface.class).checkLogin(
                    App.getInstance().getSession().getUserid()
            );
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    User user = response.body();
                    String currentPassword = App.getInstance().getSession().getPassword();
                    if (currentPassword == null || user.getPassword() == null){
                        logoutAccount();
                        return;
                    }
                    //MyLog.e(user.getPassword());
                   if (user.getPassword().equals(currentPassword)) {
                        //refresh user save details
                        Session session = App.getInstance().getSession();
                        session.setUserid(response.body().getId());
                        session.setFirstName(response.body().getFirstName());
                        session.setLastName(response.body().getLastName());
                        session.setPassword(response.body().getPassword());
                        session.setBio(response.body().getBio());
                        session.setCity(response.body().getCity());
                        session.setState(response.body().getState());
                        session.setAvatar(response.body().getAvatar());
                        session.setCover(response.body().getCover());
                    } else {
                        //logout the user
                        logoutAccount();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    //silent for now
                }
            });
        }
    }


    public static class searchfriendsFragment extends Fragment {
    }
}

