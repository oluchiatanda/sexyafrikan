package com.procrea8.sexyafrikan.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.hashtag.activity.HashtagActivity;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;
import com.wang.avi.AVLoadingIndicatorView;

public class WebViewActivity extends ActivityBase {
    WebView webView;
    String url = "";
    AVLoadingIndicatorView loadImage;
    Context context;
    AppCompatActivity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        context = this;
        activity = this;
        Intent intent = getIntent();
        if (intent.hasExtra("url")) url = intent.getStringExtra("url");
        webView = (WebView) findViewById(R.id.container);
        loadImage = (AVLoadingIndicatorView) findViewById(R.id.loading_image);
        WebSettings settings = webView.getSettings();
        url = url + "?webview=true&api_userid=" + App.getInstance().getSession().getUserid();
        settings.setJavaScriptEnabled(true);
        settings.setDefaultTextEncodingName("utf-8");
        settings.setLoadWithOverviewMode(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setAppCacheEnabled(false);
        //webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new AppWebViewClient());
        webView.clearCache(true);

        webView.loadUrl(url);
        loadImage.show();
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    class AppWebViewClient extends android.webkit.WebViewClient implements Contants {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            loadImage.show();
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            webView.clearCache(true);
            loadImage.hide();
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, final String url) {
            if(Uri.parse(url).getHost().length() == 0) {
                return false;
            }

            if (url.startsWith("hashtag:")) {
                String[] str = url.split(":");
                String hashtag = str[1];
                HashtagActivity.launch(view.getContext(), hashtag);
            } else if(url.startsWith("mention:")) {
                String[] str = url.split(":");
                String userid = str[1];
                ProfileActivity.load(view.getContext(), userid);
            } else {
                if (Uri.parse(url).getHost().endsWith(DOMAIN)) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String userid = App.getInstance().getSession().getUserid();
                            String newUrl = url;
                            if (url.contains("&")) {
                                newUrl = url.concat("&webview=true&api_userid=" + userid);
                            } else {
                                newUrl = url.concat("?webview=true&api_userid=" + userid);
                            }
                            webView.loadUrl(newUrl);
                            //Toast.makeText(getContext(), "THis is Url - " + newUrl, Toast.LENGTH_LONG).show();
                        }
                    });
                    return true;
                } else {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                }

            }
            return true;
        }

    }
}
