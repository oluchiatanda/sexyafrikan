package com.procrea8.sexyafrikan.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.api.ApiInterface;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.helper.Session;
import com.procrea8.sexyafrikan.response.LoginResponse;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends ActivityBase implements GoogleApiClient.OnConnectionFailedListener,View.OnClickListener {
    Button buttonlogin;
    TextView textView,buttonsignup;
    ImageView imageView;
    Integer currentI = 0;
    SliderLayout sliderLayout;
    Button fFbButton,fTwButton,fGButton;

    LoginButton fbButton;
    TwitterLoginButton twButton;
    SignInButton gButton;

    CallbackManager callbackManager;
    GoogleSignInOptions gso;
    GoogleApiClient mGoogleApiClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!SPLASH_SCREEN) {
            setContentView(R.layout.activity_regular);
        } else {
            setContentView(R.layout.activity_main);
            sliderLayout = (SliderLayout) findViewById(R.id.slider);
            DefaultSliderView defaultSliderView = new DefaultSliderView(this);
            defaultSliderView.image(R.drawable.slider1);
            defaultSliderView.setScaleType(BaseSliderView.ScaleType.CenterCrop);
            sliderLayout.addSlider(defaultSliderView);
            DefaultSliderView defaultSliderView2 = new DefaultSliderView(this);
            defaultSliderView2.image(R.drawable.slide2);
            defaultSliderView2.setScaleType(BaseSliderView.ScaleType.CenterCrop);
            sliderLayout.addSlider(defaultSliderView2);

            DefaultSliderView defaultSliderView3 = new DefaultSliderView(this);
            defaultSliderView3.image(R.drawable.slide3);
            defaultSliderView3.setScaleType(BaseSliderView.ScaleType.CenterCrop);
            sliderLayout.addSlider(defaultSliderView3);

            sliderLayout.setPresetTransformer(SliderLayout.Transformer.Fade);

        }



        if (App.getInstance().getSession().isLogin()) {
            Intent intent = new Intent(this, AppActivity.class);
            startActivity(intent);
            finish();
        }
        FacebookSdk.sdkInitialize(this.getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("194748591622-atqp3fc73beklpojae7mspnieqgbr0h3.apps.googleusercontent.com")
                .requestServerAuthCode("194748591622-atqp3fc73beklpojae7mspnieqgbr0h3.apps.googleusercontent.com")
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        buttonlogin=(Button)findViewById(R.id.login);
        buttonsignup=(TextView)findViewById(R.id.signup);
        textView=(TextView)findViewById(R.id.text);
        imageView=(ImageView)findViewById(R.id.splash);
        fFbButton = (Button) findViewById(R.id.f_fb_button);
        fTwButton = (Button) findViewById(R.id.f_tw_button);
        fGButton = (Button) findViewById(R.id.f_g_button);
        fbButton = (LoginButton) findViewById(R.id.fb_button);
        twButton = (TwitterLoginButton) findViewById(R.id.tw_button);
        gButton = (SignInButton) findViewById(R.id.g_button);
        gButton.setSize(SignInButton.SIZE_STANDARD);
        gButton.setScopes(gso.getScopeArray());
        gButton.setOnClickListener(this);



        fFbButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fbButton.performClick();
            }
        });

        fTwButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                twButton.performClick();
            }
        });

        fGButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, 1200);
            }
        });
        buttonsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(intent);
            }
        });

        buttonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });
        fbButton.setReadPermissions(Arrays.asList("email", "public_profile", "user_birthday"));
        fbButton.setLoginBehavior(LoginBehavior.WEB_ONLY);
        fbButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                setLoading();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                //Log.v("LoginActivity", response.toString());

                                // Application code
                                try {
                                    String email = object.getString("email");
                                    String id = object.getString("id");
                                    String firstName = object.getString("first_name");
                                    String lastName = object.getString("last_name");
                                    String gender = object.getString("gender");
                                    String username = "fb_" + id;
                                    if (email.isEmpty()) {
                                        email = "fb_" + id + "@facebook.com";
                                    }
                                    socialAuth(id, email, firstName, lastName, gender, username, "facebook", "");
                                } catch (Exception e) {
                                    MyLog.e(e.getMessage());
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,first_name,last_name");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                MyLog.e("Social Auth" +error.getMessage());
            }
        });
        twButton.setCallback(new com.twitter.sdk.android.core.Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {

                setLoading();
                Call<User> call = TwitterCore.getInstance().getApiClient(result.data).getAccountService().verifyCredentials(true, false);
                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        User user = response.body();
                        String id = String.valueOf(response.body().getId());
                        String username = user.screenName;
                        String firstName = user.name;
                        String lastName = "";
                        String gender = "";
                        String email = "tw_" + id + "@twitter.com";
                        String image = user.profileImageUrl;
                        socialAuth(id, email, firstName, lastName, gender, username, "twitter", image);
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        MyLog.e("Social Auth" + t.getMessage());
                    }
                });

            }

            @Override
            public void failure(TwitterException exception) {
                MyLog.e("Social Auth" +exception.getMessage());
            }
        });

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    public void socialAuth(String id, String email, String firstName, String lastName, String gender, String username, String type, String image) {
        Call<LoginResponse> call = Api.getRetrofit().create(ApiInterface.class).socialLogin(
                id,firstName,lastName,username,email,gender,image,type
        );
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideLoading();
                if (response.body().getStatus() == 1) {
                    //MyLog.e(response.body().toString());
                    Session session = App.getInstance().getSession();
                    session.setUserid(response.body().getId());
                    session.setFirstName(response.body().getFirstName());
                    session.setLastName(response.body().getLastName());
                    session.setPassword(response.body().getPassword());
                    session.setBio(response.body().getBio());
                    session.setCity(response.body().getCity());
                    session.setState(response.body().getState());
                    session.setAvatar(response.body().getAvatar());

                    session.setCover(response.body().getCover());

                    //MyLog.e("userid - " + App.getInstance().getSession().getUserid() + "- FROM Server- " + response.body().getFirstName());
                    Intent intent = new Intent(getApplicationContext(), AppActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.failed_to_login, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                hideLoading();
                Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
                LoginManager.getInstance().logOut();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        twButton.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1200) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        //Log.d(TAG, "handleSignInResult:" + result.isSuccess());

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String id = acct.getId();
            String username = acct.getDisplayName();
            String firstName = acct.getDisplayName();
            String lastName = "";
            String gender = "";
            String email = acct.getEmail();
            String image = "";
            socialAuth(id, email, firstName, lastName, gender, username, "google", image);
        } else {
            MyLog.e("Failed to login Google+");
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.g_button:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, 1200);
                break;
            // ...
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
