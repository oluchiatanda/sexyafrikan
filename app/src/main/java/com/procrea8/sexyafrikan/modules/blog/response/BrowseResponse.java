package com.procrea8.sexyafrikan.modules.blog.response;


import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.modules.blog.model.Blog;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;

import java.util.ArrayList;

public class BrowseResponse {
    @SerializedName("categories")
    ArrayList<MusicCategory> categories;
    @SerializedName("blogs")
    ArrayList<Blog> blogs;

    public ArrayList<MusicCategory> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<MusicCategory> categories) {
        this.categories = categories;
    }

    public ArrayList<Blog> getBlogs() {
        return blogs;
    }

    public void setSongs(ArrayList<Blog> blogs) {
        this.blogs = blogs;
    }
}
