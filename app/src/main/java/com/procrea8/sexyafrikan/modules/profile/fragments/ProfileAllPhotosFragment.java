package com.procrea8.sexyafrikan.modules.profile.fragments;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.GridSpacingItemDecoration;
import com.procrea8.sexyafrikan.modules.photo.adapter.PhotosAdapter;
import com.procrea8.sexyafrikan.modules.photo.model.Photo;
import com.procrea8.sexyafrikan.modules.profile.ProfileApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileAllPhotosFragment extends Fragment {
    static String userid;
    RecyclerView recyclerView;
    PhotosAdapter photosAdapter;
    ArrayList<Photo> photos = new ArrayList<>();

    Integer limit = 20;
    Integer offset = 0;

    Boolean paginating = false;

    public ProfileAllPhotosFragment() {
        // Required empty public constructor
    }

    public static ProfileAllPhotosFragment getInstance(String id) {
        userid = id;
        return new ProfileAllPhotosFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_photos, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleview);
        photosAdapter = new PhotosAdapter(getContext(), photos);

        final RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(2), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(photosAdapter);

        loadPhotos(false);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisibleItems = ((GridLayoutManager) layoutManager).findLastVisibleItemPosition();
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount ) {
                        //we are in the last item
                        if (!paginating) {
                            offset = offset + limit;
                            loadPhotos(true);
                            paginating = true;
                        }
                    }
                }
            }
        });
        return view;
    }

    public void loadPhotos(Boolean more) {
        //if (more) MyLog.e("Paginating - " + limit + "-" + offset);
        Call<ArrayList<Photo>> call = Api.getRetrofit().create(ProfileApiInterface.class).getPhotos(
                App.getInstance().getSession().getUserid(),
                userid,
                limit,
                offset
        );
        call.enqueue(new Callback<ArrayList<Photo>>() {
            @Override
            public void onResponse(Call<ArrayList<Photo>> call, Response<ArrayList<Photo>> response) {
                photos.addAll(response.body());
                photosAdapter.notifyDataSetChanged();
                paginating = false;
            }

            @Override
            public void onFailure(Call<ArrayList<Photo>> call, Throwable t) {
                //MyLog.e("Failed - " + t.getMessage());
            }
        });
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
