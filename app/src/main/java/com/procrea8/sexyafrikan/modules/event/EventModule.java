package com.procrea8.sexyafrikan.modules.event;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.AppActivity;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.event.activity.EventActivity;
import com.procrea8.sexyafrikan.modules.event.fragments.EventsFragment;
import com.procrea8.sexyafrikan.modules.event.model.Event;
import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;


public class EventModule implements ModuleInterface {
    @Override
    public void registerMenu(final Context context, Drawer drawer, int i) {
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_event_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = new EventsFragment();
                        AppActivity.setFragment(fragment);
                        AppActivity.setCurrentTitle(context.getResources().getString(R.string.events));
                        return false;
                    }
                })
                .withName(context.getResources().getString(R.string.events)));
    }

    @Override
    public Fragment getFragment(Context context) {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type, Object object) {
        if (type.equals("notification-title")) {
            NotificationModel model = (NotificationModel) object;
            switch (model.getType()) {
                case "event.invite":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.invited_to_event));

                case "event.birthday":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.is_having_birthday));
                case "event.rsvp":
                    if (model.getDataTwo().equals("1")) {
                        return TextUtils.concat("", "", context.getResources().getString(R.string.is_going_to_event));
                    } else {
                        return TextUtils.concat("", "", context.getResources().getString(R.string.might_going_to_event));
                    }

                case "event.post":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.posted_on_your_event));
            }
        } else if (type.equals("notification-own-title")) {
            NotificationModel model = (NotificationModel) object;
            if (model.getType().equals("event.events")) {
                if (model.getDataThree().equals("on")) {
                    return TextUtils.concat(model.getDataOne() +" ", "", context.getResources().getString(R.string.event_happening_today));
                } else {
                    return TextUtils.concat(model.getDataOne() +" ", "", context.getResources().getString(R.string.event_happening_tomorrow));
                }
            }
        } else if (type.equals("notification-action")) {
            NotificationModel model = (NotificationModel) object;
            String nType = model.getType();
            if (nType.equals("event.invite") || nType.equals("event.events") || nType.equals("event.post") || nType.equals("event.rsvp")) {
                Event event = model.getEvents().get(0);
                EventActivity.load(context, event);
            } else if(nType.equals("event.birthday")) {
                ProfileActivity.load(context, model.getUserid());
            }
        }
        return null;
    }

    @Override
    public Object invoke(Context context, String type) {
        return null;
    }
}
