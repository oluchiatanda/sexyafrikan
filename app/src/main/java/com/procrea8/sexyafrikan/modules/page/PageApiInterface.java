package com.procrea8.sexyafrikan.modules.page;


import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;
import com.procrea8.sexyafrikan.modules.page.model.PageBrowseResponse;
import com.procrea8.sexyafrikan.modules.page.model.Page;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface PageApiInterface {
    @GET("page/get/categories")
    Call<ArrayList<MusicCategory>> getCategories(@Query("userid") String userid);

    @Multipart
    @POST("page/create")
    Call <Page> add (@Query("userid") String userid, @Query("title") String title,
                     @Query("description") String description, @Part MultipartBody.Part image);

    @POST("page/create")
    Call<Page> add(@Query("userid") String userid, @Query("title") String title,
                   @Query("description") String description);

    @GET("page/browse")
    Call<PageBrowseResponse> browse(@Query("userid") String userid, @Query("category_id") String category, @Query("term") String term,
                                @Query("type") String type, @Query("limit") Integer limit,
                                @Query("page") Integer page);
    @GET("page/like")

    Call<Response> like(@Query("userid") String userid, @Query("page_id") String eventId, @Query("action") String status);

    @Multipart
    @POST("page/cover")
    Call<Response> changeCover(@Query("userid") String userid, @Query("page_id") String eventId, @Part MultipartBody.Part photo);

    @Multipart
    @POST("page/logo")
    Call<Response> changeLogo(@Query("userid") String userid, @Query("page_id") String eventId, @Part MultipartBody.Part photo);

    @POST("page/create")
    Call<Page> create(@Query("userid") String userid, @Query("title") String title, @Query("description") String description);

}
