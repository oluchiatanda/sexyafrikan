package com.procrea8.sexyafrikan.modules.feed.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.feed.model.AnswerModel;
import com.procrea8.sexyafrikan.modules.feed.model.FileModel;
import com.procrea8.sexyafrikan.modules.feed.model.LinkModel;
import com.procrea8.sexyafrikan.modules.feed.model.TagUserModel;
import com.procrea8.sexyafrikan.modules.music.model.Music;

import java.util.ArrayList;

public class FeedResponse implements Parcelable, Contants {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Object createFromParcel(Parcel parcel) {
            return new FeedResponse();
        }

        @Override
        public FeedResponse[] newArray(int i) {
            return new FeedResponse[i];
        }
    };

    @SerializedName("id")
    String feedId;
    @SerializedName("name")
    String authorName;
//    @SerializedName("users")
    @SerializedName("avatar")
    String authorAvatar;
    @SerializedName("message")
    String message;
    @SerializedName("full_message")
    String fullMessage;

    public String getFullMessage() {
        return fullMessage;
    }

    public void setFullMessage(String fullMessage) {
        this.fullMessage = fullMessage;
    }

    @SerializedName("images")
    ArrayList<String> images;
    @SerializedName("has_like")
    boolean hasLike = false;
    @SerializedName("has_dislike")
    boolean hasDislike = false;
    @SerializedName("has_react")
    boolean hasReact = false;
    @SerializedName("like_count")
    Integer likeCount = 0;
    @SerializedName("dislike_count")
    Integer dislikeCount = 0;
    @SerializedName("type")
    String type;
    @SerializedName("type_id")
    String typeId;

    @SerializedName("musics")
    ArrayList<Music> musics;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public ArrayList<Music> getMusics() {
        return musics;
    }

    public void setMusics(ArrayList<Music> musics) {
        this.musics = musics;
    }

    //feeling data
    @SerializedName("has_feeling")
    Boolean hasFeeling = false;
    @SerializedName("feeling_image")
    String feelingImage;
    @SerializedName("feeling_type")
    String feelingType;
    @SerializedName("feeling_text")
    String feelingText;

    //video file and embed
    @SerializedName("video_embed")
    String videoEmbed;
    @SerializedName("video_file")
    String videoFile;
    @SerializedName("video_title")
    String videoTitle;
    @SerializedName("video_count")
    String videoCount;

    //files
    @SerializedName("files")
    ArrayList<FileModel> files;

    @SerializedName("location")
    String location;
    @SerializedName("privacy")
    String privacy;
    @SerializedName("time")
    String time;

    //link details
    @SerializedName("link")
    ArrayList<LinkModel> link;

    //poll
    @SerializedName("poll_type")
    Integer pollType;
    @SerializedName("answers")
    ArrayList<AnswerModel> answers;

    //shared
    @SerializedName("shared")
    Boolean shared;
    @SerializedName("shared_id")
    String sharedId;
    @SerializedName("shared_name")
    String sharedName;
    @SerializedName("shared_avatar")
    String sharedAvatar;
    @SerializedName("shared_time")
    String sharedTime;
    @SerializedName("shared_entity_type")
    String sharedEntityType;
    @SerializedName("shared_entity_id")
    String sharedEntityId;

    //tagged users
    @SerializedName("tag_users")
    ArrayList<TagUserModel> tagUsers;

    //album details
    @SerializedName("album")
    ArrayList<String> album;

    @SerializedName("feed_title")
    String feedTitle = "";

    @SerializedName("shared_title")
    String sharedTitle = "";

    @SerializedName("entity_id")
    String entityId;
    @SerializedName("entity_type")
    String entityType;

    @SerializedName("has_subscribed")
    Boolean hasSubscribed;
    @SerializedName("can_pin_post")
    Boolean canPinPost;
    @SerializedName("is_pinned")
    Boolean isPinned;

    @SerializedName("is_ad")
    Boolean isAd;

    @SerializedName("ads")
    ArrayList<LinkModel> ads;

    public ArrayList<LinkModel> getAds() {
        return ads;
    }

    public void setAds(ArrayList<LinkModel> ads) {
        this.ads = ads;
    }

    public Boolean getIsAd() {
        return isAd;
    }

    public void setIsAd(Boolean isAd) {
        this.isAd = isAd;
    }

    public Boolean getIsPinned() {
        return isPinned;
    }

    public void setIsPinned(Boolean isPinned) {
        this.isPinned = isPinned;
    }

    @SerializedName("can_edit_post")
    Boolean canEditPost;

    public Boolean getHasSubscribed() {
        return hasSubscribed;
    }

    public void setHasSubscribed(Boolean hasSubscribed) {
        this.hasSubscribed = hasSubscribed;
    }

    public Boolean getCanPinPost() {
        return canPinPost;
    }

    public void setCanPinPost(Boolean canPinPost) {
        this.canPinPost = canPinPost;
    }

    public Boolean getCanEditPost() {
        return canEditPost;
    }

    public void setCanEditPost(Boolean canEditPost) {
        this.canEditPost = canEditPost;
    }

    public Boolean getHasFeeling() {
        return hasFeeling;
    }

    public void setHasFeeling(Boolean hasFeeling) {
        this.hasFeeling = hasFeeling;
    }

    public String getFeelingImage() {
        return feelingImage;
    }

    public void setFeelingImage(String feelingImage) {
        this.feelingImage = feelingImage;
    }

    public String getFeelingType() {
        return feelingType;
    }

    public void setFeelingType(String feelingType) {
        this.feelingType = feelingType;
    }

    public String getFeelingText() {
        return feelingText;
    }

    public void setFeelingText(String feelingText) {
        this.feelingText = feelingText;
    }

    public String getVideoEmbed() {
        return videoEmbed;
    }

    public void setVideoEmbed(String videoEmbed) {
        this.videoEmbed = videoEmbed;
    }

    public String getVideoFile() {
        return videoFile;
    }

    public void setVideoFile(String videoFile) {
        this.videoFile = videoFile;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoCount() {
        return videoCount;
    }

    public void setVideoCount(String videoCount) {
        this.videoCount = videoCount;
    }

    public ArrayList<FileModel> getFiles() {
        return files;
    }

    public void setFiles(ArrayList<FileModel> files) {
        this.files = files;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public ArrayList<LinkModel> getLink() {
        return link;
    }

    public void setLink(ArrayList<LinkModel> link) {
        this.link = link;
    }

    public Integer getPollType() {
        return pollType;
    }

    public void setPollType(Integer pollType) {
        this.pollType = pollType;
    }

    public ArrayList<AnswerModel> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<AnswerModel> answers) {
        this.answers = answers;
    }

    public Boolean getShared() {
        return shared;
    }

    public void setShared(Boolean shared) {
        this.shared = shared;
    }

    public String getSharedId() {
        return sharedId;
    }

    public void setSharedId(String sharedId) {
        this.sharedId = sharedId;
    }

    public String getSharedName() {
        return sharedName;
    }

    public void setSharedName(String sharedName) {
        this.sharedName = sharedName;
    }

    public String getSharedAvatar() {
        return sharedAvatar;
    }

    public void setSharedAvatar(String sharedAvatar) {
        this.sharedAvatar = sharedAvatar;
    }

    public String getSharedTime() {
        return sharedTime;
    }

    public void setSharedTime(String sharedTime) {
        this.sharedTime = sharedTime;
    }

    public String getSharedEntityType() {
        return sharedEntityType;
    }

    public void setSharedEntityType(String sharedEntityType) {
        this.sharedEntityType = sharedEntityType;
    }

    public String getSharedEntityId() {
        return sharedEntityId;
    }

    public void setSharedEntityId(String sharedEntityId) {
        this.sharedEntityId = sharedEntityId;
    }

    public ArrayList<TagUserModel> getTagUsers() {
        return tagUsers;
    }

    public void setTagUsers(ArrayList<TagUserModel> tagUsers) {
        this.tagUsers = tagUsers;
    }

    public ArrayList<String> getAlbum() {
        return album;
    }

    public void setAlbum(ArrayList<String> album) {
        this.album = album;
    }

    public String getFeedTitle() {
        return feedTitle;
    }

    public void setFeedTitle(String feedTitle) {
        this.feedTitle = feedTitle;
    }

    public String getSharedTitle() {
        return sharedTitle;
    }

    public void setSharedTitle(String sharedTitle) {
        this.sharedTitle = sharedTitle;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public Boolean getIsFeedItem() {
        return isFeedItem;
    }

    public void setIsFeedItem(Boolean isFeedItem) {
        this.isFeedItem = isFeedItem;
    }

    Boolean isFeedItem = true;

    @SerializedName("comments")
    Integer comments;

    public ArrayList<ArrayList<String>> getReactMembers() {
        return reactMembers;
    }

    public void setReactMembers(ArrayList<ArrayList<String>> reactMembers) {
        this.reactMembers = reactMembers;
    }

    @SerializedName("react_members")
    ArrayList<ArrayList<String>> reactMembers;

    public Integer getComments() {
        return comments;
    }

    public void setComments(Integer comments) {
        this.comments = comments;
    }

    public boolean isHasLike() {
        return hasLike;
    }

    public void setHasLike(boolean hasLike) {
        this.hasLike = hasLike;
    }

    public boolean isHasDislike() {
        return hasDislike;
    }

    public void setHasDislike(boolean hasDislike) {
        this.hasDislike = hasDislike;
    }

    public boolean isHasReact() {
        return hasReact;
    }

    public void setHasReact(boolean hasReact) {
        this.hasReact = hasReact;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getDislikeCount() {
        return dislikeCount;
    }

    public void setDislikeCount(Integer dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

//    public FeedResponse() {
//
//    }

    public String getFeedId() {
        return feedId;
    }

    public void setFeedId(String feedId) {
        this.feedId = feedId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorAvatar() {
        return authorAvatar;
    }

    public void setAuthorAvatar(String authorAvatar) {
        this.authorAvatar = authorAvatar;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }

    public Integer getFollowing() {
        return following;
    }

    public void setFollowing(Integer following) {
        this.following = following;
    }

    @SerializedName("is_following")
    Integer following = 0;
}
