package com.procrea8.sexyafrikan.modules.music;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.AppActivity;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.music.activity.MusicPlayerActivity;
import com.procrea8.sexyafrikan.modules.music.fragment.MusicFragment;
import com.procrea8.sexyafrikan.modules.music.model.Music;
import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;

import java.util.ArrayList;


public class MusicModule implements ModuleInterface {
    @Override
    public void registerMenu(final Context context, final Drawer drawer, int i) {
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_music_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = new MusicFragment();
                        AppActivity.setFragment(fragment);
                        AppActivity.setCurrentTitle(context.getResources().getString(R.string.music));
                        return false;
                    }
                })
                .withName(context.getResources().getString(R.string.music)));
    }

    @Override
    public Fragment getFragment(Context context) {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type, Object object) {
        if (type.equals("notification-title")) {
            NotificationModel model = (NotificationModel) object;
            switch (model.getType()) {
                case "music.like":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.like_your_music));
                case "music.like.react":
                    if (model.getTitle().equals("1")) return TextUtils.concat("", "", context.getResources().getString(R.string.like_your_music));
                    return TextUtils.concat("", "", context.getResources().getString(R.string.reacted_to_your_music));
                case "music.dislike":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.dislike_your_music));
                case "music.like.comment":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.like_your_comment));
                case "music.dislike.comment":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.dislike_your_comment));
                case "music.comment":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.commented_on_music));
                case "music.comment.reply":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.reply_your_comment));
            }
        } else if(type.equals("notification-action")) {
            NotificationModel model = (NotificationModel) object;
            String nType = model.getType();
            ArrayList<String> types = new ArrayList<>();
            String[] str  = {"music.like",
                    "music.like.react",
                    "music.dislike", "music.like.comment", "music.comment", "music.comment.reply"};
            for(int i = 0; i < str.length;i++) {
                types.add(str[i]);
            }
            if (types.contains(nType)) {
                Music music = model.getMusics().get(0);
                MusicPlayerActivity.launch(context, music);
            }

        }

        return null;
    }

    @Override
    public Object invoke(Context context, String type) {
        return null;
    }
}
