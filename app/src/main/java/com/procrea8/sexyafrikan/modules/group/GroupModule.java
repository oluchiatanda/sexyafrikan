package com.procrea8.sexyafrikan.modules.group;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.AppActivity;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.group.activity.GroupActivity;
import com.procrea8.sexyafrikan.modules.group.fragments.GroupsFragment;
import com.procrea8.sexyafrikan.modules.group.model.Group;
import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;

/**
 * Created by Tiamiyu waliu kola on 9/18/2016.
 */
public class GroupModule implements ModuleInterface {
    @Override
    public void registerMenu(final Context context, Drawer drawer, int i) {
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_people_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = new GroupsFragment();
                        AppActivity.setFragment(fragment);
                        AppActivity.setCurrentTitle(context.getResources().getString(R.string.groups));
                        return false;
                    }
                })
                .withName(context.getResources().getString(R.string.groups)));
    }

    @Override
    public Fragment getFragment(Context context) {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type, Object object) {
        if (type.equals("notification-title")) {
            NotificationModel model = (NotificationModel) object;
            switch (model.getType()) {
                case "group.role":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.change_your_role_in_group));

                case "group.add.member":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.added_you_to_group));
            }
        }else if (type.equals("notification-action")) {
            NotificationModel model = (NotificationModel) object;
            String nType = model.getType();
            if (nType.equals("group.role") || nType.equals("group.add.member")) {
                Group group = model.getGroups().get(0);
                GroupActivity.load(context, group);
            }
        }
        return null;
    }

    @Override
    public Object invoke(Context context, String type) {
        return null;
    }
}
