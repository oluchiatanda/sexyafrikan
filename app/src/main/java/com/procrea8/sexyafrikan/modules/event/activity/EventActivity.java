package com.procrea8.sexyafrikan.modules.event.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.activity.AppActivity;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.modules.event.EventApiInterface;
import com.procrea8.sexyafrikan.modules.event.dialog.EventCreateDialog;
import com.procrea8.sexyafrikan.modules.event.model.Event;
import com.procrea8.sexyafrikan.modules.feed.FeedApiInterface;
import com.procrea8.sexyafrikan.modules.feed.adapter.FeedAdapter;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventActivity extends ActivityBase {
    static Event event;

    ListView feedListsView;
    FeedAdapter feedAdapter;
    ArrayList<FeedResponse> feedLists = new ArrayList<>();
    ImageView profileCover;
    TextView profileTitle;
    Spinner rsvp;
    LinearLayout rsvpContainer;
    TextView goingCount;
    TextView maybeCount;
    TextView invitedCount;
    TextView eventTime;
    TextView eventLocation;
    TextView eventAddress;
    TextView eventDescription;
    ImageView editButton;
    ImageView deleteButton;


    String limit = "10";
    String offset = "0";

    View footerLoading;

    Boolean moreFeedsLoading = false;

    //cover and avatar
    String loggedInId;
    ImageView changeCover;
    String changePhotoType = "";
    static String selectedPhoto = "";

    Context context;
    LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = this;
        inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        feedListsView = (ListView) findViewById(R.id.feed_lists);

        feedListsView.addHeaderView(inflater.inflate(R.layout.event_profile_header, null));
        footerLoading = (View) inflater.inflate(R.layout.loading_indicator, null);
        profileCover = (ImageView) findViewById(R.id.profile_cover);
        profileTitle = (TextView) findViewById(R.id.profile_name);
        changeCover  = (ImageView)findViewById(R.id.change_cover);
        rsvp = (Spinner) findViewById(R.id.rsvp);
        rsvpContainer = (LinearLayout) findViewById(R.id.rsvp_container);
        goingCount = (TextView) findViewById(R.id.going_count);
        maybeCount = (TextView) findViewById(R.id.maybe_count);
        invitedCount = (TextView) findViewById(R.id.invited_count);
        eventTime = (TextView) findViewById(R.id.event_time);
        eventLocation = (TextView) findViewById(R.id.event_location);
        eventAddress = (TextView) findViewById(R.id.event_address);
        eventDescription  = (TextView) findViewById(R.id.event_description);
        editButton  = (ImageView) findViewById(R.id.edit);
        deleteButton = (ImageView) findViewById(R.id.delete);

        loggedInId = App.getInstance().getSession().getUserid();

        feedListsView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                final int lastItem = i + i1;
                if (i2 > 5 && lastItem == i2) {
                    if (!moreFeedsLoading) {
                        loadMoreFeeds();
                        moreFeedsLoading = true;
                    }
                }
            }
        });

        changeCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePhoto("cover");
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventCreateDialog createDialog = new EventCreateDialog(context, getSupportFragmentManager(), "edit", event);
                createDialog.addListener(new EventCreateDialog.OnEventCreatedListener() {
                    @Override
                    public void onEventCreated(Event newEvent) {
                        event = newEvent;
                        setEventDetails();
                    }
                });
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder confirmDialog = new AlertDialog.Builder(context);
                confirmDialog.setMessage(R.string.are_you_sure);
                confirmDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        setLoading(getResources().getString(R.string.deleting));
                        Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(EventApiInterface.class).delete(
                                App.getInstance().getSession().getUserid(),
                                event.getId()
                        );
                        call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                            @Override
                            public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, retrofit2.Response<com.procrea8.sexyafrikan.response.Response> response) {
                                hideLoading();
                                Intent intent = new Intent(context, AppActivity.class);
                                startActivity(intent);
                                finish();
                            }

                            @Override
                            public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {

                            }
                        });
                    }
                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
            }
        });

        feedAdapter = new FeedAdapter(context, feedLists, "event", event.getId(), loggedInId, "user");
        feedListsView.addFooterView(footerLoading);
        feedListsView.setAdapter(feedAdapter);
        feedListsView.removeFooterView(footerLoading);

        rsvp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Integer w = i;
                Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(EventApiInterface.class).rsvp(
                        App.getInstance().getSession().getUserid(),
                        event.getId(),
                        w.toString()
                );
                call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                    @Override
                    public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, Response<com.procrea8.sexyafrikan.response.Response> response) {
                        goingCount.setText(response.body().getDataOne());
                        invitedCount.setText(response.body().getDataThree());
                        maybeCount.setText(response.body().getDataTwo());
                    }

                    @Override
                    public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        loadPosts(false);
    }

    public void setEventDetails() {

    }

    @Override
    public void onStart() {
        super.onStart();
        profileTitle.setText(event.getTitle());
        goingCount.setText(event.getCountGoing());
        invitedCount.setText(event.getCountInvited());
        maybeCount.setText(event.getCountMaybe());
        eventTime.setText(event.getEventTime());
        eventLocation.setText(event.getLocation());
        eventAddress.setText(event.getAddress());
        eventDescription.setText(event.getDescription());
        Glide.with(context).load(event.getCover()).into(profileCover);

        if (event.getIsAdmin()) {
            rsvpContainer.setVisibility(View.GONE);
            changeCover.setVisibility(View.VISIBLE);
            //editButton.setVisibility(View.VISIBLE);
            deleteButton.setVisibility(View.VISIBLE);
        } else {
            rsvpContainer.setVisibility(View.VISIBLE);
            ArrayList<String> items = new ArrayList<String>();
            items.add(getResources().getString(R.string.not_going));
            items.add(getResources().getString(R.string.going));
            items.add(getResources().getString(R.string.maybe));
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, items);
            rsvp.setAdapter(arrayAdapter);
            if (event.getRsvp().equals("0")) {
                rsvp.setSelection(0);
            } else if(event.getRsvp().equals("1")) {
                rsvp.setSelection(1);
            } else {
                rsvp.setSelection(2);
            }
        }
    }

    public static void load(Context c, Event e) {
        event = e;
        Intent intent = new Intent(c, EventActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        c.startActivity(intent);
    }

    public void addDefaultPostEditor() {
        FeedResponse feedResponse = new FeedResponse();
        feedResponse.setIsFeedItem(false);
        feedLists.add(feedResponse);
    }

    public void loadPosts(final Boolean more) {

        Call<ArrayList<FeedResponse>> call = Api.getRetrofit().create(FeedApiInterface.class).getFeeds(App.getInstance().getSession().getUserid(),
                "event", event.getId(), limit, offset);
        call.enqueue(new Callback<ArrayList<FeedResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<FeedResponse>> call, Response<ArrayList<FeedResponse>> response) {
                if (!more) {
                    feedLists.clear();
                    addDefaultPostEditor();
                }

                if (response.body() != null && response.body().size() > 0) {
                    feedLists.addAll(response.body());

                } else {

                }
                feedListsView.removeFooterView(footerLoading);
                feedAdapter.notifyDataSetChanged();
                moreFeedsLoading = false;
            }

            @Override
            public void onFailure(Call<ArrayList<FeedResponse>> call, Throwable t) {
                feedListsView.removeFooterView(footerLoading);
                moreFeedsLoading = false;
            }
        });
    }

    public void loadMoreFeeds() {
        offset = offset + limit;
        feedListsView.addFooterView(footerLoading);
        loadPosts(true);
    }

    public void changePhoto(String type) {
        changePhotoType = type;
        setPermissionGranted(new ActivityBase.OnPermissionGranted() {
            @Override
            public void onGranted() {
                MultiImageSelector.create()
                        .showCamera(true)
                        .count(1)
                        .single()
                        .start(EventActivity.this, PROFILE_SELECT_PHOTO);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PROFILE_SELECT_PHOTO && resultCode == Activity.RESULT_OK) {
            List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
            selectedPhoto = path.get(0);;
            setPermissionGranted(new OnPermissionGranted(){
                @Override
                public void onGranted() {
                    startPhotoUpload();
                }
            });

        }
    }

    public void startPhotoUpload() {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Uploading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
        Call<com.procrea8.sexyafrikan.response.Response> call;
        File file = new File(selectedPhoto);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        call = Api.getRetrofit().create(EventApiInterface.class).changeCover(loggedInId, event.getId(), body);

        call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
            @Override
            public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, Response<com.procrea8.sexyafrikan.response.Response> response) {
                progressDialog.dismiss();
                if (response.body().getStatus()==1) {
                    event.setCover(response.body().getDataOne());
                    Glide.with(context).load(event.getCover()).into(profileCover);
                    Toast.makeText(context, R.string.photo_successful_changed, Toast.LENGTH_LONG).show();
                    selectedPhoto = "";
                    changePhotoType = "";
                }
            }

            @Override
            public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
            }
        });
    }

}
