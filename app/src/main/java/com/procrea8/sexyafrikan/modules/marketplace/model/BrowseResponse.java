package com.procrea8.sexyafrikan.modules.marketplace.model;


import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;

import java.util.ArrayList;

public class BrowseResponse {
    @SerializedName("categories")
    ArrayList<MusicCategory> categories;

    public ArrayList<Listing> getListings() {
        return listings;
    }

    public void setListings(ArrayList<Listing> listings) {
        this.listings = listings;
    }

    public ArrayList<MusicCategory> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<MusicCategory> categories) {
        this.categories = categories;
    }

    @SerializedName("listings")
    ArrayList<Listing> listings;

}
