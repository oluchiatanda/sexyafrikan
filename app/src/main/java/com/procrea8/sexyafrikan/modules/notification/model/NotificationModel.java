package com.procrea8.sexyafrikan.modules.notification.model;

import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.modules.blog.model.Blog;
import com.procrea8.sexyafrikan.modules.event.model.Event;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;
import com.procrea8.sexyafrikan.modules.group.model.Group;
import com.procrea8.sexyafrikan.modules.marketplace.model.Listing;
import com.procrea8.sexyafrikan.modules.music.model.Music;
import com.procrea8.sexyafrikan.modules.page.model.Page;
import com.procrea8.sexyafrikan.modules.video.model.Video;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;


public class NotificationModel extends Response {
    @SerializedName("id")
    String id;
    @SerializedName("time")
    String time;
    @SerializedName("avatar")
    String avatar;
    @SerializedName("title")
    String title;
    @SerializedName("content")
    String content;
    @SerializedName("name")
    String name;
    @SerializedName("type")
    String type;
    @SerializedName("type_id")
    String typeId;

    @SerializedName("event")
    ArrayList<Event> events;
    @SerializedName("page")
    ArrayList<Page> pages;
    @SerializedName("listing")
    ArrayList<Listing> listings;
    @SerializedName("group")
    ArrayList<Group> groups;
    @SerializedName("music")
    ArrayList<Music> musics;
    @SerializedName("video")
    ArrayList<Video> videos;
    @SerializedName("blog")
    ArrayList<Blog> blogs;
    @SerializedName("feed")
    ArrayList<FeedResponse> feeds;
    @SerializedName("userid")
    String userid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public ArrayList<FeedResponse> getFeeds() {
        return feeds;
    }

    public void setFeeds(ArrayList<FeedResponse> feeds) {
        this.feeds = feeds;
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Event> events) {
        this.events = events;
    }

    public ArrayList<Page> getPages() {
        return pages;
    }

    public void setPages(ArrayList<Page> pages) {
        this.pages = pages;
    }

    public ArrayList<Listing> getListings() {
        return listings;
    }

    public void setListings(ArrayList<Listing> listings) {
        this.listings = listings;
    }

    public ArrayList<Group> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<Group> groups) {
        this.groups = groups;
    }

    public ArrayList<Music> getMusics() {
        return musics;
    }

    public void setMusics(ArrayList<Music> musics) {
        this.musics = musics;
    }

    public ArrayList<Video> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<Video> videos) {
        this.videos = videos;
    }

    public ArrayList<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(ArrayList<Blog> blogs) {
        this.blogs = blogs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
}
