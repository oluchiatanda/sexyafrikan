package com.procrea8.sexyafrikan.modules.page.model;


import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;

import java.util.ArrayList;

public class PageBrowseResponse {
    @SerializedName("categories")
    ArrayList<MusicCategory> categories;

    @SerializedName("pages")
    ArrayList<Page> pages;

    public ArrayList<MusicCategory> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<MusicCategory> categories) {
        this.categories = categories;
    }

    public ArrayList<Page> getPages() {
        return pages;
    }

    public void setPages(ArrayList<Page> pages) {
        this.pages = pages;
    }
}
