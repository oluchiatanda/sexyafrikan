package com.procrea8.sexyafrikan.modules.marketplace.dialog;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.modules.marketplace.MarketplaceApiInterface;
import com.procrea8.sexyafrikan.modules.marketplace.model.Listing;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;
import com.satsuware.usefulviews.LabelledSpinner;

import java.io.File;
import java.util.ArrayList;

import me.nereo.multi_image_selector.MultiImageSelector;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class MarketplaceCreateDialog implements Contants {
    String type = "create";
    Listing listing; //for edit type

    static String listingFile = "";
    static String artFile = "";
    String privacy = "1";
    String category = "1";
    Context context;

    EditText title;
    EditText description;
    EditText tags;
    EditText link;
    EditText price;
    EditText address;
    LabelledSpinner spinner;
    LabelledSpinner categorySpinner;
    Button chooseListing;
    Button chooseCover;

    ArrayList<MusicCategory> loadedCategories = new ArrayList<>();

    OnListingCreatedListener listener;

    public MarketplaceCreateDialog(final Context context, final String type, final Listing listing) {
        this.type = type;
        this.context = context;
        this.listing = listing;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (type.equals("create")) {
            builder.setTitle(R.string.add_new_listing);
        } else {
            builder.setTitle(R.string.edit_listing);
        }
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.listing_create, null);
        title = (EditText) convertView.findViewById(R.id.title);
        description = (EditText) convertView.findViewById(R.id.description);
        tags = (EditText) convertView.findViewById(R.id.tags);
        address = (EditText) convertView.findViewById(R.id.address);
        link = (EditText) convertView.findViewById(R.id.link);
        price = (EditText) convertView.findViewById(R.id.price);

        categorySpinner = (LabelledSpinner) convertView.findViewById(R.id.category);
        chooseCover = (Button) convertView.findViewById(R.id.cover_button);


        //lets public the category label
        Call<ArrayList<MusicCategory>> call = Api.getRetrofit().create(MarketplaceApiInterface.class).getCategories(App.getInstance().getSession().getUserid());
        call.enqueue(new Callback<ArrayList<MusicCategory>>() {
            @Override
            public void onResponse(Call<ArrayList<MusicCategory>> call, retrofit2.Response<ArrayList<MusicCategory>> response) {
                ArrayList<String> items = new ArrayList<String>();
                for(int i =0;i <response.body().size();i++) {
                    items.add(response.body().get(i).getTitle());
                }
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, items);
                categorySpinner.setCustomAdapter(arrayAdapter);
                categorySpinner.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
                    @Override
                    public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
                        if (loadedCategories.size() > 0) {
                            category = loadedCategories.get(position).getId();
                        }
                    }

                    @Override
                    public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onFailure(Call<ArrayList<MusicCategory>> call, Throwable t) {
                Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
            }
        });

        if (type.equals("edit")) {
            //chooseListing.setVisibility(View.GONE);
            categorySpinner.setVisibility(View.GONE);
            title.setText(listing.getTitle());
            tags.setText(listing.getTags());
            description.setText(listing.getDescription());
            link.setText(listing.getLink());
            price.setText(listing.getPrice());
            address.setText(listing.getAddress());
        }

        chooseCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ActivityBase activity = (ActivityBase) context;
                activity.setPermissionGranted(new ActivityBase.OnPermissionGranted() {
                    @Override
                    public void onGranted() {
                        MultiImageSelector.create()
                                .showCamera(true)
                                .count(1)
                                .single()
                                .start(activity, MARKETPLACE_SELECT_COVER);
                    }
                });

            }
        });


        builder.setView(convertView);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok, null).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String mtitle = title.getText().toString();
                        String mDesc = description.getText().toString();
                        String mTags = tags.getText().toString();
                        String mPrice = price.getText().toString();
                        String mLink = link.getText().toString();
                        String mAddress = address.getText().toString();

                        if (mtitle.isEmpty()) {
                            Toast.makeText(context, R.string.provide_title, Toast.LENGTH_LONG).show();
                            return;
                        }

                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        if (type.equals("create")) {
                            progressDialog.setMessage(context.getResources().getString(R.string.adding));
                        } else {
                            progressDialog.setMessage(context.getResources().getString(R.string.saving));
                        }
                        progressDialog.setCancelable(false);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressDialog.show();

                        Call<Listing> call1 = null;

                        MultipartBody.Part artBody = null;
                        if (!artFile.isEmpty()) {
                            File file2 = new File(artFile);
                            RequestBody requestBody2 = RequestBody.create(MediaType.parse("multipart/form-data"), file2);
                            artBody = MultipartBody.Part.createFormData("image", file2.getName(), requestBody2);
                        }

                        if (type.equals("create")) {
                            if (artFile.isEmpty()) {
                                call1 = Api.getRetrofit().create(MarketplaceApiInterface.class).create(
                                        App.getInstance().getSession().getUserid(),
                                        mtitle,
                                        mDesc,
                                        mTags,
                                        mAddress,
                                        mLink,
                                        mPrice,
                                        category
                                );
                            } else {
                                call1 = Api.getRetrofit().create(MarketplaceApiInterface.class).create(
                                        App.getInstance().getSession().getUserid(),
                                        mtitle,
                                        mDesc,
                                        mTags,
                                        mAddress,
                                        mLink,
                                        mPrice,
                                        category,
                                        artBody
                                );
                            }
                        } else {
                            if (artFile.isEmpty()) {
                                call1 = Api.getRetrofit().create(MarketplaceApiInterface.class).edit(
                                        App.getInstance().getSession().getUserid(),
                                        listing.getId(),
                                        mtitle,
                                        mDesc,
                                        mTags,
                                        mAddress,
                                        mLink,
                                        mPrice,
                                        category
                                );
                            } else {
                                call1 = Api.getRetrofit().create(MarketplaceApiInterface.class).edit(
                                        App.getInstance().getSession().getUserid(),
                                        listing.getId(),
                                        mtitle,
                                        mDesc,
                                        mTags,
                                        mAddress,
                                        mLink,
                                        mPrice,
                                        category,
                                        artBody
                                );
                            }
                        }


                        if (call1 != null) {
                            call1.enqueue(new Callback<Listing>() {
                                @Override
                                public void onResponse(Call<Listing> call, retrofit2.Response<Listing> response) {
                                    progressDialog.dismiss();
                                    if (response.body().getStatus()==1) {
                                        dialog.dismiss();
                                        ///MyLog.e("Item created - " + response.body().getTitle());
                                        if (listener != null)
                                            listener.onListingCreated(response.body());
                                    } else {
                                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Listing> call, Throwable t) {
                                    progressDialog.dismiss();
                                    ///MyLog.e("Add Song error" + t.getMessage());
                                    Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                                }
                            });


                        }
                    }
                });
            }
        });
        dialog.show();
    }

    public static void setArtFile(String file) {
        artFile = file;
    }

    public void addListener(OnListingCreatedListener l) {
        listener = l;
    }

    public static class OnListingCreatedListener {
        public void onListingCreated(Listing listing) {}
    }
}
