package com.procrea8.sexyafrikan.modules.video;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.AppActivity;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;
import com.procrea8.sexyafrikan.modules.video.activity.VideoActivity;
import com.procrea8.sexyafrikan.modules.video.fragment.VideosFragment;
import com.procrea8.sexyafrikan.modules.video.model.Video;

import java.util.ArrayList;


public class VideoModule implements ModuleInterface {
    @Override
    public void registerMenu(final Context context,final Drawer drawer, int i) {
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_video_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = VideosFragment.getInstance();
                        AppActivity.setFragment(fragment);
                        AppActivity.setCurrentTitle(context.getResources().getString(R.string.videos));
                        return false;
                    }
                })
                .withName(context.getResources().getString(R.string.videos)));
    }

    @Override
    public Fragment getFragment(Context context) {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type, Object object) {
        if (type.equals("notification-title")) {
            NotificationModel model = (NotificationModel) object;
            switch (model.getType()) {
                case "video.like":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.like_your_video));
                case "video.like.react":
                    if (model.getTitle().equals("1")) return TextUtils.concat("", "", context.getResources().getString(R.string.like_your_video));
                    return TextUtils.concat("", "", context.getResources().getString(R.string.reacted_to_your_video));
                case "video.dislike":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.dislike_your_video));
                case "video.like.comment":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.like_your_comment));
                case "video.dislike.comment":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.dislike_your_comment));
                case "video.comment":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.commented_on_video));
                case "video.comment.reply":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.reply_your_comment));
            }
        } else if(type.equals("notification-action")) {
            NotificationModel model = (NotificationModel) object;
            String nType = model.getType();
            ArrayList<String> types = new ArrayList<>();
            String[] str  = {"video.like",
                    "video.like.react",
                    "video.dislike", "video.like.comment", "video.comment", "video.comment.reply"};
            for(int i = 0; i < str.length;i++) {
                types.add(str[i]);
            }
            if (types.contains(nType)) {
                Video video = model.getVideos().get(0);
                VideoActivity.load(context, video);
            }

        }
        return null;
    }

    @Override
    public Object invoke(Context context, String type) {
        return null;
    }
}
