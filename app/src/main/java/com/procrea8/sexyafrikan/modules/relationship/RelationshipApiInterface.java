package com.procrea8.sexyafrikan.modules.relationship;

import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RelationshipApiInterface {
    @GET("friend/requests")
    Call<ArrayList<User>> list(@Query("userid") String userid, @Query("page") String page);

    @GET("friend/confirm")
    Call<Response> accept(@Query("userid") String userid, @Query("to_userid") String toUserid);

    @GET("friend/remove")
    Call<Response> remove(@Query("userid") String userid, @Query("to_userid") String toUserid);

    @GET("friend/add")
    Call<Response> add(@Query("userid") String userid, @Query("to_userid") String toUserid);

    @GET("friend/follow/check")
    Call<Response> checkFollow(@Query("to_userid") String userid);

    @GET("friend/follow")
    Call<Response> follow(@Query("userid") String userid, @Query("to_userid") String toUserid, @Query("type") String type);

    @GET("friend/suggestions")
    Call<ArrayList<User>> suggestions(@Query("userid") String userid, @Query("page") String page, @Query("term") String term);

    @GET("friend/online")
    Call<ArrayList<User>> getOnline(@Query("userid") String userid, @Query("page") String page, @Query("term") String term);
}
