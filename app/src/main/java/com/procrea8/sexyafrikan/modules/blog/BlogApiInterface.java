package com.procrea8.sexyafrikan.modules.blog;


import com.procrea8.sexyafrikan.modules.blog.response.BrowseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BlogApiInterface {
    @GET("blog/browse")
    Call<BrowseResponse> getBlogs(@Query("userid") String userid, @Query("category") String category, @Query("term") String term,
                                  @Query("type") String type, @Query("filter") String filter, @Query("limit") Integer limit,
                                  @Query("page") Integer page);
}
