package com.procrea8.sexyafrikan.modules.notification.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.notification.NotificationApiInterface;
import com.procrea8.sexyafrikan.modules.notification.adapter.NotificationAdapter;
import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment implements Contants {

    ListView notificationList;
    NotificationAdapter notificationAdapter;
    ArrayList<NotificationModel> notificationLists = new ArrayList<>();
    String limit = "12";
    Integer page = 1;
    AVLoadingIndicatorView loadingImage;
    TextView loadingText;
    Boolean notificationLoading = false;
    SwipeRefreshLayout swipeRefreshLayout;
    View footerLoading;

    String cacheTag = "Notifications";

    Gson gson;
    SharedPreferences pref;

    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_notification, container, false);
        notificationList = (ListView) view.findViewById(R.id.notification_list);
        loadingImage = (AVLoadingIndicatorView) view.findViewById(R.id.loading_image);
        loadingText = (TextView)view.findViewById(R.id.loading_text);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        notificationAdapter = new NotificationAdapter(getContext(), notificationLists);
        footerLoading = layoutInflater.inflate(R.layout.loading_indicator, null);
        notificationList.addFooterView(footerLoading);
        notificationList.setAdapter(notificationAdapter);
        notificationList.removeFooterView(footerLoading);
        //footerLoading.setVisibility(View.GONE);
        gson = new Gson();
        pref = getContext().getSharedPreferences(TAG, Context.MODE_PRIVATE);

        loadNotifications(false);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                notificationLoading = false;
                loadNotifications(false);
            }
        });

        notificationList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                final int lastItem = i + i1;
                if (i2 > 0 && lastItem == i2) {
                    if (!notificationLoading) {

                        notificationList.addFooterView(footerLoading);
                        page++;
                        notificationLoading = true;
                        loadNotifications(true);
                    }
                }
            }
        });
        return view;
    }

    public void loadNotifications(final Boolean more) {
        String userid = App.getInstance().getSession().getUserid();
        //MyLog.e("notification page" + page.toString());
        if (notificationLists.size() < 1 && !more) {
            firstLoadFromCache();
        }
        if (!more && notificationLists.size() < 1) {
            loadingImage.show();
        }
        Call<ArrayList<NotificationModel>> call = Api.getRetrofit(true).create(NotificationApiInterface.class).getNotifications(
                userid,
                limit,
                page.toString()
        );
        call.enqueue(new Callback<ArrayList<NotificationModel>>() {
            @Override
            public void onResponse(Call<ArrayList<NotificationModel>> call, Response<ArrayList<NotificationModel>> response) {
                loadingImage.hide();
                if (response.body() != null) {
                    if (!more) {
                        notificationLists.clear();
                        if (response.body().size() < 1) {
                            loadingText.setText(R.string.no_new_notifications);
                        } else {
                            loadingText.setVisibility(View.GONE);
                        }
                        String arrayText = gson.toJson(response.body());
                        pref.edit().putString(cacheTag, arrayText).apply();
                    }
                    notificationLists.addAll(response.body());
                    notificationAdapter.notifyDataSetChanged();
                }
                notificationLoading = false;
                swipeRefreshLayout.setRefreshing(false);
                notificationList.removeFooterView(footerLoading);
            }

            @Override
            public void onFailure(Call<ArrayList<NotificationModel>> call, Throwable t) {
                MyLog.e(t.getMessage());
                loadingImage.hide();
                if (!more) {
                    loadingText.setText(R.string.no_new_notifications);
                }
                notificationLoading = false;
                swipeRefreshLayout.setRefreshing(false);
                notificationList.removeFooterView(footerLoading);
            }
        });
    }

    public void firstLoadFromCache() {
        String cache = pref.getString(cacheTag, null);
        if (cache != null) {
            ArrayList<NotificationModel> notifications = gson.fromJson(cache, new TypeToken<ArrayList<NotificationModel>>(){}.getType());
            notificationLists.addAll(notifications);
            notificationAdapter.notifyDataSetChanged();
        }
    }
}
