package com.procrea8.sexyafrikan.modules.feed.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.feed.FeedApiInterface;
import com.procrea8.sexyafrikan.modules.feed.model.AnswerModel;
import com.procrea8.sexyafrikan.modules.feed.model.TagUserModel;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PollsAnswerAdapter extends BaseAdapter implements Contants{
    Context context;
    ArrayList<AnswerModel> pollsList;
    Integer type;
    ArrayList<String> selectedAnswers = new ArrayList<>();

    public PollsAnswerAdapter(Context c, ArrayList<AnswerModel> list, Integer type) {
        this.context = c;
        this.pollsList = list;
        this.type = type;
    }

    @Override
    public int getCount() {
        return pollsList.size();
    }

    @Override
    public Object getItem(int i) {
        return pollsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = (View) layoutInflater.inflate(R.layout.feed_poll_answer_item, null);
            viewHolder = new ViewHolder();
            viewHolder.singleSelector = (RadioButton) view.findViewById(R.id.single_selector);
            viewHolder.multipleSelector = (CheckBox) view.findViewById(R.id.multiple_selector);
            viewHolder.progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
            viewHolder.userViewer = (CircleImageView) view.findViewById(R.id.view_users);
            viewHolder.userOne = (CircleImageView) view.findViewById(R.id.user_one);
            viewHolder.userTwo = (CircleImageView) view.findViewById(R.id.user_two);
            viewHolder.userThree = (CircleImageView) view.findViewById(R.id.user_three);
            viewHolder.answerText = (TextView) view.findViewById(R.id.answer_text);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        final AnswerModel answerModel = pollsList.get(i);
        viewHolder.progressBar.setProgress(answerModel.getPercent());
        viewHolder.answerText.setText(answerModel.getText());
        if (type == 1) {
            viewHolder.multipleSelector.setVisibility(View.GONE);
            viewHolder.singleSelector.setVisibility(View.VISIBLE);
            if (answerModel.getAnswered()) {
                viewHolder.singleSelector.setChecked(true);
            } else {
                viewHolder.singleSelector.setChecked(false);
                viewHolder.singleSelector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            String userid = App.getInstance().getSession().getUserid();
                            String answers = selectedAnswers.toString();
                            //MyLog.e("Poll_id" + answerModel.getPollId());
                            Call<FeedResponse> call = Api.getRetrofit().create(FeedApiInterface.class).submitPoll(userid, answerModel.getPollId(), answerModel.getAnswerId(), answers);
                            call.enqueue(new Callback<FeedResponse>() {
                                @Override
                                public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
                                    pollsList.clear();
                                    pollsList.addAll(response.body().getAnswers());
                                    //MyLog.e("return " + response.body().getAnswers().size());
                                    notifyDataSetChanged();
                                }

                                @Override
                                public void onFailure(Call<FeedResponse> call, Throwable t) {
                                    MyLog.e("poll-error " +t.getMessage());
                                }
                            });
                        }
                    }
                });
            }
        } else {
            viewHolder.multipleSelector.setVisibility(View.VISIBLE);
            viewHolder.singleSelector.setVisibility(View.GONE);
            if (answerModel.getAnswered()) {
                viewHolder.multipleSelector.setChecked(true);
            } else {
                viewHolder.multipleSelector.setChecked(false);

            }
            viewHolder.multipleSelector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        selectedAnswers.add(answerModel.getAnswerId());
                    } else {
                        selectedAnswers.remove(answerModel.getAnswerId());
                    }
                    String userid = App.getInstance().getSession().getUserid();
                    String answers = selectedAnswers.toString();
                    MyLog.e(answers);
                    Call<FeedResponse> call = Api.getRetrofit().create(FeedApiInterface.class).submitPoll(userid, answerModel.getPollId(), answerModel.getAnswerId(), answers);
                    call.enqueue(new Callback<FeedResponse>() {
                        @Override
                        public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
                            pollsList.clear();
                            pollsList.addAll(response.body().getAnswers());
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onFailure(Call<FeedResponse> call, Throwable t) {
                            MyLog.e("poll-error " +t.getMessage());
                        }
                    });
                }
            });
        }
        if (answerModel.getUsers().size() > 0) {
            viewHolder.userViewer.setVisibility(View.GONE);
            viewHolder.userOne.setVisibility(View.VISIBLE);
            final TagUserModel user1 = answerModel.getUsers().get(0);
            viewHolder.userOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProfileActivity.load(context, user1.getId());
                }
            });
            Glide.with(context).load(user1.getAvatar()).into(viewHolder.userOne);
            if (answerModel.getUsers().size() > 1) {
                viewHolder.userTwo.setVisibility(View.VISIBLE);
                final TagUserModel user2 = answerModel.getUsers().get(1);
                viewHolder.userTwo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ProfileActivity.load(context, user2.getId());
                    }
                });
                Glide.with(context).load(user2.getAvatar()).into(viewHolder.userTwo);
            } else {
                viewHolder.userTwo.setVisibility(View.GONE);
            }

            if (answerModel.getUsers().size() > 2) {
                viewHolder.userThree.setVisibility(View.VISIBLE);
                final TagUserModel user3 = answerModel.getUsers().get(2);
                viewHolder.userThree.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ProfileActivity.load(context, user3.getId());
                    }
                });
                Glide.with(context).load(user3.getAvatar()).into(viewHolder.userThree);
            } else {
                viewHolder.userThree.setVisibility(View.GONE);
            }

        } else {
            viewHolder.userViewer.setVisibility(View.GONE);
            viewHolder.userOne.setVisibility(View.GONE);
            viewHolder.userThree.setVisibility(View.GONE);
            viewHolder.userTwo.setVisibility(View.GONE);
        }

        return view;
    }

    static class ViewHolder{
        RadioButton singleSelector;
        CheckBox multipleSelector;
        ProgressBar progressBar;
        CircleImageView userViewer;
        CircleImageView userOne;
        CircleImageView userTwo;
        CircleImageView userThree;
        TextView answerText;
    }
}
