package com.procrea8.sexyafrikan.modules.setting;


import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SettingsApiInterface {
    @POST("settings/save")
    Call<Response> save(@Query("userid") String userid,@Query("first_name") String firstName, @Query("last_name") String lastName, @Query("bio") String bio, @Query("city") String city,
                        @Query("state") String state, @Query("notify-following-you") String follow, @Query("notify-site-mention-you")String mention,
                        @Query("notify-site-tag-you") String tag, @Query("notify-site-comment") String comment, @Query("notify-site-reply-comment") String reply,
                        @Query("notify-site-like") String like, @Query("who_can_view_profile") String profile, @Query("who_can_post_profile") String post, @Query("who_can_see_birth") String birth);

    @POST("settings/password")
    Call<Response> changePassword(@Query("userid") String userid, @Query("current_password") String cPassword, @Query("new_password") String nPassword);

    @GET("settings/blocked")
    Call<ArrayList<User>> getBlockedMembers(@Query("userid") String userid, @Query("page") String page);

    @POST("userpay/setting/user/update")
    Call<Response> updateMoney(@Query("userid") String userid, @Query("enable_paypal") String enable_paypal,
                               @Query("paypal_corporate_email") String paypal_corporate_email, @Query("enable_stripe") String enable_stripe,
                               @Query("stripe_secret_key") String stripe_secret_key, @Query("stripe_publishable_key") String stripe_publishable_key);
}
