package com.procrea8.sexyafrikan.modules.chat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.helper.TimeAgo;
import com.procrea8.sexyafrikan.modules.chat.model.Chat;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class ChatsAdapter extends BaseAdapter {
    ArrayList<Chat> chats = new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;

    public ChatsAdapter(Context c, ArrayList<Chat> chats) {
        this.chats = chats;
        context = c;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return chats.size();
    }

    @Override
    public Object getItem(int i) {
        return chats.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MyViewHolder holder;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.chat_list_item, null);
            holder = new MyViewHolder();
            holder.avatar = (CircleImageView) view.findViewById(R.id.avatar);
            holder.title = (TextView) view.findViewById(R.id.title);
            holder.lastMessage = (TextView) view.findViewById(R.id.last_message);
            holder.time = (TextView) view.findViewById(R.id.time);
            holder.unread = (TextView) view.findViewById(R.id.unread);
            view.setTag(holder);
        } else {
            holder = (MyViewHolder) view.getTag();
        }

        Chat chat = chats.get(i);
        Glide.with(context).load(chat.getAvatar()).crossFade().into(holder.avatar);
        holder.title.setText(chat.getTitle());
        holder.lastMessage.setText(chat.getLastMessage());
        holder.time.setText(TimeAgo.format(context, chat.getTime()));
        holder.unread.setVisibility(View.GONE);
        if (chat.getUnread() > 0) {
            holder.unread.setText(chat.getUnread().toString());
            holder.unread.setVisibility(View.VISIBLE);
        }
        return view;
    }

    static class MyViewHolder{
        CircleImageView avatar;
        TextView title;
        TextView lastMessage;
        TextView time;
        TextView unread;
    }
}
