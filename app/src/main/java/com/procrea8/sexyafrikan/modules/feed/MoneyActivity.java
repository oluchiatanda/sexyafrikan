package com.procrea8.sexyafrikan.modules.feed;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;
import com.procrea8.sexyafrikan.modules.hashtag.activity.HashtagActivity;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

/**
 * Created by Aminat on 11/13/2017.
 */

public class MoneyActivity extends ActivityBase {
    WebView webView;
    String sendMoneyUrl = "";
    String amount = "";
    String userid = "";
    AVLoadingIndicatorView loadImage;
    Context context;
    AppCompatActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        activity = this;
        webView = (WebView) findViewById(R.id.container);
        loadImage = (AVLoadingIndicatorView) findViewById(R.id.loading_image);

        loadImage.show();

        Intent intent = getIntent();
        if (intent.hasExtra("amount")) amount = intent.getStringExtra("amount");
        if (intent.hasExtra("user_id")) userid = intent.getStringExtra("user_id");

        WebSettings settings = webView.getSettings();
        sendMoneyUrl = SITE_BASE_URL + "userpay/payment/0?amount=" + amount + "&user_id=" + userid + "&webview=true"+
                "&api_userid=" + App.getInstance().getSession().getUserid();

        MyLog.e("moneyActivity- " + sendMoneyUrl);

        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setAppCacheEnabled(false);
        //webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new AppWebViewClient());
        webView.clearCache(true);

        webView.loadUrl(sendMoneyUrl);

    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    class AppWebViewClient extends android.webkit.WebViewClient implements Contants {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            loadImage.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            webView.clearCache(true);
            loadImage.hide();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, final String url) {
            if (Uri.parse(url).getHost().length() == 0) {
                return false;
            }

            if (url.startsWith("hashtag:")) {
                String[] str = url.split(":");
                String hashtag = str[1];
                HashtagActivity.launch(view.getContext(), hashtag);
            } else if (url.startsWith("mention:")) {
                String[] str = url.split(":");
                String userid = str[1];
                ProfileActivity.load(view.getContext(), userid);
            } else {
                if (Uri.parse(url).getHost().endsWith(DOMAIN)) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String userid = App.getInstance().getSession().getUserid();
                            String newUrl = url;
                            if (url.contains("&")) {
                                newUrl = url.concat("&webview=true&api_userid=" + userid);
                            } else {
                                newUrl = url.concat("?webview=true&api_userid=" + userid);
                            }
                            webView.loadUrl(newUrl);
                            //Toast.makeText(getContext(), "THis is Url - " + newUrl, Toast.LENGTH_LONG).show();
                        }
                    });
                    return true;
                } else {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                }
            }
            return true;
        }

    }

}
