package com.procrea8.sexyafrikan.modules.video.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.helper.MyWebViewClient;
import com.procrea8.sexyafrikan.modules.video.model.Video;

public class VideoActivity extends ActivityBase {
    static Video video;

    WebView container;
    LinearLayout layout_share;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        container = (WebView) findViewById(R.id.container);
        layout_share = (LinearLayout) findViewById(R.id.ex_share_button);

        layout_share.setVisibility(View.VISIBLE);
        View.OnClickListener shareListener = new View.OnClickListener() {
        @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("video/*");
                if(video.getSludge().equals("")) {
                    String shareUrl = SITE_BASE_URL + "video/" + video.getId();
                    intent.putExtra(Intent.EXTRA_TEXT, shareUrl);
                }
                else{
                    String shareUrl = SITE_BASE_URL + "video/" + video.getSludge();
                    intent.putExtra(Intent.EXTRA_TEXT, shareUrl);
                }
                startActivity(Intent.createChooser(intent, "Share via"));
            }
        };
        layout_share.setOnClickListener(shareListener);
    }

    @Override
    public void onStart() {
        super.onStart();
        container.setWebViewClient(new MyWebViewClient());
        container.setWebChromeClient(new WebChromeClient());
        WebSettings wS2 = container.getSettings();
        wS2.setJavaScriptEnabled(true);
        wS2.setBuiltInZoomControls(true);
        container.loadData(video.getCode(), "text/html", null);

        initGlobalFooter(this, "video", video.getId(), video.isHasReact(), video.isHasLike(),
                video.isHasDislike(),
                video.getLikeCount(), video.getDislikeCount(), video.getComments());
    }

    public static void load(Context context, Video v) {
        video = v;
        Intent intent = new Intent(context, VideoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        context.startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        container.destroy();
        super.onBackPressed();
    }
}

