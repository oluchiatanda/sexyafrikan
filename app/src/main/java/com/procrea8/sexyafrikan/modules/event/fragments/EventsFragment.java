package com.procrea8.sexyafrikan.modules.event.fragments;


import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.procrea8.sexyafrikan.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventsFragment extends Fragment {
    ViewPager container;
    TabLayout tabLayout;

    public EventsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup group,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_events, container, false);
        container = (ViewPager) view.findViewById(R.id.container);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        //adapter.add(new FeedFragment());
        adapter.add(new EventsBrowseFragment(), getContext().getResources().getString(R.string.all));

        Fragment myEventFragment = new EventsBrowseFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", "me");
        myEventFragment.setArguments(bundle);
        adapter.add(myEventFragment, getContext().getResources().getString(R.string.my_events));
        //adapter.add(new BirthdaysFragment(), getContext().getResources().getString(R.string.birthdays));
        container.setAdapter(adapter);
        tabLayout.setupWithViewPager(container);
        return view;
    }

    static class ViewPagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<Fragment> fragments = new ArrayList<>();
        ArrayList<String> title = new ArrayList<>();
        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        public void clear() {
            fragments.clear();
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }
        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
        @Override
        public Parcelable saveState() {
            return null;
        }
        @Override
        public int getCount() {
            return fragments.size();
        }

        public void add(Fragment fragment, String t) {
            fragments.add(fragment);
            title.add(t);
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return title.get(position);
        }
    }
}
