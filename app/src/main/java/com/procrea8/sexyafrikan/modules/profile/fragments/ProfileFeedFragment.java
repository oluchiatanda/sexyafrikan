package com.procrea8.sexyafrikan.modules.profile.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.chat.activity.ChatActivity;
import com.procrea8.sexyafrikan.modules.feed.FeedApiInterface;
import com.procrea8.sexyafrikan.modules.feed.adapter.FeedAdapter;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;
import com.procrea8.sexyafrikan.modules.photo.activity.PhotoViewerActivity;
import com.procrea8.sexyafrikan.modules.profile.ProfileApiInterface;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;
import com.procrea8.sexyafrikan.modules.relationship.RelationshipApiInterface;
import com.procrea8.sexyafrikan.modules.setting.activity.SettingsActivity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import me.nereo.multi_image_selector.MultiImageSelector;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFeedFragment extends Fragment implements Contants {

    static ProfileFeedFragment instance;

    ListView feedListsView;
    FeedAdapter feedAdapter;
    static String userid = "";
    ArrayList<FeedResponse> feedLists = new ArrayList<>();
    static ImageView profileCover;
    static ImageView profileAvatar;
    TextView profileTitle;
    ImageView sendMessage;
    ImageView verifyBadge;
    String limit = "10";
    String offset = "0";

    View footerLoading;

    Boolean moreFeedsLoading = false;

    //friend connection
    LinearLayout friendConnectionContainer;
    LinearLayout respondButton;
    LinearLayout removeFriendButton;
    LinearLayout requestSentButton;
    LinearLayout addFriendButton;
    LinearLayout followButton;
    LinearLayout followingButton;

    LinearLayout recentPhotosContainer;
    ImageView photo1;
    ImageView photo2;
    ImageView photo3;

    //cover and avatar
    static ImageView changeAvatar;
    static ImageView changeCover;
    static String changePhotoType = "";
    static String selectedPhoto = "";

    LinearLayout aboutPage, photosPage, friendsPage, profileMore;


    static User user;
    static String loggedInId;
    static Context context;
    Button editButton;

    public ProfileFeedFragment() {
        // Required empty public constructor
    }

    public static ProfileFeedFragment getInstance(String userId) {
        if (!userId.equals(userid)) {
            instance = null;
        }
        //if (instance != null) return instance;
        instance = new ProfileFeedFragment();
        userid = userId;
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context = getContext();
        View view = inflater.inflate(R.layout.fragment_profile_feed, container, false);
        feedListsView = (ListView) view.findViewById(R.id.feed_lists);

        feedListsView.addHeaderView(inflater.inflate(R.layout.user_profile_header, null));
        footerLoading = (View) inflater.inflate(R.layout.loading_indicator, null);
        profileCover = (ImageView) view.findViewById(R.id.profile_cover);
        profileTitle = (TextView) view.findViewById(R.id.profile_name);
        profileAvatar = (ImageView) view.findViewById(R.id.profile_avatar);
        recentPhotosContainer = (LinearLayout) view.findViewById(R.id.recent_photos);
        photo1 = (ImageView) view.findViewById(R.id.photo_1);
        photo2 = (ImageView) view.findViewById(R.id.photo_2);
        photo3 = (ImageView) view.findViewById(R.id.photo_3);
        sendMessage = (ImageView) view.findViewById(R.id.send_message);
        verifyBadge = (ImageView) view.findViewById(R.id.verified_badge);
        editButton = (Button) view.findViewById(R.id.editprofile);

        changeAvatar = (ImageView) view.findViewById(R.id.change_avatar);
        changeCover = (ImageView) view.findViewById(R.id.change_cover);

        aboutPage = (LinearLayout) view.findViewById(R.id.about_page);
        friendsPage = (LinearLayout) view.findViewById(R.id.friend_page);
        photosPage = (LinearLayout) view.findViewById(R.id.photos_page);
        profileMore = (LinearLayout) view.findViewById(R.id.profile_more);

        loggedInId = App.getInstance().getSession().getUserid();

        feedListsView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                final int lastItem = i + i1;
                if (i2 > 5 && lastItem == i2) {
                    if (!moreFeedsLoading) {
                        loadMoreFeeds();
                        moreFeedsLoading = true;
                    }
                }
            }
        });

        if (loggedInId.equals(userid)) {
            editButton.setVisibility(View.VISIBLE);
            changeCover.setVisibility(View.VISIBLE);
            changeAvatar.setVisibility(View.VISIBLE);
            sendMessage.setVisibility(View.GONE);
        } else {
            sendMessage.setVisibility(View.VISIBLE);

        }

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SettingsActivity.class);
                startActivity(intent);

            }
        });

        profileAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user != null) {
                    PhotoViewerActivity.launch(context, user.getAvatar());
                }
            }
        });
        profileCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user != null) {
                    PhotoViewerActivity.launch(context, user.getCover());
                }
            }
        });
        aboutPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = ProfileAboutFragment.getInstance(userid);
                ProfileActivity.loadPage(fragment);
            }
        });

        friendsPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = ProfileFriendsFragment.getInstance(userid);
                ProfileActivity.loadPage(fragment);
            }
        });
        photosPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = ProfilePhotosFragment.getInstance(userid);
                ProfileActivity.loadPage(fragment);
            }
        });

        changeAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePhoto("avatar");
            }
        });

        changeCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePhoto("cover");
            }
        });

        friendConnectionContainer = (LinearLayout) view.findViewById(R.id.friend_connection_container);
        requestSentButton = (LinearLayout) view.findViewById(R.id.friend_request_sent);
        removeFriendButton = (LinearLayout) view.findViewById(R.id.remove_friends);
        addFriendButton = (LinearLayout) view.findViewById(R.id.add_friend);
        respondButton = (LinearLayout) view.findViewById(R.id.respond_friend);
        followButton = (LinearLayout) view.findViewById(R.id.follow_button);
        followingButton = (LinearLayout) view.findViewById(R.id.following_button);

        followButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                followButton.setVisibility(View.GONE);
                followingButton.setVisibility(View.VISIBLE);
                Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).follow(
                        loggedInId,
                        user.getId(),
                        "follow"
                );
                call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                    @Override
                    public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, retrofit2.Response<com.procrea8.sexyafrikan.response.Response> response) {
                        //Toast.makeText(context, R.string.friend_requests_sent, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                        followButton.setVisibility(View.VISIBLE);
                        followingButton.setVisibility(View.GONE);
                        Toast.makeText(context, R.string.failed_to_follow, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        followingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                followButton.setVisibility(View.VISIBLE);
                followingButton.setVisibility(View.GONE);
                Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).follow(
                        loggedInId,
                        user.getId(),
                        "unfollow"
                );
                call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                    @Override
                    public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, retrofit2.Response<com.procrea8.sexyafrikan.response.Response> response) {
                        //Toast.makeText(context, R.string.friend_requests_sent, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                        followButton.setVisibility(View.GONE);
                        followingButton.setVisibility(View.VISIBLE);
                        Toast.makeText(context, R.string.failed_to_unfollow, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        addFriendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFriendButton.setVisibility(View.GONE);
                requestSentButton.setVisibility(View.VISIBLE);
                user.setFriendStatus("1");
                Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).add(
                        loggedInId,
                        user.getId()
                );
                call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                    @Override
                    public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, retrofit2.Response<com.procrea8.sexyafrikan.response.Response> response) {
                        Toast.makeText(context, R.string.friend_requests_sent, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                        addFriendButton.setVisibility(View.VISIBLE);
                        requestSentButton.setVisibility(View.GONE);
                        Toast.makeText(context, R.string.failed_to_add_friend, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        removeFriendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(R.string.are_you_sure)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                addFriendButton.setVisibility(View.VISIBLE);
                                removeFriendButton.setVisibility(View.GONE);
                                user.setFriendStatus("0");
                                Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).remove(
                                        loggedInId,
                                        user.getId()
                                );
                                call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                                    @Override
                                    public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, retrofit2.Response<com.procrea8.sexyafrikan.response.Response> response) {

                                    }

                                    @Override
                                    public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                                        addFriendButton.setVisibility(View.GONE);
                                        removeFriendButton.setVisibility(View.VISIBLE);
                                        Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                respondButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                        String[] items = {context.getResources().getString(R.string.accept), context.getResources().getString(R.string.reject)};
                        builder1.setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (i == 0) {
                                    respondButton.setVisibility(View.GONE);
                                    removeFriendButton.setVisibility(View.VISIBLE);
                                    user.setFriendStatus("2");
                                    Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).accept(
                                            App.getInstance().getSession().getUserid(),
                                            user.getId()
                                    );
                                    call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                                        @Override
                                        public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, retrofit2.Response<com.procrea8.sexyafrikan.response.Response> response) {

                                        }

                                        @Override
                                        public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                                            respondButton.setVisibility(View.VISIBLE);
                                            removeFriendButton.setVisibility(View.GONE);
                                            Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                                        }
                                    });
                                } else {
                                    respondButton.setVisibility(View.GONE);
                                    addFriendButton.setVisibility(View.VISIBLE);
                                    user.setFriendStatus("0");
                                    Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).remove(
                                            App.getInstance().getSession().getUserid(),
                                            user.getId()
                                    );
                                    call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                                        @Override
                                        public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, retrofit2.Response<com.procrea8.sexyafrikan.response.Response> response) {

                                        }

                                        @Override
                                        public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                                            respondButton.setVisibility(View.VISIBLE);
                                            addFriendButton.setVisibility(View.GONE);
                                            Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
                builder.create().show();
            }
        });

        requestSentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(R.string.are_you_sure_cancel_request)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                addFriendButton.setVisibility(View.VISIBLE);
                                requestSentButton.setVisibility(View.GONE);
                                user.setFriendStatus("0");
                                Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).remove(
                                        loggedInId,
                                        user.getId()
                                );
                                call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                                    @Override
                                    public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, retrofit2.Response<com.procrea8.sexyafrikan.response.Response> response) {

                                    }

                                    @Override
                                    public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                                        addFriendButton.setVisibility(View.GONE);
                                        requestSentButton.setVisibility(View.VISIBLE);
                                        Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
            }
        });


        loadProfileDetails();
        return view;
    }

    public void loadProfileDetails() {
        Call<User> call = Api.getRetrofit().create(ProfileApiInterface.class).get(
                App.getInstance().getSession().getUserid(),
                userid
        );
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                profileTitle.setText(response.body().getName());
                user = response.body();
                //if (context == null) return;
                MyLog.e("User details loaded");
                feedAdapter = new FeedAdapter(context, feedLists, "feed", loggedInId, loggedInId, "user");
                if (!loggedInId.equals(user.getId()) && user.getCanPostTimeline()) {
                    feedAdapter.setToUserid(user.getId());
                }

                sendMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ChatActivity.loadChat(getContext(), user.getName(), "", userid);
                    }
                });

                if (user.getVerified()) {
                    verifyBadge.setVisibility(View.VISIBLE);
                } else {
                    verifyBadge.setVisibility(View.GONE);
                }

                feedListsView.addFooterView(footerLoading);
                feedListsView.setAdapter(feedAdapter);
                feedListsView.removeFooterView(footerLoading);
                Glide.with(context).load(response.body().getAvatar()).crossFade().into(profileAvatar);
                Glide.with(context).load(response.body().getCover()).crossFade().into(profileCover);
                if (!userid.equals(App.getInstance().getSession().getUserid())) {
                    friendConnectionContainer.setVisibility(View.VISIBLE);
                    if (user.getFollowing()) {
                        followingButton.setVisibility(View.VISIBLE);
                    } else {
                        followButton.setVisibility(View.VISIBLE);
                    }

                    switch (user.getFriendStatus()) {
                        case "0":
                            addFriendButton.setVisibility(View.VISIBLE);
                            break;
                        case "1":
                            requestSentButton.setVisibility(View.VISIBLE);
                            break;
                        case "2":
                            removeFriendButton.setVisibility(View.VISIBLE);
                            break;
                        case "3":
                            respondButton.setVisibility(View.VISIBLE);
                            break;
                    }
                }

                //MyLog.e("Recent Photos - " + user.getRecentPhotos().size());
                if (user.getRecentPhotos().size() > 0) {
                    final ArrayList<String> photos = user.getRecentPhotos();
                    recentPhotosContainer.setVisibility(View.VISIBLE);
                    photo1.setVisibility(View.VISIBLE);
                    Glide.with(context).load(photos.get(0)).into(photo1);
                    photo1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            PhotoViewerActivity.launch(context, photos.get(0));
                        }
                    });
                    if (photos.size() > 1) {
                        photo2.setVisibility(View.VISIBLE);
                        Glide.with(context).load(photos.get(1)).into(photo2);
                        photo2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                PhotoViewerActivity.launch(context, photos.get(1));
                            }
                        });
                    }
                    if (photos.size() > 2) {
                        photo3.setVisibility(View.VISIBLE);
                        Glide.with(context).load(photos.get(2)).into(photo3);
                        photo3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                PhotoViewerActivity.launch(context, photos.get(2));
                            }
                        });
                    }
                }

                loadPosts(false);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    public void addDefaultPostEditor() {
        FeedResponse feedResponse = new FeedResponse();
        feedResponse.setIsFeedItem(false);
        feedLists.add(feedResponse);
    }

    public void loadPosts(final Boolean more) {

        Call<ArrayList<FeedResponse>> call = Api.getRetrofit().create(FeedApiInterface.class).getFeeds(App.getInstance().getSession().getUserid(),
                "timeline", userid, limit, offset);
        call.enqueue(new Callback<ArrayList<FeedResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<FeedResponse>> call, Response<ArrayList<FeedResponse>> response) {
                if (!more) {
                    feedLists.clear();
                    if (user.getCanPostTimeline()) addDefaultPostEditor();
                }

                if (response.body() != null && response.body().size() > 0) {
                    feedLists.addAll(response.body());

                } else {

                }
                feedListsView.removeFooterView(footerLoading);
                feedAdapter.notifyDataSetChanged();
                moreFeedsLoading = false;
            }

            @Override
            public void onFailure(Call<ArrayList<FeedResponse>> call, Throwable t) {
                feedListsView.removeFooterView(footerLoading);
                moreFeedsLoading = false;
            }
        });
    }

    public void loadMoreFeeds() {
        offset = offset + limit;
        feedListsView.addFooterView(footerLoading);
        loadPosts(true);
    }

    public void changePhoto(String type) {
        changePhotoType = type;
        final ActivityBase activity = (ActivityBase) context;
        activity.setPermissionGranted(new ActivityBase.OnPermissionGranted() {
            @Override
            public void onGranted() {
                MultiImageSelector.create()
                        .showCamera(true)
                        .count(1)
                        .single()
                        .start(activity, PROFILE_SELECT_PHOTO);
            }
        });
    }

    public Uri getOutputMediaFileUri() {
        File mediaStoreDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "sets"
        );
        if (!mediaStoreDir.exists()) {
            if (!mediaStoreDir.mkdirs()) {
                MyLog.e("Failed to create photo directory");
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile = new File(mediaStoreDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

        return Uri.fromFile(mediaFile);

    }

    public static void setSelectedPhoto(String pho) {
        selectedPhoto = pho;
        //startPhotoUpload();
    }

    public static void startPhotoUpload() {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading..");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
        Call<com.procrea8.sexyafrikan.response.Response> call;
        File file = new File(selectedPhoto);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        if (changePhotoType.equals("cover")) {
            MultipartBody.Part body = MultipartBody.Part.createFormData("cover", file.getName(), requestFile);
            call = Api.getRetrofit().create(ProfileApiInterface.class).changeCover(loggedInId, body);
        } else {
            MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);
            call = Api.getRetrofit().create(ProfileApiInterface.class).changeAvatar(loggedInId, body);
        }

        call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
            @Override
            public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, Response<com.procrea8.sexyafrikan.response.Response> response) {
                progressDialog.dismiss();
                if (response.body().getStatus()==1) {
                    if (changePhotoType.equals("cover")) {
                        user.setCover(response.body().getDataOne());
                        Glide.with(context).load(response.body().getDataOne()).into(profileCover);
                    } else {
                        user.setAvatar(response.body().getDataOne());
                        Glide.with(context).load(response.body().getDataOne()).into(profileAvatar);
                    }
                    Toast.makeText(context, R.string.photo_successful_changed, Toast.LENGTH_LONG).show();
                    selectedPhoto = "";
                    changePhotoType = "";
                }
            }

            @Override
            public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
            }
        });
    }
}
