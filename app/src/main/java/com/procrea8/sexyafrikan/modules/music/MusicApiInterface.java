package com.procrea8.sexyafrikan.modules.music;


import com.procrea8.sexyafrikan.modules.music.model.BrowseResponse;
import com.procrea8.sexyafrikan.modules.music.model.Music;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface MusicApiInterface {
    @GET("music/get/categories")
    Call<ArrayList<MusicCategory>> getCategories(@Query("userid") String userid);
    @GET("music/browse")
    Call<BrowseResponse>  browse(@Query("userid") String userid, @Query("category") String category, @Query("term") String term,
                                 @Query("type") String type, @Query("filter") String filter, @Query("limit") Integer limit,
                                 @Query("page") Integer page);
    @GET("music/page")
    Call<Music> getSong(@Query("userid") String userid, @Query("music_id") String musicId);

    @GET("music/delete")
    Call<Response> delete(@Query("userid") String userid, @Query("music_id") String musicId);

    @Multipart
    @POST("music/edit")
    Call<Music> edit(@Query("userid") String userid, @Query("music_id") String musicId, @Query("title") String title, @Query("artist") String artist, @Query("album") String album,
                       @Query("privacy") String privacy, @Query("category_id") String categoryId,
                       @Part MultipartBody.Part coverArt);

    @POST("music/edit")
    Call<Music> edit(@Query("userid") String userid, @Query("music_id") String musicId, @Query("title") String title, @Query("artist") String artist, @Query("album") String album,
                     @Query("privacy") String privacy, @Query("category_id") String categoryId);

    @Multipart
    @POST("music/create")
    Call<Music> create(@Query("userid") String userid, @Query("title") String title, @Query("artist") String artist, @Query("album") String album,
                       @Query("privacy") String privacy, @Query("category_id") String categoryId, @Part MultipartBody.Part music_file,
                       @Part MultipartBody.Part coverArt);

    @Multipart
    @POST("music/create")
    Call<Music> create(@Query("userid") String userid, @Query("title") String title, @Query("artist") String artist, @Query("album") String album,
                       @Query("privacy") String privacy, @Query("category_id") String categoryId, @Part MultipartBody.Part music_file);


}
