package com.procrea8.sexyafrikan.modules.blog;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.AppActivity;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.blog.activity.BlogActivity;
import com.procrea8.sexyafrikan.modules.blog.fragment.BlogsFragment;
import com.procrea8.sexyafrikan.modules.blog.model.Blog;
import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;

public class BlogModule implements ModuleInterface {
    @Override
    public void registerMenu(final Context context, Drawer drawer, int i) {
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_content_paste_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = new BlogsFragment();
                        AppActivity.setFragment(fragment);
                        AppActivity.setCurrentTitle(context.getResources().getString(R.string.blogs));
                        return false;
                    }
                })
                .withName(context.getResources().getString(R.string.blogs)));
    }

    @Override
    public Fragment getFragment(Context context) {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type, Object object) {
        if (type.equals("notification-title")) {
            NotificationModel model = (NotificationModel) object;
            switch (model.getType()) {
                case "blog.like":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.like_your_blog));

                case "blog.like.comment":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.like_your_comment));
                case "blog.comment":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.commented_on_blog));
            }
        } else if(type.equals("notification-action")) {
            NotificationModel model = (NotificationModel) object;
            String nType = model.getType();
            if (nType.equals("blog.like") || nType.equals("blog.like.comment") || nType.equals("blog.comment")) {
                Blog blog = model.getBlogs().get(0);
                BlogActivity.load(context, blog);
            }
        }
        return null;
    }

    @Override
    public Object invoke(Context context, String type) {
        return null;
    }
}
