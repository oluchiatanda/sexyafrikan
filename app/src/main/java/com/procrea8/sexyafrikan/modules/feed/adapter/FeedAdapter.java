package com.procrea8.sexyafrikan.modules.feed.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.reflect.TypeToken;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.adapter.ReactorsAdapter;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.api.LikeApiInterface;
import com.procrea8.sexyafrikan.dialogs.CommentDialog;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.helper.MyWebViewClient;
import com.procrea8.sexyafrikan.helper.TimeAgo;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.feed.FeedApiInterface;
import com.procrea8.sexyafrikan.modules.feed.MoneyActivity;
import com.procrea8.sexyafrikan.modules.feed.PurchaseActivity;
import com.procrea8.sexyafrikan.modules.feed.activity.FeedActivity;
import com.procrea8.sexyafrikan.modules.feed.dialogs.EditorDialog;
import com.procrea8.sexyafrikan.modules.feed.model.LinkModel;
import com.procrea8.sexyafrikan.modules.feed.response.FeedAddResponse;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;
import com.procrea8.sexyafrikan.modules.hashtag.activity.HashtagActivity;
import com.procrea8.sexyafrikan.modules.music.activity.MusicPlayerActivity;
import com.procrea8.sexyafrikan.modules.music.model.Music;
import com.procrea8.sexyafrikan.modules.photo.activity.PhotoViewerActivity;
import com.procrea8.sexyafrikan.modules.profile.ProfileModule;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;
import com.procrea8.sexyafrikan.modules.relationship.RelationshipApiInterface;
import com.procrea8.sexyafrikan.response.LikeResponse;
import com.procrea8.sexyafrikan.response.ReactResponse;
import com.procrea8.sexyafrikan.response.Response;

import java.io.File;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;


public class FeedAdapter extends BaseAdapter implements Contants {
    private ArrayList<FeedResponse> feedLists = new ArrayList<>();
    EditText amountOfMoney;
    Button sendMoney;
    EditText amountOfCredit;
    Button sendCredit;
    public static AlertDialog dialog;
    Context context;
    LayoutInflater layoutInflater;
    String type, typeId, entityType, entityId, toUserid;
    Boolean useFullMessage = false;
    WebView webView;
    ProgressDialog progressDialog;
    SharedPreferences pref;
//    boolean clicked = false;
//    boolean following = false;

    public FeedAdapter(Context c, ArrayList<FeedResponse> feeds, String type, String typeId, String entityId, String entityType) {
//        super(c, 0, feeds);
        this.context = c;
        this.feedLists = feeds;
        this.type = type;
        this.typeId = typeId;
        this.entityId = entityId;
        this.entityType = entityType;
    }

    public void enableFullMessage() {
        this.useFullMessage = true;

    }

//    public void followClicked(){
//        this.clicked = true;
//    }


    public void setToUserid(String toUserid) {
        this.toUserid = toUserid;
    }

    @Override
    public int getCount() {
        return feedLists.size();
    }

    //
    @Override
    public Object getItem(int i) {
        return feedLists.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        final FeedResponse feedResponse = feedLists.get(i);
        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (view == null) {
            view = (View) layoutInflater.inflate(R.layout.feed_item, null);
            holder = new ViewHolder();
            holder.follow = (LinearLayout) view.findViewById(R.id.followFeed);
            holder.followContainer = (LinearLayout) view.findViewById(R.id.followContainer);
            holder.following = (LinearLayout) view.findViewById(R.id.following);
            holder.buyVote = (LinearLayout) view.findViewById(R.id.buyvote);
            holder.vote = (LinearLayout) view.findViewById(R.id.vote);
            holder.sendMoney = (LinearLayout) view.findViewById(R.id.sendmoney);
            holder.ext_share_btn = (ImageView) view.findViewById(R.id.share_b);
            holder.authorAvatar = (ImageView) view.findViewById(R.id.author_avatar);
            holder.feedTime = (TextView) view.findViewById(R.id.feed_time);
            holder.authorName = (TextView) view.findViewById(R.id.author_name);
            holder.feedAction = (ImageView) view.findViewById(R.id.feed_action);
            holder.message = (WebView) view.findViewById(R.id.feed_message);
            holder.photosPager = (ViewPager) view.findViewById(R.id.photos_viewpager);
            holder.likeButton = (LinearLayout) view.findViewById(R.id.like_button);
            holder.dislikeButton = (LinearLayout) view.findViewById(R.id.dislike_button);
            holder.likeButtonIcon = (ImageView) view.findViewById(R.id.like_button_icon);
            holder.dislikeButtonIcon = (ImageView) view.findViewById(R.id.dislike_button_icon);
            holder.replyButton = (LinearLayout) view.findViewById(R.id.reply_button);
            holder.reactIcons = (LinearLayout) view.findViewById(R.id.react_icons);
            holder.react_sad = (ImageView) view.findViewById(R.id.react_sad);
            holder.reactHaha = (ImageView) view.findViewById(R.id.react_haha);
            holder.reactLike = (ImageView) view.findViewById(R.id.react_like);
            holder.reactLove = (ImageView) view.findViewById(R.id.react_love);
            holder.reactWow = (ImageView) view.findViewById(R.id.react_wow);
            holder.reactAngry = (ImageView) view.findViewById(R.id.react_angry);
            holder.reactYay = (ImageView) view.findViewById(R.id.react_yay);
            holder.commentCount = (TextView) view.findViewById(R.id.comment_count);
            holder.likeCount = (TextView) view.findViewById(R.id.like_count);
            holder.dislikeCount = (TextView) view.findViewById(R.id.dislike_count);
            holder.feedReacts = (LinearLayout) view.findViewById(R.id.feed_reacts);
            holder.reactorsUser1 = (CircleImageView) view.findViewById(R.id.reactors_user1);
            holder.reactorsUser2 = (CircleImageView) view.findViewById(R.id.reactors_user2);
            holder.reactorsUser3 = (CircleImageView) view.findViewById(R.id.reactors_user3);
            holder.reactorsUser4 = (CircleImageView) view.findViewById(R.id.reactors_user4);
            holder.reactorsMore = (CircleImageView) view.findViewById(R.id.reactors_more);
            holder.feedEditor = (LinearLayout) view.findViewById(R.id.feed_editor);
            holder.feedItemContainer = (LinearLayout) view.findViewById(R.id.item_container);
            holder.editorAvatar = (ImageView) view.findViewById(R.id.editor_avatar);
            holder.edtitorText = (TextView) view.findViewById(R.id.editor_text);
            holder.locationImage = (ImageView) view.findViewById(R.id.location_image);
            holder.linkImage = (ImageView) view.findViewById(R.id.link_image);
            holder.linkCode = (WebView) view.findViewById(R.id.link_code);
            holder.linkDescription = (TextView) view.findViewById(R.id.link_description);
            holder.linkTitle = (TextView) view.findViewById(R.id.link_title);
            holder.linkProvider = (TextView) view.findViewById(R.id.link_provider);
            holder.linkContainer = (LinearLayout) view.findViewById(R.id.link_container);
            holder.feelingContainer = (LinearLayout) view.findViewById(R.id.feeling_container);
            holder.feelingImage = (ImageView) view.findViewById(R.id.feeling_image);
            holder.feelingType = (TextView) view.findViewById(R.id.feeling_type);
            holder.feelingValue = (TextView) view.findViewById(R.id.feeling_value);
            holder.videoContainer = (LinearLayout) view.findViewById(R.id.video_container);
            holder.videoPlayer = (WebView) view.findViewById(R.id.video_player);
            holder.videoTitle = (TextView) view.findViewById(R.id.video_title);
            holder.sharedContainer = (LinearLayout) view.findViewById(R.id.shared_container);
            holder.sharedImage = (ImageView) view.findViewById(R.id.shared_image);
            holder.sharedTitle = (TextView) view.findViewById(R.id.shared_title);
            holder.sharedTime = (TextView) view.findViewById(R.id.shared_time);
            holder.filesListView = (ListView) view.findViewById(R.id.files_list);
            holder.pollAnswers = (ListView) view.findViewById(R.id.poll_answers);
            holder.shareButton = (LinearLayout) view.findViewById(R.id.share_button);
            holder.musicContainer = (RelativeLayout) view.findViewById(R.id.music_container);
            holder.musicTitle = (TextView) view.findViewById(R.id.music_title);
            holder.musicPlayCount = (TextView) view.findViewById(R.id.music_play_count);
            holder.adContainer = (LinearLayout) view.findViewById(R.id.feed_ad);
            holder.adImage = (ImageView) view.findViewById(R.id.ad_image);
            holder.adTitle = (TextView) view.findViewById(R.id.ad_title);
            holder.adDesc = (TextView) view.findViewById(R.id.ad_description);
            //holder.photosCount = (TextView)view.findViewById(R.id.add_photos);
            holder.editorStatusBtn = (LinearLayout) view.findViewById(R.id.feed_editor_status_btn);
            holder.editorPhotoBtn = (LinearLayout) view.findViewById(R.id.feed_editor_photo_btn);
            holder.editorLocationBtn = (LinearLayout) view.findViewById(R.id.feed_editor_location_btn);

            holder.singleImageContainer = (LinearLayout) view.findViewById(R.id.single_image_container);
            holder.twoImageContainer = (LinearLayout) view.findViewById(R.id.two_image_container);
            holder.threeImageContainer = (LinearLayout) view.findViewById(R.id.three_image_container);
            holder.fourImageContainer = (LinearLayout) view.findViewById(R.id.four_image_container);
            holder.imageMoreContainer = (LinearLayout) view.findViewById(R.id.image_more_container);
            holder.singleImage = (ImageView) view.findViewById(R.id.single_image);
            holder.twoImageLeft = (ImageView) view.findViewById(R.id.two_image_left);
            holder.twoImageRight = (ImageView) view.findViewById(R.id.two_image_right);
            holder.threeImageLeft = (ImageView) view.findViewById(R.id.three_image_left);
            holder.threeImageRight1 = (ImageView) view.findViewById(R.id.three_image_right_1);
            holder.threeImageRight2 = (ImageView) view.findViewById(R.id.three_image_right_2);
            holder.fourImageLeft = (ImageView) view.findViewById(R.id.four_image_left);
            holder.fourImageRight1 = (ImageView) view.findViewById(R.id.four_image_right_1);
            holder.fourImageRight2 = (ImageView) view.findViewById(R.id.four_image_right_2);
            holder.fourImageRight3 = (ImageView) view.findViewById(R.id.four_image_right_3);
            holder.imageMoreText = (TextView) view.findViewById(R.id.image_more_text);
            pref = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.reactIcons.setTag(i);
        holder.likeButton.setTag(i);
        holder.react_sad.setTag(i);
        holder.reactHaha.setTag(i);
        holder.reactLike.setTag(i);
        holder.reactLove.setTag(i);
        holder.reactWow.setTag(i);
        holder.reactYay.setTag(i);
        holder.reactAngry.setTag(i);
        holder.commentCount.setTag(i);
        holder.replyButton.setTag(i);
        holder.reactorsMore.setTag(i);
        webView = holder.message;

        progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(context.getResources().getString(R.string.posting));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        final String loggedinId = App.getInstance().getSession().getUserid();

        if (feedResponse.getIsFeedItem()) {
            holder.feedItemContainer.setVisibility(View.VISIBLE);
            holder.feedEditor.setVisibility(View.GONE);
            holder.message.setWebChromeClient(new WebChromeClient() {
                @Override
                public boolean onConsoleMessage(ConsoleMessage cm) {
                    MyLog.e(String.format("%s @ %d: %s", cm.message(),
                            cm.lineNumber(), cm.sourceId()));
                    return true;
                }
            });
            holder.message.setWebViewClient(new MyWebViewClient());
            holder.message.addJavascriptInterface(this, "MyApp");
            holder.message.setVisibility(View.GONE);

            if (feedResponse.getAds() != null && feedResponse.getAds().size() > 0) {
                holder.adContainer.setVisibility(View.VISIBLE);
                final LinkModel adLink = feedResponse.getAds().get(0);
                Glide.with(context).load(adLink.getImage()).into(holder.adImage);
                holder.adTitle.setText(adLink.getTitle());
                holder.adDesc.setText(adLink.getDescription());
                holder.adImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(adLink.getLink()));
                        context.startActivity(browserIntent);
                    }
                });
                holder.adTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(adLink.getLink()));
                        context.startActivity(browserIntent);
                    }
                });
            } else {
                holder.adContainer.setVisibility(View.GONE);
            }
            if (!feedResponse.getMessage().isEmpty()) {
                if (useFullMessage) {
                    String feedText = "<style>a{color:grey;text-decoration:none}</style>" + feedResponse.getFullMessage();
                    holder.message.loadData(feedText, "text/html; charset=utf-8", "utf-8");
                } else {
                    String feedText = "<style>a{color:grey;text-decoration:none}</style>" + feedResponse.getMessage();
                    holder.message.loadData(feedText, "text/html; charset=utf-8", "utf-8");
                }
                holder.message.getSettings().setDefaultTextEncodingName("utf-8");
                holder.message.setVisibility(View.VISIBLE);
            }


            Glide.with(context).load(feedResponse.getAuthorAvatar()).crossFade().into(holder.authorAvatar);
            ArrayList<String> images = feedResponse.getImages();
            //MyLog.e("Binding view - " + images.toString());

            if (feedResponse.getMusics().size() > 0) {
                holder.musicContainer.setVisibility(View.VISIBLE);
                final Music music = feedResponse.getMusics().get(0);
                holder.musicTitle.setText(music.getTitle());
                holder.musicPlayCount.setText(music.getPlayCount());
                holder.musicContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MusicPlayerActivity.launch(context, music);
                    }
                });
            } else {
                holder.musicContainer.setVisibility(View.GONE);
            }

            if (feedResponse.getPrivacy().equals("1")) {
                holder.shareButton.setVisibility(View.VISIBLE);
            }

            holder.authorAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyLog.e("entitiy id" + feedResponse.getEntityId());
                    if (feedResponse.getEntityType().equals("user")) {
                        ProfileActivity.load(context, feedResponse.getEntityId());
                    }
                }
            });
            holder.ext_share_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    String shareUrl = SITE_BASE_URL + "feed/" + feedResponse.getFeedId();
                    intent.putExtra(android.content.Intent.EXTRA_TEXT, shareUrl);
                    context.startActivity(Intent.createChooser(intent, "Share via"));
                }
            });

            holder.shareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shareFeed(feedResponse);
                }
            });

            holder.follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    clicked = true;
                    holder.follow.setVisibility(View.GONE);
                    holder.following.setVisibility(View.VISIBLE);
                    updateFollow(feedResponse.getEntityId(), 1);
                    Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).follow(
                            App.getInstance().getSession().getUserid(),
                            feedResponse.getEntityId(),
                            "follow"
                    );
                    call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                        @Override
                        public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, retrofit2.Response<com.procrea8.sexyafrikan.response.Response> response) {
                            MyLog.e("following visible");
                        }

                        @Override
                        public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                            holder.follow.setVisibility(View.VISIBLE);
                            holder.following.setVisibility(View.GONE);
                            Toast.makeText(context, R.string.failed_to_follow, Toast.LENGTH_LONG).show();
                            MyLog.e(t.getMessage() + "failure to follow");
                        }
                    });
                }
            });

            holder.following.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    clicked = false;
                    holder.follow.setVisibility(View.VISIBLE);
                    holder.following.setVisibility(View.GONE);
                    updateFollow(feedResponse.getEntityId(), 0);
                    Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).follow(
                            App.getInstance().getSession().getUserid(),
                            feedResponse.getEntityId(),
                            "unfollow"
                    );
                    call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                        @Override
                        public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, retrofit2.Response<com.procrea8.sexyafrikan.response.Response> response) {
                            MyLog.e("Unfollow");
                        }

                        @Override
                        public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                            holder.follow.setVisibility(View.GONE);
                            holder.following.setVisibility(View.VISIBLE);
                            Toast.makeText(context, R.string.failed_to_unfollow, Toast.LENGTH_LONG).show();
                            MyLog.e(t.getMessage() + "unfollow failure");
                        }
                    });
                }
            });

            holder.sendMoney.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View convertView = layoutInflater.inflate(R.layout.send_money, null);

                    amountOfMoney = (EditText) convertView.findViewById(R.id.amountOfMoney);
                    sendMoney = (Button) convertView.findViewById(R.id.sendMoney);
                    sendMoney.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String amount = amountOfMoney.getText().toString();
                            if (!amount.equals("")) {
                                MyLog.e("amount " + amount);
                                Intent intent = new Intent(context, MoneyActivity.class);
                                intent.putExtra("amount", amount);
                                intent.putExtra("user_id", feedResponse.getEntityId());
                                context.startActivity(intent);
                            }
                        }
                    });

                    builder.setView(convertView);
                    dialog = builder.create();
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                }
            });

            holder.vote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final ProgressDialog progressDialog;
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Sending");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                        builder.setTitle("Send Credit");

                    LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View convertView = layoutInflater.inflate(R.layout.send_credit, null);

                    amountOfCredit = (EditText) convertView.findViewById(R.id.amountOfCredit);
                    sendCredit = (Button) convertView.findViewById(R.id.sendCredit);

                    sendCredit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            progressDialog.show();
                            Call<FeedAddResponse> call = Api.getRetrofit().create(FeedApiInterface.class).sendcredit(
                                    feedResponse.getEntityId(),
                                    amountOfCredit.getText().toString()
                            );

                            call.enqueue(new Callback<FeedAddResponse>() {
                                @Override
                                public void onResponse(Call<FeedAddResponse> call, retrofit2.Response<FeedAddResponse> response) {
                                    dialog.dismiss();
                                    progressDialog.dismiss();
                                    if (response.body() != null) {
                                        if (response.body().getStatus() == 1) {
                                            Toast.makeText(context, amountOfCredit.getText().toString() + " credits, was successfully sent to " + feedResponse.getAuthorName(), Toast.LENGTH_LONG).show();
                                            MyLog.e(response.body().message);
                                        } else {
                                            Toast.makeText(context, "Insufficient Votes", Toast.LENGTH_LONG).show();
                                        }
                                    } else {
                                        Toast.makeText(context, "Insufficient Votes", Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<FeedAddResponse> call, Throwable t) {
                                    dialog.dismiss();
                                    progressDialog.dismiss();
                                    Toast.makeText(context, "Insufficient Votes", Toast.LENGTH_LONG).show();
                                    MyLog.e("Failure sending credits - " + t.getMessage());
                                }
                            });
                        }
                    });

                    builder.setView(convertView);
                    dialog = builder.create();
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                }
            });

            holder.buyVote.setVisibility(View.VISIBLE);
            holder.buyVote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, PurchaseActivity.class);
                    context.startActivity(intent);
                }
            });
            processFeedPhotos(holder, feedResponse);
            if (images != null && images.size() > 0) {
                /**holder.photosPager.setVisibility(View.VISIBLE);
                 PhotoPagerAdapter photoPagerAdapter;
                 photoPagerAdapter = new PhotoPagerAdapter(context, images);
                 holder.photosPager.setAdapter(photoPagerAdapter);
                 /**if (images.size() > 1) {
                 Integer c = images.size();
                 String t = "+" + c.toString();
                 holder.photosCount.setText(t);
                 holder.photosCount.setVisibility(View.VISIBLE);
                 }**/
                holder.photosPager.setVisibility(View.GONE);
            } else {
                holder.photosPager.setVisibility(View.GONE);
                //holder.photosCount.setVisibility(View.GONE);
            }
            holder.reactIcons.setVisibility(View.GONE);

            if (!App.getInstance().getSession().isDislikeEnabled()) {
                holder.dislikeButton.setVisibility(View.GONE);
                holder.dislikeCount.setVisibility(View.GONE);
            }

            if (App.getInstance().getSession().getLikeType().equals("reaction")) {
                //holder.reactIcons.setVisibility(View.VISIBLE);
                holder.feedReacts.setVisibility(View.VISIBLE);
                holder.dislikeButton.setVisibility(View.GONE);
                holder.dislikeCount.setVisibility(View.GONE);
                holder.likeCount.setVisibility(View.GONE);

                //load reactions
                show_react_members(holder, i);
                if (feedResponse.isHasReact()) {
                    holder.likeButtonIcon.setImageResource(R.drawable.ic_thumb_up_done);
                } else {
                    holder.likeButtonIcon.setImageResource(R.drawable.ic_thumb_up);
                }
            } else {
                holder.feedReacts.setVisibility(View.GONE);
                holder.likeCount.setText(feedResponse.getLikeCount().toString());
                holder.dislikeCount.setText(feedResponse.getDislikeCount().toString());
                if (feedResponse.isHasLike()) {
                    holder.likeButtonIcon.setImageResource(R.drawable.ic_thumb_up_done);
                } else {
                    holder.likeButtonIcon.setImageResource(R.drawable.ic_thumb_up);
                }

                if (feedResponse.isHasDislike()) {
                    holder.dislikeButtonIcon.setImageResource(R.drawable.ic_thumb_down_done);
                } else {
                    holder.dislikeButtonIcon.setImageResource(R.drawable.ic_thumb_down);
                }
            }

            holder.feedAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showFeedMenu(holder, i);
                }
            });

            holder.likeButton.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (App.getInstance().getSession().getLikeType().equals("reaction")) {
                        holder.reactIcons.setVisibility(View.VISIBLE);
                    }
                    return true;
                }
            });
            holder.likeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //if (v.getResources())
                    if (App.getInstance().getSession().getLikeType().equals("reaction")) {
                        //holder.reactIcons.setVisibility(View.VISIBLE);
                        if (!feedResponse.isHasReact()) {
                            processReact(holder, "1", i);
                            holder.likeButtonIcon.setImageResource(R.drawable.ic_thumb_up_done);
                        } else {
                            processReact(holder, "0", i); //remove reaction
                            holder.likeButtonIcon.setImageResource(R.drawable.ic_thumb_up);
                        }
                    } else {
                        holder.likeButtonIcon.setImageResource(R.drawable.ic_thumb_up_done);
                        String userid = App.getInstance().getSession().getUserid();
                        String id = feedLists.get(i).getFeedId();
                        Call<LikeResponse> call = Api.getRetrofit().create(LikeApiInterface.class).likeItem(userid, "feed", id);
                        call.enqueue(new Callback<LikeResponse>() {
                            @Override
                            public void onResponse(Call<LikeResponse> call, retrofit2.Response<LikeResponse> response) {
                                processLike("like", holder, i, response.body());
                            }

                            @Override
                            public void onFailure(Call<LikeResponse> call, Throwable t) {
                                Toast.makeText(context, "Failed to complete that" + t.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
            });

            holder.dislikeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.dislikeButtonIcon.setImageResource(R.drawable.ic_thumb_down_done);
                    String userid = App.getInstance().getSession().getUserid();
                    String id = feedLists.get(i).getFeedId();
                    Call<LikeResponse> call = Api.getRetrofit().create(LikeApiInterface.class).dislikeItem(userid, "feed", id);
                    call.enqueue(new Callback<LikeResponse>() {
                        @Override
                        public void onResponse(Call<LikeResponse> call, retrofit2.Response<LikeResponse> response) {
                            processLike("dislike", holder, i, response.body());
                        }

                        @Override
                        public void onFailure(Call<LikeResponse> call, Throwable t) {
                            Toast.makeText(context, "Failed to complete that" + t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });

            //always hide the react icons open click on of the view container
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.reactIcons.setVisibility(View.GONE);
                }
            });

            holder.reactLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    processReact(holder, "1", position);
                }
            });
            holder.reactLove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    processReact(holder, "4", position);
                }
            });
            holder.reactWow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    processReact(holder, "7", position);
                }
            });
            holder.reactHaha.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    processReact(holder, "5", position);
                }
            });
            holder.react_sad.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    processReact(holder, "8", position);
                }
            });
            holder.reactAngry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    processReact(holder, "9", position);
                }
            });
            holder.reactYay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    processReact(holder, "6", position);
                }
            });
            holder.reactorsMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    showReactorsDialog(position);
                }
            });

            holder.replyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showComments(holder, i);
                }
            });

            if (feedResponse.getComments() > 0) {
                holder.commentCount.setText(feedResponse.getComments().toString());
                holder.commentCount.setVisibility(View.VISIBLE);
            } else {
                holder.commentCount.setText("");
                holder.commentCount.setVisibility(View.VISIBLE);
            }


            setFeedTitle(holder, i);
            holder.feedTime.setText(TimeAgo.format(context, feedResponse.getTime()));
            if (!feedResponse.getLocation().isEmpty()) {
                holder.locationImage.setVisibility(View.VISIBLE);
                String googleKey = (!GOOGLE_API_KEY.isEmpty()) ? "&key=" + GOOGLE_API_KEY : "";
                String locationUrl = "https://maps.googleapis.com/maps/api/staticmap?center=" + feedResponse.getLocation() + "&zoom=15&size=700x200&maptype=roadmap&markers=color:red%7C"
                        + feedResponse.getLocation() + "&sensor=false&scale=1&visual_refresh=true" + googleKey;
                Glide.with(context).load(locationUrl).crossFade().into(holder.locationImage);
            } else {
                holder.locationImage.setVisibility(View.GONE);
            }

            //show link
            if (feedResponse.getLink().size() > 0) {
                final LinkModel linkModel = feedResponse.getLink().get(0);
                holder.linkContainer.setVisibility(View.VISIBLE);
                holder.linkTitle.setText(linkModel.getTitle());
                holder.linkTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(linkModel.getLink()));
                        context.startActivity(browserIntent);
                    }
                });
                holder.linkDescription.setText(linkModel.getDescription());
                holder.linkProvider.setText(linkModel.getProviderUrl());
                if (linkModel.getType().equals("link")) {
                    if (linkModel.getImage() != null) {
                        holder.linkCode.setVisibility(View.GONE);
                        holder.linkImage.setVisibility(View.VISIBLE);
                        Glide.with(context).load(linkModel.getImage()).crossFade().into(holder.linkImage);
                    } else {
                        holder.linkCode.setVisibility(View.GONE);
                        holder.linkImage.setVisibility(View.GONE);
                    }

                } else {
                    if (linkModel.getCode() != null) {
                        holder.linkCode.setVisibility(View.VISIBLE);
                        holder.linkImage.setVisibility(View.GONE);
                        holder.linkCode.setWebChromeClient(new WebChromeClient());
                        WebSettings wS = holder.linkCode.getSettings();
                        wS.setJavaScriptEnabled(true);
                        wS.setBuiltInZoomControls(true);

                        holder.linkCode.getSettings().setDefaultTextEncodingName("utf-8");
                        holder.linkCode.loadData(linkModel.getCode(), "text/html; charset=utf-8", "utf-8");
                    } else {
                        holder.linkCode.setVisibility(View.GONE);
                        holder.linkImage.setVisibility(View.GONE);
                    }
                    ;
                }
            } else {
                holder.linkContainer.setVisibility(View.GONE);
            }

            if (feedResponse.getHasFeeling()) {
                int feelingTypeId = getFeelingText(feedResponse.getFeelingType());
                if (feelingTypeId != 0) {
                    holder.feelingContainer.setVisibility(View.VISIBLE);
                    Glide.with(context).load(feedResponse.getFeelingImage()).into(holder.feelingImage);

                    holder.feelingType.setText(feelingTypeId);
                    holder.feelingValue.setText(feedResponse.getFeelingText());
                }
            } else {
                holder.feelingContainer.setVisibility(View.GONE);
            }

            if (!feedResponse.getVideoEmbed().isEmpty()) {
                holder.videoContainer.setVisibility(View.VISIBLE);
                holder.videoPlayer.loadData(feedResponse.getVideoEmbed(), "text/html", null);
                holder.videoPlayer.setWebChromeClient(new WebChromeClient());
                WebSettings wS2 = holder.videoPlayer.getSettings();
                wS2.setJavaScriptEnabled(true);
                wS2.setBuiltInZoomControls(true);
                if (feedResponse.getVideoTitle().isEmpty()) {
                    holder.videoTitle.setVisibility(View.GONE);
                } else {
                    holder.videoTitle.setText(feedResponse.getVideoTitle());
                }

            } else {
                holder.videoContainer.setVisibility(View.GONE);
            }

            if (feedResponse.getShared()) {
                holder.sharedContainer.setVisibility(View.VISIBLE);
                Glide.with(context).load(feedResponse.getSharedAvatar()).into(holder.sharedImage);
                holder.sharedTitle.setText(feedResponse.getSharedName());
                holder.sharedTime.setText(TimeAgo.format(context, feedResponse.getSharedTime()));
            } else {
                holder.sharedContainer.setVisibility(View.GONE);
            }

            if (feedResponse.getFiles().size() > 0) {
                holder.filesListView.setVisibility(View.VISIBLE);
                //MyLog.e("Size " + feedResponse.getFiles().size());
                FilesAdapter filesAdapter = new FilesAdapter(context, feedResponse.getFiles());
                holder.filesListView.setAdapter(filesAdapter);
                reCalculateListHeight(holder.filesListView);
            } else {
                holder.filesListView.setVisibility(View.GONE);
            }

            if (feedResponse.getPollType() == 0) {
                holder.pollAnswers.setVisibility(View.GONE);
            } else {
                holder.pollAnswers.setVisibility(View.VISIBLE);
                PollsAnswerAdapter pollsAnswerAdapter = new PollsAnswerAdapter(context, feedResponse.getAnswers(), feedResponse.getPollType());
                holder.pollAnswers.setAdapter(pollsAnswerAdapter);
                reCalculateListHeight(holder.pollAnswers);
            }

        } else

        {
            holder.feedItemContainer.setVisibility(View.GONE);
            holder.feedEditor.setVisibility(View.VISIBLE);
            holder.adContainer.setVisibility(View.GONE);
            Glide.with(context).load(App.getInstance().getSession().getAvatar()).crossFade().into(holder.editorAvatar);
            holder.edtitorText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    launchEditor("");
                }
            });

            holder.editorStatusBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    launchEditor("");
                }
            });
            holder.editorLocationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    launchEditor("location");
                }
            });
            holder.editorPhotoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    launchEditor("photo");
                }
            });
        }

        if (!(loggedinId.equals(feedResponse.getEntityId()))) {
            holder.vote.setVisibility(View.VISIBLE);
            holder.sendMoney.setVisibility(View.VISIBLE);
            holder.followContainer.setVisibility(View.VISIBLE);
            holder.following.setVisibility(View.GONE);
            holder.follow.setVisibility(View.GONE);
            if (feedResponse.getFollowing() == 1) {
                holder.following.setVisibility(View.VISIBLE);
            } else {
                holder.follow.setVisibility(View.VISIBLE);
            }
            MyLog.e(feedResponse.getFollowing().toString() + " -getfollowing");
        } else {
            holder.followContainer.setVisibility(View.GONE);
            holder.vote.setVisibility(View.GONE);
            holder.sendMoney.setVisibility(View.GONE);
        }
        return view;
    }

    public void updateFollow(String entityId, int val){
        for (int i=0;i<feedLists.size();i++){
            MyLog.e("i is less than feedLists");
            FeedResponse f = feedLists.get(i);
            if (entityId.equals(f.getEntityId())) {
                f.setFollowing(val);
                notifyDataSetChanged();
            }
        }

    }

//        public void checkFollowing(final ViewHolder holder, int position) {
//        final FeedResponse feedResponse = feedLists.get(position);
//        Call<Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).checkFollow(feedResponse.getEntityId());
//        call.enqueue(new Callback<Response>() {
//            @Override
//            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
//                if (!(App.getInstance().getSession().getUserid().equals(feedResponse.getEntityId()))) {
//                if (response.body().getStatus() == 1) {
//                    holder.following.setVisibility(View.VISIBLE);
//                    holder.follow.setVisibility(View.GONE);
//                } else {
//                    holder.following.setVisibility(View.GONE);
//                    holder.follow.setVisibility(View.VISIBLE);
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Response> call, Throwable t) {
//                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
//                MyLog.e("Check network" + t.getMessage());
//            }
//        });
//    }

    public void launchEditor(String launch) {
        EditorDialog dialog = new EditorDialog(context, type, typeId, entityType, entityId, launch);
        if (toUserid != null && !toUserid.isEmpty()) dialog.setToUserid(toUserid);
        dialog.setFeedAddedListener(new EditorDialog.FeedAddedListener() {
            @Override
            public void added(FeedResponse response) {
                if (feedLists.size() > 0) {
                    feedLists.add(1, response);
                    MyLog.e(response.getMessage() + " feedLists.size() > 0" + " -feedadapter launch editor");

                } else {
                    feedLists.add(response);
                    MyLog.e(response.getMessage() + " feedLists.size() < 0" + " -feedadapter launch editor");

                }
                notifyDataSetChanged();
            }
        });
        dialog.show();
    }

    public void setFeedTitle(ViewHolder holder, int position) {
        final FeedResponse feedResponse = feedLists.get(position);
        //holder.authorName.setText();
        String authorName = feedResponse.getAuthorName();
        SpannableStringBuilder str = new SpannableStringBuilder(authorName);
        CharSequence mStr = "";
        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, authorName.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if (feedResponse.getEntityType().equals("user")) {
                    ProfileModule.loadProfile(context, feedResponse.getEntityId());
                }
            }

            @Override
            public void updateDrawState(TextPaint tP) {
                tP.setUnderlineText(false);
            }
        };
        str.setSpan(clickableSpan, 0, authorName.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        Resources resources = context.getResources();
        mStr = TextUtils.concat("", "", str);

        if (!feedResponse.getFeedTitle().isEmpty()) {
            CharSequence s = resources.getString(R.string.change_profile_picture);
            if (feedResponse.getFeedTitle().equals("changed-profile-cover"))
                s = resources.getString(R.string.change_profile_cover);
            if (feedResponse.getFeedTitle().equals("shared-a-video"))
                s = resources.getString(R.string.shared_a_video);
            if (feedResponse.getFeedTitle().equals("add-photo-to-album")) {
                s = resources.getString(R.string.added) + " ";
                final ArrayList<String> album = feedResponse.getAlbum();
                s = s + album.get(0) + " " + resources.getString(R.string.photo_s_to) + " ";
                SpannableStringBuilder albumTitle = new SpannableStringBuilder(album.get(2));
                albumTitle.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, album.get(2).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                ClickableSpan cS = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        String albumId = album.get(1);
                        //take to the album page
                    }

                    @Override
                    public void updateDrawState(TextPaint t) {
                        t.setUnderlineText(false);
                    }
                };
                albumTitle.setSpan(clickableSpan, 0, album.get(2).length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                s = TextUtils.concat(s, "", albumTitle);
            }
            //SpannableStringBuilder nStr = new SpannableStringBuilder(s);
            mStr = TextUtils.concat(mStr, " ", s);

        }

        if (feedResponse.getShared()) {
            String s2 = " " + resources.getString(R.string.shared) + " ";
            mStr = TextUtils.concat(mStr, "", s2);
            SpannableStringBuilder s2S = new SpannableStringBuilder(feedResponse.getSharedName());
            s2S.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, feedResponse.getSharedName().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            ClickableSpan cS = new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    //String albumId = album.get(1);
                    //take to the album page
                    MyLog.e("photo user");
                }

                @Override
                public void updateDrawState(TextPaint t) {
                    t.setUnderlineText(false);
                }
            };
            s2S.setSpan(cS, 0, feedResponse.getSharedName().length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            s2S.append("'s ");


            mStr = TextUtils.concat(mStr, " ", s2S);
            switch (feedResponse.getSharedTitle()) {
                case "post":
                    mStr = TextUtils.concat(mStr, "", resources.getString(R.string.post_t));
                    break;
                case "photos":
                    mStr = TextUtils.concat(mStr, "", resources.getString(R.string.photo_t));
                    break;
                case "video":
                    mStr = TextUtils.concat(mStr, "", resources.getString(R.string.video));
                    break;
                case "file":
                    mStr = TextUtils.concat(mStr, "", resources.getString(R.string.file));

                    break;
            }

        }

        holder.authorName.setText(mStr);
        holder.authorName.setMovementMethod(LinkMovementMethod.getInstance());
        holder.authorName.setHighlightColor(Color.TRANSPARENT);
    }

    public void showComments(final ViewHolder holder, int position) {
        final FeedResponse response = feedLists.get(position);
        CommentDialog dialog = new CommentDialog(context, false);
        dialog.setType("feed");
        dialog.setTypeId(response.getFeedId());
        dialog.setEntityType("user");
        dialog.setEntityTypeId(App.getInstance().getSession().getUserid());
        dialog.setOnCommentsChangeListener(new CommentDialog.CommentsChangedListener() {
            @Override
            public void onChange(Integer count) {
                response.setComments(count);
                holder.commentCount.setText(count.toString());
                holder.commentCount.setVisibility(View.VISIBLE);
                notifyDataSetChanged();
            }
        });

        dialog.show();
    }

    public void processLike(String type, ViewHolder holder, int position, LikeResponse response) {
        FeedResponse feedResponse = feedLists.get(position);
        feedResponse.setLikeCount(response.getLikes());
        feedResponse.setDislikeCount(response.getDislikes());
        feedResponse.setHasLike(response.getHas_like());
        feedResponse.setHasDislike(response.getHas_dislike());
        notifyDataSetChanged();
    }

    public void processReact(final ViewHolder holder, String type, final Integer position) {
        holder.likeButtonIcon.setImageResource(R.drawable.ic_thumb_up_done);
        holder.reactIcons.setVisibility(View.GONE);

        String userid = App.getInstance().getSession().getUserid();
        String typeId = feedLists.get(position).getFeedId();
        Call<Response> call = Api.getRetrofit().create(LikeApiInterface.class).reactItem(userid, "feed", typeId, type);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                //we can load the react stats now
                load_reactions(holder, position);
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Toast.makeText(context, "Failed to react to feed, check your network connection", Toast.LENGTH_LONG).show();
                holder.likeButtonIcon.setImageResource(R.drawable.ic_thumb_up);
            }
        });
    }

    public void show_react_members(ViewHolder holder, int position) {
        FeedResponse response = feedLists.get(position);
        final ArrayList<ArrayList<String>> members = response.getReactMembers();
        if (members.size() > 0) {
            holder.reactorsUser1.setVisibility(View.VISIBLE);
            holder.reactorsMore.setVisibility(View.VISIBLE);
            Glide.with(context).load(members.get(0).get(0)).crossFade().into(holder.reactorsUser1);
            holder.reactorsUser1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProfileActivity.load(context, members.get(0).get(3));
                }
            });
            if (members.size() > 1) {
                holder.reactorsUser2.setVisibility(View.VISIBLE);
                Glide.with(context).load(members.get(1).get(0)).crossFade().into(holder.reactorsUser2);
                holder.reactorsUser2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ProfileActivity.load(context, members.get(1).get(3));
                    }
                });
            } else {
                holder.reactorsUser2.setVisibility(View.GONE);
            }

            if (members.size() > 2) {
                holder.reactorsUser3.setVisibility(View.VISIBLE);
                Glide.with(context).load(members.get(2).get(0)).crossFade().into(holder.reactorsUser3);
                holder.reactorsUser3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ProfileActivity.load(context, members.get(2).get(3));
                    }
                });
            } else {
                holder.reactorsUser3.setVisibility(View.GONE);
            }

            if (members.size() > 3) {
                holder.reactorsUser4.setVisibility(View.VISIBLE);
                Glide.with(context).load(members.get(3).get(0)).crossFade().into(holder.reactorsUser4);
                holder.reactorsUser4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ProfileActivity.load(context, members.get(3).get(3));
                    }
                });
            } else {
                holder.reactorsUser4.setVisibility(View.GONE);
            }

        } else {
            //we need to hide the members
            holder.reactorsUser1.setVisibility(View.GONE);
            holder.reactorsUser2.setVisibility(View.GONE);
            holder.reactorsUser3.setVisibility(View.GONE);
            holder.reactorsUser4.setVisibility(View.GONE);
            holder.reactorsMore.setVisibility(View.GONE);
        }
    }

    public void load_reactions(final ViewHolder holder, final int position) {
        String typeId = feedLists.get(position).getFeedId();


        Call<ReactResponse> call = Api.getRetrofit().create(LikeApiInterface.class).getReacts("feed", typeId);
        call.enqueue(new Callback<ReactResponse>() {
            @Override
            public void onResponse(Call<ReactResponse> call, retrofit2.Response<ReactResponse> response) {
                holder.feedReacts.setVisibility(View.VISIBLE);

                ArrayList<ArrayList<String>> members = response.body().getMembers();
                feedLists.get(position).setReactMembers(members);
                show_react_members(holder, position);
            }

            @Override
            public void onFailure(Call<ReactResponse> call, Throwable t) {

            }
        });
    }

    public void showReactorsDialog(int position) {
        FeedResponse response = feedLists.get(position);
        AlertDialog.Builder alertBuider = new AlertDialog.Builder(context);
        View convertView = layoutInflater.inflate(R.layout.reactors_dialog, null);
        alertBuider.setView(convertView);
        alertBuider.setTitle("People who react to this");
        ListView reactorsListView = (ListView) convertView.findViewById(R.id.reactorLists);
        ArrayList<ArrayList<String>> members = new ArrayList<>();
        final ReactorsAdapter reactorsAdapter = new ReactorsAdapter(context, members);
        reactorsListView.setAdapter(reactorsAdapter);
        Call<ReactResponse> call = Api.getRetrofit().create(LikeApiInterface.class).getReacts("feed", response.getFeedId(), "50");
        call.enqueue(new Callback<ReactResponse>() {
            @Override
            public void onResponse(Call<ReactResponse> call, retrofit2.Response<ReactResponse> response) {
                reactorsAdapter.addAll(response.body().getMembers());
            }

            @Override
            public void onFailure(Call<ReactResponse> call, Throwable t) {

            }
        });
        alertBuider.create().show();

    }

    public void showFeedMenu(final ViewHolder holder, final int position) {
        final FeedResponse feedResponse = feedLists.get(position);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final Resources r = context.getResources();

        final ArrayList<String> items = new ArrayList<String>();
        items.add(r.getString(R.string.dont_like_this_post));

        if (feedResponse.getHasSubscribed()) {
            items.add(r.getString(R.string.turn_off_notification));
        } else {
            items.add(r.getString(R.string.turn_on_notification));
        }

        if (feedResponse.getCanPinPost()) {
            if (feedResponse.getIsPinned()) {
                items.add(r.getString(R.string.unpin_from_top));
            } else {
                items.add(r.getString(R.string.pin_to_top));
            }
        }

        if (feedResponse.getCanEditPost()) {
            items.add(r.getString(R.string.edit_post));
            items.add(r.getString(R.string.delete_post));
        }
        items.add(r.getString(R.string.view_post));

        ArrayAdapter<String> aAdapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, items);
        builder.setAdapter(aAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String s = items.get(i);
                if (s.equals(r.getString(R.string.dont_like_this_post))) {
                    doFeedAction("hide", holder, position);
                } else if (s.equals(r.getString(R.string.turn_off_notification))) {
                    doFeedAction("unsubscribe", holder, position);
                } else if (s.equals(r.getString(R.string.turn_on_notification))) {
                    doFeedAction("subscribe", holder, position);
                } else if (s.equals(r.getString(R.string.unpin_from_top))) {
                    doFeedAction("unpin", holder, position);
                } else if (s.equals(r.getString(R.string.pin_to_top))) {
                    doFeedAction("pin", holder, position);
                } else if (s.equals(r.getString(R.string.delete_post))) {
                    AlertDialog.Builder confirmALert = new AlertDialog.Builder(context);
                    confirmALert.setMessage(R.string.are_you_sure);
                    confirmALert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            doFeedAction("remove", holder, position);
                        }
                    }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).create().show();
                } else if (s.equals(r.getString(R.string.view_post))) {
                    FeedActivity.load(context, feedResponse);
                } else if (s.equals(r.getString(R.string.edit_post))) {

                    final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                    final ProgressDialog progressDialog;
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Saving");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                    final EditText input = new EditText(context);
                    dialog.setView(input);
                    input.setText(feedResponse.getFullMessage());

                    dialog.setTitle("Edit Post");
                    dialog.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            final String text = input.getText().toString();
                            if (text.isEmpty()) {
                                Toast.makeText(context, context.getResources().getString(R.string.write_something), Toast.LENGTH_LONG).show();
                                return;
                            }

                            progressDialog.show();
                            feedResponse.setFullMessage(text);
                            notifyDataSetChanged();

                            Call<FeedAddResponse> call = null;

                            call = Api.getRetrofit().create(FeedApiInterface.class).edit(
                                    App.getInstance().getSession().getUserid(), text, feedResponse.getFeedId());

                            call.enqueue(new Callback<FeedAddResponse>() {
                                @Override
                                public void onResponse(Call<FeedAddResponse> call, retrofit2.Response<FeedAddResponse> response) {
                                    if (response.body().getStatus() == 1) {
                                        feedLists.set(position, feedResponse);
                                        enableFullMessage();
                                        notifyDataSetChanged();
                                        progressDialog.dismiss();

                                    } else {
                                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                        progressDialog.dismiss();
                                    }
                                }

                                @Override
                                public void onFailure(Call<FeedAddResponse> call, Throwable t) {
                                    MyLog.e(t.getMessage());
                                    progressDialog.dismiss();
                                }
                            });
                        }

                    }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).create().show();
                }
            }
        });

        builder.create().show();
    }


    public void doFeedAction(final String action, ViewHolder holder, final int position) {
        final FeedResponse feedResponse = feedLists.get(position);
        final String userid = App.getInstance().getSession().getUserid();
        Call<Response> call = Api.getRetrofit().create(FeedApiInterface.class).action(
                userid,
                action,
                feedResponse.getFeedId()
        );
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (action.equals("hide") || action.equals("remove")) {
                    feedLists.remove(position);
                    notifyDataSetChanged();
                } else if (action.equals("subscribe")) {
                    feedLists.get(position).setHasSubscribed(true);
                } else if (action.equals("unsuscribe")) {
                    feedLists.get(position).setHasSubscribed(false);
                } else if (action.equals("pin")) {
                    feedLists.get(position).setIsPinned(true);
                } else if (action.equals("unpin")) {
                    feedLists.get(position).setIsPinned(false);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Toast.makeText(context, "Feed action failed", Toast.LENGTH_LONG).show();
            }
        });
    }

    public int getFeelingText(String type) {
        int feelingTypeText = 0;
        switch (type) {
            case "listening":
                feelingTypeText = R.string.listening_to;
                break;
            case "watching":
                feelingTypeText = R.string.watching;
                break;
            case "feeling":
                feelingTypeText = R.string.feeling;
                break;
            case "thinking":
                feelingTypeText = R.string.thinking_about;
                break;
            case "reading":
                feelingTypeText = R.string.reading;
                break;
            case "eating":
                feelingTypeText = R.string.eating;
                break;
            case "drinking":
                feelingTypeText = R.string.drinking;
                break;
            case "celebrating":
                feelingTypeText = R.string.celebrating;
                break;
            case "traveling-to":
                feelingTypeText = R.string.traveling_to;
                break;
            case "exercising-to":
                feelingTypeText = R.string.exercising_to;
                break;
            case "meeting":
                feelingTypeText = R.string.meeting;
                break;
            case "playing":
                feelingTypeText = R.string.playing;
                break;
            case "looking-for":
                feelingTypeText = R.string.looking_for;
                break;
        }
        return feelingTypeText;
    }

    public void reCalculateListHeight(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
    }

    public void shareFeed(final FeedResponse feedResponse) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(context.getResources().getString(R.string.alert_share_on_timeline));
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Call<Response> call = Api.getRetrofit().create(FeedApiInterface.class).action(
                        App.getInstance().getSession().getUserid(),
                        "share",
                        feedResponse.getFeedId()
                );
                call.enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        Toast.makeText(context, R.string.post_shared_on_timeline, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.create().show();
    }

    public void processFeedPhotos(ViewHolder holder, FeedResponse feedResponse) {
        holder.singleImageContainer.setVisibility(View.GONE);
        holder.twoImageContainer.setVisibility(View.GONE);
        holder.threeImageContainer.setVisibility(View.GONE);
        holder.fourImageContainer.setVisibility(View.GONE);
        holder.imageMoreContainer.setVisibility(View.GONE);

        final ArrayList<String> images = feedResponse.getImages();
        Integer s = images.size();
        if (s > 0) {
            if (s.equals(1)) {
                //single image
                holder.singleImageContainer.setVisibility(View.VISIBLE);
                Glide.with(context).load(images.get(0)).crossFade().into(holder.singleImage);
                holder.singleImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PhotoViewerActivity.launch(context, images.get(0));
                    }
                });
            } else if (s.equals(2)) {
                //two images
                holder.twoImageContainer.setVisibility(View.VISIBLE);
                Glide.with(context).load(images.get(0)).crossFade().into(holder.twoImageLeft);
                holder.twoImageLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PhotoViewerActivity.launch(context, images.get(0));
                    }
                });
                Glide.with(context).load(images.get(1)).crossFade().into(holder.twoImageRight);
                holder.twoImageRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PhotoViewerActivity.launch(context, images.get(1));
                    }
                });
            } else if (s.equals(3)) {
                //three images
                holder.threeImageContainer.setVisibility(View.VISIBLE);
                Glide.with(context).load(images.get(0)).crossFade().into(holder.threeImageLeft);
                holder.threeImageLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PhotoViewerActivity.launch(context, images.get(0));
                    }
                });
                Glide.with(context).load(images.get(1)).crossFade().into(holder.threeImageRight1);
                holder.threeImageRight1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PhotoViewerActivity.launch(context, images.get(1));
                    }
                });
                Glide.with(context).load(images.get(2)).crossFade().into(holder.threeImageRight2);
                holder.threeImageRight2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PhotoViewerActivity.launch(context, images.get(2));
                    }
                });
            } else {
                //images is 4 and up
                holder.fourImageContainer.setVisibility(View.VISIBLE);
                Glide.with(context).load(images.get(0)).crossFade().into(holder.fourImageLeft);
                holder.fourImageLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PhotoViewerActivity.launch(context, images.get(0));
                    }
                });
                Glide.with(context).load(images.get(1)).crossFade().into(holder.fourImageRight1);
                holder.fourImageRight1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PhotoViewerActivity.launch(context, images.get(1));
                    }
                });
                Glide.with(context).load(images.get(2)).crossFade().into(holder.fourImageRight2);
                holder.fourImageRight2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PhotoViewerActivity.launch(context, images.get(2));
                    }
                });
                Glide.with(context).load(images.get(3)).crossFade().into(holder.fourImageRight3);
                holder.fourImageRight3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PhotoViewerActivity.launch(context, images.get(3));
                    }
                });

                if (s > 4) {
                    holder.imageMoreContainer.setVisibility(View.VISIBLE);
                    Integer d = s - 4;
                    String v = "+" + String.valueOf(d);
                    holder.imageMoreText.setText(v);
                }
            }
        }

    }

    @JavascriptInterface
    public void resize(final float height) {
        final Activity activity = (Activity) context;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.setLayoutParams(new LinearLayout.LayoutParams(activity.getResources().getDisplayMetrics().widthPixels, (int) (height * activity.getResources().getDisplayMetrics().density)));
            }
        });
    }


    static class ViewHolder {
        TextView authorName;
        TextView feedTime;
        ImageView authorAvatar;
        ImageView feedAction;
        WebView message;
        LinearLayout likeButton;
        ViewPager photosPager;
        LinearLayout follow, following, buyVote, vote, sendMoney, followContainer;
        //        WebView feedContent;
        LinearLayout replyButton;
        LinearLayout dislikeButton;
        ImageView likeButtonIcon;
        ImageView dislikeButtonIcon;
        ImageView reactLike;
        ImageView reactLove;
        ImageView reactHaha;
        ImageView react_sad;
        ImageView reactWow;
        ImageView reactYay;
        ImageView reactAngry;
        LinearLayout reactIcons;

        TextView commentCount;
        TextView likeCount;
        TextView dislikeCount;
        LinearLayout shareButton;
        LinearLayout feedReacts;
        LinearLayout feedEditor;
        LinearLayout feedItemContainer;
        CircleImageView reactorsUser1;
        CircleImageView reactorsUser2;
        CircleImageView reactorsUser3;
        CircleImageView reactorsUser4;
        CircleImageView reactorsMore;

        ImageView editorAvatar;
        TextView edtitorText;

        ImageView locationImage;

        ImageView ext_share_btn;

        LinearLayout linkContainer;
        ImageView linkImage;
        WebView linkCode;
        TextView linkTitle;
        TextView linkDescription;
        TextView linkProvider;

        LinearLayout feelingContainer;
        ImageView feelingImage;
        TextView feelingType;
        TextView feelingValue;

        LinearLayout videoContainer;
        WebView videoPlayer;
        TextView videoTitle;

        LinearLayout sharedContainer;
        ImageView sharedImage;
        TextView sharedTitle;
        TextView sharedTime;

        ListView filesListView;
        ListView pollAnswers;

        //music
        RelativeLayout musicContainer;
        TextView musicTitle;
        TextView musicPlayCount;

        //ad
        LinearLayout adContainer;
        ImageView adImage;
        TextView adTitle;
        TextView adDesc;

        TextView photosCount;


        LinearLayout editorStatusBtn;
        LinearLayout editorPhotoBtn;
        LinearLayout editorLocationBtn;


        //photo tags
        LinearLayout singleImageContainer, twoImageContainer, threeImageContainer, fourImageContainer, imageMoreContainer;
        ImageView singleImage, twoImageLeft, twoImageRight, threeImageLeft, threeImageRight1, threeImageRight2, fourImageLeft, fourImageRight1, fourImageRight2, fourImageRight3;
        TextView imageMoreText;
    }
}
