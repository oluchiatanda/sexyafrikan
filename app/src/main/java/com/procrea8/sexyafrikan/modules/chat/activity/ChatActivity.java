package com.procrea8.sexyafrikan.modules.chat.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.chat.ChatApiInterface;
import com.procrea8.sexyafrikan.modules.chat.adapter.MessagesAdapter;
import com.procrea8.sexyafrikan.modules.chat.model.Message;
import com.procrea8.sexyafrikan.modules.mediachat.activity.CallActivity;
import com.procrea8.sexyafrikan.response.FCMResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends ActivityBase {

    String chatTitle = "";
    String cid  = "";
    String userid = "";

    ListView listView;
    ImageView selectPhoto;
    ImageView sendButton,audioCallButton,videoCallButton;
    TextView messageText,vChatTitle;

    String photo = "";

    MessagesAdapter adapter;
    ActivityBase activity;
    Context context;
    ArrayList<Message> messages = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        activity = this;
        Intent intent = getIntent();
        if (intent.hasExtra("title")) chatTitle = intent.getStringExtra("title");
        if (intent.hasExtra("cid")) cid = intent.getStringExtra("cid");
        if (intent.hasExtra("userid")) userid = intent.getStringExtra("userid");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        vChatTitle = (TextView) findViewById(R.id.chat_title);
        //getSupportActionBar().setTitle(chatTitle);
        vChatTitle.setText(chatTitle);

        listView = (ListView) findViewById(R.id.message_list);
        adapter = new MessagesAdapter(this, messages);
        listView.setAdapter(adapter);
        sendButton = (ImageView) findViewById(R.id.chat_send);
        selectPhoto = (ImageView) findViewById(R.id.select_photo);
        messageText = (TextView) findViewById(R.id.message_text);
        audioCallButton = (ImageView) findViewById(R.id.audio_call_button);
        videoCallButton = (ImageView) findViewById(R.id.video_call_button);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });

        selectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPermissionGranted(null);
                MultiImageSelector.create()
                        .showCamera(true)
                        .count(1)
                        .single()
                        .start(ChatActivity.this, 150);
            }
        });

        if (ENABLE_VIDEO_CALL) {
            Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(ChatApiInterface.class).getIdentity(cid, App.getInstance().getSession().getUserid(), userid);
            call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                @Override
                public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, Response<com.procrea8.sexyafrikan.response.Response> response) {
                    if (response.body().getStatus()==1) {
                        final String theUserid = response.body().getDataOne();
                        final String theName = response.body().getDataTwo();
                        final String theAvatar = response.body().getDataThree();
                        MyLog.e("call -id" + theUserid);
                        videoCallButton.setVisibility(View.VISIBLE);
                        audioCallButton.setVisibility(View.VISIBLE);
                        audioCallButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                CallActivity.initCall(activity, 2, theUserid, theName, theAvatar, "call", "");
                            }
                        });

                        videoCallButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                CallActivity.initCall(activity, 1, theUserid, theName, theAvatar, "call", "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {

                }
            });
        }

        loadMessages();
    }

    public void sendMessage() {
        String text = messageText.getText().toString();
        if (text.isEmpty() && photo.isEmpty()) return;
        setLoading(getResources().getString(R.string.sending));
        Call<Message> call;
        String loggedIn = App.getInstance().getSession().getUserid();
        if (photo.isEmpty()) {
            call = Api.getRetrofit().create(ChatApiInterface.class).sendMessage(loggedIn, cid, userid, text);
        } else {
            MultipartBody.Part body = null;
            File file = new File(photo);
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            body = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            call = Api.getRetrofit().create(ChatApiInterface.class).sendMessage(loggedIn, cid, userid, text, body);
        }
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                Message message = response.body();
                hideLoading();
                if (message.getStatus().equals("1")) {
                    messages.add(message);
                    adapter.notifyDataSetChanged();
                    scrollDown();
                    cid = message.getCid();
                    messageText.setText("");
                    photo = "";
                } else {
                    Toast.makeText(getApplicationContext(), R.string.failed_to_send_message, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                Toast.makeText(getApplicationContext(), R.string.failed_to_send_message, Toast.LENGTH_LONG).show();
                MyLog.e("Chat failed - " + t.getMessage());
                hideLoading();
            }
        });
    }

    public void loadMessages() {
        Call<ArrayList<Message>> call = Api.getRetrofit().create(ChatApiInterface.class).getMessages(
                App.getInstance().getSession().getUserid(),
                cid, userid
        );
        call.enqueue(new Callback<ArrayList<Message>>() {
            @Override
            public void onResponse(Call<ArrayList<Message>> call, Response<ArrayList<Message>> response) {
                messages.clear();
                messages.addAll(response.body());
                adapter.notifyDataSetChanged();
                scrollDown();
            }

            @Override
            public void onFailure(Call<ArrayList<Message>> call, Throwable t) {

            }
        });
    }

    public void scrollDown() {
        if (messages.size() > 0) {
            //listView.scrollTo(0, listView.getHeight());
        }
    }

    public static void loadChat(Context c, String title, String cid, String userid) {
        Intent intent = new Intent(c, ChatActivity.class);
        intent.putExtra("title", title);
        intent.putExtra("cid", cid);
        intent.putExtra("userid", userid);
        c.startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 150 && resultCode == RESULT_OK) {
            List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
            final String photoPath = path.get(0);
            setPermissionGranted(new OnPermissionGranted() {
                @Override
                public void onGranted() {
                    photo = photoPath;
                }
            });

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFCMResponseEvent(FCMResponse event) {
        if (event.getType().equals("new-event")) {
            //lets check for new events
            loadMessages();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

}
