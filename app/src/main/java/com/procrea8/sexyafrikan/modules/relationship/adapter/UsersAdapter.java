package com.procrea8.sexyafrikan.modules.relationship.adapter;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;
import com.procrea8.sexyafrikan.modules.relationship.RelationshipApiInterface;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class UsersAdapter extends BaseAdapter{
    ArrayList<User> users;
    Context context;
    LayoutInflater layoutInflater;

    public UsersAdapter(Context c, ArrayList<User> users) {
        this.context = c;
        this.users = users;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final MyViewHolder holder;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.user_item, null);
            holder = new MyViewHolder();
            holder.avatar = (ImageView) view.findViewById(R.id.avatar);
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.responseContainer = (LinearLayout) view.findViewById(R.id.response_container);
            holder.acceptButton = (TextView) view.findViewById(R.id.accept_button);
            holder.rejectButton = (TextView) view.findViewById(R.id.reject_button);
            holder.addButton = (ImageButton) view.findViewById(R.id.add_friend);
            holder.removeButton = (ImageButton) view.findViewById(R.id.remove_friend);
            holder.requestSentButton = (ImageButton) view.findViewById(R.id.request_sent);
            view.setTag(holder);
        } else {
            holder = (MyViewHolder) view.getTag();
        }

        final User user = users.get(i);
        //if (user.getAvatar() == null) return null;
        //holder.name.setText(user.getName());
        Glide.with(context).load(user.getAvatar()).crossFade().into(holder.avatar);
        holder.name.setText(user.getName());
        holder.responseContainer.setVisibility(View.GONE);
        holder.addButton.setVisibility(View.GONE);
        holder.removeButton.setVisibility(View.GONE);
        holder.requestSentButton.setVisibility(View.GONE);
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfileActivity.load(context, user.getId());
            }
        });
        holder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfileActivity.load(context, user.getId());
            }
        });
        String status = user.getFriendStatus();
        switch (status) {
            case "0":
                holder.addButton.setVisibility(View.VISIBLE);
                break;
            case "1":
                holder.requestSentButton.setVisibility(View.VISIBLE);
                break;
            case "2":
                holder.removeButton.setVisibility(View.VISIBLE);
                break;
            case "3":
                holder.responseContainer.setVisibility(View.VISIBLE);
                break;
        }

        final String userid = App.getInstance().getSession().getUserid();
        holder.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.addButton.setVisibility(View.GONE);
                holder.requestSentButton.setVisibility(View.VISIBLE);
                user.setFriendStatus("1");
                MyLog.e("Adding user - " + user.getId());
                Call<Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).add(
                        userid,
                        user.getId()
                );
                call.enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        Toast.makeText(context, R.string.friend_requests_sent, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        holder.addButton.setVisibility(View.VISIBLE);
                        holder.requestSentButton.setVisibility(View.GONE);
                        Toast.makeText(context, R.string.failed_to_add_friend, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        holder.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(R.string.are_you_sure)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                holder.addButton.setVisibility(View.VISIBLE);
                                holder.removeButton.setVisibility(View.GONE);
                                user.setFriendStatus("0");
                                Call<Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).remove(
                                        userid,
                                        user.getId()
                                );
                                call.enqueue(new Callback<Response>() {
                                    @Override
                                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

                                    }

                                    @Override
                                    public void onFailure(Call<Response> call, Throwable t) {
                                        holder.addButton.setVisibility(View.GONE);
                                        holder.removeButton.setVisibility(View.VISIBLE);
                                        Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
            }
        });

        holder.requestSentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(R.string.are_you_sure_cancel_request)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                holder.addButton.setVisibility(View.VISIBLE);
                                holder.requestSentButton.setVisibility(View.GONE);
                                user.setFriendStatus("0");
                                Call<Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).remove(
                                        userid,
                                        user.getId()
                                );
                                call.enqueue(new Callback<Response>() {
                                    @Override
                                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

                                    }

                                    @Override
                                    public void onFailure(Call<Response> call, Throwable t) {
                                        holder.addButton.setVisibility(View.GONE);
                                        holder.requestSentButton.setVisibility(View.VISIBLE);
                                        Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
            }
        });

        holder.rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.responseContainer.setVisibility(View.GONE);
                holder.addButton.setVisibility(View.VISIBLE);
                user.setFriendStatus("0");
                Call<Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).remove(
                        App.getInstance().getSession().getUserid(),
                        user.getId()
                );
                call.enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        holder.responseContainer.setVisibility(View.VISIBLE);
                        holder.addButton.setVisibility(View.GONE);
                        Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        holder.acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.responseContainer.setVisibility(View.GONE);
                holder.removeButton.setVisibility(View.VISIBLE);
                user.setFriendStatus("2");
                Call<Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).accept(
                        App.getInstance().getSession().getUserid(),
                        user.getId()
                );
                call.enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        holder.responseContainer.setVisibility(View.VISIBLE);
                        holder.removeButton.setVisibility(View.GONE);
                        Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        return view;
    }

    static class MyViewHolder{
        ImageView avatar;
        TextView name;
        LinearLayout responseContainer;
        TextView rejectButton;
        TextView acceptButton;
        ImageButton addButton;
        ImageButton removeButton;
        ImageButton requestSentButton;
    }
}
