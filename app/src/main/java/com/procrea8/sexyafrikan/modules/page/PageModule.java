package com.procrea8.sexyafrikan.modules.page;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.AppActivity;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;
import com.procrea8.sexyafrikan.modules.page.activity.PageActivity;
import com.procrea8.sexyafrikan.modules.page.fragments.PagesFragment;
import com.procrea8.sexyafrikan.modules.page.model.Page;

/**
 * Created by Tiamiyu waliu kola on 9/18/2016.
 */
public class PageModule implements ModuleInterface {
    @Override
    public void registerMenu(final Context context, Drawer drawer, int i) {
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_home_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = new PagesFragment();
                        AppActivity.setFragment(fragment);
                        AppActivity.setCurrentTitle(context.getResources().getString(R.string.pages));
                        return false;
                    }
                })
                .withName(context.getResources().getString(R.string.pages)));
    }

    @Override
    public Fragment getFragment(Context context) {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type, Object object) {
        if (type.equals("notification-title")) {
            NotificationModel model = (NotificationModel) object;
            switch (model.getType()) {
                case "page.invite":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.invited_to_page));
                case "page.like":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.like_your_page));


            }
        }else if (type.equals("notification-action")) {
            NotificationModel model = (NotificationModel) object;
            String nType = model.getType();
            if (nType.equals("page.invite")) {
                Page page = model.getPages().get(0);
                PageActivity.load(context, page);
            }else if (nType.equals("page.like")) {
                    Page page = model.getPages().get(0);
                    PageActivity.load(context, page);
            }
        }
        return null;
    }

    @Override
    public Object invoke(Context context, String type) {
        return null;
    }
}
