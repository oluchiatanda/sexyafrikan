package com.procrea8.sexyafrikan.modules.feed.dialogs;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.chat.dialog.SelectFriendsDialog;
import com.procrea8.sexyafrikan.modules.feed.FeedApiInterface;
import com.procrea8.sexyafrikan.modules.feed.adapter.TagsAdapter;
import com.procrea8.sexyafrikan.modules.feed.fragments.FeedFragment;
import com.procrea8.sexyafrikan.modules.feed.response.FeedAddResponse;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;
import com.procrea8.sexyafrikan.modules.relationship.adapter.OnlineAdapter;

import net.alhazmy13.mediapicker.Video.VideoPicker;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import me.nereo.multi_image_selector.MultiImageSelector;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditorDialog implements Contants {
    Context context;
    ArrayList<FeedResponse> feeds = new ArrayList<>();
    LayoutInflater layoutInflater;
    String typeId, typeName, entityType, entityId;
    Dialog dialog;
    ProgressDialog progressDialog;
    FeedAddedListener feedAddedListener;
    //    FeedAddListener feedaddsListener;
    TextView cancelButton;
    Button postButton, saveButton;
    TextView privacyButton;
    ImageView imageButton;
    ImageView videoButton;
    ImageView locationButton;
    ImageView moreButton;
    ImageView tagButton;
    String privacy = "1";
    EditText editorText;
    LinearLayout locationContainer;
    LinearLayout feelingContainer;
    TextView locationText;
    TextView feelingText;
    ImageView locationClose;
    ImageView feelingClose;
    TextView feelingType;
    LinearLayout pollAnswers;
    Boolean isPoll = false;
    EditText pollOptionOne;
    EditText pollOptionTwo;
    EditText pollOptionThree;
    ImageView pollClose;
    CheckBox pollMultiple;
    CircleImageView editorAvatar;
    static TextView photoSelectCount, videoSelectCount;
    LinearLayout tagContainer;
    ListView tagsListView;

    ArrayList<User> tagUsers = new ArrayList<>();
    TagsAdapter tagsAdapter;
    public static String filePath = "";
    public static String attachement = "";
    public static List<String> imagesPath = null;

    String location = "";
    String to_userid = "";
    String feeling_type = "";
    String feeling_text = "";
    String is_poll = "0";
    String poll_option_one = "";
    String poll_option_two = "";
    String poll_option_three = "";
    String poll_multiple = "0";
    String tags = "";

    int feelingTypeText = 0;

    String doLaunch = "";

    public EditorDialog(final Context c, String type, final String typeId, final String entityType, final String entityId, String launch) {
        this.context = c;
        this.typeId = typeId;
        this.typeName = type;
        this.entityId = entityId;
        this.entityType = type;
        this.doLaunch = launch;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.feed_editor, null);
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context, R.style.MYDIALOG);
        alertBuilder.setView(convertView);

        cancelButton = (TextView) convertView.findViewById(R.id.post_cancel);
        postButton = (Button) convertView.findViewById(R.id.post_button);
        saveButton = (Button) convertView.findViewById(R.id.save_button);
        privacyButton = (TextView) convertView.findViewById(R.id.editor_privacy);
        moreButton = (ImageView) convertView.findViewById(R.id.more_button);
        imageButton = (ImageView) convertView.findViewById(R.id.image_button);
        videoButton = (ImageView) convertView.findViewById(R.id.video_button);
        locationButton = (ImageView) convertView.findViewById(R.id.location_button);
        editorText = (EditText) convertView.findViewById(R.id.text);
        locationContainer = (LinearLayout) convertView.findViewById(R.id.location_container);
        locationClose = (ImageView) convertView.findViewById(R.id.location_close);
        locationText = (TextView) convertView.findViewById(R.id.location_text);
        feelingContainer = (LinearLayout) convertView.findViewById(R.id.feeling_container);
        feelingClose = (ImageView) convertView.findViewById(R.id.feeling_close);
        feelingText = (TextView) convertView.findViewById(R.id.feeling_text);
        feelingType = (TextView) convertView.findViewById(R.id.feeling_type);
        pollAnswers = (LinearLayout) convertView.findViewById(R.id.poll_answers);
        pollOptionOne = (EditText) convertView.findViewById(R.id.poll_option_one);
        pollOptionTwo = (EditText) convertView.findViewById(R.id.poll_option_two);
        pollOptionThree = (EditText) convertView.findViewById(R.id.poll_option_three);
        pollClose = (ImageView) convertView.findViewById(R.id.poll_close);
        pollMultiple = (CheckBox) convertView.findViewById(R.id.poll_multiple);
        tagButton = (ImageView) convertView.findViewById(R.id.tag_button);
        tagContainer = (LinearLayout) convertView.findViewById(R.id.tag_container);
        tagsListView = (ListView) convertView.findViewById(R.id.tags_list);
        editorAvatar = (CircleImageView) convertView.findViewById(R.id.editor_avatar);
        photoSelectCount = (TextView) convertView.findViewById(R.id.photo_select_count);
        videoSelectCount = (TextView) convertView.findViewById(R.id.video_select_count);

        Glide.with(context).load(App.getInstance().getSession().getAvatar()).crossFade().into(editorAvatar);

        ImageView closePopup = (ImageView) convertView.findViewById(R.id.close_popup);
        dialog = alertBuilder.create();
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(context.getResources().getString(R.string.posting));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                filePath = "";
                attachement = "";
            }
        });

        tagsAdapter = new TagsAdapter(context, tagUsers);
        tagsAdapter.setOnUserSelected(new TagsAdapter.OnUserSelected() {
            @Override
            public void onSelected(User user, int i) {
                //tagUsers.remove(i);
                if (tagUsers.size() < 1) {
                    tagContainer.setVisibility(View.GONE);
                }
                tagsAdapter.notifyDataSetChanged();
            }
        });
        tagsListView.setAdapter(tagsAdapter);
        tagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectFriendsDialog dialog = new SelectFriendsDialog(context);
                dialog.setOnUserSelected(new OnlineAdapter.OnUserSelected() {
                    @Override
                    public void onSelected(User user) {
                        if (tagUserIsAdded(user)) return;
                        tagUsers.add(user);
                        tagsAdapter.notifyDataSetChanged();
                        tagContainer.setVisibility(View.VISIBLE);
                        reCalculateListHeight(tagsListView);
                    }
                });
            }
        });

        privacyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.privacy);
                builder.setItems(R.array.privacy_list, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0) {
                            privacy = "1";
                            privacyButton.setText(R.string.public_str);
                        } else if (i == 1) {
                            privacy = "2";
                            privacyButton.setText(R.string.friends);
                        } else if (i == 2) {
                            privacy = "3";
                            privacyButton.setText(R.string.only_me);
                        }
                    }
                });
                builder.create().show();
            }
        });

        moreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setItems(R.array.feed_editor_menu, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0) {
                            //feelings
                            dialogInterface.dismiss();
                            showFeelingsDialog();
                        } else if (i == 1) {
                            //add poll
                            initPolling();

                        }
                    }
                });
                builder.create().show();
            }
        });

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initPicture();
            }
        });

        videoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initVideo();
            }
        });

        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initLocation();
            }
        });

        locationClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideLocationText();
            }
        });

        feelingClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideFeelingText();
            }
        });

        pollClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closePolling();
            }
        });

        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (editorText.getText().toString().equals("") && filePath.equals("") && location.equals("") && feeling_text.equals("") && imagesPath.isEmpty()) {
                    //progressDialog.dismiss();
                    Toast.makeText(context, context.getResources().getString(R.string.write_something), Toast.LENGTH_LONG).show();
                    return;
                }

                if (isPoll) {
                    if (pollOptionOne.getText().toString().isEmpty() && pollOptionTwo.getText().toString().isEmpty()) {
                        Toast.makeText(context, "Enter poll options", Toast.LENGTH_LONG).show();
                        return;
                    }

                    is_poll = "1";
                    poll_option_one = pollOptionOne.getText().toString();
                    poll_option_two = pollOptionTwo.getText().toString();
                    poll_option_three = pollOptionThree.getText().toString();
                }
                if (pollMultiple.isChecked()) {
                    poll_multiple = "1";
                }
                progressDialog.show();

                if (tagUsers.size() > 0) {
                    for (int i = 0; i < tagUsers.size(); i++) {
                        User thisUser = tagUsers.get(i);
                        tags = tags.concat("," + thisUser.getId());
                    }
                    tags = tags.substring(1, tags.length());
                    //MyLog.e("Tag Members - " + tags);
                }

                Call<FeedAddResponse> call = null;
                if (attachement.equals("image")) {
                    MultipartBody.Part image1 = null;
                    MultipartBody.Part image2 = null;
                    MultipartBody.Part image3 = null;
                    MultipartBody.Part image4 = null;
                    MultipartBody.Part image5 = null;

                    File file1 = new File(imagesPath.get(0));
                    //MyLog.e("Image path - " + filePath);
                    RequestBody requestBody1 = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
                    image1 = MultipartBody.Part.createFormData("image1", file1.getName(), requestBody1);

                    if (imagesPath.size() > 1) {
                        File file2 = new File(imagesPath.get(1));
                        //MyLog.e("Image path - " + filePath);
                        RequestBody requestBody2 = RequestBody.create(MediaType.parse("multipart/form-data"), file2);
                        image2 = MultipartBody.Part.createFormData("image2", file2.getName(), requestBody2);
                    }

                    if (imagesPath.size() > 2) {
                        File file3 = new File(imagesPath.get(2));
                        //MyLog.e("Image path - " + filePath);
                        RequestBody requestBody3 = RequestBody.create(MediaType.parse("multipart/form-data"), file3);
                        image3 = MultipartBody.Part.createFormData("image3", file3.getName(), requestBody3);
                    }

                    if (imagesPath.size() > 3) {
                        File file4 = new File(imagesPath.get(3));
                        //MyLog.e("Image path - " + filePath);
                        RequestBody requestBody4 = RequestBody.create(MediaType.parse("multipart/form-data"), file4);
                        image4 = MultipartBody.Part.createFormData("image4", file4.getName(), requestBody4);
                    }

                    if (imagesPath.size() > 4) {
                        File file5 = new File(imagesPath.get(4));
                        //MyLog.e("Image path - " + filePath);
                        RequestBody requestBody5 = RequestBody.create(MediaType.parse("multipart/form-data"), file5);
                        image5 = MultipartBody.Part.createFormData("image5", file5.getName(), requestBody5);
                    }

                    call = Api.getRetrofit().create(FeedApiInterface.class).add(
                            App.getInstance().getSession().getUserid(), typeName, typeId, editorText.getText().toString(),entityType, entityId,
                            location, to_userid, privacy, feeling_type, feeling_text, is_poll, poll_option_one,
                            poll_option_two, poll_option_three, poll_multiple, tags, image1, image2, image3, image4, image5
                    );

                    MyLog.e("userid=" + App.getInstance().getSession().getUserid() + ", type=" + typeName + "/n type_id" + typeId
                            + ", text = " + editorText.getText().toString() + ", entity_type = " + entityType + ", entity_id=" + entityId + ", location =" + location +
                            ", to_user_id = " + to_userid + ", privacy" + privacy + ", feeling_type=" + feeling_type + ", feeling_text=" +
                            feeling_text + ", is_poll=" + is_poll + ",poll_option_one =" + poll_option_one + ", poll_option_two" + poll_option_two
                            + ", poll_option_three =" + poll_option_three + ", poll_multiple" + poll_multiple + ", tags=" + tags + ", imge1=" +
                            image1 + ", image2=" + image2 + ", image3= " + image3 + ", image4 = " + image4 + ", image5=" + image5);
                } else {
                    if (filePath.isEmpty()) {
                        call = Api.getRetrofit().create(FeedApiInterface.class).add(
                                App.getInstance().getSession().getUserid(), typeName, typeId, editorText.getText().toString(), entityType, entityId,
                                location, to_userid, privacy, feeling_type, feeling_text, is_poll, poll_option_one,
                                poll_option_two, poll_option_three, poll_multiple, tags
                        );
                        MyLog.e("filepath.isEmpty " + "userid=" + App.getInstance().getSession().getUserid() + ", type=" + typeName + "/n type_id" + typeId
                                + ", text = " + editorText.getText().toString() + ", entity_type = " + entityType + ", entity_id=" + entityId + ", location =" + location +
                                ", to_user_id = " + to_userid + ", privacy" + privacy + ", feeling_type=" + feeling_type + ", feeling_text=" +
                                feeling_text + ", is_poll=" + is_poll + ",poll_option_one =" + poll_option_one + ", poll_option_two" + poll_option_two
                                + ", poll_option_three =" + poll_option_three + ", poll_multiple" + poll_multiple + ", tags=" + tags);
                    } else {
                        MultipartBody.Part body = null;
                        File file = new File(filePath);
                        //MyLog.e("Imag e path - " + filePath);
                        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        body = MultipartBody.Part.createFormData(attachement, file.getName(), requestBody);
                        call = Api.getRetrofit().create(FeedApiInterface.class).add(
                                App.getInstance().getSession().getUserid(), typeName, typeId, editorText.getText().toString(), entityType, entityId,
                                location, to_userid, privacy, feeling_type, feeling_text, is_poll, poll_option_one,
                                poll_option_two, poll_option_three, poll_multiple, tags, body
                        );
                        MyLog.e("body" + "userid=" + App.getInstance().getSession().getUserid() + ", type=" + typeName + "/n type_id" + typeId
                                + ", text = " + editorText.getText().toString() + ", entity_type = " + entityType + ", entity_id=" + entityId + ", location =" + location +
                                ", to_user_id = " + to_userid + ", privacy" + privacy + ", feeling_type=" + feeling_type + ", feeling_text=" +
                                feeling_text + ", is_poll=" + is_poll + ",poll_option_one =" + poll_option_one + ", poll_option_two" + poll_option_two
                                + ", poll_option_three =" + poll_option_three + ", poll_multiple" + poll_multiple + ", tags=" + tags + ", image" +
                                body);
                    }
                }

                call.enqueue(new Callback<FeedAddResponse>() {
                    @Override
                    public void onResponse(Call<FeedAddResponse> call, Response<FeedAddResponse> response) {
                        if (response.body().getStatus() == 1) {
                            feedAddedListener.added(response.body().getFeedResponse());
                            progressDialog.dismiss();
                            dialog.dismiss();
                            filePath = "";
                            attachement = "";
                            MyLog.e(response.body().getFeedResponse().toString() + " -message- " +response.body().getMessage() + " -errors");
                        } else {
                            Toast.makeText(context, response.body().getMessage() + " else if response is 0", Toast.LENGTH_LONG).show();
                            MyLog.e(response.body().getMessage() + " -feedresponse" + response.body().getFeedResponse().toString() + " else if response is 0");
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<FeedAddResponse> call, Throwable t) {

                        //MyLog.e(call.execute().message());
                        MyLog.e(t.getMessage() + " -Failed- ");
                        progressDialog.dismiss();
                    }
                });
            }
        });
    }

    public void initLocation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.where_are_you));
        View v = layoutInflater.inflate(R.layout.feed_dialog_location, null);
        builder.setView(v);
        final EditText lText = (EditText) v.findViewById(R.id.location_text);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String text = lText.getText().toString();
                if (!text.isEmpty()) {
                    setLocationText(text);
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    public void initVideo() {
        final ActivityBase activity = (ActivityBase) context;
        activity.setPermissionGranted(new ActivityBase.OnPermissionGranted() {
            @Override
            public void onGranted() {
                VideoPicker builder = new VideoPicker.Builder(activity)
                        .mode(VideoPicker.Mode.GALLERY)
                        .build();
            }
        });
    }

    public void initPicture() {
        final ActivityBase activity = (ActivityBase) context;
        activity.setPermissionGranted(new ActivityBase.OnPermissionGranted() {
            @Override
            public void onGranted() {
                MultiImageSelector.create()
                        .showCamera(true)
                        .count(5)
                        .multi()
                        .start(activity, FEED_PHOTO_SELECT);
            }
        });
    }

    public EditorDialog(final Context c, String type, final String typeId, final String entityType, final String entityId) {
        this(c, type, typeId, entityType, entityId, "");
    }

    public boolean tagUserIsAdded(User user) {
        Boolean result = false;
        if (tagUsers.size() < 1) result = false;
        for (int i = 0; i < tagUsers.size(); i++) {
            User dUser = tagUsers.get(i);
            if (user.getId().equals(dUser.getId())) result = true;
        }
        return result;
    }

    public void show() {
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.copyFrom(dialog.getWindow().getAttributes());
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(params);

        //pre-launch action
        switch (doLaunch) {
            case "photo":
                initPicture();
                break;
            case "video":
                initVideo();
                break;
            case "location":
                initLocation();
                break;
            case "poll":
                initPolling();
                break;
            case "feeling":
                showFeelingsDialog();
                break;
        }
    }

    public void showFeelingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.what_are_you_feeling);
        builder.setItems(R.array.feeling_list, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                setFeelingText(i);
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    public void reCalculateListHeight(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
    }

    public void setFeelingText(int i) {

        switch (i) {
            case 0:
                feeling_type = "listening-to";
                feelingTypeText = R.string.listening_to;
                break;
            case 1:
                feeling_type = "watching";
                feelingTypeText = R.string.watching;
                break;
            case 2:
                feeling_type = "feeling";
                feelingTypeText = R.string.feeling;
                break;
            case 3:
                feeling_type = "thinking";
                feelingTypeText = R.string.thinking_about;
                break;
            case 4:
                feeling_type = "Reading";
                feelingTypeText = R.string.reading;
                break;
            case 5:
                feeling_type = "eating";
                feelingTypeText = R.string.eating;
                break;
            case 6:
                feeling_type = "drinking";
                feelingTypeText = R.string.drinking;
                break;
            case 7:
                feeling_type = "celebrating";
                feelingTypeText = R.string.celebrating;
                break;
            case 8:
                feeling_type = "traveling-to";
                feelingTypeText = R.string.traveling_to;
                break;
            case 9:
                feeling_type = "exercising-to";
                feelingTypeText = R.string.exercising_to;
                break;
            case 10:
                feeling_type = "meeting";
                feelingTypeText = R.string.meeting;
                break;
            case 11:
                feeling_type = "playing";
                feelingTypeText = R.string.playing;
                break;
            case 12:
                feeling_type = "looking-for";
                feelingTypeText = R.string.looking_for;
                break;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(feelingTypeText);
        final LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.feed_feeling_value, null);
        final EditText editText = (EditText) v.findViewById(R.id.feeling_text);
        editText.setHint(feelingTypeText);
        builder.setView(v);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                String text = editText.getText().toString();
                if (!text.isEmpty()) {
                    feeling_text = text;
                    feelingContainer.setVisibility(View.VISIBLE);
                    feelingType.setText(feelingTypeText);
                    feelingText.setText(text);
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    public void hideFeelingText() {
        feeling_text = "";
        feelingContainer.setVisibility(View.GONE);
        feelingType.setText("");
        feelingText.setText("");
    }

    public void setLocationText(String text) {
        location = text;
        locationContainer.setVisibility(View.VISIBLE);
        locationText.setText(text);
    }

    public void hideLocationText() {
        location = "";
        locationContainer.setVisibility(View.GONE);
        locationText.setText("");
    }

    public static void setImagePath(List<String> images) {
        imagesPath = images;
        videoSelectCount.setText("0");
        videoSelectCount.setVisibility(View.GONE);
        photoSelectCount.setText(String.valueOf(images.size()));
        photoSelectCount.setVisibility(View.VISIBLE);
        filePath = "";
        attachement = "image";
    }

    public static void setVideoPath(String video) {
        filePath = video;
        videoSelectCount.setText("1");
        videoSelectCount.setVisibility(View.VISIBLE);
        photoSelectCount.setText("0");
        photoSelectCount.setVisibility(View.GONE);
        attachement = "video_attach";
    }

    public static void setFilePath(String file) {
        filePath = file;
        attachement = "file_attach";
    }

    public void setToUserid(String id) {
        this.to_userid = id;
    }

    public void initPolling() {
        isPoll = true;
        pollAnswers.setVisibility(View.VISIBLE);
        editorText.setHint(R.string.ask_question);
    }

    public void closePolling() {
        isPoll = false;
        pollAnswers.setVisibility(View.GONE);
        editorText.setHint(R.string.whats_on_your_mind);
    }

    public void setFeedAddedListener(FeedAddedListener listener) {
        feedAddedListener = listener;
    }

    public static class FeedAddedListener {
        public void added(FeedResponse feedResponse) {

        }
    }

//    public static class FeedAddListener {
//        public void adds(List<FeedResponse> feedResponse) {
//
//        }
//    }
}
