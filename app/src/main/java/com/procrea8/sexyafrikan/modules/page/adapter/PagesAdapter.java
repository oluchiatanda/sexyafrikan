package com.procrea8.sexyafrikan.modules.page.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.page.activity.PageActivity;
import com.procrea8.sexyafrikan.modules.page.model.Page;

import java.util.ArrayList;

public class PagesAdapter extends RecyclerView.Adapter<PagesAdapter.MyViewHolder>{
    Context context;
    ArrayList<Page> listings = new ArrayList<>();
    LayoutInflater layoutInflater;

    public PagesAdapter(Context c, ArrayList<Page> lists) {
        context = c;
        listings = lists;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView title;
        TextView likes;
        public MyViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.title);
            likes = (TextView) itemView.findViewById(R.id.likes);
        }
    }
    @Override
    public PagesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.page_card, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PagesAdapter.MyViewHolder holder, int position) {
        final Page page = listings.get(position);
        holder.title.setText(page.getTitle());
        holder.likes.setText(page.getLikes());
        Glide.with(context).load(page.getLogo()).crossFade().into(holder.image);
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageActivity.load(context, page);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listings.size();
    }
}
