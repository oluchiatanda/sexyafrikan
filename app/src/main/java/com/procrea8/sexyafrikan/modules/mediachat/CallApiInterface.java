package com.procrea8.sexyafrikan.modules.mediachat;


import com.procrea8.sexyafrikan.response.Response;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CallApiInterface {
    @GET("call/init")
    Call<Response> init(@Query("userid") String userid, @Query("identity") String identity, @Query("call_type") Integer callType, @Query("which") String which, @Query("connection_id") String conId);

    @GET("mediachat/disconnect")
    Call<Response> endCall(@Query("userid") String userid, @Query("conid") String connid);

    @GET("call/get/pending")
    Call<Response> getPending(@Query("userid") String userid);
}
