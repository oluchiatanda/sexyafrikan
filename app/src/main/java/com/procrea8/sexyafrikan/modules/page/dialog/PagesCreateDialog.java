package com.procrea8.sexyafrikan.modules.page.dialog;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;

import com.procrea8.sexyafrikan.modules.page.PageApiInterface;
import com.procrea8.sexyafrikan.modules.page.model.Page;
import com.satsuware.usefulviews.LabelledSpinner;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pRocrea8 on 11/23/2016.
 */
public class PagesCreateDialog implements Contants {
    Page page;
    AlertDialog dialog;
    ProgressDialog progressDialog;
    Context context;
    String category;
    static String filePath = "";

    EditText title, description;
    Button createPage;
    Button cancel;
    LabelledSpinner categorySpinner;

    ArrayList<MusicCategory> loadedCategories = new ArrayList<>();

    OnPagesCreatedListener listener;

    public PagesCreateDialog(final Context context, final Page page) {
        this.context = context;
        this.page = page;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.page_create, null);

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);



        title = (EditText) view.findViewById(R.id.title);
        description = (EditText) view.findViewById(R.id.description);
        categorySpinner = (LabelledSpinner) view.findViewById(R.id.category);

        Call<ArrayList<MusicCategory>> call = Api.getRetrofit().create(PageApiInterface.class).
                                                getCategories(App.getInstance().getSession().getUserid());
        call.enqueue(new Callback<ArrayList<MusicCategory>>() {
            @Override
            public void onResponse(Call<ArrayList<MusicCategory>> call, retrofit2.Response<ArrayList<MusicCategory>> response) {
                ArrayList<String> items = new ArrayList<String>();
                for (int i = 0; i < response.body().size(); i++) {
                    items.add(response.body().get(i).getTitle());
                }
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, items);
                categorySpinner.setCustomAdapter(arrayAdapter);
                categorySpinner.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
                    @Override
                    public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
                        if (loadedCategories.size() > 0) {
                            category = loadedCategories.get(position).getId();
                        }
                    }

                    @Override
                    public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onFailure(Call<ArrayList<MusicCategory>> call, Throwable t) {
                Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
            }
        });

        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok, null).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String stitle = title.getText().toString();
                        String sdescription = description.getText().toString();

                        if (stitle.isEmpty()) {
                            Toast.makeText(context, "Provide Title", Toast.LENGTH_LONG).show();
                            return;
                        }
                        progressDialog = new ProgressDialog(context);
                        progressDialog.setMessage(context.getResources().getString(R.string.adding));
                        progressDialog.setCancelable(false);
                        progressDialog.setIndeterminate(true);
                        progressDialog.show();

                        Call<Page> call = null;

                        call = Api.getRetrofit().create(PageApiInterface.class).add(
                                App.getInstance().getSession().getUserid(), stitle, sdescription
                        );
                        if (call != null) {
                            call.enqueue(new Callback<Page>() {
                                @Override
                                public void onResponse(Call<Page> call, Response<Page> response) {
                                    progressDialog.dismiss();
                                    if (response.body().getStatus()==1) {
                                        dialog.dismiss();
                                        if (listener != null) {
                                            listener.onPagesCreated(response.body());
                                        }
                                        filePath = "";
                                    } else {
                                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Page> call, Throwable t) {
                                    MyLog.e("Create new page error " + t.getMessage());
                                    Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                }
                            });
                        }
                    }
                });
            }
        });

        dialog.show();
    }

    public void addListener(OnPagesCreatedListener l) {
        listener = l;
    }

    public static class OnPagesCreatedListener {
        public void onPagesCreated(Page page) {
        }
    }
}
