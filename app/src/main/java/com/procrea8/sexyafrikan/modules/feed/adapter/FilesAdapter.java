package com.procrea8.sexyafrikan.modules.feed.adapter;

import android.content.Context;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.notification.BaseNotificationItem;
import com.liulishuo.filedownloader.notification.FileDownloadNotificationHelper;
import com.liulishuo.filedownloader.notification.FileDownloadNotificationListener;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.helper.NotificationItem;
import com.procrea8.sexyafrikan.modules.feed.model.FileModel;

import java.util.ArrayList;

public class FilesAdapter extends BaseAdapter implements Contants {
    Context context;
    ArrayList<FileModel> fileLists;

    public FilesAdapter(Context c, ArrayList<FileModel> lists) {
        this.fileLists = lists;
        context = c;
    }

    @Override
    public int getCount() {
        return fileLists.size();
    }

    @Override
    public Object getItem(int i) {
        return fileLists.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = (View) layoutInflater.inflate(R.layout.feed_file_item, null);
            viewHolder = new ViewHolder();
            viewHolder.fileIcon = (ImageView) view.findViewById(R.id.file_icon);
            viewHolder.fileTitle = (TextView) view.findViewById(R.id.file_title);
            viewHolder.fileDownload = (ImageView) view.findViewById(R.id.file_download);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        final FileModel file = fileLists.get(i);
        Glide.with(context).load(file.getExtensionIcon()).into(viewHolder.fileIcon);
        viewHolder.fileTitle.setText(file.getName());
        viewHolder.fileDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!App.getInstance().isExternalStorageAvailable()) {
                    Toast.makeText(context, R.string.failed_to_download, Toast.LENGTH_LONG).show();
                    return;
                }
                FileDownloadNotificationHelper<NotificationItem> notificationHelper = new FileDownloadNotificationHelper<NotificationItem>();
                FileDownloader.getImpl().create(file.getDownload())
                        .setForceReDownload(true)
                        .setPath(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + file.getName())
                        .setListener(new FileDownloadListener() {
                            @Override
                            protected void pending(BaseDownloadTask task, int soFarBytes, int totalBytes) {

                            }

                            @Override
                            protected void progress(BaseDownloadTask task, int soFarBytes, int totalBytes) {

                            }

                            @Override
                            protected void completed(BaseDownloadTask task) {
                                //MyLog.e("download completed");
                                Toast.makeText(context, R.string.download_completed, Toast.LENGTH_LONG).show();
                            }

                            @Override
                            protected void paused(BaseDownloadTask task, int soFarBytes, int totalBytes) {

                            }

                            @Override
                            protected void error(BaseDownloadTask task, Throwable e) {
                                MyLog.e(e.getMessage());
                            }

                            @Override
                            protected void warn(BaseDownloadTask task) {

                            }
                        })
                        .setListener(new FileDownloadNotificationListener(notificationHelper) {
                            @Override
                            protected BaseNotificationItem create(BaseDownloadTask task) {
                                return new NotificationItem(task.getId(), context.getResources().getString(R.string.downloading_file), "Downloading " + file.getName());
                            }
                        }).start();
            }
        });
        return view;
    }

    static class ViewHolder{
        ImageView fileIcon;
        TextView fileTitle;
        ImageView fileDownload;

    }
}
