package com.procrea8.sexyafrikan.modules.relationship.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.relationship.RelationshipApiInterface;
import com.procrea8.sexyafrikan.modules.relationship.adapter.RequestAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FriendRequestFragment extends Fragment {

    RequestAdapter requestAdapter;
    ListView requestListView;
    ArrayList<User> requestList = new ArrayList<>();
    AVLoadingIndicatorView loadingImage;
    TextView statusText;
    View footerLoading;
    SwipeRefreshLayout swipeRefreshLayout;
    Boolean requestLoading = false;
    Integer page = 1;

    public FriendRequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_friend_request, container, false);
        requestListView = (ListView) view.findViewById(R.id.friend_request_list);
        statusText = (TextView) view.findViewById(R.id.status_text);
        loadingImage = (AVLoadingIndicatorView) view.findViewById(R.id.loading_image);
        footerLoading = inflater.inflate(R.layout.loading_indicator, null);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);

        requestAdapter = new RequestAdapter(getContext(), requestList);
        requestListView.addFooterView(footerLoading);
        requestListView.setAdapter(requestAdapter);
        requestListView.removeFooterView(footerLoading);

        loadingImage.show();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                loadRequests(false);
            }
        });
        requestListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                final int lastItem = i + i1;
                if (i2 > 0 && lastItem == i2) {
                    if (!requestLoading && requestList.size() > 5) {

                        requestListView.addFooterView(footerLoading);
                        page++;
                        requestLoading = true;
                        loadRequests(true);
                    }
                }
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadRequests(false);
    }

    public void loadRequests(final Boolean more) {
        String userid = App.getInstance().getSession().getUserid();
        Call<ArrayList<User>> call = Api.getRetrofit().create(RelationshipApiInterface.class).list(userid, page.toString());
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                if (!more) requestList.clear();
                requestList.addAll(response.body());
                requestAdapter.notifyDataSetChanged();
                loadingImage.hide();
                swipeRefreshLayout.setRefreshing(false);
                if (more) {
                    requestListView.removeFooterView(footerLoading);
                    requestLoading = false;
                } else {
                    if (response.body().size() < 1) {
                        statusText.setText(R.string.no_new_friend_requests);
                        statusText.setVisibility(View.VISIBLE);
                    } else {
                        statusText.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                loadingImage.hide();
                if (!more) {
                    statusText.setText(R.string.no_new_friend_requests);
                    statusText.setVisibility(View.VISIBLE);
                } else {
                    page++;
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
}
