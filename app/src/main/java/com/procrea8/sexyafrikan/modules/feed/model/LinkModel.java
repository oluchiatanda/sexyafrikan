package com.procrea8.sexyafrikan.modules.feed.model;


import com.google.gson.annotations.SerializedName;

public class LinkModel {
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProviderUrl() {
        return providerUrl;
    }

    public void setProviderUrl(String providerUrl) {
        this.providerUrl = providerUrl;
    }

    @SerializedName("type")
    String type;
    @SerializedName("image")
    String image;
    @SerializedName("link")
    String link;
    @SerializedName("title")
    String title;
    @SerializedName("description")
    String description;
    @SerializedName("code")
    String code;
    @SerializedName("provider_url")
    String providerUrl;
}
