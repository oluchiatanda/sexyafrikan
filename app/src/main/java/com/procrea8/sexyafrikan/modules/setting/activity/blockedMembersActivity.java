package com.procrea8.sexyafrikan.modules.setting.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.relationship.adapter.UsersAdapter;
import com.procrea8.sexyafrikan.modules.setting.SettingsApiInterface;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class blockedMembersActivity extends ActivityBase {
    AVLoadingIndicatorView loadingImage;
    TextView statusText;
    ArrayList<User> users = new ArrayList<>();
    UsersAdapter usersAdapter;
    ListView listView;

    Integer page = 1;
    Boolean paginating = false;
    View footerLoading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked_members);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadingImage = (AVLoadingIndicatorView) findViewById(R.id.loading_image);
        statusText = (TextView) findViewById(R.id.status_text);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        footerLoading = layoutInflater.inflate(R.layout.loading_indicator, null);
        listView = (ListView) findViewById(R.id.list_view);
        listView.addFooterView(footerLoading);
        usersAdapter = new UsersAdapter(this, users);
        listView.setAdapter(usersAdapter);
        listView.removeFooterView(footerLoading);

        loadUsers(false);

        listView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                //view.getId();
                return false;
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                final int lastItem = i + i1;
                if (i2 > 0 && lastItem == i2) {
                    if (!paginating ) {

                        listView.addFooterView(footerLoading);
                        page++;
                        paginating = true;
                        loadUsers(true);
                    }
                }
            }
        });
    }

    public void loadUsers(final Boolean more) {
        if (!more) {
            loadingImage.show();
        }
        Call<ArrayList<User>> call = Api.getRetrofit().create(SettingsApiInterface.class).getBlockedMembers(
                App.getInstance().getSession().getUserid(),
                page.toString()
        );
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                loadingImage.hide();
                if (!more) {
                    users.clear();
                    if (response.body().size() == 0) {
                        statusText.setText(R.string.no_members_found);
                        statusText.setVisibility(View.VISIBLE);
                    } else{
                        statusText.setVisibility(View.GONE);
                    }
                }
                users.addAll(response.body());

            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                loadingImage.hide();
                statusText.setText(R.string.no_members_found);
                statusText.setVisibility(View.VISIBLE);
            }
        });
    }
}
