package com.procrea8.sexyafrikan.modules.chat.model;


import com.google.gson.annotations.SerializedName;

public class Message {
    @SerializedName("from_me")
    Boolean fromMe;
    @SerializedName("text")
    String text;
    @SerializedName("time")
    String time;
    @SerializedName("image")
    String image;
    @SerializedName("avatar")
    String avatar;

    @SerializedName("status")
    String status;
    @SerializedName("cid")
    String cid;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public Boolean getFromMe() {
        return fromMe;
    }

    public void setFromMe(Boolean fromMe) {
        this.fromMe = fromMe;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
