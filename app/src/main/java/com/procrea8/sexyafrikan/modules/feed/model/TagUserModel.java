package com.procrea8.sexyafrikan.modules.feed.model;


import com.google.gson.annotations.SerializedName;

public class TagUserModel {
    @SerializedName("id")
    String id;
    @SerializedName("name")
    String name;

    @SerializedName("avatar")
    String avatar;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
