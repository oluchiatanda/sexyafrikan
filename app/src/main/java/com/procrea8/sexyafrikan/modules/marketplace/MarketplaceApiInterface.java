package com.procrea8.sexyafrikan.modules.marketplace;

import com.procrea8.sexyafrikan.modules.marketplace.model.BrowseResponse;
import com.procrea8.sexyafrikan.modules.marketplace.model.Listing;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface MarketplaceApiInterface {
    @GET("marketplace/get/categories")
    Call<ArrayList<MusicCategory>> getCategories(@Query("userid") String userid);
    @GET("marketplace/browse")
    Call<BrowseResponse>  browse(@Query("userid") String userid, @Query("category_id") String category, @Query("term") String term,
                                 @Query("type") String type, @Query("limit") Integer limit,
                                 @Query("page") Integer page);
    @GET("marketplace/delete")
    Call<Response> delete(@Query("userid") String userid, @Query("listing_id") String musicId);

    @Multipart
    @POST("marketplace/create")
    Call<Listing> create(@Query("userid") String userid, @Query("title") String title, @Query("description") String description,
                         @Query("tags") String tags,@Query("address") String address,@Query("link") String link,
                         @Query("price") String price, @Query("category_id") String categoryId, @Part MultipartBody.Part image);

    @POST("marketplace/create")
    Call<Listing> create(@Query("userid") String userid, @Query("title") String title, @Query("description") String description,
                         @Query("tags") String tags,@Query("address") String address,@Query("link") String link,
                         @Query("price") String price, @Query("category_id") String categoryId);

    @Multipart
    @POST("marketplace/edit")
    Call<Listing> edit(@Query("userid") String userid, @Query("listing_id") String listingId, @Query("title") String title, @Query("description") String description,
                         @Query("tags") String tags,@Query("address") String address,@Query("link") String link,
                         @Query("price") String price, @Query("category_id") String categoryId, @Part MultipartBody.Part image);

    @POST("marketplace/edit")
    Call<Listing> edit(@Query("userid") String userid, @Query("listing_id") String listingId, @Query("title") String title, @Query("description") String description,
                         @Query("tags") String tags,@Query("address") String address,@Query("link") String link,
                         @Query("price") String price, @Query("category_id") String categoryId);

}
