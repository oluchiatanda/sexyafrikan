package com.procrea8.sexyafrikan.modules.blog.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.helper.MyWebViewClient;
import com.procrea8.sexyafrikan.modules.blog.model.Blog;

public class BlogActivity extends ActivityBase {
    static Blog blog;
    TextView blogTitle, blogTime;
    WebView blogContent;
    ImageView blogImage;
    LinearLayout layout_share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        blogTitle = (TextView) findViewById(R.id.blog_title);
        blogTime = (TextView) findViewById(R.id.blog_time);
        blogContent = (WebView) findViewById(R.id.blog_content);
        blogImage = (ImageView) findViewById(R.id.blog_image);
        layout_share = (LinearLayout) findViewById(R.id.ex_share_button);

        layout_share.setVisibility(View.VISIBLE);
        View.OnClickListener shareListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/plain");
                if(blog.getSludge().equals("")) {
                    String shareUrl = SITE_BASE_URL + "blog/" + blog.getId();
                    intent.putExtra(android.content.Intent.EXTRA_TEXT, shareUrl);
                }
                else{
                    String shareUrl = SITE_BASE_URL + "blog/" + blog.getSludge();
                    intent.putExtra(android.content.Intent.EXTRA_TEXT, shareUrl);
                }
                startActivity(Intent.createChooser(intent, "Share via"));

            }
        };
        layout_share.setOnClickListener(shareListener);

    }

    @Override
    public void onStart() {
        super.onStart();
        blogTime.setText(blog.getTime());
        blogTitle.setText(blog.getTitle());
        blogContent.setWebChromeClient(new WebChromeClient());
        blogContent.setWebViewClient(new MyWebViewClient());
        blogContent.loadData(blog.getContent(), "text/html", null);

        initGlobalFooter(this, "blog", blog.getId(), blog.isHasReact(), blog.isHasLike(),
                blog.isHasDislike(),
                blog.getLikeCount(), blog.getDislikeCount(), blog.getComments());
        if (blog.getImage().isEmpty()) {
            blogImage.setVisibility(View.GONE);
        } else {
            blogImage.setVisibility(View.VISIBLE);
            Glide.with(this).load(blog.getImage()).crossFade().into(blogImage);
        }
    }

    public static void load(Context context, Blog b) {
        blog = b;
        Intent intent = new Intent(context, BlogActivity.class);
        context.startActivity(intent);
    }

}
