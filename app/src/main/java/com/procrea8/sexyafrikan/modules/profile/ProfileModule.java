package com.procrea8.sexyafrikan.modules.profile;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.mikepenz.materialdrawer.Drawer;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;

public class ProfileModule implements ModuleInterface {
    @Override
    public void registerMenu(final Context context, final Drawer drawer, int i) {

    }

    @Override
    public Fragment getFragment(Context context) {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type, Object object) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type) {
        return null;
    }

    public static void loadProfile(Context context, String userid) {
        Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtra("user_id", userid);
        context.startActivity(intent);
    }
}
