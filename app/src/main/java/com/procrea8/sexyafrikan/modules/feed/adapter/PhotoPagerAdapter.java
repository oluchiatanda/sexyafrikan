package com.procrea8.sexyafrikan.modules.feed.adapter;


import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.photo.activity.PhotoViewerActivity;

import java.util.ArrayList;

public class PhotoPagerAdapter extends PagerAdapter {
    Context context;
    ArrayList<String> photos;

    public PhotoPagerAdapter(Context c, ArrayList<String> photos) {
        this.photos = photos;
        this.context = c;
    }
    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.feed_photo_item, container, false);
        ImageView photo = (ImageView) view.findViewById(R.id.photo);
        Glide.with(context).load(photos.get(position)).crossFade().into(photo);
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PhotoViewerActivity.launch(context, photos.get(position));
            }
        });
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }

    public void addAll(ArrayList<String> images) {
        photos.clear();
        photos.addAll(images);
        notifyDataSetChanged();
    }
}
