package com.procrea8.sexyafrikan.modules.feed.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.modules.feed.FeedApiInterface;
import com.procrea8.sexyafrikan.modules.feed.adapter.FeedAdapter;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedActivity extends ActivityBase {
    static FeedResponse feedResponse;
    static String feedId;

    ArrayList<FeedResponse> feeds = new ArrayList<>();
    FeedAdapter feedAdapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.feed_lists);
        if (feedResponse != null) {
            feedAdapter = new FeedAdapter(this, feeds, feedResponse.getType(), feedResponse.getTypeId(), App.getInstance().getSession().getUserid(),"user" );
            feedAdapter.enableFullMessage();
            listView.setAdapter(feedAdapter);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (feedResponse != null) {
            feeds.clear();
            feeds.add(feedResponse);
            feedAdapter.notifyDataSetChanged();
        } else if(feedId != null) {
            Call<ArrayList<FeedResponse>> call = Api.getRetrofit().create(FeedApiInterface.class).getFeeds(
                    App.getInstance().getSession().getUserid(),
                    "single_feed",
                    feedId,
                    "1",
                    "0"
            );
            call.enqueue(new Callback<ArrayList<FeedResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<FeedResponse>> call, Response<ArrayList<FeedResponse>> response) {
                    feedResponse = response.body().get(0);
                    feeds.clear();
                    feeds.addAll(response.body());
                    reload();
                }

                @Override
                public void onFailure(Call<ArrayList<FeedResponse>> call, Throwable t) {

                }
            });
        }
    }

    public void reload() {
        feedAdapter = new FeedAdapter(this, feeds, feedResponse.getType(), feedResponse.getTypeId(), App.getInstance().getSession().getUserid(),"user" );
        feedAdapter.enableFullMessage();
        listView.setAdapter(feedAdapter);
    }

    public static void load(Context c, FeedResponse f) {
        feedResponse = f;
        Intent intent = new Intent(c, FeedActivity.class);
        c.startActivity(intent);
    }

    public static void load(Context c, String f) {
        feedId = f;
        feedResponse = null;
        Intent intent = new Intent(c, FeedActivity.class);
        c.startActivity(intent);
    }
}
