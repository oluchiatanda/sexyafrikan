package com.procrea8.sexyafrikan.modules.chat.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.helper.TimeAgo;
import com.procrea8.sexyafrikan.modules.chat.model.Message;

import java.util.ArrayList;


public class MessagesAdapter extends BaseAdapter {
    ArrayList<Message> messages = new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;

    public MessagesAdapter(Context c, ArrayList<Message> lists) {
        messages = lists;
        context = c;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int i) {
        return messages.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MyViewHolder holder;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.message_item, null);
            holder = new MyViewHolder();
            holder.left = (LinearLayout) view.findViewById(R.id.left);
            holder.right = (RelativeLayout) view.findViewById(R.id.right);
            holder.leftImage = (ImageView) view.findViewById(R.id.left_image);
            holder.leftText = (TextView) view.findViewById(R.id.left_text);
            holder.leftTime = (TextView) view.findViewById(R.id.left_time);
            holder.leftAvatar = (ImageView) view.findViewById(R.id.left_avatar);
            holder.rightImage = (ImageView) view.findViewById(R.id.right_image);
            holder.rightText = (TextView) view.findViewById(R.id.right_text);
            holder.rightTime = (TextView) view.findViewById(R.id.right_time);
            view.setTag(holder);
        } else {
            holder = (MyViewHolder) view.getTag();
        }

        Message message = messages.get(i);
        if (!message.getFromMe()) {
            holder.left.setVisibility(View.VISIBLE);
            holder.right.setVisibility(View.GONE);
            //holder.leftText.setWebViewClient(new MyWebViewClient());
            //holder.leftText.getSettings().setDefaultTextEncodingName("utf-8");
            //holder.leftText.setBackgroundColor(Color.TRANSPARENT);
            String text = "";
            text = text  + message.getText() + "";
            holder.leftText.setText(Html.fromHtml(text));
            holder.leftTime.setText(TimeAgo.format(context, message.getTime()));
            if (!message.getImage().isEmpty()) {
                Glide.with(context).load(message.getImage()).into(holder.leftImage);
                holder.leftImage.setVisibility(View.VISIBLE);
            } else {
                holder.leftImage.setVisibility(View.GONE);
            }
            Glide.with(context).load(message.getAvatar()).into(holder.leftAvatar);
        } else {
            holder.left.setVisibility(View.GONE);
            holder.right.setVisibility(View.VISIBLE);
            //holder.rightText.setWebViewClient(new MyWebViewClient());
            //holder.rightText.getSettings().setDefaultTextEncodingName("utf-8");
            String text =  message.getText();
            holder.rightText.setText(Html.fromHtml(text));
            holder.rightTime.setText(TimeAgo.format(context, message.getTime()));
            if (!message.getImage().isEmpty()) {
                Glide.with(context).load(message.getImage()).into(holder.rightImage);
                holder.rightImage.setVisibility(View.VISIBLE);
            } else {
                holder.rightImage.setVisibility(View.GONE);
            }
        }
        return view;
    }

    private class ImageGetter implements Html.ImageGetter {

        @Override
        public Drawable getDrawable(String s) {
            //Glide.with(context).load(s).
            return null;
        }
    }

    static  class MyViewHolder{
        LinearLayout left;
        RelativeLayout right;
        ImageView leftImage;
        ImageView rightImage;
        ImageView leftAvatar;
        TextView leftText;
        TextView rightText;
        TextView leftTime;
        TextView rightTime;
    }
}
