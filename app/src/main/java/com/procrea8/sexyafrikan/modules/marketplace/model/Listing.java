package com.procrea8.sexyafrikan.modules.marketplace.model;


import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.response.Response;

public class Listing extends Response{
    @SerializedName("id")
    String id;
    @SerializedName("title")
    String title;
    @SerializedName("description")
    String description;
    @SerializedName("price")
    String price;
    @SerializedName("short_description")
    String shortDescription;
    @SerializedName("image")
    String image;
    @SerializedName("time")
    String time;
    @SerializedName("link")
    String link;
    @SerializedName("tags")
    String tags;
    @SerializedName("comments")
    Integer comments;
    @SerializedName("address")
    String address;
    @SerializedName("slug")
    String sludge;

    public String getSludge(){return sludge;}
    public void setSludge(String slugde){this.sludge = sludge;}

    public Boolean getCanEdit() {
        return canEdit;
    }

    public void setCanEdit(Boolean canEdit) {
        this.canEdit = canEdit;
    }

    @SerializedName("can_edit")
    Boolean canEdit;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Integer getComments() {
        return comments;
    }

    public void setComments(Integer comments) {
        this.comments = comments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
