package com.procrea8.sexyafrikan.modules.hashtag;

import com.procrea8.sexyafrikan.modules.hashtag.model.HashtagResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HashtagApiInterface {
    @GET("hashtag/get")
    Call<HashtagResponse> getHashtags(@Query("userid") String userid, @Query("hashtag") String hashtag
            , @Query("limit") Integer limit, @Query("offset") Integer offset);
}
