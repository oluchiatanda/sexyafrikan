package com.procrea8.sexyafrikan.modules.feed.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.model.User;

import java.util.ArrayList;

/**
 * Created by Tiamiyu waliu kola on 9/30/2016.
 */
public class TagsAdapter extends BaseAdapter {
    ArrayList<User> users;
    Context context;
    LayoutInflater layoutInflater;
    OnUserSelected onUserSelected;
    public TagsAdapter(Context c, ArrayList<User> users) {
        this.context = c;
        this.users = users;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void  setOnUserSelected(OnUserSelected s) {
        onUserSelected = s;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final MyViewHolder holder;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.user_item, null);
            holder = new MyViewHolder();
            holder.avatar = (ImageView) view.findViewById(R.id.avatar);
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.responseContainer = (LinearLayout) view.findViewById(R.id.response_container);
            holder.acceptButton = (TextView) view.findViewById(R.id.accept_button);
            holder.rejectButton = (TextView) view.findViewById(R.id.reject_button);
            holder.addButton = (ImageButton) view.findViewById(R.id.add_friend);
            holder.removeButton = (ImageButton) view.findViewById(R.id.remove_friend);
            holder.requestSentButton = (ImageButton) view.findViewById(R.id.request_sent);
            holder.remove = (ImageView) view.findViewById(R.id.remove_user);
            view.setTag(holder);
        } else {
            holder = (MyViewHolder) view.getTag();
        }

        final User user = users.get(i);
        //if (user.getAvatar() == null) return null;
        //holder.name.setText(user.getName());
        Glide.with(context).load(user.getAvatar()).crossFade().into(holder.avatar);
        holder.name.setText(user.getName());
        holder.responseContainer.setVisibility(View.GONE);
        holder.addButton.setVisibility(View.GONE);
        holder.removeButton.setVisibility(View.GONE);
        holder.requestSentButton.setVisibility(View.GONE);
        holder.remove.setVisibility(View.VISIBLE);
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                users.remove(i);
                notifyDataSetChanged();
                if (onUserSelected != null) {
                    onUserSelected.onSelected(user, i);
                }
            }
        });
        return view;
    }

    public static class OnUserSelected {
        public void onSelected(User user, int i) {

        }
    }

    static class MyViewHolder{
        ImageView avatar;
        TextView name;
        LinearLayout responseContainer;
        TextView rejectButton;
        TextView acceptButton;
        ImageButton addButton;
        ImageButton removeButton;
        ImageButton requestSentButton;
        ImageView remove;
    }
}
