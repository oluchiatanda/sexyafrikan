package com.procrea8.sexyafrikan.modules.group.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.group.activity.GroupActivity;
import com.procrea8.sexyafrikan.modules.group.model.Group;

import java.util.ArrayList;

/**
 * Created by Tiamiyu waliu kola on 9/20/2016.
 */
public class GroupsAdapter extends RecyclerView.Adapter<GroupsAdapter.MyViewHolder> {
    Context context;
    ArrayList<Group> listings = new ArrayList<>();
    LayoutInflater layoutInflater;

    public GroupsAdapter(Context c, ArrayList<Group> lists) {
        context = c;
        listings = lists;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView title;
        TextView description;
        public MyViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.title);
            description = (TextView) itemView.findViewById(R.id.description);
        }
    }
    @Override
    public GroupsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.group_card, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GroupsAdapter.MyViewHolder holder, int position) {
        final Group group = listings.get(position);
        holder.title.setText(group.getTitle());
        holder.description.setText(group.getDescription());
        Glide.with(context).load(group.getLogo()).crossFade().into(holder.image);
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GroupActivity.load(context, group);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listings.size();
    }
}
