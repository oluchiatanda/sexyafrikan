package com.procrea8.sexyafrikan.modules.blog.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.blog.activity.BlogActivity;
import com.procrea8.sexyafrikan.modules.blog.model.Blog;

import java.util.ArrayList;


public class BlogsAdapter extends RecyclerView.Adapter<BlogsAdapter.MyViewHolder> implements Contants{
    Context context;
    ArrayList<Blog> blogs = new ArrayList<>();
    LayoutInflater layoutInflater;

    public BlogsAdapter(Context c, ArrayList<Blog> lists) {
        context = c;
        blogs = lists;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView title;
        TextView time;

        public MyViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.title);
            time = (TextView) itemView.findViewById(R.id.time);

        }
    }
    @Override
    public BlogsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.blog_card, null);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(BlogsAdapter.MyViewHolder holder, int position) {
        final Blog blog = blogs.get(position);
        holder.title.setText(blog.getTitle());
        holder.time.setText(blog.getTime());
        Glide.with(context).load(blog.getCover()).into(holder.image);
        //MyLog.e(music.getCover());
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //BlogPlayerActivity.launch(context, music);
                BlogActivity.load(context, blog);
            }
        });


    }

    @Override
    public int getItemCount() {
        return blogs.size();
    }
}
