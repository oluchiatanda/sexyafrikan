package com.procrea8.sexyafrikan.modules.chat.dialog;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.profile.ProfileApiInterface;
import com.procrea8.sexyafrikan.modules.relationship.adapter.OnlineAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectFriendsDialog implements Contants {

    Context context;
    EditText searchText;
    ImageView searchButton;
    TextView titlePane;
    SwipeRefreshLayout swipeRefreshLayout;
    ListView listView;
    TextView statusText;
    View footerLoading;
    AVLoadingIndicatorView loadingImage;

    ArrayList<User> usersList = new ArrayList<>();
    OnlineAdapter usersAdapter;
    Boolean suggestionLoading = false;
    Integer page = 1;
    String search_text = "";
    public static  AlertDialog dialog;
    public SelectFriendsDialog(Context context) {
        this.context = context;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(R.string.select_friend);

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.general_list, null);
        titlePane = (TextView) view.findViewById(R.id.list_title);
        searchText = (EditText) view.findViewById(R.id.search_text);
        searchButton = (ImageView) view.findViewById(R.id.search_button);
        listView = (ListView) view.findViewById(R.id.list);
        loadingImage = (AVLoadingIndicatorView) view.findViewById(R.id.loading_image);
        statusText = (TextView) view.findViewById(R.id.status_text);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        footerLoading = layoutInflater.inflate(R.layout.loading_indicator, null);

        usersAdapter = new OnlineAdapter(context, usersList);
        listView.setAdapter(usersAdapter);
        listView.removeFooterView(footerLoading);
        titlePane.setVisibility(View.GONE);
        loadFriends(false);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                loadFriends(false);
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                final int lastItem = i + i1;
                if (i2 > 0 && lastItem == i2) {
                    if (!suggestionLoading && usersList.size() > 5) {

                        //listView.addFooterView(footerLoading);
                        page++;
                        suggestionLoading = true;
                        loadFriends(true);
                    }
                }
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String search = searchText.getText().toString();
                if (!search.isEmpty()) {
                    search_text = search;
                    loadFriends(false);
                }
            }
        });
        builder.setView(view);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        dialog = builder.create();
        dialog.show();
    }

    public void setOnUserSelected(OnlineAdapter.OnUserSelected selected) {
        usersAdapter.setOnUserSelected(selected);
    }

    public void loadFriends(final boolean more) {
        String userid = App.getInstance().getSession().getUserid();
        //MyLog.e("loading suggestion");
        Call<ArrayList<User>> call = Api.getRetrofit().create(ProfileApiInterface.class).getFriends(userid, userid, page.toString(), search_text);
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                if (!more) usersList.clear();
                usersList.addAll(response.body());
                usersAdapter.notifyDataSetChanged();
                loadingImage.hide();
                swipeRefreshLayout.setRefreshing(false);
                if (more) {
                    //listView.removeFooterView(footerLoading);
                    suggestionLoading = false;
                } else {
                    if (response.body().size() < 1) {
                        statusText.setText(R.string.no_new_friend_found);
                        statusText.setVisibility(View.VISIBLE);
                    } else {
                        statusText.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                loadingImage.hide();
                suggestionLoading = false;
                if (!more) {
                    statusText.setText(R.string.no_new_friend_found);
                    statusText.setVisibility(View.VISIBLE);
                } else {
                    page--;
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
}
