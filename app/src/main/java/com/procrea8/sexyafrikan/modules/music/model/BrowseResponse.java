package com.procrea8.sexyafrikan.modules.music.model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BrowseResponse {
    @SerializedName("categories")
    ArrayList<MusicCategory> categories;
    @SerializedName("songs")
    ArrayList<Music> songs;

    public ArrayList<MusicCategory> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<MusicCategory> categories) {
        this.categories = categories;
    }

    public ArrayList<Music> getSongs() {
        return songs;
    }

    public void setSongs(ArrayList<Music> songs) {
        this.songs = songs;
    }
}
