package com.procrea8.sexyafrikan.modules.photo.model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Photo {
    @SerializedName("status")
    Integer status;

    @SerializedName("id")
    String id;
    @SerializedName("path")
    String path;

    @SerializedName("has_like")
    boolean hasLike = false;
    @SerializedName("has_dislike")
    boolean hasDislike = false;
    @SerializedName("has_react")
    boolean hasReact = false;
    @SerializedName("like_count")
    Integer likeCount = 0;
    @SerializedName("dislike_count")
    Integer dislikeCount = 0;

    @SerializedName("comments")
    Integer comments;

    @SerializedName("react_members")
    ArrayList<ArrayList<String>> reactMembers;

    @SerializedName("imageBefore")
    String imageBefore;

    @SerializedName("slug")
    String sludge;

    public String getSludge(){return sludge;}
    public void setSludge(String sludge) {this.sludge = sludge;}


    public String getImageAfter() {
        return imageAfter;
    }

    public void setImageAfter(String imageAfter) {
        this.imageAfter = imageAfter;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public boolean isHasLike() {
        return hasLike;
    }

    public void setHasLike(boolean hasLike) {
        this.hasLike = hasLike;
    }

    public boolean isHasDislike() {
        return hasDislike;
    }

    public void setHasDislike(boolean hasDislike) {
        this.hasDislike = hasDislike;
    }

    public boolean isHasReact() {
        return hasReact;
    }

    public void setHasReact(boolean hasReact) {
        this.hasReact = hasReact;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getDislikeCount() {
        return dislikeCount;
    }

    public void setDislikeCount(Integer dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    public Integer getComments() {
        return comments;
    }

    public void setComments(Integer comments) {
        this.comments = comments;
    }

    public ArrayList<ArrayList<String>> getReactMembers() {
        return reactMembers;
    }

    public void setReactMembers(ArrayList<ArrayList<String>> reactMembers) {
        this.reactMembers = reactMembers;
    }

    public String getImageBefore() {
        return imageBefore;
    }

    public void setImageBefore(String imageBefore) {
        this.imageBefore = imageBefore;
    }

    @SerializedName("imageAfter")
    String imageAfter;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
