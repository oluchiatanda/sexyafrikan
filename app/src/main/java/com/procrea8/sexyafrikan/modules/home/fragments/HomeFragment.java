package com.procrea8.sexyafrikan.modules.home.fragments;


import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.api.ApiInterface;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.chat.fragments.MessagesFragment;
import com.procrea8.sexyafrikan.modules.feed.fragments.FeedFragment;
import com.procrea8.sexyafrikan.modules.notification.fragments.NotificationFragment;
import com.procrea8.sexyafrikan.modules.relationship.fragments.FriendRequestFragment;
import com.procrea8.sexyafrikan.response.FCMResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    final  int HOME_FRAGMENT = 1;
    final int FRIEND_FRAGMENT = 2;
    final int MESSAGE_FRAGMENT = 3;
    final int NOTIFICATION_FRAGMENT = 4;

    private int currentFragment = 1;

    ViewPager container;

    TabLayout tabLayout;

    AdView mAdView;
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup con,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        container = (ViewPager) view.findViewById(R.id.container);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());

        mAdView = (AdView) view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)        // All emulators
                //.addTestDevice("AC98C820A50B4AD8A2106EDE96FB87D4")
                .build();
        mAdView.loadAd(adRequest);

        adapter.add(new FeedFragment());
        adapter.add(new FriendRequestFragment());
        adapter.add(new MessagesFragment());
        adapter.add(new NotificationFragment());
        container.setAdapter(adapter);
        tabLayout.setupWithViewPager(container);
        setTabViews();

        checkNewEvents();
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(container){
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 1:
                        View requests = tabLayout.getTabAt(1).getCustomView();
                        TextView countView = (TextView) requests.findViewById(R.id.text);
                        countView.setText("");
                        countView.setVisibility(View.GONE);
                        break;
                    case 2:
                        View messages = tabLayout.getTabAt(2).getCustomView();
                        TextView countView2 = (TextView) messages.findViewById(R.id.text);
                        countView2.setText("");
                        countView2.setVisibility(View.GONE);
                        break;
                    case 3:
                        View notifications = tabLayout.getTabAt(3).getCustomView();
                        TextView countView3 = (TextView) notifications.findViewById(R.id.text);
                        countView3.setText("");
                        countView3.setVisibility(View.GONE);
                        break;
                }
                super.onTabSelected(tab);
            }
        });
        return view;
    }

    public void setTabViews() {
        //LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View tabOne = LayoutInflater.from(getContext()).inflate(R.layout.home_tab_layout, null);
        ImageView icon = (ImageView) tabOne.findViewById(R.id.icon);
        icon.setImageResource(R.drawable.ic_home_color);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        //tab two
        View tab2 = LayoutInflater.from(getContext()).inflate(R.layout.home_tab_layout, null);
        ImageView icon2 = (ImageView) tab2.findViewById(R.id.icon);
        icon2.setImageResource(R.drawable.ic_friend_request_color);
        TextView text = (TextView) tab2.findViewById(R.id.text);
        text.setText("3");
        //text.setVisibility(View.VISIBLE);
        tabLayout.getTabAt(1).setCustomView(tab2);

        //tab 3
        View tab3 = LayoutInflater.from(getContext()).inflate(R.layout.home_tab_layout, null);
        ImageView icon3 = (ImageView) tab3.findViewById(R.id.icon);
        icon3.setImageResource(R.drawable.ic_message_color);
        tabLayout.getTabAt(2).setCustomView(tab3);


        //tab 4
        View tab4 = LayoutInflater.from(getContext()).inflate(R.layout.home_tab_layout, null);
        ImageView icon4 = (ImageView) tab4.findViewById(R.id.icon);
        icon4.setImageResource(R.drawable.ic_notification_color);
        tabLayout.getTabAt(3).setCustomView(tab4);
    }

    static class ViewPagerAdapter extends FragmentStatePagerAdapter{
        ArrayList<Fragment> fragments = new ArrayList<>();
        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        public void clear() {
            fragments.clear();
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }
        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
        @Override
        public Parcelable saveState() {
            return null;
        }
        @Override
        public int getCount() {
            return fragments.size();
        }

        public void add(Fragment fragment) {
            fragments.add(fragment);
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFCMResponseEvent(FCMResponse event) {
        if (event.getType().equals("new-event")) {
            //lets check for new events
            checkNewEvents();
        }
    }

    public void checkNewEvents() {
        Call<User> call = Api.getRetrofit().create(ApiInterface.class).checkEvents(App.getInstance().getSession().getUserid());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                try {
                    View friendRequest = tabLayout.getTabAt(1).getCustomView();
                    TextView countView = (TextView) friendRequest.findViewById(R.id.text);
                    Integer count = response.body().getFriendRequestsCount();
                    if (count > 0) {
                        countView.setText(count.toString());
                        countView.setVisibility(View.VISIBLE);
                    } else {
                        countView.setVisibility(View.GONE);
                    }

                    View newMessages = tabLayout.getTabAt(2).getCustomView();
                    TextView countView2 = (TextView) newMessages.findViewById(R.id.text);
                    Integer count2 = response.body().getMessagesCount();
                    if (count2 > 0) {
                        countView2.setText(count2.toString());
                        countView2.setVisibility(View.VISIBLE);
                    } else {
                        countView2.setVisibility(View.GONE);
                    }

                    View notifications = tabLayout.getTabAt(3).getCustomView();
                    TextView countView3 = (TextView) notifications.findViewById(R.id.text);
                    Integer count3 = response.body().getNotificationCount();
                    if (count3 > 0) {
                        countView3.setText(count3.toString());
                        countView3.setVisibility(View.VISIBLE);
                    } else {
                        countView3.setVisibility(View.GONE);
                    }
                } catch (NullPointerException e){

                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        checkNewEvents();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }
}
