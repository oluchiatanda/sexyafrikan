package com.procrea8.sexyafrikan.modules.forum;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.forum.activity.ForumActivity;
import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;


public class ForumModule implements ModuleInterface{
    @Override
    public void registerMenu(final Context context, Drawer drawer, int i) {
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_message_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Intent intent = new Intent(context, ForumActivity.class);
                        context.startActivity(intent);
                        return false;
                    }
                })
                .withName(context.getResources().getString(R.string.forum)));
    }

    @Override
    public Fragment getFragment(Context context) {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type, Object object) {
        if (type.equals("notification-title")) {
            NotificationModel model = (NotificationModel) object;
            switch (model.getType()) {
                case "forum.reply.thread":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.reply_thread_you_follow));

                case "forum.reply.post":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.reply_post_on_thread));
                case "forum.like.post":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.like_your_post_on_thread));
            }
        }else if (type.equals("notification-action")) {
            NotificationModel model = (NotificationModel) object;
            String nType = model.getType();
            if (nType.equals("forum.reply.thread") || nType.equals("forum.reply.post") || nType.equals("forum.like.post")) {
                Intent intent = new Intent(context, ForumActivity.class);
                context.startActivity(intent);
            }
        }
        return null;
    }

    @Override
    public Object invoke(Context context, String type) {
        return null;
    }
}
