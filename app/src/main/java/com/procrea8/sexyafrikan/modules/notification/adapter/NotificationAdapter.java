package com.procrea8.sexyafrikan.modules.notification.adapter;


import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.TimeAgo;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.notification.NotificationApiInterface;
import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class NotificationAdapter extends BaseAdapter{
    private ArrayList<NotificationModel> notificationList = new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<ModuleInterface> modules;

    public NotificationAdapter(Context c, ArrayList<NotificationModel> list) {
        this.context = c;
        this.notificationList = list;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        modules = App.getInstance().getModules();
    }

    @Override
    public int getCount() {
        return notificationList.size();
    }

    @Override
    public Object getItem(int i) {
        return notificationList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.notification_item, null);
            viewHolder = new ViewHolder();
            viewHolder.notificationTitle = (TextView) view.findViewById(R.id.notification_title);
            viewHolder.notificationTime = (TextView) view.findViewById(R.id.notification_time);
            viewHolder.notificationDelete = (ImageView) view.findViewById(R.id.notification_delete);
            viewHolder.notificationAvatar = (ImageView) view.findViewById(R.id.notification_avatar);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        final NotificationModel notificationModel = notificationList.get(i);
        Glide.with(context).load(notificationModel.getAvatar()).crossFade().into(viewHolder.notificationAvatar);

        viewHolder.notificationAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notificationAction(notificationModel);
            }
        });

        viewHolder.notificationTime.setText(TimeAgo.format(context, notificationModel.getTime()));
        viewHolder.notificationDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notificationList.remove(i);
                notifyDataSetChanged();
                Call<Response> call = Api.getRetrofit().create(NotificationApiInterface.class).delete(
                        App.getInstance().getSession().getUserid(),
                        notificationModel.getId()
                );
                //MyLog.e("NotificationId- " + notificationModel.getId());
                call.enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        //MyLog.e("Notification deleted successfully");
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        //MyLog.e("notification delete -" + t.getMessage());
                    }
                });
            }
        });

        viewHolder.notificationAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notificationAction(notificationModel);
            }
        });

        viewHolder.notificationTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notificationAction(notificationModel);
            }
        });

        viewHolder.notificationTitle.setText(getNotificationTitle(notificationModel));
        return view;
    }

    public CharSequence getNotificationTitle(NotificationModel notificationModel) {
        for (int i = 0; i < modules.size(); i++) {
            ModuleInterface module = modules.get(i);
            CharSequence str = (CharSequence) module.invoke(context, "notification-own-title", notificationModel);
            if (str != null) return str;
        }
        CharSequence title = TextUtils.concat("", "", "");
        SpannableStringBuilder spanName = new SpannableStringBuilder(notificationModel.getName());
        spanName.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, notificationModel.getName().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        title = TextUtils.concat(title, "",spanName);
        CharSequence aTitle = null;
        for (int i = 0; i < modules.size(); i++) {
            ModuleInterface module = modules.get(i);
            CharSequence str = (CharSequence) module.invoke(context, "notification-title", notificationModel);
            if (str != null) aTitle = str;
        }

        if (aTitle != null) title = TextUtils.concat(title, " ", aTitle);
        return title;
    }

    public void notificationAction(NotificationModel notificationModel) {
        for (int i = 0; i < modules.size(); i++) {
            ModuleInterface module = modules.get(i);
            module.invoke(context, "notification-action", notificationModel);
        }
    }

    static class ViewHolder{
        TextView notificationTitle;
        TextView notificationTime;
        ImageView notificationDelete;
        ImageView notificationAvatar;
    }
}
