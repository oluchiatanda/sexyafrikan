package com.procrea8.sexyafrikan.modules.video.dialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;
import com.procrea8.sexyafrikan.modules.video.VideoApiInterface;
import com.procrea8.sexyafrikan.modules.video.model.Video;
import com.satsuware.usefulviews.LabelledSpinner;

import net.alhazmy13.mediapicker.Video.VideoPicker;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tiamiyu waliu kola on 12/16/2016.
 */
public class VideoCreateDialog implements Contants {
    Context context;
    OnVideoCreated event;

    LabelledSpinner videoType,categories,privacy;

    EditText title,description,link;

    LinearLayout videouploadFields,externalVideoFields;

    Button selectVideo;
    
    ArrayList<MusicCategory> loadedCategories = new ArrayList<>();
    
    String sTitle,sDescription,sLink,sCategory,sPrivacy;

    int addType = 1;

    static String videoPath = "";

    public VideoCreateDialog(Context c) {
        this.context = c;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.create_video);

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.video_create, null);
        videoType = (LabelledSpinner) view.findViewById(R.id.video_type);
        categories = (LabelledSpinner) view.findViewById(R.id.categories);
        privacy = (LabelledSpinner) view.findViewById(R.id.privacy);
        title = (EditText) view.findViewById(R.id.title);
        description = (EditText) view.findViewById(R.id.description);
        link = (EditText) view.findViewById(R.id.link);
        videouploadFields = (LinearLayout) view.findViewById(R.id.video_upload_fields);
        externalVideoFields = (LinearLayout) view.findViewById(R.id.external_video_fields);
        selectVideo = (Button) view.findViewById(R.id.select_video);

        if (ENABLE_VIDEO_UPLOAD) {
            videoType.setItemsArray(R.array.video_types);
        } else {
            videoType.setItemsArray(R.array.video_type);
        }
        privacy.setItemsArray(R.array.privacy_list);

        selectVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ActivityBase activity = (ActivityBase) context;
                activity.setPermissionGranted(new ActivityBase.OnPermissionGranted() {
                    @Override
                    public void onGranted() {
                        VideoPicker builder = new VideoPicker.Builder(activity)
                                .mode(VideoPicker.Mode.GALLERY)
                                .build();
                    }
                });
            }
        });
        final Call<ArrayList<MusicCategory>> call = Api.getRetrofit().create(VideoApiInterface.class).getCategories(App.getInstance().getSession().getUserid());
        call.enqueue(new Callback<ArrayList<MusicCategory>>() {
            @Override
            public void onResponse(Call<ArrayList<MusicCategory>> call, Response<ArrayList<MusicCategory>> response) {
                ArrayList<String> items = new ArrayList<String>();
                for (int i = 0; i < response.body().size(); i++) {
                    items.add(response.body().get(i).getTitle());
                }

                loadedCategories = response.body();
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, items);
                categories.setCustomAdapter(arrayAdapter);
                categories.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
                    @Override
                    public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
                        if (loadedCategories.size() > 0) {
                            sCategory = loadedCategories.get(position).getId();
                        }
                    }

                    @Override
                    public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onFailure(Call<ArrayList<MusicCategory>> call, Throwable t) {

            }
        });
        
        videoType.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
            @Override
            public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
                if (position == 0) {
                    videouploadFields.setVisibility(View.GONE);
                    externalVideoFields.setVisibility(View.VISIBLE);
                    addType = 1;
                } else {
                    videouploadFields.setVisibility(View.VISIBLE);
                    externalVideoFields.setVisibility(View.GONE);
                    addType = 2;
                }
            }

            @Override
            public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

            }
        });
        
        privacy.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
            @Override
            public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
                switch (position) {
                    case 0:
                        sPrivacy = "1";
                        break;
                    case 1:
                        sPrivacy = "2";
                        break;
                    case 2:
                        sPrivacy = "3";
                        break;
                }
            }

            @Override
            public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

            }
        });

        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok, null).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        progressDialog.setMessage(context.getResources().getString(R.string.adding));
                        progressDialog.setCancelable(false);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressDialog.show();

                       sTitle = title.getText().toString();
                        sDescription = description.getText().toString();
                        sLink  = link.getText().toString();

                        if (addType == 1) {
                            //external link
                            if (sLink.isEmpty()) {
                                link.setError(context.getString(R.string.provide_your_video_link));
                                return;
                            }
                        } else {
                            //video upload
                            if (sTitle.isEmpty()) {
                                title.setError(context.getString(R.string.provide_video_title));
                                return;
                            }

                            if (videoPath.isEmpty()) {
                                Toast.makeText(context, R.string.select_a_video, Toast.LENGTH_LONG).show();
                                return;
                            }
                        }

                        Call<Video> call1;

                        if (videoPath.isEmpty()) {
                            call1 = Api.getRetrofit().create(VideoApiInterface.class).create(
                                    App.getInstance().getSession().getUserid(),
                                    sLink,
                                    sTitle,
                                    sDescription,
                                    sPrivacy,
                                    sCategory
                            );
                        } else {
                            MultipartBody.Part body = null;
                            File file = new File(videoPath);
                            //MyLog.e("Image path - " + filePath);
                            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            body = MultipartBody.Part.createFormData("video", file.getName(), requestBody);
                            call1 = Api.getRetrofit().create(VideoApiInterface.class).create(
                                    App.getInstance().getSession().getUserid(),
                                    sLink,
                                    sTitle,
                                    sDescription,
                                    sPrivacy,
                                    sCategory,
                                    body
                            );
                        }

                        call1.enqueue(new Callback<Video>() {
                            @Override
                            public void onResponse(Call<Video> call, Response<Video> response) {
                                progressDialog.dismiss();
                                if (response.body().getStatus()==0) {
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                } else {
                                    Video video = response.body();
                                    if (event != null) {
                                        event.onCreate(video);
                                    }
                                    dialog.dismiss();
                                }

                            }

                            @Override
                            public void onFailure(Call<Video> call, Throwable t) {
                                progressDialog.dismiss();
                            }
                        });
                    }
                });
            }
        });
        dialog.show();
    }

    public void addEvent(OnVideoCreated e) {
        this.event = e;
    }

    public static void setVideoPath(String path) {
        videoPath = path;
    }

    public static class OnVideoCreated{
        public void onCreate(Video video) {

        }
    }
}
