package com.procrea8.sexyafrikan.modules.mediachat.activity;

import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;

import de.hdodenhof.circleimageview.CircleImageView;

public class IncommingCallActivity extends ActivityBase {

    static String identity = "";
    static String connectionId = "";
    static String identityName = "";
    static String identityAvatar = "";
    static String type = "";

    TextView callTitle,vIdentityName;
    LinearLayout rejectButton,acceptButton;
    CircleImageView vIdentityAvatar;

    Context context;

    Ringtone ringTone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incomming_call);

        context = this;
        callTitle = (TextView) findViewById(R.id.call_title);
        vIdentityName = (TextView) findViewById(R.id.identity_name);
        vIdentityAvatar = (CircleImageView) findViewById(R.id.identity_avatar);
        rejectButton = (LinearLayout) findViewById(R.id.reject_call);
        acceptButton = (LinearLayout) findViewById(R.id.accept_call);

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            ringTone = RingtoneManager.getRingtone(getApplicationContext(), notification);
            ringTone.play();
        } catch (Exception e) {
            //e.printStackTrace();
        }

        //end it if after 60 seconds user didn't take any action
        Thread timer = new Thread(){
            public void run(){
                try{
                    sleep(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    endCall();
                }
            }
        };
        timer.start();

        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                endCall();
            }
        });
        vIdentityName.setText(identityName);
        Glide.with(this).load(identityAvatar).into(vIdentityAvatar);

        if (type.equals("video")) {
            callTitle.setText(R.string.incomming_video_call);
        } else {
            callTitle.setText(R.string.incomming_voice_call);
        }

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer t = (type.equals("video")) ? 1 : 2;
                CallActivity.initCall(context, t, identity, identityName,  identityAvatar, "receive", connectionId);
                endCall();
            }
        });
    }

    public void endCall() {
        ringTone.stop();
        finish();
    }

    public static void launch(Context context, String id, String conId, String idName, String idAvatar, String t) {
        identity = id;
        connectionId = conId;
        identityName = idName;
        identityAvatar = idAvatar;
        type = t;
        Intent intent = new Intent(context, IncommingCallActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

}
