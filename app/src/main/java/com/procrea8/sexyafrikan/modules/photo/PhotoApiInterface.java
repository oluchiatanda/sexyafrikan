package com.procrea8.sexyafrikan.modules.photo;


import com.procrea8.sexyafrikan.modules.photo.model.Photo;
import com.procrea8.sexyafrikan.modules.photo.model.PhotoAlbum;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface PhotoApiInterface {
    @GET("photo/album/add")
    Call<Response> addAlbum(@Query("userid") String userid, @Query("title") String title, @Query("desc") String desc, @Query("privacy") String privacy);

    @GET("photo/album/photos")
    Call<ArrayList<Photo>> getAlbumPhotos(@Query("userid") String userid, @Query("album_id") String albumId, @Query("limit") Integer limit, @Query("offset") Integer offset, @Query("the_userid") String theUserid);

    @GET("photo/album/details")
    Call<PhotoAlbum> getAlbum(@Query("userid") String userid, @Query("album_id") String id);

    @Multipart
    @POST("photo/album/upload")
    Call<Photo> upload(@Query("userid") String userid, @Query("album_id") String id, @Part MultipartBody.Part photo);

    @GET("photo/album/delete")
    Call<Response> deleteAlbum(@Query("userid") String userid, @Query("album_id") String id);

    @GET("photo/album/edit")
    Call<Response> editAlbum(@Query("userid") String userid, @Query("album_id") String id, @Query("title") String title, @Query("desc") String desc, @Query("privacy") String privacy);

    @GET("photo/details")
    Call<Photo> getDetails(@Query("userid") String userid, @Query("photo") String id);
}
