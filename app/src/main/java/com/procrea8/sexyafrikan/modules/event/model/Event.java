package com.procrea8.sexyafrikan.modules.event.model;

import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.response.Response;


public class Event extends Response {
    @SerializedName("id")
    String id;
    @SerializedName("title")
    String title;
    @SerializedName("description")
    String description;
    @SerializedName("location")
    String location;
    @SerializedName("address")
    String address;
    @SerializedName("image")
    String image;
    @SerializedName("cover")
    String cover;
    @SerializedName("is_admin")
    Boolean isAdmin;
    @SerializedName("rsvp")
    String rsvp;
    @SerializedName("count_going")
    String countGoing;
    @SerializedName("count_maybe")
    String countMaybe;
    @SerializedName("count_invited")
    String countInvited;
    @SerializedName("event_time")
    String eventTime;

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getRsvp() {
        return rsvp;
    }

    public void setRsvp(String rsvp) {
        this.rsvp = rsvp;
    }

    public String getCountGoing() {
        return countGoing;
    }

    public void setCountGoing(String countGoing) {
        this.countGoing = countGoing;
    }

    public String getCountMaybe() {
        return countMaybe;
    }

    public void setCountMaybe(String countMaybe) {
        this.countMaybe = countMaybe;
    }

    public String getCountInvited() {
        return countInvited;
    }

    public void setCountInvited(String countInvited) {
        this.countInvited = countInvited;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }
}
