package com.procrea8.sexyafrikan.modules.music.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.NotificationTarget;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.activity.AppActivity;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.music.MusicApiInterface;
import com.procrea8.sexyafrikan.modules.music.dialog.MusicCreateDialog;
import com.procrea8.sexyafrikan.modules.music.model.Music;
import com.procrea8.sexyafrikan.response.Response;

import retrofit2.Call;
import retrofit2.Callback;

public class MusicPlayerActivity extends ActivityBase {
    static Music music;
    static String musicId;

    ImageView largeCover;
    TextView coverTitle;
    ImageView thumbnail;
    TextView musicTitle;
    ImageView actionButton;
    ImageView stopButton;
    SeekBar progressBar;
    TextView timeProgress;
    TextView artistName;


    static MediaPlayer mediaPlayer;
    Boolean playing = false;
    Boolean paused = false;
    Boolean seekbarChanging = false;

    Integer notificationId = 200;
    NotificationManager notificationManager;

    ImageView editButton;
    ImageView deleteButton;
    LinearLayout layout_share;

    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_player);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        largeCover = (ImageView) findViewById(R.id.large_cover);
        coverTitle = (TextView) findViewById(R.id.cover_title);
        thumbnail = (ImageView) findViewById(R.id.thumbnail);
        musicTitle = (TextView) findViewById(R.id.music_title);
        actionButton = (ImageView) findViewById(R.id.action_button);
        stopButton = (ImageView) findViewById(R.id.stop_button);
        progressBar = (SeekBar) findViewById(R.id.progress_bar);
        timeProgress = (TextView) findViewById(R.id.time_progress);
        artistName = (TextView) findViewById(R.id.artist);
        editButton = (ImageView) findViewById(R.id.edit);
        deleteButton = (ImageView) findViewById(R.id.delete);
        layout_share = (LinearLayout) findViewById(R.id.ex_share_button);

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        context = this;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (music == null) {
            onBackPressed();
            return;
        }

        setMusicDetails();
        if (music.getCanEdit()) {
            editButton.setVisibility(View.VISIBLE);
            deleteButton.setVisibility(View.VISIBLE);
        } else {
            editButton.setVisibility(View.GONE);
            deleteButton.setVisibility(View.GONE);
        }

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MusicCreateDialog createDialog = new MusicCreateDialog(context, "edit", music);
                createDialog.addListener(new MusicCreateDialog.OnMusicCreatedListener(){
                    @Override
                    public void onMusicCreated(Music newMusic) {
                        music = newMusic;
                        setMusicDetails();
                    }
                });
            }
        });

        layout_share.setVisibility(View.VISIBLE);
        layout_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("music/*");
                if(music.getSludge().equals("")) {
                    String shareUrl = SITE_BASE_URL + "music/" + music.getId();
                    intent.putExtra(android.content.Intent.EXTRA_TEXT, shareUrl);
                }
                else{
                    String shareUrl = SITE_BASE_URL + "music/" + music.getSludge();
                    intent.putExtra(android.content.Intent.EXTRA_TEXT, shareUrl);
                }
                context.startActivity(Intent.createChooser(intent, "Share via"));
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder confirmDialog = new AlertDialog.Builder(context);
                confirmDialog.setMessage(R.string.are_you_sure);
                confirmDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        setLoading(getResources().getString(R.string.deleting));
                        Call<Response> call = Api.getRetrofit().create(MusicApiInterface.class).delete(
                                App.getInstance().getSession().getUserid(),
                                music.getId()
                        );
                        call.enqueue(new Callback<Response>() {
                            @Override
                            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                hideLoading();
                                Intent intent = new Intent(context, AppActivity.class);
                                startActivity(intent);
                                finish();
                            }

                            @Override
                            public void onFailure(Call<Response> call, Throwable t) {

                            }
                        });
                    }
                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
            }
        });

        initGlobalFooter(this, "music", music.getId(), music.isHasReact(), music.isHasLike(),
                music.isHasDislike(),
                music.getLikeCount(), music.getDislikeCount(), music.getComments());

        if (mediaPlayer != null) {
            if (musicId != null && musicId.equals(music.getId())) {
                //progressBar.setProgress(0);
                progressBar.setMax(mediaPlayer.getDuration());
                //timeProgress.setText(milliSecondsToTimer(mediaPlayer.getDuration() - mediaPlayer.getCurrentPosition()));
                playing = true;
                paused = false;
                actionButton.setImageResource(R.drawable.ic_pause_circle_grey_large);
                mediaPlayer.start();
                showNotification();

            } else {
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            }
            MyLog.e("One music is attached");
        } else {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }
        autoPlay();
        musicId = music.getId();
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                //actionButton.setImageResource(R.drawable.ic_play_circle_grey);
                progressBar.setProgress(0);
                progressBar.setMax(mp.getDuration());

                //updateSeekBar(mp);
                timeProgress.setText(milliSecondsToTimer(mp.getDuration()));
                showNotification();
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                actionButton.setImageResource(R.drawable.ic_play_circle_grey_large);
                progressBar.setProgress(0);
                playing = false;
                paused = false;
                mediaPlayer.stop();
                mediaPlayer.reset();
                hideNotification();
            }
        });

        mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int i) {
                if (mediaPlayer.isPlaying()) {
                    progressBar.setProgress(mediaPlayer.getCurrentPosition());
                    timeProgress.setText(milliSecondsToTimer(mediaPlayer.getDuration() - mediaPlayer.getCurrentPosition()));
                }
            }
        });
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mediaPlayer.isPlaying() && playing) {
                    //we want to pause
                    actionButton.setImageResource(R.drawable.ic_play_circle_grey_large);
                    mediaPlayer.pause();
                    playing = false;
                    paused = true;
                    hideNotification();
                } else {
                    actionButton.setImageResource(R.drawable.ic_pause_circle_grey_large);
                    if (paused) {
                        mediaPlayer.start();
                        paused = false;
                        showNotification();
                    } else {
                        try {
                            mediaPlayer.setDataSource(music.getFile());
                            mediaPlayer.prepareAsync();
                        } catch (Exception e) {
                        }
                    }
                    playing = true;
                }
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.stop();
                mediaPlayer.reset();
                actionButton.setImageResource(R.drawable.ic_play_circle_grey_large);
                progressBar.setProgress(0);
                hideNotification();
            }
        });

        progressBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (mediaPlayer.isPlaying() && mediaPlayer.getCurrentPosition() != i && seekbarChanging) {
                    mediaPlayer.seekTo(i);
                    seekbarChanging = false;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekbarChanging = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void setMusicDetails() {
        Glide.with(this).load(music.getCover()).crossFade().into(largeCover);
        Glide.with(this).load(music.getCover()).crossFade().into(thumbnail);
        coverTitle.setText(music.getTitle());
        musicTitle.setText(music.getTitle());
        artistName.setText(music.getArtist());
    }

    public static void launch(Context context, Music song) {
        music = song;
        Intent intent = new Intent(context, MusicPlayerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        context.startActivity(intent);
    }

    public void autoPlay() {
        if (mediaPlayer != null) {
            if (musicId != null && musicId.equals(music.getId())) {
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
            } else {
                startPlay();
            }
        } else {
            startPlay();
        }
    }

    public void startPlay() {
        actionButton.setImageResource(R.drawable.ic_pause_circle_grey_large);
        try {
            mediaPlayer.setDataSource(music.getFile());
            mediaPlayer.prepareAsync();
        } catch (Exception e) {
        }
        playing = true;
    }

    public void showNotification() {
        final RemoteViews rv = new RemoteViews(this.getPackageName(), R.layout.music_player_notification);
        rv.setImageViewResource(R.id.music_icon, R.mipmap.ic_launcher);
        rv.setTextViewText(R.id.music_title, music.getTitle());
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_play_circle_grey)
                .setContentTitle(music.getTitle())
                .setContent(rv)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setOngoing(true)
                .setAutoCancel(false);
        Intent musicIntent = new Intent(this, MusicPlayerActivity.class);
        android.support.v4.app.TaskStackBuilder stackBuilder = android.support.v4.app.TaskStackBuilder.create(this);
        stackBuilder.addParentStack(AppActivity.class);
        stackBuilder.addNextIntent(musicIntent);
        PendingIntent musicPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(musicPendingIntent);
        final Notification notification = builder.build();
        //notification.flags = Notification.FLAG_ONGOING_EVENT;
        if (Build.VERSION.SDK_INT >= 16) {
            notification.bigContentView = rv;
        }

        notificationManager.notify(notificationId, notification);
        NotificationTarget target = new NotificationTarget(this, rv, R.id.music_icon, notification, notificationId);
        Glide.with(this.getApplicationContext())
                .load(music.getCover())
                .asBitmap()
                .into(target);
    }

    public void hideNotification() {
        notificationManager.cancel(notificationId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /**mediaPlayer.stop();
        mediaPlayer.reset();
        mediaPlayer.release();
        mediaPlayer = null;
        notificationManager.cancel(notificationId);*/
    }
}
