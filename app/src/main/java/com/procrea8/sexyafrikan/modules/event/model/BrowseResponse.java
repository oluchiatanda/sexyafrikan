package com.procrea8.sexyafrikan.modules.event.model;


import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;

import java.util.ArrayList;

public class BrowseResponse {
    @SerializedName("categories")
    ArrayList<MusicCategory> categories;
    @SerializedName("events")
    ArrayList<Event> events;

    public ArrayList<MusicCategory> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<MusicCategory> categories) {
        this.categories = categories;
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Event> events) {
        this.events = events;
    }
}
