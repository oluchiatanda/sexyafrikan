package com.procrea8.sexyafrikan.modules.forum.fragments;


import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.hashtag.activity.HashtagActivity;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;
import com.wang.avi.AVLoadingIndicatorView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForumFragment extends Fragment implements Contants {
    WebView webView;
    String forumUrl = SITE_BASE_URL + "forum?webview=true";
    AVLoadingIndicatorView loadImage;
    public ForumFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forum, container, false);
        webView = (WebView) view.findViewById(R.id.container);
        loadImage = (AVLoadingIndicatorView) view.findViewById(R.id.loading_image);
        WebSettings settings = webView.getSettings();
        forumUrl = forumUrl + "?api_userid=" + App.getInstance().getSession().getUserid();
        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setAppCacheEnabled(false);
        //webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new AppWebViewClient());
        webView.clearCache(true);

        webView.loadUrl(forumUrl);
        loadImage.show();
        //webView.loa
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    class AppWebViewClient extends android.webkit.WebViewClient implements Contants{
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            loadImage.show();
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            webView.clearCache(true);
            loadImage.hide();
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, final String url) {
            if (url.startsWith("hashtag:")) {
                String[] str = url.split(":");
                String hashtag = str[1];
                HashtagActivity.launch(view.getContext(), hashtag);
            } else if(url.startsWith("mention:")) {
                String[] str = url.split(":");
                String userid = str[1];
                ProfileActivity.load(view.getContext(), userid);
            } else {
                if (Uri.parse(url).getHost().endsWith(DOMAIN)) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String userid = App.getInstance().getSession().getUserid();
                            String newUrl = url;
                            if (url.contains("&")) {
                                newUrl = url.concat("&webview=true&api_userid=" +userid);
                            } else {
                                newUrl = url.concat("?webview=true&api_userid=" +userid) ;
                            }
                            webView.loadUrl(newUrl);
                            //Toast.makeText(getContext(), "THis is Url - " + newUrl, Toast.LENGTH_LONG).show();
                        }
                    });
                    return true;
                } else {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    getActivity().startActivity(intent);
                }

            }
            return true;
        }

    }
}
