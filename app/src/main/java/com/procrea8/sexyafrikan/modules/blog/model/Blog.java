package com.procrea8.sexyafrikan.modules.blog.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Blog {
    @SerializedName("id")
    String id;
    @SerializedName("title")
    String title;
    @SerializedName("cover")
    String cover;
    @SerializedName("time")
    String time;
    @SerializedName("content")
    String content;
    @SerializedName("image")
    String image;
    @SerializedName("slug")
    String sludge;

    public String getSludge(){return sludge;}
    public void setSludge(String sludge) { this.sludge = sludge; }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @SerializedName("has_like")
    boolean hasLike = false;
    @SerializedName("has_dislike")
    boolean hasDislike = false;
    @SerializedName("has_react")
    boolean hasReact = false;
    @SerializedName("like_count")
    Integer likeCount = 0;
    @SerializedName("dislike_count")
    Integer dislikeCount = 0;

    @SerializedName("comments")
    Integer comments;



    public boolean isHasLike() {
        return hasLike;
    }

    public void setHasLike(boolean hasLike) {
        this.hasLike = hasLike;
    }

    public boolean isHasDislike() {
        return hasDislike;
    }

    public void setHasDislike(boolean hasDislike) {
        this.hasDislike = hasDislike;
    }

    public boolean isHasReact() {
        return hasReact;
    }

    public void setHasReact(boolean hasReact) {
        this.hasReact = hasReact;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getDislikeCount() {
        return dislikeCount;
    }

    public void setDislikeCount(Integer dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    public Integer getComments() {
        return comments;
    }

    public void setComments(Integer comments) {
        this.comments = comments;
    }

    public ArrayList<ArrayList<String>> getReactMembers() {
        return reactMembers;
    }

    public void setReactMembers(ArrayList<ArrayList<String>> reactMembers) {
        this.reactMembers = reactMembers;
    }

    @SerializedName("react_members")
    ArrayList<ArrayList<String>> reactMembers;


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }
}
