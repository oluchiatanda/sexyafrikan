package com.procrea8.sexyafrikan.modules.feed;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.mikepenz.materialdrawer.Drawer;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.feed.activity.FeedActivity;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;
import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;

import java.util.ArrayList;


public class FeedModule implements ModuleInterface {
    @Override
    public void registerMenu(Context context, Drawer drawer, int i) {
        //drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_home).withName("News Feed"));
    }

    @Override
    public Fragment getFragment(Context context) {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        return "News Feed";
    }

    @Override
    public Object invoke(Context context, String type, Object object) {
        if (type.equals("notification-title")) {
            NotificationModel model = (NotificationModel) object;
            switch (model.getType()) {
                case "feed.like":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.like_your_post));
                case "feed.like.react":
                    if (model.getTitle().equals("1")) return TextUtils.concat("", "", context.getResources().getString(R.string.like_your_post));
                    return TextUtils.concat("", "", context.getResources().getString(R.string.reacted_to_your_post));
                case "feed.dislike":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.dislike_your_post));
                case "feed.like.comment":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.like_your_comment));
                case "feed.dislike.comment":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.dislike_your_comment));
                case "feed.comment":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.commented_on_post));
                case "feed.comment.reply":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.reply_your_comment));
                case "feed.shared":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.shared_your_post));
                case "feed.tag":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.tag_you_in_post));
                case "post-on-timeline":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.posted_on_your_timeline));
            }
        } else if(type.equals("notification-action")) {
            NotificationModel model = (NotificationModel) object;
            String nType = model.getType();
            ArrayList<String> types = new ArrayList<>();
            String[] str  = {"feed.like",
                    "post-on-timeline","feed.like.react",
                    "feed.dislike", "feed.like.comment", "feed.comment", "feed.comment.reply", "feed.shared","feed.tag"};
            for(int i = 0; i < str.length;i++) {
                types.add(str[i]);
            }
            if (types.contains(nType)) {
                FeedResponse feed = model.getFeeds().get(0);
                FeedActivity.load(context, feed);
            }

        }
        return null;
    }

    @Override
    public Object invoke(Context context, String type) {
        return null;
    }


}
