package com.procrea8.sexyafrikan.modules.profile;

import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.photo.model.Photo;
import com.procrea8.sexyafrikan.modules.photo.model.PhotoAlbum;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ProfileApiInterface {
    @GET("profile/details")
    Call<User> get(@Query("userid") String userid, @Query("the_userid") String theUserid);

    @Multipart
    @POST("profile/change/avatar")
    Call<Response> changeAvatar(@Query("userid") String userid, @Part MultipartBody.Part photo);

    @Multipart
    @POST("profile/change/cover")
    Call<Response> changeCover(@Query("userid") String userid, @Part MultipartBody.Part photo);

    @GET("profile/friends")
    Call<ArrayList<User>> getFriends(@Query("userid") String userid, @Query("the_userid") String theUserid, @Query("page") String page);

    @GET("profile/friends")
    Call<ArrayList<User>> getFriends(@Query("userid") String userid, @Query("the_userid") String theUserid, @Query("page") String page, @Query("term") String term);

    @GET("profile/photos")
    Call<ArrayList<Photo>> getPhotos(@Query("userid") String userid, @Query("the_userid") String theUserid, @Query("limit") Integer limit, @Query("offset") Integer offset);

    @GET("profile/albums")
    Call<ArrayList<PhotoAlbum>> getAlbums(@Query("userid") String userid, @Query("the_userid") String theUserid, @Query("limit") Integer limit, @Query("offset") Integer offset);


}
