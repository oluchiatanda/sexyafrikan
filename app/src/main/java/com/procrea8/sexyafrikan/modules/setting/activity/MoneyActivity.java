package com.procrea8.sexyafrikan.modules.setting.activity;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.modules.setting.SettingsApiInterface;
import com.procrea8.sexyafrikan.response.Response;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by pRocrea8 on 11/1/2017.
 */

public class MoneyActivity extends ActivityBase {
    EditText corporateEmail, notificationEmail, stripeSecretKey, stipePublishableKey;
    Spinner enablePaypal, enableStripe;
    Button save;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money_transaction);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = this;
        save = (Button) findViewById(R.id.save_button);
        corporateEmail = (EditText) findViewById(R.id.corporateEmail);
        notificationEmail = (EditText) findViewById(R.id.notificationEmail);
        stripeSecretKey = (EditText) findViewById(R.id.stripeSKey);
        stipePublishableKey = (EditText) findViewById(R.id.stripePKey);
        enablePaypal = (Spinner) findViewById(R.id.enablePaypal);
        enableStripe = (Spinner) findViewById(R.id.enableStripe);

        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(this, R.array.enable, android.R.layout.select_dialog_item);
        enableStripe.setAdapter(adapter);
        enablePaypal.setAdapter(adapter);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!corporateEmail.getText().toString().isEmpty() && !stripeSecretKey.getText().toString().isEmpty() && !stipePublishableKey.getText().toString().isEmpty()) {
                    setLoading();
                    Call<Response> call = Api.getRetrofit().create(SettingsApiInterface.class).updateMoney(
                            App.getInstance().getSession().getUserid(),
                            enablePaypal.getSelectedItem().toString(),
                            corporateEmail.getText().toString(),
                            enableStripe.getSelectedItem().toString(),
                            stripeSecretKey.getText().toString(),
                            stipePublishableKey.getText().toString()
                    );
                    call.enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                            hideLoading();
                            if (response.body().getStatus()==1) {
                                Toast.makeText(context, R.string.settings_saved, Toast.LENGTH_LONG).show();
                                onBackPressed();
                                finish();
                            }else {
                                Toast.makeText(getApplicationContext(), R.string.failed_password_changed, Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {
                            hideLoading();
                            Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });


    }
}
