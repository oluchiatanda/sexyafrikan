package com.procrea8.sexyafrikan.modules.group;


import com.procrea8.sexyafrikan.modules.group.model.Group;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface GroupApiInterface {
    @GET("group/browse")
    Call<ArrayList<Group>> browse(@Query("userid") String userid, @Query("term") String term,
                                @Query("type") String type, @Query("limit") Integer limit,
                                @Query("page") Integer page);
    @GET("group/delete")
    Call<Response> delete(@Query("userid") String userid, @Query("group_id") String eventId);

    @GET("group/join")
    Call<Response> join(@Query("userid") String userid, @Query("group_id") String eventId, @Query("status") String status);

    @Multipart
    @POST("group/cover")
    Call<Response> changeCover(@Query("userid") String userid, @Query("group_id") String eventId, @Part MultipartBody.Part photo);

    @Multipart
    @POST("group/logo")
    Call<Response> changeLogo(@Query("userid") String userid, @Query("group_id") String eventId, @Part MultipartBody.Part photo);

    @POST("group/create")
    Call<Group> create(@Query("userid") String userid, @Query("title") String title, @Query("description") String description,
                       @Query("name") String name,@Query("privacy") String privacy);

    @POST("group/edit")
    Call<Group> edit(@Query("userid") String userid, @Query("title") String title, @Query("description") String description,
                       @Query("name") String name,@Query("privacy") String privacy, @Query("group_id") String groupId);
}
