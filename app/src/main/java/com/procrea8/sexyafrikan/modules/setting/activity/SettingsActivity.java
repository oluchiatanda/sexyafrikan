package com.procrea8.sexyafrikan.modules.setting.activity;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.modules.setting.SettingsApiInterface;
import com.procrea8.sexyafrikan.response.Response;

import retrofit2.Call;
import retrofit2.Callback;

public class SettingsActivity extends ActivityBase {
    static SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        Fragment fragment = new MainSettingFragment();
        Bundle bundle = new Bundle();

        Intent intent = getIntent();
        String setting = "main";
        if (intent.getDataString() != null && !intent.getDataString().isEmpty())
            setting = intent.getDataString();

        bundle.putString("setting", setting);
        fragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.settings_frame, fragment).commit();
    }

    public static void saveSettings(final Context context) {
        String firstName = pref.getString("firstname", "");
        String bio = pref.getString("bio", "");
//        String email = pref.getString("email", "");
        String lastName = pref.getString("lastname", "");
        String state = pref.getString("state", "");
        String city = pref.getString("city", "");
        Boolean follow = pref.getBoolean("notify-following-you", true);
        String followStr = (follow) ? "1" : "0";
        Boolean mention = pref.getBoolean("notify-site-mention-you", true);
        String mentionStr = (mention) ? "1" : "0";
        Boolean tag = pref.getBoolean("notify-site-tag-you", true);
        String tagStr = (tag) ? "1" : "0";
        Boolean comment = pref.getBoolean("notify-site-comment", true);
        String commentStr = (comment) ? "1" : "0";
        Boolean reply = pref.getBoolean("notify-site-reply-comment", true);
        String replyStr = (reply) ? "1" : "0";
        Boolean like = pref.getBoolean("notify-site-like", true);
        String likeStr = (like) ? "1" : "0";
        //Boolean friends = pref.getBoolean("turn_on_friend_requests", true);
        String profile = pref.getString("who_can_view_profile", "1");
        String post = pref.getString("who_can_post_profile", "1");
        String birth = pref.getString("who_can_see_birth", "2");
        Call<Response> call = Api.getRetrofit().create(SettingsApiInterface.class).save(
                App.getInstance().getSession().getUserid(),
                firstName, lastName, bio, city, state, followStr, mentionStr, tagStr, commentStr, replyStr, likeStr, profile, post, birth
        );
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Toast.makeText(context, R.string.settings_saved, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
            }
        });
    }

    public static void updatePaymentSettings(final Context context) {
        String enablePaypal = pref.getString("enable_paypal_payment_gateway", "2");
        String paypalCorporateEmail = pref.getString("paypal_gateway_corporate_email_address", "");
        String enableStripe = pref.getString("enable_stripe_payment_gateway", "2");
        String stripeSecretKey = pref.getString("stripe_secret_key", "");
        String stipePublishableKey = pref.getString("stripe_publishable_key", "");

        Call<Response> call = Api.getRetrofit().create(SettingsApiInterface.class).updateMoney(
                App.getInstance().getSession().getUserid(),
                enablePaypal,
                paypalCorporateEmail,
                enableStripe,
                stripeSecretKey,
                stipePublishableKey
        );
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Toast.makeText(context, R.string.settings_saved, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
            }
        });
    }

    public static class MainSettingFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle bundle = getArguments();
            String setting = bundle.getString("setting", "main");
            switch (setting) {
                case "main":
                    addPreferencesFromResource(R.xml.main_settings);
                    break;
                case "general":
                    addPreferencesFromResource(R.xml.general_settings);
                    SharedPreferences pref = getPreferenceManager().getSharedPreferences();
                    Preference firstName = findPreference("firstname");
                    firstName.setSummary(pref.getString("firstname", ""));
                    Preference lastName = findPreference("lastname");
                    lastName.setSummary(pref.getString("lastname", ""));
//                    Preference email = findPreference("email");
//                    email.setSummary(pref.getString("email", ""));
                    Preference bio = findPreference("bio");
                    bio.setSummary(App.getInstance().getSession().getBio());
                    Preference city = findPreference("city");
                    city.setSummary(pref.getString("city", ""));
                    Preference state = findPreference("state");
                    state.setSummary(pref.getString("state", ""));

                    break;
                case "notifications":
                    addPreferencesFromResource(R.xml.notification_settings);
                    break;
                case "privacy":
                    addPreferencesFromResource(R.xml.privacy_settings);
                    break;
                case "money":
                    addPreferencesFromResource(R.xml.money_payment_detils);
                    SharedPreferences prefs = getPreferenceManager().getSharedPreferences();
                    Preference paypal_gateway_corporate_email_address = findPreference("paypal_gateway_corporate_email_address");
                    paypal_gateway_corporate_email_address.setSummary(prefs.getString("paypal_gateway_corporate_email_address", ""));

                    Preference paypal_gatewa_notification_email_address = findPreference("paypal_gatewa_notification_email_address");
                    paypal_gatewa_notification_email_address.setSummary(prefs.getString("paypal_gatewa_notification_email_address", ""));

                    Preference stripe_secret_key = findPreference("stripe_secret_key");
                    stripe_secret_key.setSummary(prefs.getString("stripe_secret_key", ""));

                    Preference stripe_publishable_key = findPreference("stripe_publishable_key");
                    stripe_publishable_key.setSummary(prefs.getString("stripe_publishable_key", ""));
                    break;
            }

        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals("firstname") || key.equals("lastname") || key.equals("bio") || key.equals("city") || key.equals("state") || key.equals("paypal_gateway_corporate_email_address") || key.equals("paypal_gatewa_notification_email_address") || key.equals("stripe_secret_key") || key.equals("stripe_publishable_key")) {
                Preference preference = findPreference(key);
                preference.setSummary(sharedPreferences.getString(key, ""));
            }
            SettingsActivity.saveSettings(getActivity());
            SettingsActivity.updatePaymentSettings(getActivity());
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }
    }


}
