package com.procrea8.sexyafrikan.modules.relationship.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;
import com.procrea8.sexyafrikan.modules.relationship.RelationshipApiInterface;
import com.procrea8.sexyafrikan.response.Response;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class RequestAdapter extends BaseAdapter {
    Context context;
    ArrayList<User> requests;
    LayoutInflater layoutInflater;

    public RequestAdapter(Context c, ArrayList<User> requests) {
        this.context = c;
        this.requests = requests;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return requests.size();
    }

    @Override
    public Object getItem(int i) {
        return requests.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final MyViewHolder holder;
        if (view == null) {
            view = (View) layoutInflater.inflate(R.layout.friend_request_item, null);
            holder = new MyViewHolder();
            holder.avatar = (CircleImageView) view.findViewById(R.id.avatar);
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.acceptButton = (Button) view.findViewById(R.id.accept_button);
            holder.rejectButton = (TextView) view.findViewById(R.id.reject_button);
            holder.acceptLoadingIndicator = (AVLoadingIndicatorView) view.findViewById(R.id.loading_image);
            view.setTag(holder);
        } else {
            holder = (MyViewHolder) view.getTag();
        }

        final User user = requests.get(i);
        Glide.with(context).load(user.getAvatar()).crossFade().into(holder.avatar);
        holder.name.setText(user.getName());
        holder.acceptLoadingIndicator.hide();
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfileActivity.load(context, user.getId());
            }
        });
        holder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfileActivity.load(context, user.getId());
            }
        });
        holder.acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.acceptButton.setVisibility(View.GONE);
                holder.acceptLoadingIndicator.show();
                Call<Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).accept(
                        App.getInstance().getSession().getUserid(),
                        user.getId()
                );
                call.enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        holder.acceptLoadingIndicator.hide();
                        holder.rejectButton.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        holder.rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).remove(
                        App.getInstance().getSession().getUserid(),
                        user.getId()
                );
                call.enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        requests.remove(i);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        return view;
    }

    static class MyViewHolder{
        CircleImageView avatar;
        TextView name;
        TextView rejectButton;
        Button acceptButton;
        AVLoadingIndicatorView acceptLoadingIndicator;
    }
}
