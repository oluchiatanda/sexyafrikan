package com.procrea8.sexyafrikan.modules.hashtag.model;


import com.google.gson.annotations.SerializedName;

public class Hashtags {
    @SerializedName("tag")
    String tag;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
