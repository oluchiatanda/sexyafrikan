package com.procrea8.sexyafrikan.modules.feed;

import com.procrea8.sexyafrikan.modules.feed.response.FeedAddResponse;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface FeedApiInterface {
    @GET("feeds")
    Call<ArrayList<FeedResponse>> getFeeds(@Query("userid") String userid, @Query("type") String type,
                                           @Query("type_id") String typeId, @Query("limit") String limit, @Query("offset") String offset);

    @Multipart
    @POST("feed/add")
    Call<FeedAddResponse> add(@Query("userid") String userid, @Query("type")
    String type, @Query("type_id") String typeId, @Query("text") String text,@Query("entity_type") String entityType,
                              @Query("entity_id") String entityId, @Query("location") String location,
                              @Query("to_user_id") String toUserid, @Query("privacy") String privacy,
                              @Query("feeling_type") String feelingType, @Query("feeling_text") String feelingText,
                              @Query("is_poll") String is_poll, @Query("poll_option_one") String optionOne,
                              @Query("poll_option_two") String optionTwo, @Query("poll_option_three") String optionThree,
                              @Query("poll_multiple") String pollMultiple, @Query("tags") String tags, @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("feed/add")
    Call<FeedAddResponse> add(@Field("userid") String userid, @Query("type")
    String type, @Query("type_id") String typeId, @Query("text") String text,@Query("entity_type") String entityType,
                              @Query("entity_id") String entityId, @Query("location") String location,
                              @Query("to_user_id") String toUserid, @Query("privacy") String privacy,
                              @Query("feeling_type") String feelingType, @Query("feeling_text") String feelingText,
                              @Query("is_poll") String is_poll, @Query("poll_option_one") String optionOne,
                              @Query("poll_option_two") String optionTwo, @Query("poll_option_three") String optionThree,
                              @Query("poll_multiple") String pollMultiple, @Query("tags") String tags);

    @Multipart
    @POST("feed/add")
    Call<FeedAddResponse> add(@Query("userid") String userid, @Query("type")
    String type, @Query("type_id") String typeId, @Query("text") String text,@Query("entity_type") String entityType,
                              @Query("entity_id") String entityId, @Query("location") String location,
                              @Query("to_user_id") String toUserid, @Query("privacy") String privacy,
                              @Query("feeling_type") String feelingType, @Query("feeling_text") String feelingText,
                              @Query("is_poll") String is_poll, @Query("poll_option_one") String optionOne,
                              @Query("poll_option_two") String optionTwo, @Query("poll_option_three") String optionThree,
                              @Query("poll_multiple") String pollMultiple, @Query("tags") String tags,
                              @Part MultipartBody.Part image1, @Part MultipartBody.Part image2, @Part MultipartBody.Part image3, @Part MultipartBody.Part image4, @Part MultipartBody.Part image5);

    @GET("feed/submit/poll")
    Call<FeedResponse> submitPoll(@Query("userid") String userid, @Query("poll_id") String pollId,
                                  @Query("answer_id") String answerId, @Query("answers") String answers);

    @GET("feed/action")
    Call<Response> action(@Query("userid") String userid, @Query("action") String action, @Query("feed_id") String feedId);

    @GET("feed/save")
    Call<FeedAddResponse> edit (@Query("userid") String userid, @Query("text") String text, @Query("id") String id);

    @GET("feed/action")
    Call<Response> action(@Query("userid") String userid, @Query("action") String action, @Query("feed_id") String feedId, @Query("text") String text);

    @GET("credit/feed/send")
    Call<FeedAddResponse> sendcredit(@Query("friend") String theuserid, @Query("amount") String amountOfCredit);
}

