package com.procrea8.sexyafrikan.modules.video;

import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;
import com.procrea8.sexyafrikan.modules.video.model.VideoBrowseResponse;
import com.procrea8.sexyafrikan.modules.video.model.Video;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface VideoApiInterface {
    @GET("videos/get/categories")
    Call<ArrayList<MusicCategory>> getCategories(@Query("userid") String userid);
    @GET("videos/browse")
    Call<VideoBrowseResponse>  browse(@Query("userid") String userid, @Query("category_id") String category, @Query("term") String term,
                                 @Query("type") String type, @Query("limit") Integer limit,
                                 @Query("page") Integer page);

    @Multipart
    @POST("video/create")
    Call<Video> create(@Query("userid") String userid, @Query("link") String link, @Query("title") String title, @Query("description") String description, @Query("privacy") String privacy,
                       @Query("category") String category, @Part MultipartBody.Part video);

    @POST("video/create")
    Call<Video> create(@Query("userid") String userid, @Query("link") String link, @Query("title") String title, @Query("description") String description, @Query("privacy") String privacy,
                       @Query("category") String category);
}
