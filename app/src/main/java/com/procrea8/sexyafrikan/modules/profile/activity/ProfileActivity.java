package com.procrea8.sexyafrikan.modules.profile.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.modules.profile.fragments.ProfileFeedFragment;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

public class ProfileActivity extends ActivityBase {
    LinearLayout profileCover;
    CircleImageView profileAvatar;
    TextView profileName;
    FrameLayout containerBody;
    LinearLayout profileMenu;
    TextView profileMenu1;
    TextView profileMenu2;
    TextView profileMenu3;
    ImageView profileMoreMenu;
    static  FragmentManager fragmentManager;
    Fragment fragment;


    public String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try{
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e){}

        containerBody = (FrameLayout) findViewById(R.id.container_body);
        fragmentManager = getSupportFragmentManager();

        //this profile logic here
        userId = App.getInstance().getSession().getUserid();
        Intent intent = getIntent();
        if (intent.hasExtra("user_id")) userId = intent.getStringExtra("user_id");
        //MyLog.e("profile ID - " + userId);
        fragment = ProfileFeedFragment.getInstance(userId);
        fragmentManager.beginTransaction().replace(R.id.container_body, fragment).commit();
        //MyLog.e("Profile visited");;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public static void loadPage(Fragment fragment) {
        fragmentManager.beginTransaction().replace(R.id.container_body, fragment).addToBackStack(null).commit();
    }

    public static void load(Context context, String userid) {
        Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtra("user_id", userid);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        context.startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PROFILE_SELECT_PHOTO && resultCode == Activity.RESULT_OK) {
            List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
            final String selectedPhoto = path.get(0);

            ProfileFeedFragment.setSelectedPhoto(selectedPhoto);

            setPermissionGranted(new OnPermissionGranted() {
                @Override
                public void onGranted() {
                    ProfileFeedFragment.startPhotoUpload();
                }
            });
        }
    }
}
