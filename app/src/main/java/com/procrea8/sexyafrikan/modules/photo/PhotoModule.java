package com.procrea8.sexyafrikan.modules.photo;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.AppActivity;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.profile.fragments.ProfilePhotosFragment;

/**
 * Created by pRocrea8 on 8/16/2017.
 */

public class PhotoModule implements ModuleInterface {
        @Override
        public void registerMenu(final Context context, Drawer drawer, int i) {
            drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_home_grey)
                    .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                        @Override
                        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                            Fragment fragment = ProfilePhotosFragment.getInstance(App.getInstance().getSession().getUserid());
                            AppActivity.setFragment(fragment);
                            AppActivity.setCurrentTitle(context.getResources().getString(R.string.photo));
                            return false;
                        }
                    })
                    .withName(context.getResources().getString(R.string.photo_s)));
        }

        @Override
        public Fragment getFragment(Context context) {
            return null;
        }

        @Override
        public String getTitle(Context context) {
            return null;
        }

        @Override
        public Object invoke(Context context, String type, Object object) {

            return null;
        }

        @Override
        public Object invoke(Context context, String type) {
            return null;
        }

}
