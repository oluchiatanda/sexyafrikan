package com.procrea8.sexyafrikan.modules.video.model;

import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.response.Response;


public class Video extends Response {
    @SerializedName("title")
    String title;
    @SerializedName("description")
    String description;
    @SerializedName("id")
    String id;
    @SerializedName("photo")
    String photo;
    @SerializedName("source")
    String source;
    @SerializedName("code")
    String code;
    @SerializedName("file")
    String file;
    @SerializedName("featured")
    boolean featured;
    @SerializedName("view_count")
    String viewCount;
    @SerializedName("time")
    String time;
    @SerializedName("has_like")
    boolean hasLike = false;
    @SerializedName("has_dislike")
    boolean hasDislike = false;
    @SerializedName("has_react")
    boolean hasReact = false;
    @SerializedName("like_count")
    Integer likeCount = 0;
    @SerializedName("dislike_count")
    Integer dislikeCount = 0;

    @SerializedName("comments")
    Integer comments;
    @SerializedName("slug")
    String sludge;

    public String getSludge() {
        return sludge;
    }

    public void setSludge(String sludge) {
        this.sludge = sludge;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public String getViewCount() {
        return viewCount;
    }

    public void setViewCount(String viewCount) {
        this.viewCount = viewCount;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isHasLike() {
        return hasLike;
    }

    public void setHasLike(boolean hasLike) {
        this.hasLike = hasLike;
    }

    public boolean isHasDislike() {
        return hasDislike;
    }

    public void setHasDislike(boolean hasDislike) {
        this.hasDislike = hasDislike;
    }

    public boolean isHasReact() {
        return hasReact;
    }

    public void setHasReact(boolean hasReact) {
        this.hasReact = hasReact;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getDislikeCount() {
        return dislikeCount;
    }

    public void setDislikeCount(Integer dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    public Integer getComments() {
        return comments;
    }

    public void setComments(Integer comments) {
        this.comments = comments;
    }
}

