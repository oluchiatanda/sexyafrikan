package com.procrea8.sexyafrikan.modules.marketplace.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.marketplace.activity.MarketplaceActivity;
import com.procrea8.sexyafrikan.modules.marketplace.model.Listing;

import java.util.ArrayList;

/**
 * Created by Tiamiyu waliu kola on 9/18/2016.
 */
public class MarketplaceAdapter extends RecyclerView.Adapter<MarketplaceAdapter.MyViewHolder> {
    Context context;
    ArrayList<Listing> listings = new ArrayList<>();
    LayoutInflater layoutInflater;

    public MarketplaceAdapter(Context c, ArrayList<Listing> lists) {
        context = c;
        listings = lists;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        ImageView playButton;
        TextView title;
        TextView description;
        public MyViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.title);
            description = (TextView) itemView.findViewById(R.id.description);
        }
    }

    @Override
    public MarketplaceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.listing_card, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MarketplaceAdapter.MyViewHolder holder, int position) {
        final Listing listing = listings.get(position);
        Glide.with(context).load(listing.getImage()).crossFade().into(holder.image);
        holder.title.setText(listing.getTitle());
        holder.description.setText(listing.getShortDescription());
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MarketplaceActivity.load(context, listing);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listings.size();
    }
}
