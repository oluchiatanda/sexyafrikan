package com.procrea8.sexyafrikan.modules.photo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.photo.activity.PhotoAlbumActivity;
import com.procrea8.sexyafrikan.modules.photo.model.PhotoAlbum;

import java.util.ArrayList;

/**
 * Created by Tiamiyu waliu kola on 8/30/2016.
 */
public class PhotoAlbumsAdapter extends RecyclerView.Adapter<PhotoAlbumsAdapter.MyViewHolder> {
        ArrayList<PhotoAlbum> albums = new ArrayList<>();
        Context context;
        LayoutInflater layoutInflater;

public PhotoAlbumsAdapter(Context c, ArrayList<PhotoAlbum> lists) {
        context = c;
        albums = lists;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

class MyViewHolder extends RecyclerView.ViewHolder{
    ImageView photo;
    TextView title;
    public MyViewHolder(View itemView) {
        super(itemView);
        photo = (ImageView) itemView.findViewById(R.id.photo);
        title = (TextView) itemView.findViewById(R.id.title);
    }
}
    @Override
    public PhotoAlbumsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.photo_album_card, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PhotoAlbumsAdapter.MyViewHolder holder, int position) {
        final PhotoAlbum album = albums.get(position);
        Glide.with(context).load(album.getImage()).into(holder.photo);

        String text = album.getTitle();
        switch(album.getTitle()) {
            case "profile-photos":
                text = context.getResources().getString(R.string.profile_photos);
                break;
            case "cover-photos":
                text = context.getResources().getString(R.string.cover_photos);
                break;
            case "timeline-photos":
                text = context.getResources().getString(R.string.timeline_photos);
                break;
        }
        holder.title.setText(text);
        holder.photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PhotoAlbumActivity.loadAlbum(context, album.getId(), album.getUserid());
            }
        });
    }

    @Override
    public int getItemCount() {
        return albums.size();
    }
}

