package com.procrea8.sexyafrikan.modules.feed;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.hashtag.activity.HashtagActivity;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;
import com.wang.avi.AVLoadingIndicatorView;

/**
 * Created by pRocrea8 on 10/25/2017.
 */

public class PurchaseActivity extends ActivityBase {

    WebView webView;
    String purchaseUrl = "";

//    String purchaseUrl = SITE_BASE_URL + "creditgift/purchase?webview=true";
    AVLoadingIndicatorView loadImage;
    Context context;
    AppCompatActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        activity = this;
        webView = (WebView) findViewById(R.id.purchaseContainer);
        loadImage = (AVLoadingIndicatorView) findViewById(R.id.loading_image);
        WebSettings settings = webView.getSettings();
        purchaseUrl = SITE_BASE_URL + "creditgift/purchase?webview=true&api_userid=" + App.getInstance().getSession().getUserid();
//        purchaseUrl = purchaseUrl + "&api_userid=" + App.getInstance().getSession().getUserid();
        MyLog.e("Purchase " + App.getInstance().getSession().getUserid());
        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setAppCacheEnabled(false);
        settings.setDomStorageEnabled(true);
        webView.setWebViewClient(new AppWebViewClient());

        webView.loadUrl(purchaseUrl);
        webView.clearCache(true);
//        webView.clearView();
//        webView.clearHistory();
//        webView.destroy();
        loadImage.show();
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    class AppWebViewClient extends android.webkit.WebViewClient implements Contants {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            loadImage.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            webView.clearCache(true);
            loadImage.hide();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, final String url) {
            MyLog.e("url " + url + "");

            if (Uri.parse(url).getHost().length() == 0) {
                return false;
            }

            if (url.startsWith("hashtag:")) {
                String[] str = url.split(":");
                String hashtag = str[1];
                HashtagActivity.launch(view.getContext(), hashtag);
            } else if (url.startsWith("mention:")) {
                String[] str = url.split(":");
                String userid = str[1];
                ProfileActivity.load(view.getContext(), userid);
            } else {
                if (Uri.parse(url).getHost().endsWith(DOMAIN)) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String userid = App.getInstance().getSession().getUserid();
                            String newUrl = url;
                            if (url.contains("&")) {
                                newUrl = url.concat("&webview=true&api_userid=" + userid);
                                MyLog.e("url contains &" + url);
                            }else if(url.contains("?")){
                                newUrl = url.concat("&webview=true&api_userid=" + userid);
                                MyLog.e("url contains ?" + url);
                            } else {
                                newUrl = url.concat("?webview=true&api_userid=" + userid);
                                MyLog.e("url doesnt contains &" + url);
                            }
                            webView.loadUrl(newUrl);
                            MyLog.e("newUrl " + newUrl);
                            //Toast.makeText(getContext(), "THis is Url - " + newUrl, Toast.LENGTH_LONG).show();
                        }
                    });
                    return true;
                } else {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    MyLog.e("intent- " + url);
                    startActivity(intent);
                }

            }
            return true;
        }

    }

}
