package com.procrea8.sexyafrikan.modules.photo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.photo.activity.PhotoViewerActivity;
import com.procrea8.sexyafrikan.modules.photo.model.Photo;

import java.util.ArrayList;


public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.MyViewHolder> implements Contants {
    ArrayList<Photo> photos = new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;


    public PhotosAdapter(Context c, ArrayList<Photo> lists) {
        context = c;
        photos = lists;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView photo;


        public MyViewHolder(View itemView) {
            super(itemView);
            photo = (ImageView) itemView.findViewById(R.id.photo);

        }
    }

    @Override
    public PhotosAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.photo_card, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PhotosAdapter.MyViewHolder holder, final int position) {
        final Photo photo = photos.get(position);
        Glide.with(context).load(photo.getPath()).into(holder.photo);
        holder.photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PhotoViewerActivity.launch(context, photo.getPath());
            }
        });


    }

    @Override
    public int getItemCount() {
        return photos.size();
    }
}
