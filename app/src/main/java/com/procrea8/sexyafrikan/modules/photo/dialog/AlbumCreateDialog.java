package com.procrea8.sexyafrikan.modules.photo.dialog;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.modules.photo.PhotoApiInterface;
import com.procrea8.sexyafrikan.modules.photo.model.PhotoAlbum;
import com.procrea8.sexyafrikan.response.Response;
import com.satsuware.usefulviews.LabelledSpinner;

import retrofit2.Call;
import retrofit2.Callback;

public class AlbumCreateDialog {
    Context context;

    EditText albumTitle;
    EditText description;
    LabelledSpinner spinner;
    String privacy = "1";
    String type = "create";
    PhotoAlbum photoAlbum;

    OnPhotoAlbumCreatedListener listener;

    public AlbumCreateDialog(Context c, final String type, PhotoAlbum album) {
        this.context = c;
        this.type = type;
        photoAlbum = album;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (type.equals("create")) {
            builder.setTitle(R.string.create_new_album);
        } else {
            builder.setTitle(R.string.edit_album);
        }
        LayoutInflater layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.album_create, null);
        albumTitle = (EditText) convertView.findViewById(R.id.album_title);
        description = (EditText) convertView.findViewById(R.id.album_description);
        spinner = (LabelledSpinner) convertView.findViewById(R.id.privacy);
        spinner.setItemsArray(R.array.privacy_list);

        if (type.equals("edit")) {
            albumTitle.setText(photoAlbum.getTitle());
            description.setText(photoAlbum.getDescription());
            privacy = photoAlbum.getPrivacy();
            switch (photoAlbum.getPrivacy()) {
                case "1":
                    spinner.setSelection(0);
                    break;
                case "2":
                    spinner.setSelection(1);
                    break;
                case "3":
                    spinner.setSelection(2);
                    break;
            }

        }

        spinner.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
            @Override
            public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
                switch (position) {
                    case 0:
                        privacy = "1";
                        break;
                    case 1:
                        privacy = "2";
                        break;
                    case 2:
                        privacy = "3";
                        break;
                }
            }

            @Override
            public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

            }
        });
        builder.setView(convertView);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok, null).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!albumTitle.getText().toString().isEmpty()) {
                            final ProgressDialog progressDialog = new ProgressDialog(context);
                            progressDialog.setMessage(context.getResources().getString(R.string.adding));
                            progressDialog.setCancelable(false);
                            progressDialog.setIndeterminate(true);
                            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            progressDialog.show();
                            String title = albumTitle.getText().toString();
                            String desc = description.getText().toString();
                            Call<Response> call;
                            if (type.equals("create")) {
                                call = Api.getRetrofit().create(PhotoApiInterface.class).addAlbum(
                                        App.getInstance().getSession().getUserid(),
                                        title,
                                        desc,
                                        privacy
                                );
                            } else {
                                call = Api.getRetrofit().create(PhotoApiInterface.class).editAlbum(
                                        App.getInstance().getSession().getUserid(),
                                        photoAlbum.getId(),
                                        title,
                                        desc,
                                        privacy
                                );
                            }
                            call.enqueue(new Callback<Response>() {
                                @Override
                                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                    if (listener != null) {
                                        listener.onCreated(true);
                                    }
                                    dialog.dismiss();
                                    progressDialog.dismiss();
                                }

                                @Override
                                public void onFailure(Call<Response> call, Throwable t) {
                                    Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                });
            }
        });
        dialog.show();
    }

    public void setListener(OnPhotoAlbumCreatedListener lis) {
        this.listener = lis;
    }

    public static class OnPhotoAlbumCreatedListener{
        public void onCreated(Boolean created) {

        }
    }
}
