//package com.procrea8.sexyafrikan.modules.feed.adapter;
//
///**
// * Created by pRocrea8 on 10/31/2017.
// */
//
//public class SendMoneyActivity extends ActivityBase {
//        WebView webView;
//        String forumUrl = SITE_BASE_URL + "forum?webview=true";
//        AVLoadingIndicatorView loadImage;
//        Context context;
//        AppCompatActivity activity;
//        @Override
//        protected void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            setContentView(R.layout.activity_forum);
//            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//            setSupportActionBar(toolbar);
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            context = this;
//            activity = this;
//            webView = (WebView) findViewById(R.id.container);
//            loadImage = (AVLoadingIndicatorView) findViewById(R.id.loading_image);
//            WebSettings settings = webView.getSettings();
//            forumUrl = forumUrl + "&api_userid=" + App.getInstance().getSession().getUserid();
//            settings.setJavaScriptEnabled(true);
//            settings.setLoadWithOverviewMode(true);
//            settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
//            settings.setAppCacheEnabled(false);
//            //webView.setWebChromeClient(new WebChromeClient());
//            webView.setWebViewClient(new AppWebViewClient());
//            webView.clearCache(true);
//
//            webView.loadUrl(forumUrl);
//            loadImage.show();
//        }
//
//        @Override
//        public void onBackPressed() {
//            if (webView.canGoBack()) {
//                webView.goBack();
//            } else {
//                super.onBackPressed();
//            }
//        }
//
//        class AppWebViewClient extends android.webkit.WebViewClient implements Contants {
//            @Override
//            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                super.onPageStarted(view, url, favicon);
//                loadImage.show();
//            }
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                webView.clearCache(true);
//                loadImage.hide();
//            }
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, final String url) {
//                if(Uri.parse(url).getHost().length() == 0) {
//                    return false;
//                }
//
//                if (url.startsWith("hashtag:")) {
//                    String[] str = url.split(":");
//                    String hashtag = str[1];
//                    HashtagActivity.launch(view.getContext(), hashtag);
//                } else if(url.startsWith("mention:")) {
//                    String[] str = url.split(":");
//                    String userid = str[1];
//                    ProfileActivity.load(view.getContext(), userid);
//                } else {
//                    if (Uri.parse(url).getHost().endsWith(DOMAIN)) {
//                        activity.runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                String userid = App.getInstance().getSession().getUserid();
//                                String newUrl = url;
//                                if (url.contains("&")) {
//                                    newUrl = url.concat("&webview=true&api_userid=" + userid);
//                                } else {
//                                    newUrl = url.concat("?webview=true&api_userid=" + userid);
//                                }
//                                webView.loadUrl(newUrl);
//                                //Toast.makeText(getContext(), "THis is Url - " + newUrl, Toast.LENGTH_LONG).show();
//                            }
//                        });
//                        return true;
//                    } else {
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                        startActivity(intent);
//                    }
//                }
//                return true;
//            }
//
//        }
//    }
//
//}
