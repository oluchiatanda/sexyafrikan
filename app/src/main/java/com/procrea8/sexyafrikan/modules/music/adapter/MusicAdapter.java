package com.procrea8.sexyafrikan.modules.music.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.music.activity.MusicPlayerActivity;
import com.procrea8.sexyafrikan.modules.music.model.Music;

import java.util.ArrayList;

/**
 * Created by Tiamiyu waliu kola on 9/7/2016.
 */
public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.MyViewHolder> implements Contants{
    Context context;
    ArrayList<Music> songs = new ArrayList<>();
    LayoutInflater layoutInflater;

    public MusicAdapter(Context c, ArrayList<Music> lists) {
        context = c;
        songs = lists;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        ImageView playButton;
        TextView title;
        TextView playCount;
        public MyViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            playButton = (ImageView) itemView.findViewById(R.id.play_button);
            title = (TextView) itemView.findViewById(R.id.title);
            playCount = (TextView) itemView.findViewById(R.id.play_count);
        }
    }
    @Override
    public MusicAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.music_card, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MusicAdapter.MyViewHolder holder, int position) {
        final Music music = songs.get(position);
        holder.title.setText(music.getTitle());
        holder.playCount.setText(music.getPlayCount());
        Glide.with(context).load(music.getCover()).into(holder.image);
        //MyLog.e(music.getCover());
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MusicPlayerActivity.launch(context, music);
            }
        });
        holder.playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MusicPlayerActivity.launch(context, music);
            }
        });


    }

    @Override
    public int getItemCount() {
        return songs.size();
    }
}
