package com.procrea8.sexyafrikan.modules.music.dialog;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.music.MusicApiInterface;
import com.procrea8.sexyafrikan.modules.music.model.Music;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;
import com.satsuware.usefulviews.LabelledSpinner;

import java.io.File;
import java.util.ArrayList;

import me.nereo.multi_image_selector.MultiImageSelector;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class MusicCreateDialog implements Contants {
    String type = "create";
    Music music; //for edit type

    static String musicFile = "";
    static String artFile = "";
    String privacy = "1";
    String category = "";
    Context context;

    EditText musicTitle;
    EditText musicArtist;
    EditText musicAlbum;
    LabelledSpinner spinner;
    LabelledSpinner categorySpinner;
    Button chooseMusic;
    Button chooseCover;

    ArrayList<MusicCategory> loadedCategories = new ArrayList<>();

    OnMusicCreatedListener listener;

    public MusicCreateDialog(final Context context, final String type, final Music music) {
        this.type = type;
        this.context = context;
        this.music = music;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (type.equals("create")) {
            builder.setTitle(R.string.add_new_music);
        } else {
            builder.setTitle(R.string.edit_song);
        }
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.music_create, null);
        musicTitle = (EditText) convertView.findViewById(R.id.music_title);
        musicArtist = (EditText) convertView.findViewById(R.id.music_artist);
        musicAlbum = (EditText) convertView.findViewById(R.id.music_album);
        spinner = (LabelledSpinner) convertView.findViewById(R.id.privacy);
        categorySpinner = (LabelledSpinner) convertView.findViewById(R.id.category);
        chooseMusic = (Button) convertView.findViewById(R.id.music_button);
        chooseCover = (Button) convertView.findViewById(R.id.cover_button);
        spinner.setItemsArray(R.array.privacy_list);

        //lets public the category label
        Call<ArrayList<MusicCategory>> call = Api.getRetrofit().create(MusicApiInterface.class).getCategories(App.getInstance().getSession().getUserid());
        call.enqueue(new Callback<ArrayList<MusicCategory>>() {
            @Override
            public void onResponse(Call<ArrayList<MusicCategory>> call, retrofit2.Response<ArrayList<MusicCategory>> response) {
                ArrayList<String> items = new ArrayList<String>();
                for(int i =0;i <response.body().size();i++) {
                    items.add(response.body().get(i).getTitle());
                }
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, items);
                categorySpinner.setCustomAdapter(arrayAdapter);
                categorySpinner.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
                    @Override
                    public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
                        if (loadedCategories.size() > 0) {
                            category = loadedCategories.get(position).getId();
                        }
                    }

                    @Override
                    public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onFailure(Call<ArrayList<MusicCategory>> call, Throwable t) {
                Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
            }
        });

        if (type.equals("edit")) {
            chooseMusic.setVisibility(View.GONE);
            categorySpinner.setVisibility(View.GONE);
            musicTitle.setText(music.getTitle());
            musicArtist.setText(music.getArtist());
            musicAlbum.setText(music.getAlbum());
            privacy = music.getPrivacy();
            switch (music.getPrivacy()) {
                case "1":
                    spinner.setSelection(0);
                    break;
                case "2":
                    spinner.setSelection(1);
                    break;
                case "3":
                    spinner.setSelection(2);
                    break;
            }

        }

        chooseCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ActivityBase activity = (ActivityBase) context;
                activity.setPermissionGranted(new ActivityBase.OnPermissionGranted() {
                    @Override
                    public void onGranted() {
                        MultiImageSelector.create()
                                .showCamera(true)
                                .count(1)
                                .single()
                                .start(activity, MUSIC_SELECT_COVER);
                    }
                });

            }
        });

        chooseMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Activity activity = (Activity) context;
                Intent intent = new Intent();
                intent.setType("audio/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                activity.startActivityForResult(Intent.createChooser(intent, context.getResources().getString(R.string.select_song)), MUSIC_SELECT_SONG);
            }
        });

        spinner.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
            @Override
            public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
                switch (position) {
                    case 0:
                        privacy = "1";
                        break;
                    case 1:
                        privacy = "2";
                        break;
                    case 2:
                        privacy = "3";
                        break;
                }
            }

            @Override
            public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

            }
        });
        builder.setView(convertView);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok, null).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String title = musicTitle.getText().toString();
                        String artist  = musicArtist.getText().toString();
                        String album = musicAlbum.getText().toString();
                        if (musicTitle.getText().toString().isEmpty()) {
                            Toast.makeText(context, R.string.provide_title, Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (musicFile.isEmpty() && type.equals("create")) {
                            Toast.makeText(context, R.string.choose_a_song, Toast.LENGTH_LONG).show();
                            return;
                        }
                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        if (type.equals("create")) {
                            progressDialog.setMessage(context.getResources().getString(R.string.adding));
                        } else {
                            progressDialog.setMessage(context.getResources().getString(R.string.saving));
                        }
                        progressDialog.setCancelable(false);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressDialog.show();

                        Call<Music> call1 = null;
                        MultipartBody.Part musicBody = null;
                        if (type.equals("create")) {
                            File file = new File(musicFile);
                            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            musicBody = MultipartBody.Part.createFormData("music_file", file.getName(), requestBody);
                        }

                        MultipartBody.Part artBody = null;
                        if (!artFile.isEmpty()) {
                            File file2 = new File(artFile);
                            RequestBody requestBody2 = RequestBody.create(MediaType.parse("multipart/form-data"), file2);
                            artBody = MultipartBody.Part.createFormData("cover_art", file2.getName(), requestBody2);
                        }

                        if (!musicFile.isEmpty() && !artFile.isEmpty()) {
                            if (type.equals("create")) {
                                call1 = Api.getRetrofit().create(MusicApiInterface.class).create(
                                        App.getInstance().getSession().getUserid(),
                                        title,
                                        artist,
                                        album,
                                        privacy,
                                        category,
                                        musicBody,
                                        artBody
                                );
                            }
                        } else {
                            if (type.equals("create")) {
                                call1 = Api.getRetrofit().create(MusicApiInterface.class).create(
                                        App.getInstance().getSession().getUserid(),
                                        title,
                                        artist,
                                        album,
                                        privacy,
                                        category,
                                        musicBody
                                );
                            } else {
                                if (artFile.isEmpty()) {
                                    call1 = Api.getRetrofit().create(MusicApiInterface.class).edit(
                                            App.getInstance().getSession().getUserid(),
                                            music.getId(),
                                            title,
                                            artist,
                                            album,
                                            privacy,
                                            category
                                    );
                                } else {
                                    call1 = Api.getRetrofit().create(MusicApiInterface.class).edit(
                                            App.getInstance().getSession().getUserid(),
                                            music.getId(),
                                            title,
                                            artist,
                                            album,
                                            privacy,
                                            category,
                                            artBody
                                    );
                                }
                            }
                        }

                        if (call1 != null) {
                            call1.enqueue(new Callback<Music>() {
                                @Override
                                public void onResponse(Call<Music> call, retrofit2.Response<Music> response) {
                                    progressDialog.dismiss();
                                    if (response.body().getStatus()==1) {
                                        dialog.dismiss();
                                        if (listener != null) listener.onMusicCreated(response.body());
                                    } else {
                                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Music> call, Throwable t) {
                                    progressDialog.dismiss();
                                    MyLog.e("Add Song error" + t.getMessage());
                                    Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                });
            }
        });
        dialog.show();
    }

    public static void setArtFile(String file) {
        artFile = file;
    }

    public static void setMusicFile(String file) {
        musicFile = file;
    }

    public void addListener(OnMusicCreatedListener l) {
        listener = l;
    }

    public static class OnMusicCreatedListener {
        public void onMusicCreated(Music music) {}
    }
}
