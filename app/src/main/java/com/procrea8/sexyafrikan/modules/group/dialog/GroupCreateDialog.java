package com.procrea8.sexyafrikan.modules.group.dialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.modules.group.GroupApiInterface;
import com.procrea8.sexyafrikan.modules.group.model.Group;
import com.satsuware.usefulviews.LabelledSpinner;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Tiamiyu waliu kola on 9/20/2016.
 */
public class GroupCreateDialog implements Contants {
    String type = "create";
    Group group; //for edit type

    String privacy = "0";

    Context context;

    EditText title;
    EditText description;
    EditText name;
    LabelledSpinner spinner;
    OnGroupCreatedListener listener;

    FragmentManager fragmentManger;

    public GroupCreateDialog(final Context context, final String type, final Group group) {
        this.type = type;
        this.context = context;
        this.group = group;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (type.equals("create")) {
            builder.setTitle(R.string.add_new_group);
        } else {
            builder.setTitle(R.string.edit_group);
        }
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.group_create, null);
        title = (EditText) convertView.findViewById(R.id.title);
        description = (EditText) convertView.findViewById(R.id.description);
        spinner = (LabelledSpinner) convertView.findViewById(R.id.privacy);
        spinner.setItemsArray(R.array.event_privacy_list);
        name = (EditText) convertView.findViewById(R.id.name);

        if (type.equals("edit")) {
            //chooseGroup.setVisibility(View.GONE);
            title.setText(group.getTitle());
            description.setText(group.getDescription());
        }

        spinner.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
            @Override
            public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
                switch (position) {
                    case 0:
                        privacy = "0";
                        break;
                    case 1:
                        privacy = "1";
                        break;
                }
            }

            @Override
            public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

            }
        });


        builder.setView(convertView);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok, null).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String mtitle = title.getText().toString();
                        String mDesc = description.getText().toString();
                        String mName = name.getText().toString();

                        if (mtitle.isEmpty()) {
                            Toast.makeText(context, R.string.provide_title, Toast.LENGTH_LONG).show();
                            return;
                        }



                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        if (type.equals("create")) {
                            progressDialog.setMessage(context.getResources().getString(R.string.adding));
                        } else {
                            progressDialog.setMessage(context.getResources().getString(R.string.saving));
                        }
                        progressDialog.setCancelable(false);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressDialog.show();

                        Call<Group> call1 = null;

                        if (type.equals("create")) {

                            call1 = Api.getRetrofit().create(GroupApiInterface.class).create(
                                    App.getInstance().getSession().getUserid(),
                                    mtitle,
                                    mDesc,
                                    mName,
                                    privacy
                            );

                        } else {
                            call1 = Api.getRetrofit().create(GroupApiInterface.class).edit(
                                    App.getInstance().getSession().getUserid(),
                                    mtitle,
                                    mDesc,
                                    mName,
                                    privacy,
                                    group.getId()

                            );
                        }


                        if (call1 != null) {
                            call1.enqueue(new Callback<Group>() {
                                @Override
                                public void onResponse(Call<Group> call, retrofit2.Response<Group> response) {
                                    progressDialog.dismiss();
                                    if (response.body().getStatus()==1) {
                                        dialog.dismiss();
                                        ///MyLog.e("Item created - " + response.body().getTitle());
                                        if (listener != null)
                                            listener.onGroupCreated(response.body());
                                    } else {
                                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Group> call, Throwable t) {
                                    progressDialog.dismiss();
                                    ///MyLog.e("Add Song error" + t.getMessage());
                                    Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                                }
                            });


                        }
                    }
                });
            }
        });
        dialog.show();
    }

    public void addListener(OnGroupCreatedListener l) {
        listener = l;
    }

    public static class OnGroupCreatedListener {
        public void onGroupCreated(Group group) {
        }
    }


}
