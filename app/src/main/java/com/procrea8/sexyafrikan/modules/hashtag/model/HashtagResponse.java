package com.procrea8.sexyafrikan.modules.hashtag.model;


import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;

import java.util.ArrayList;

public class HashtagResponse {
    @SerializedName("hashtags")
    ArrayList<Hashtags> hashtags;

    @SerializedName("feeds")
    ArrayList<FeedResponse> feeds;

    public ArrayList<Hashtags> getHashtags() {
        return hashtags;
    }

    public void setHashtags(ArrayList<Hashtags> hashtags) {
        this.hashtags = hashtags;
    }

    public ArrayList<FeedResponse> getFeeds() {
        return feeds;
    }

    public void setFeeds(ArrayList<FeedResponse> feeds) {
        this.feeds = feeds;
    }
}
