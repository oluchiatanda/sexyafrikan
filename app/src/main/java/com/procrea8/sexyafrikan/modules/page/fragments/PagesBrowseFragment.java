package com.procrea8.sexyafrikan.modules.page.fragments;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.fragment.BaseFragment;
import com.procrea8.sexyafrikan.helper.GridSpacingItemDecoration;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;
import com.procrea8.sexyafrikan.modules.page.PageApiInterface;
import com.procrea8.sexyafrikan.modules.page.dialog.PagesCreateDialog;
import com.procrea8.sexyafrikan.modules.page.activity.PageActivity;
import com.procrea8.sexyafrikan.modules.page.adapter.PagesAdapter;
import com.procrea8.sexyafrikan.modules.page.model.PageBrowseResponse;
import com.procrea8.sexyafrikan.modules.page.model.Page;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PagesBrowseFragment extends BaseFragment {
    String term = "";
    String type = "browse";
    Integer limit = 10;
    Integer page = 1;
    String category = "";

    ArrayList<MusicCategory> categories = new ArrayList<>();

    EditText searchText;
    ImageView searchButton;
    Spinner categorySpinner;
    RecyclerView recyclerView;
    LinearLayout header;
    FloatingActionButton newButton;

    PagesAdapter adapter;
    ArrayList<Page> pages = new ArrayList<>();

    AVLoadingIndicatorView loadingImage;
    TextView statusText;

    Boolean paginating = false;

    String loggedUserid;

    public PagesBrowseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pages_browse, container, false);
        searchText = (EditText) view.findViewById(R.id.search_text);
        searchButton = (ImageView) view.findViewById(R.id.search_button);
        categorySpinner = (Spinner) view.findViewById(R.id.category);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleview);
        header = (LinearLayout) view.findViewById(R.id.header);
        loadingImage = (AVLoadingIndicatorView) view.findViewById(R.id.loading_image);
        statusText = (TextView) view.findViewById(R.id.status_text);
        newButton = (FloatingActionButton) view.findViewById(R.id.new_button);
        loggedUserid = App.getInstance().getSession().getUserid();
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey("type")) type = arguments.getString("type");


        adapter = new PagesAdapter(getContext(), pages);
        final RecyclerView.LayoutManager layoutManager;
        if (isHigh()) {
            layoutManager = new GridLayoutManager(getContext(), 2);
        } else {
            layoutManager = new GridLayoutManager(getContext(), 1);
        }
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(5), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisibleItems = ((GridLayoutManager) layoutManager).findLastVisibleItemPosition();
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        //we are in the last item
                        if (!paginating) {
                            page++;
                            getPages(true);
                            paginating = true;
                        }
                    }
                }
            }
        });

        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //Toast.makeText(getContext(), R.string.cant_create_page_on_mobile, Toast.LENGTH_LONG).show();

                PagesCreateDialog createDialog = new PagesCreateDialog(getContext(), null);
                createDialog.addListener(new PagesCreateDialog.OnPagesCreatedListener() {
                    @Override
                    public void onPagesCreated(Page page) {
                        pages.add(page);
                        adapter.notifyDataSetChanged();
                        PageActivity.load(getContext(), page);
                    }
                });
            }
        });


        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String search = searchText.getText().toString();
                loadingImage.show();
                pages.clear();
                adapter.notifyDataSetChanged();
                term = search;
                getPages(false);
            }
        });

        loadingImage.show();
        getPages(false);
        return view;
    }

//    public void launchPages(){
//        PagesCreateDialog createDialog = new PagesCreateDialog(getContext(), "create", null);
//        createDialog.addListener(new PagesCreateDialog.OnPagesCreatedListener() {
//            @Override
//            public void onPagesCreated(Page page) {
//                if (pages.size() > 0) {
//                    pages.add(1, page);
//                } else {
//                    pages.add(page);
//                }
//                adapter.notifyDataSetChanged();
//            }
//        });
//        createDialog.show();
//    }

    public void getPages(final Boolean more) {
        Call<PageBrowseResponse> call = Api.getRetrofit().create(PageApiInterface.class).browse(
                loggedUserid, category, term, type, limit, page
        );
        call.enqueue(new Callback<PageBrowseResponse>() {
            @Override
            public void onResponse(Call<PageBrowseResponse> call, Response<PageBrowseResponse> response) {
                //MyLog.e("Events Fetched");
                if (!more) {
                    header.setVisibility(View.VISIBLE);
                    if (categories.size() < 1) {
                        ArrayList<String> items = new ArrayList<String>();
                        categories = response.body().getCategories();

                        for (int i = 0; i < categories.size(); i++) {
                            items.add(response.body().getCategories().get(i).getTitle());
                        }
                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, items);
                        categorySpinner.setAdapter(arrayAdapter);
                        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                MusicCategory theCategory = categories.get(i);
                                category = theCategory.getId();
                                page = 1;
                                getPages(false);
                                //MyLog.e(category);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }

                    pages.clear();
                }

                pages.addAll(response.body().getPages());

                adapter.notifyDataSetChanged();
                if (response.body().getPages().size() > 0) {
                    statusText.setVisibility(View.GONE);
                } else {
                    if (!more) {
                        statusText.setText(R.string.no_pages_found);
                        statusText.setVisibility(View.VISIBLE);
                    }
                }
                loadingImage.hide();
                paginating = false;
            }

            @Override
            public void onFailure(Call<PageBrowseResponse> call, Throwable t) {
                loadingImage.hide();
                if (!more) {
                    statusText.setText(R.string.no_pages_found);
                    statusText.setVisibility(View.VISIBLE);
                }
                MyLog.e("Music fetch error - " + t.getMessage());
            }
        });
    }

}
