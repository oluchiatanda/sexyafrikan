package com.procrea8.sexyafrikan.modules.feed.response;

import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;


public class FeedAddResponse extends Response {
    public FeedResponse getFeedResponse() {
        return feedResponse;
    }

    public void setFeedResponse(FeedResponse feedResponse) {
        this.feedResponse = feedResponse;
    }

    @SerializedName("feed")
    FeedResponse feedResponse;
//    void contacts(Callback<List<FeedResponse>> cb);
}
