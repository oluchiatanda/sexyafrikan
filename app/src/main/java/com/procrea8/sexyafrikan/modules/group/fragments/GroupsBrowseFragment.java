package com.procrea8.sexyafrikan.modules.group.fragments;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.fragment.BaseFragment;
import com.procrea8.sexyafrikan.helper.GridSpacingItemDecoration;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.group.GroupApiInterface;
import com.procrea8.sexyafrikan.modules.group.adapter.GroupsAdapter;
import com.procrea8.sexyafrikan.modules.group.dialog.GroupCreateDialog;
import com.procrea8.sexyafrikan.modules.group.model.Group;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupsBrowseFragment extends BaseFragment {
    String term = "";
    String type = "recommend";
    Integer limit = 10;
    Integer page = 1;

    EditText searchText;
    ImageView searchButton;
    RecyclerView recyclerView;
    LinearLayout header;
    FloatingActionButton newButton;

    GroupsAdapter adapter;
    ArrayList<Group> groups = new ArrayList<>();

    AVLoadingIndicatorView loadingImage;
    TextView statusText;

    Boolean paginating = false;

    String loggedUserid;
    public GroupsBrowseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_groups_browse, container, false);

        searchText = (EditText) view.findViewById(R.id.search_text);
        searchButton = (ImageView) view.findViewById(R.id.search_button);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleview);
        header = (LinearLayout) view.findViewById(R.id.header);
        loadingImage = (AVLoadingIndicatorView) view.findViewById(R.id.loading_image);
        statusText = (TextView)view.findViewById(R.id.status_text);
        newButton = (FloatingActionButton) view.findViewById(R.id.new_button);
        loggedUserid = App.getInstance().getSession().getUserid();
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey("type")) type = arguments.getString("type");


        adapter = new GroupsAdapter(getContext(), groups);
        final RecyclerView.LayoutManager layoutManager;
        if (isHigh()) {
            layoutManager = new GridLayoutManager(getContext(), 2);
        } else {
            layoutManager = new GridLayoutManager(getContext(), 1);
        }
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(5), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisibleItems = ((GridLayoutManager) layoutManager).findLastVisibleItemPosition();
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        //we are in the last item
                        if (!paginating) {
                            page++;
                            getGroups(true);
                            paginating = true;
                        }
                    }
                }
            }
        });

        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GroupCreateDialog createDialog = new GroupCreateDialog(getContext(), "create", null);
                createDialog.addListener(new GroupCreateDialog.OnGroupCreatedListener() {
                    @Override
                    public void onGroupCreated(Group listing) {
                        //groups.add(listing);
                        ArrayList<Group> newGroups = new ArrayList<Group>();
                        newGroups.add(listing);
                        newGroups.addAll(groups);
                        groups.clear();
                        groups.addAll(newGroups);
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String search = searchText.getText().toString();
                loadingImage.show();
                groups.clear();
                adapter.notifyDataSetChanged();
                term = search;
                getGroups(false);
            }
        });

        loadingImage.show();
        getGroups(false);
        return view;
    }

    public void getGroups(final Boolean more) {
        Call<ArrayList<Group>> call = Api.getRetrofit().create(GroupApiInterface.class).browse(
                loggedUserid,term,type,limit,page
        );
        call.enqueue(new Callback<ArrayList<Group>>() {
            @Override
            public void onResponse(Call<ArrayList<Group>> call, Response<ArrayList<Group>> response) {
                //MyLog.e("Groups Fetched");
                if (!more) {
                    header.setVisibility(View.VISIBLE);
                    groups.clear();
                }

                groups.addAll(response.body());

                adapter.notifyDataSetChanged();
                if (response.body().size() > 0) {
                    statusText.setVisibility(View.GONE);
                } else {
                    if (!more) {
                        statusText.setText(R.string.no_groups_found);
                        statusText.setVisibility(View.VISIBLE);
                    }
                }
                loadingImage.hide();
                paginating = false;
            }

            @Override
            public void onFailure(Call<ArrayList<Group>> call, Throwable t) {
                loadingImage.hide();
                if (!more) {
                    statusText.setText(R.string.no_groups_found);
                    statusText.setVisibility(View.VISIBLE);
                }
                MyLog.e("Music fetch error - " + t.getMessage());
            }
        });
    }
}
