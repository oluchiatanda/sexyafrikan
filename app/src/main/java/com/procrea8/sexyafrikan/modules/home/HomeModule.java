package com.procrea8.sexyafrikan.modules.home;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.AppActivity;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.chat.fragments.MessagesFragment;
import com.procrea8.sexyafrikan.modules.home.fragments.HomeFragment;
import com.procrea8.sexyafrikan.modules.notification.fragments.NotificationFragment;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;
import com.procrea8.sexyafrikan.modules.relationship.fragments.FriendRequestFragment;
import com.procrea8.sexyafrikan.modules.relationship.fragments.FriendsSuggestionFragment;
import com.procrea8.sexyafrikan.modules.relationship.fragments.OnlineFriendsFragment;


public class HomeModule implements ModuleInterface {
    @Override
    public void registerMenu(final Context context, final Drawer drawer, int i) {
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_home_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = new HomeFragment();
                        AppActivity.setFragment(fragment);
                        AppActivity.setCurrentTitle(context.getResources().getString(R.string.news_feed));
                        return false;
                    }
                })
                .withName(context.getResources().getString(R.string.home)));
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_friend_request_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = new FriendsSuggestionFragment();
                        AppActivity.setFragment(fragment);
                        AppActivity.setCurrentTitle(context.getResources().getString(R.string.find_friends));
                        return false;
                    }
                })


                .withName(context.getResources().getString(R.string.find_friends)));
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_friend_request_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = new OnlineFriendsFragment();
                        AppActivity.setFragment(fragment);
                        AppActivity.setCurrentTitle(context.getResources().getString(R.string.online_friends));
                        return false;
                    }
                })
                .withName(context.getResources().getString(R.string.online_friends)));
        drawer.addItem(new DividerDrawerItem());
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_friend_request_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Intent intent = new Intent(context, ProfileActivity.class);
                        context.startActivity(intent);
                        drawer.closeDrawer();
                        return false;
                    }
                })

                .withName(context.getResources().getString(R.string.my_profile)));
        drawer.addItem(new DividerDrawerItem());
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_message_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = new MessagesFragment();
                        AppActivity.setFragment(fragment);
                        AppActivity.setCurrentTitle(context.getResources().getString(R.string.messages));
                        return false;
                    }
                })
                .withName(context.getResources().getString(R.string.messages)));
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_notification_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = new NotificationFragment();
                        AppActivity.setFragment(fragment);
                        AppActivity.setCurrentTitle(context.getResources().getString(R.string.notifications));
                        return false;
                    }
                })
                .withName(context.getResources().getString(R.string.notifications)));

        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_friend_request_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = new FriendRequestFragment();
                        AppActivity.setFragment(fragment);
                        AppActivity.setCurrentTitle(context.getResources().getString(R.string.friend_requests));
                        return false;
                    }
                })
                .withName(context.getResources().getString(R.string.friend_requests)));

        drawer.addItem(new DividerDrawerItem());
    }

    @Override
    public Fragment getFragment(Context context) {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        return context.getResources().getString(R.string.app_name);
    }

    @Override
    public Object invoke(Context context, String type, Object object) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type) {
        return null;
    }


}
