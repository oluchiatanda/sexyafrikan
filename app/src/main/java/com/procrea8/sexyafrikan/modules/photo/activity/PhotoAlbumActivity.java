package com.procrea8.sexyafrikan.modules.photo.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.GridSpacingItemDecoration;
import com.procrea8.sexyafrikan.modules.photo.PhotoApiInterface;
import com.procrea8.sexyafrikan.modules.photo.adapter.PhotosAdapter;
import com.procrea8.sexyafrikan.modules.photo.dialog.AlbumCreateDialog;
import com.procrea8.sexyafrikan.modules.photo.model.Photo;
import com.procrea8.sexyafrikan.modules.photo.model.PhotoAlbum;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotoAlbumActivity extends ActivityBase implements Contants {

    FloatingActionButton addPhoto;
    RecyclerView recyclerView;
    ImageView editAlbum;
    ImageView deleteAlbum;
    PhotosAdapter photosAdapter;
    ArrayList<Photo> photos = new ArrayList<>();

    Integer limit = 20;
    Integer offset = 0;

    Boolean paginating = false;

    String albumId = "";
    String theUserid = "";
    PhotoAlbum photoAlbum;
    
    String selectedPhoto = "";

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_album);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        if (intent.hasExtra("album_id")) albumId = intent.getStringExtra("album_id");
        if (intent.hasExtra("the_userid")) theUserid = intent.getStringExtra("the_userid");

        addPhoto = (FloatingActionButton) findViewById(R.id.add_photos);
        editAlbum = (ImageView) findViewById(R.id.edit_album);
        deleteAlbum = (ImageView) findViewById(R.id.delete_album);
        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycleview);
        photosAdapter = new PhotosAdapter(this, photos);

        final RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(2), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(photosAdapter);

        loadPhotos(false);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisibleItems = ((GridLayoutManager) layoutManager).findLastVisibleItemPosition();
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        //we are in the last item
                        if (!paginating) {
                            offset = offset + limit;
                            loadPhotos(true);
                            paginating = true;
                        }
                    }
                }
            }
        });

        deleteAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(R.string.are_you_sure);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        setLoading();
                        Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(PhotoApiInterface.class).deleteAlbum(
                                App.getInstance().getSession().getUserid(),
                                albumId
                        );
                        call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                            @Override
                            public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, Response<com.procrea8.sexyafrikan.response.Response> response) {
                                hideLoading();
                                onBackPressed();
                            }

                            @Override
                            public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                                Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.create().show();
            }
        });

        editAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlbumCreateDialog dialog = new AlbumCreateDialog(context, "edit", photoAlbum);
                dialog.setListener(new AlbumCreateDialog.OnPhotoAlbumCreatedListener() {
                    @Override
                    public void onCreated(Boolean created) {
                        if (created) {
                             // reload the album lists
                        }
                    }
                });
            }
        });

        if(!albumId.equals("profile") && !albumId.equals("timeline") && !albumId.equals("cover")) {
            Call<PhotoAlbum> call = Api.getRetrofit().create(PhotoApiInterface.class).getAlbum(
                    App.getInstance().getSession().getUserid(),
                    albumId
            );

            call.enqueue(new Callback<PhotoAlbum>() {
                @Override
                public void onResponse(Call<PhotoAlbum> call, Response<PhotoAlbum> response) {
                    if (response.body().getUserid().equals(App.getInstance().getSession().getUserid())) {
                        addPhoto.setVisibility(View.VISIBLE);
                        editAlbum.setVisibility(View.VISIBLE);
                        deleteAlbum.setVisibility(View.VISIBLE);

                        photoAlbum = response.body();
                    }
                }

                @Override
                public void onFailure(Call<PhotoAlbum> call, Throwable t) {

                }
            });
        }


        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Activity activity = (Activity) context;
                setPermissionGranted(new ActivityBase.OnPermissionGranted() {
                    @Override
                    public void onGranted() {
                        MultiImageSelector.create()
                                .showCamera(true)
                                .count(1)
                                .single()
                                .start(PhotoAlbumActivity.this, ALBUM_SELECT_PHOTO);
                    }
                });

            }
        });
    }

    public void loadPhotos(Boolean more) {
        //if (more) MyLog.e("Paginating - " + limit + "-" + offset);
        Call<ArrayList<Photo>> call = Api.getRetrofit().create(PhotoApiInterface.class).getAlbumPhotos(
                App.getInstance().getSession().getUserid(),
                albumId,
                limit,
                offset,
                theUserid
        );
        call.enqueue(new Callback<ArrayList<Photo>>() {
            @Override
            public void onResponse(Call<ArrayList<Photo>> call, Response<ArrayList<Photo>> response) {
                photos.addAll(response.body());
                photosAdapter.notifyDataSetChanged();
                paginating = false;
            }

            @Override
            public void onFailure(Call<ArrayList<Photo>> call, Throwable t) {
                //MyLog.e("Failed - " + t.getMessage());
            }
        });
    }

    public static void loadAlbum(Context context, String id, String theUserid) {
        Intent intent = new Intent(context, PhotoAlbumActivity.class);
        intent.putExtra("album_id", id);
        intent.putExtra("the_userid", theUserid);
        context.startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ALBUM_SELECT_PHOTO && resultCode == Activity.RESULT_OK) {
            List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
            selectedPhoto = path.get(0);
            setPermissionGranted(new OnPermissionGranted() {
                @Override
                public void onGranted() {
                    startPhotoUpload();
                }
            });
        }
    }

    public void startPhotoUpload() {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading..");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
        Call<Photo> call;
        File file = new File(selectedPhoto);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part body = MultipartBody.Part.createFormData("photo", file.getName(), requestFile);
        call = Api.getRetrofit().create(PhotoApiInterface.class).upload(App.getInstance().getSession().getUserid(), albumId, body);
       
        call.enqueue(new Callback<Photo>() {
            @Override
            public void onResponse(Call<Photo> call, Response<Photo> response) {
                progressDialog.dismiss();
                photos.add(response.body());
                photosAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), R.string.photo_successful_upload, Toast.LENGTH_LONG).show();
                selectedPhoto = "";
            }

            @Override
            public void onFailure(Call<Photo> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), R.string.failed_internet, Toast.LENGTH_LONG).show();
            }
        });
    }
}
