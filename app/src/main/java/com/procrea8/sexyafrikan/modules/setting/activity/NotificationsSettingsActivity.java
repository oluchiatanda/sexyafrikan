package com.procrea8.sexyafrikan.modules.setting.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;

public class NotificationsSettingsActivity extends ActivityBase {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Fragment fragment = new SettingsActivity.MainSettingFragment();
        Bundle bundle = new Bundle();

        bundle.putString("setting", "notifications");
        fragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.settings_frame, fragment).commit();
    }
}

