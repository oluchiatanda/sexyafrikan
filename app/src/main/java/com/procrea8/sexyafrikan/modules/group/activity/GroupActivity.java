package com.procrea8.sexyafrikan.modules.group.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.modules.feed.FeedApiInterface;
import com.procrea8.sexyafrikan.modules.feed.adapter.FeedAdapter;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;
import com.procrea8.sexyafrikan.modules.group.GroupApiInterface;
import com.procrea8.sexyafrikan.modules.group.model.Group;
import com.procrea8.sexyafrikan.modules.photo.activity.PhotoViewerActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupActivity extends ActivityBase {
    static Group group;

    ListView feedListsView;
    FeedAdapter feedAdapter;
    ArrayList<FeedResponse> feedLists = new ArrayList<>();
    ImageView profileCover;
    ImageView profileLogo;
    TextView profileTitle;
    TextView description;
    ImageView editButton;
    ImageView deleteButton;
    Button joinButton;
    Button leaveButton;
    LinearLayout joinContainer;


    String limit = "10";
    String offset = "0";

    View footerLoading;

    Boolean moreFeedsLoading = false;

    //cover and avatar
    String loggedInId;
    ImageView changeCover;
    ImageView changeLogo;
    String changePhotoType = "";
    static String selectedPhoto = "";

    Context context;
    LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = this;
        inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        feedListsView = (ListView) findViewById(R.id.feed_lists);

        feedListsView.addHeaderView(inflater.inflate(R.layout.group_profile_header, null));
        footerLoading = (View) inflater.inflate(R.layout.loading_indicator, null);
        profileCover = (ImageView) findViewById(R.id.profile_cover);
        profileLogo = (ImageView) findViewById(R.id.profile_logo);
        profileTitle = (TextView) findViewById(R.id.profile_name);
        changeCover  = (ImageView)findViewById(R.id.change_cover);
        changeLogo  = (ImageView)findViewById(R.id.change_logo);
        joinButton = (Button) findViewById(R.id.join_button);
        leaveButton = (Button) findViewById(R.id.leave_button);
        description = (TextView) findViewById(R.id.group_description);
        joinContainer = (LinearLayout) findViewById(R.id.join_container);

        loggedInId = App.getInstance().getSession().getUserid();

        profileCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (group != null) {
                    PhotoViewerActivity.launch(context, group.getCover());
                }
            }
        });

        profileLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (group != null) {
                    PhotoViewerActivity.launch(context, group.getLogo());
                }
            }
        });

        feedListsView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                final int lastItem = i + i1;
                if (i2 > 5 && lastItem == i2) {
                    if (!moreFeedsLoading) {
                        loadMoreFeeds();
                        moreFeedsLoading = true;
                    }
                }
            }
        });

        changeCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePhoto("cover");
            }
        });

        changeLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePhoto("logo");
            }
        });

        joinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(GroupApiInterface.class).join(
                        App.getInstance().getSession().getUserid(),
                        group.getId(),
                        "0"
                );
                call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                    @Override
                    public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, Response<com.procrea8.sexyafrikan.response.Response> response) {
                        leaveButton.setVisibility(View.VISIBLE);
                        joinButton.setVisibility(View.GONE);
                        group.setIsMember(true);
                    }

                    @Override
                    public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {

                    }
                });
            }
        });

        leaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(GroupApiInterface.class).join(
                        App.getInstance().getSession().getUserid(),
                        group.getId(),
                        "1"
                );
                call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                    @Override
                    public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, Response<com.procrea8.sexyafrikan.response.Response> response) {
                        leaveButton.setVisibility(View.VISIBLE);
                        joinButton.setVisibility(View.GONE);
                        group.setIsMember(false);
                    }

                    @Override
                    public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                        Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        /**editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**EventCreateDialog createDialog = new EventCreateDialog(context, getSupportFragmentManager(), "edit", event);
                createDialog.addListener(new EventCreateDialog.OnEventCreatedListener() {
                    @Override
                    public void onEventCreated(Event newEvent) {
                        event = newEvent;
                        setEventDetails();
                    }
                });
            }
        });**/
        feedAdapter = new FeedAdapter(context, feedLists, "group", group.getId(), loggedInId, "user");
        feedListsView.addFooterView(footerLoading);
        feedListsView.setAdapter(feedAdapter);
        feedListsView.removeFooterView(footerLoading);

        loadPosts(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        profileTitle.setText(group.getTitle());
        description.setText(group.getDescription());
        Glide.with(context).load(group.getCover()).into(profileCover);
        Glide.with(context).load(group.getLogo()).into(profileLogo);

        if (group.getIsAdmin()) {
            joinContainer.setVisibility(View.GONE);
            changeCover.setVisibility(View.VISIBLE);
            changeLogo.setVisibility(View.VISIBLE);
        } else {
            if (group.getIsMember()) {
                joinButton.setVisibility(View.GONE);
                leaveButton.setVisibility(View.VISIBLE);
            } else {
                joinButton.setVisibility(View.VISIBLE);
                leaveButton.setVisibility(View.GONE);
            }
        }
    }

    public static void load(Context c, Group g){
        group = g;
        Intent intent = new Intent(c, GroupActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        c.startActivity(intent);
    }

    public void addDefaultPostEditor() {
        FeedResponse feedResponse = new FeedResponse();
        feedResponse.setIsFeedItem(false);
        feedLists.add(feedResponse);
    }

    public void loadPosts(final Boolean more) {

        Call<ArrayList<FeedResponse>> call = Api.getRetrofit().create(FeedApiInterface.class).getFeeds(App.getInstance().getSession().getUserid(),
                "group", group.getId(), limit, offset);
        call.enqueue(new Callback<ArrayList<FeedResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<FeedResponse>> call, Response<ArrayList<FeedResponse>> response) {
                if (!more) {
                    feedLists.clear();
                    addDefaultPostEditor();
                }

                if (response.body() != null && response.body().size() > 0) {
                    feedLists.addAll(response.body());

                } else {

                }
                feedListsView.removeFooterView(footerLoading);
                feedAdapter.notifyDataSetChanged();
                moreFeedsLoading = false;
            }

            @Override
            public void onFailure(Call<ArrayList<FeedResponse>> call, Throwable t) {
                feedListsView.removeFooterView(footerLoading);
                moreFeedsLoading = false;
            }
        });
    }

    public void loadMoreFeeds() {
        offset = offset + limit;
        feedListsView.addFooterView(footerLoading);
        loadPosts(true);
    }

    public void changePhoto(String type) {
        changePhotoType = type;
        setPermissionGranted(new ActivityBase.OnPermissionGranted() {
            @Override
            public void onGranted() {
                MultiImageSelector.create()
                        .showCamera(true)
                        .count(1)
                        .single()
                        .start(GroupActivity.this, PROFILE_SELECT_PHOTO);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PROFILE_SELECT_PHOTO && resultCode == Activity.RESULT_OK) {
            List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
            selectedPhoto = path.get(0);;
            setPermissionGranted(new OnPermissionGranted(){
                @Override
                public void onGranted() {
                    startPhotoUpload();
                }
            });

        }
    }

    public void startPhotoUpload() {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Uploading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
        Call<com.procrea8.sexyafrikan.response.Response> call;
        File file = new File(selectedPhoto);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        if (changePhotoType.equals("logo")) {
            call = Api.getRetrofit().create(GroupApiInterface.class).changeLogo(loggedInId, group.getId(), body);
        } else {
            call = Api.getRetrofit().create(GroupApiInterface.class).changeCover(loggedInId, group.getId(), body);
        }

        call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
            @Override
            public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, Response<com.procrea8.sexyafrikan.response.Response> response) {
                progressDialog.dismiss();
                if (response.body().getStatus()==1) {

                    if (changePhotoType.equals("cover")) {
                        group.setCover(response.body().getDataOne());
                        Glide.with(context).load(group.getCover()).into(profileCover);
                    } else {
                        group.setLogo(response.body().getDataOne());
                        Glide.with(context).load(group.getLogo()).into(profileLogo);
                    }
                    Toast.makeText(context, R.string.photo_successful_changed, Toast.LENGTH_LONG).show();
                    selectedPhoto = "";
                    changePhotoType = "";
                }
            }

            @Override
            public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
            }
        });
    }

}
