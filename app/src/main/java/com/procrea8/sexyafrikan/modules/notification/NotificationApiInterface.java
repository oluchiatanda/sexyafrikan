package com.procrea8.sexyafrikan.modules.notification;


import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NotificationApiInterface {
    @GET("notifications")
    Call<ArrayList<NotificationModel>> getNotifications(@Query("userid") String userid, @Query("limit") String limit, @Query("page") String page);
    @GET("notification/delete")
    Call<Response> delete(@Query("userid") String userid, @Query("id") String id);
    @GET("notifications/unread")
    Call<ArrayList<NotificationModel>> getUnreadNotifications(@Query("userid") String userid, @Query("limit") String limit);
}
