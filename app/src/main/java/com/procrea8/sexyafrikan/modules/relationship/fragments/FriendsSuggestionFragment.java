package com.procrea8.sexyafrikan.modules.relationship.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.relationship.RelationshipApiInterface;
import com.procrea8.sexyafrikan.modules.relationship.adapter.UsersAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsSuggestionFragment extends Fragment implements Contants{
    TextView titlePane;
    EditText searchText;
    ImageView searchButton;
    ListView listView;
    AVLoadingIndicatorView loadingImage;
    View footerLoading;
    TextView statusText;
    RelativeLayout searchPane;
    SwipeRefreshLayout swipeRefreshLayout;

    ArrayList<User> usersList = new ArrayList<>();
    UsersAdapter usersAdapter;
    Boolean suggestionLoading = false;
    Integer page = 1;
    String search_text = "";
    public FriendsSuggestionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.general_list, container, false);
        titlePane = (TextView) view.findViewById(R.id.list_title);
        searchText = (EditText) view.findViewById(R.id.search_text);
        searchButton = (ImageView) view.findViewById(R.id.search_button);
        listView = (ListView) view.findViewById(R.id.list);
        loadingImage = (AVLoadingIndicatorView) view.findViewById(R.id.loading_image);
        footerLoading = inflater.inflate(R.layout.loading_indicator, null);
        statusText = (TextView) view.findViewById(R.id.status_text);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        searchPane = (RelativeLayout) view.findViewById(R.id.search_pane);


        listView.addFooterView(footerLoading);
        usersAdapter = new UsersAdapter(getContext(), usersList);
        listView.setAdapter(usersAdapter);
        listView.removeFooterView(footerLoading);
        titlePane.setText(R.string.find_friends);

        loadSuggestions(false);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                loadSuggestions(false);
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                final int lastItem = i + i1;
                if (i2 > 0 && lastItem == i2) {
                    if (!suggestionLoading && usersList.size() > 5) {

                        listView.addFooterView(footerLoading);
                        page++;
                        suggestionLoading = true;
                        loadSuggestions(true);
                    }
                }
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String search = searchText.getText().toString();
                if (!search.isEmpty()) {
                    search_text = search;
                    loadSuggestions(false);
                }
            }
        });
        return view;
    }

    public void  loadSuggestions(final Boolean more) {
        String userid = App.getInstance().getSession().getUserid();
        //MyLog.e("loading suggestion");
        Call<ArrayList<User>> call = Api.getRetrofit().create(RelationshipApiInterface.class).suggestions(userid, page.toString(), search_text);
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                if (!more) usersList.clear();
                usersList.addAll(response.body());
                usersAdapter.notifyDataSetChanged();
                loadingImage.hide();
                swipeRefreshLayout.setRefreshing(false);
                if (more) {
                    listView.removeFooterView(footerLoading);
                    suggestionLoading = false;
                } else {
                    if (response.body().size() < 1) {
                        statusText.setText(R.string.no_new_friend_found);
                        statusText.setVisibility(View.VISIBLE);
                    } else {
                        statusText.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                loadingImage.hide();
                suggestionLoading = false;
                if (!more) {
                    statusText.setText(R.string.no_new_friend_found);
                    statusText.setVisibility(View.VISIBLE);
                } else {
                    page--;
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

}
