package com.procrea8.sexyafrikan.modules.video.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.video.activity.VideoActivity;
import com.procrea8.sexyafrikan.modules.video.model.Video;

import java.util.ArrayList;

public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.MyViewHolder>{
    Context context;
    ArrayList<Video> listings = new ArrayList<>();
    LayoutInflater layoutInflater;

    public VideosAdapter(Context c, ArrayList<Video> lists) {
        context = c;
        listings = lists;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        ImageView playButton;
        TextView title;
        TextView viewCount;
        public MyViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.title);
            playButton = (ImageView) itemView.findViewById(R.id.play_button);
            viewCount = (TextView) itemView.findViewById(R.id.view_count);
        }
    }

    @Override
    public VideosAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.video_card, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VideosAdapter.MyViewHolder holder, int position) {
        final Video video = listings.get(position);
        Glide.with(context).load(video.getPhoto()).into(holder.image);
        holder.title.setText(video.getTitle());
        holder.viewCount.setText(video.getViewCount());
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VideoActivity.load(context, video);
            }
        });
        holder.playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VideoActivity.load(context, video);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listings.size();
    }
}