package com.procrea8.sexyafrikan.modules.profile.fragments;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.GridSpacingItemDecoration;
import com.procrea8.sexyafrikan.modules.photo.adapter.PhotoAlbumsAdapter;
import com.procrea8.sexyafrikan.modules.photo.dialog.AlbumCreateDialog;
import com.procrea8.sexyafrikan.modules.photo.model.PhotoAlbum;
import com.procrea8.sexyafrikan.modules.profile.ProfileApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfilePhotoAlbumsFragment extends Fragment {
    RecyclerView recyclerView;
    static String userid;
    String loggedInId;
    FloatingActionButton floatingActionButton;
    
    PhotoAlbumsAdapter adapter;
    ArrayList<PhotoAlbum> albums = new ArrayList<>();

    Integer limit = 20;
    Integer offset = 0;

    Boolean paginating = false;

    public ProfilePhotoAlbumsFragment() {
        // Required empty public constructor
    }


    public static ProfilePhotoAlbumsFragment getInstance(String id) {
        userid = id;
        return new ProfilePhotoAlbumsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_photo_albums, container, false);
        loggedInId = App.getInstance().getSession().getUserid();

        recyclerView = (RecyclerView) view.findViewById(R.id.recycleview);
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.add_album);
        final RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(2), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        if (userid.equals(loggedInId)) {
            floatingActionButton.setVisibility(View.VISIBLE);
        }
        adapter = new PhotoAlbumsAdapter(getContext(), albums);
        recyclerView.setAdapter(adapter);
        


        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlbumCreateDialog dialog = new AlbumCreateDialog(getContext(), "create", null);
                dialog.setListener(new AlbumCreateDialog.OnPhotoAlbumCreatedListener() {
                    @Override
                    public void onCreated(Boolean created) {
                        if (created) {
                            loadAlbums(false); // reload the album lists
                        }
                    }
                });
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisibleItems = ((GridLayoutManager) layoutManager).findLastVisibleItemPosition();
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        //we are in the last item
                        if (!paginating) {
                            offset = offset + limit;
                            loadAlbums(true);
                            paginating = true;
                        }
                    }
                }
            }
        });


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        loadAlbums(false); //we need to refresh list
    }

    public void loadAlbums(final Boolean more) {
        //if (more) MyLog.e("Paginating - " + limit + "-" + offset);
        Call<ArrayList<PhotoAlbum>> call = Api.getRetrofit().create(ProfileApiInterface.class).getAlbums(
                App.getInstance().getSession().getUserid(),
                userid,
                limit,
                offset
        );
        call.enqueue(new Callback<ArrayList<PhotoAlbum>>() {
            @Override
            public void onResponse(Call<ArrayList<PhotoAlbum>> call, Response<ArrayList<PhotoAlbum>> response) {
                if (!more) albums.clear();
                albums.addAll(response.body());
                adapter.notifyDataSetChanged();
                paginating = false;
                //MyLog.e("Albums loaded - " + response.body().size());
            }

            @Override
            public void onFailure(Call<ArrayList<PhotoAlbum>> call, Throwable t) {
                //MyLog.e("Failed - " + t.getMessage());
            }
        });
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
