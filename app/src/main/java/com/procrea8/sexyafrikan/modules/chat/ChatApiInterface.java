package com.procrea8.sexyafrikan.modules.chat;

import com.procrea8.sexyafrikan.modules.chat.model.Chat;
import com.procrea8.sexyafrikan.modules.chat.model.Message;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ChatApiInterface {
    @GET("chat/conversations")
    Call<ArrayList<Chat>> getConversations(@Query("userid") String userid);

    @GET("chat/get/messages")
    Call<ArrayList<Message>> getMessages(@Query("userid") String userid, @Query("cid") String cid, @Query("theuserid") String theUserid);

    @POST("chat/send/message")
    Call<Message> sendMessage(@Query("userid") String userid, @Query("cid") String cid, @Query("theuserid") String theUserid, @Query("text") String text);

    @Multipart
    @POST("chat/send/message")
    Call<Message> sendMessage(@Query("userid") String userid, @Query("cid") String cid, @Query("theuserid") String theUserid, @Query("text") String text, @Part MultipartBody.Part image);

    @GET("call/get/identity")
    Call<Response> getIdentity(@Query("cid") String cid, @Query("userid") String userid, @Query("theuserid") String dUserid);
}
