package com.procrea8.sexyafrikan.modules.event.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.event.activity.EventActivity;
import com.procrea8.sexyafrikan.modules.event.model.Event;

import java.util.ArrayList;

/**
 * Created by Tiamiyu waliu kola on 9/19/2016.
 */
public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.MyViewHolder> {
    Context context;
    ArrayList<Event> listings = new ArrayList<>();
    LayoutInflater layoutInflater;

    public EventsAdapter(Context c, ArrayList<Event> lists) {
        context = c;
        listings = lists;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView title;
        TextView time;
        public MyViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.title);
            time = (TextView) itemView.findViewById(R.id.time);
        }
    }

    @Override
    public EventsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.event_card, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EventsAdapter.MyViewHolder holder, int position) {
        final Event event = listings.get(position);
        Glide.with(context).load(event.getImage()).crossFade().into(holder.image);
        holder.title.setText(event.getTitle());
        holder.time.setText(event.getEventTime());
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventActivity.load(context, event);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listings.size();
    }
}
