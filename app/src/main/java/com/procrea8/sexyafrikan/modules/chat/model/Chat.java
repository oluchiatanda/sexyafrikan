package com.procrea8.sexyafrikan.modules.chat.model;

import com.google.gson.annotations.SerializedName;

public class Chat {
    @SerializedName("cid")
    String cid;
    @SerializedName("avatar")
    String avatar;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Integer getUnread() {
        return unread;
    }

    public void setUnread(Integer unread) {
        this.unread = unread;
    }

    @SerializedName("title")
    String title;
    @SerializedName("userid")
    String userid;
    @SerializedName("time")
    String time;
    @SerializedName("message")
    String lastMessage;
    @SerializedName("unread")
    Integer unread;
}
