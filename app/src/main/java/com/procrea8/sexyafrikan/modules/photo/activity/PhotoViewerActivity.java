package com.procrea8.sexyafrikan.modules.photo.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.graphics.Bitmap;

import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.photo.PhotoApiInterface;
import com.procrea8.sexyafrikan.modules.photo.model.Photo;
import com.procrea8.sexyafrikan.modules.profile.fragments.ProfileFeedFragment;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.FileOutputStream;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.senab.photoview.PhotoViewAttacher;

import android.view.MotionEvent;

public class PhotoViewerActivity extends ActivityBase {

    String photo = "";
    ImageView image;
    LinearLayout viewerFooter, viewerHeader;
    Context context;
    ImageView navigateLeft;
    ImageView navigateRight;
    ImageView photoAction;
    String mCurrentPhotoPath;
    PhotoViewAttacher pva;
//    AVLoadingIndicatorView loadingImage;
    ProfileFeedFragment feedFragment = new ProfileFeedFragment();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_viewer);

        Intent intent = getIntent();
        if (intent.hasExtra("photo")) photo = intent.getStringExtra("photo");
        if (photo.isEmpty()) {
            onBackPressed(); //force go back
        }
        context = this;
        image = (ImageView) findViewById(R.id.image);
        viewerFooter = (LinearLayout) findViewById(R.id.viewer_footer);
        viewerHeader = (LinearLayout) findViewById(R.id.viewer_header);
        navigateLeft = (ImageView) findViewById(R.id.navigate_left);
        navigateRight = (ImageView) findViewById(R.id.navigat_right);
        photoAction = (ImageView) findViewById(R.id.photo_action);

        loadPhotoDetails();

//        loadingImage.hide();
        image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                pva = new PhotoViewAttacher(image);
                pva.update();
                return true;
            }
        });

        viewerHeader.setVisibility(View.VISIBLE);
        photoAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                final ArrayList<String> items = new ArrayList<String>();
                items.add("Save");
                items.add("Share");

                ArrayAdapter<String> aAdapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, items);
                builder.setAdapter(aAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        switch (i) {
                            case 0:
                                if (image.getDrawable()!=null) {
                                    Bitmap bitmap = ((GlideBitmapDrawable) image.getDrawable().getCurrent()).getBitmap();
                                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                                    String fileName = "IMG_" + timeStamp + ".jpg";
                                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), getResources().getString(R.string.app_name));

                                    if (!file.isDirectory() && file.canWrite()) {
                                        file.delete();
                                    }
                                    file.mkdirs();


                                    try {
                                        File storageDir = new File(file, fileName);
                                        FileOutputStream fos = new FileOutputStream(storageDir);
                                        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                                        fos.close();
                                        mCurrentPhotoPath = storageDir.getAbsolutePath();
                                        Toast.makeText(context, "Photo Saved", Toast.LENGTH_SHORT).show();
                                        galleryAddPic();
                                    } catch (Exception e) {
                                        Log.e("Not getting bitmap", e.getMessage());
                                        e.printStackTrace();
                                    }
                                }else return;
                                break;
                            case 1:
                                ImageView image = (ImageView) findViewById(R.id.image);
                                if (image.getDrawable()!=null) {
                                    Bitmap bitmaps = ((GlideBitmapDrawable) image.getDrawable()).getBitmap(); //file to be saved as per user selection
                                    String sd = Environment.getExternalStorageDirectory().getAbsolutePath();
                                    File ssd = new File(sd, "share");
                                    if (!ssd.exists())
                                        ssd.mkdirs();
                                    String fileNames = "test"; //saved as png file
                                    File dest = new File(ssd, fileNames + ".png");
                                    FileOutputStream out = null;
                                    if (dest.canWrite()) {
                                        dest.setWritable(true);
                                    }
                                    try {
                                        out = new FileOutputStream(dest);
                                        bitmaps.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                                        // PNG is a lossless format, the compression factor (100) is ignored
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } finally {
                                        try {
                                            if (out != null) {
                                                out.close();
                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    Uri uri = Uri.fromFile(dest);
                                    Intent shareIntent = new Intent();
                                    shareIntent.setAction(Intent.ACTION_SEND);
                                    shareIntent.setType("*/*");
                                    shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                                    startActivity(Intent.createChooser(shareIntent, "Share Image")); //shared via Intent
                                }
                                break;
                        }
                    }
                });
                builder.create().show();
            }
        });
//        viewerFooter.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        Glide.with(this).load(photo).crossFade().into(image);
    }

    public void loadPhotoDetails() {
        Call<Photo> call = Api.getRetrofit().create(PhotoApiInterface.class).getDetails(
                App.getInstance().getSession().getUserid(),
                photo
        );
        call.enqueue(new Callback<Photo>() {
            @Override
            public void onResponse(Call<Photo> call, final Response<Photo> response) {
                if (response.body() != null){
//                    loadingImage.show();
                    if (response.body().getStatus() == 1) {
                        final Photo dDhoto = response.body();
                        if (context == null) return;

                        initGlobalFooter(context, "photo", dDhoto.getId(), dDhoto.isHasReact(), dDhoto.isHasLike(),
                                dDhoto.isHasDislike(),
                                dDhoto.getLikeCount(), dDhoto.getDislikeCount(), dDhoto.getComments());
                        try {
                            Glide.with(context).load(dDhoto.getPath()).crossFade().into(image);
                        } catch (IllegalArgumentException e) {
                        }
                        if (dDhoto.getImageBefore() != null) {
                            navigateLeft.setVisibility(View.VISIBLE);
//                        navigateRight.setVisibility(View.GONE);
                            navigateLeft.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    photo = dDhoto.getImageBefore();
                                    loadPhotoDetails(); //reload the viewer
                                    MyLog.e("anotherPhot - " + photo);
                                }
                            });
                        } else {
                            navigateLeft.setVisibility(View.GONE);
                        }

                        if (dDhoto.getImageAfter() != null) {
                            navigateRight.setVisibility(View.VISIBLE);
                            navigateRight.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    photo = dDhoto.getImageAfter();
                                    MyLog.e("anotherPhot - " + photo);
                                    loadPhotoDetails(); //reload the viewer
                                }
                            });
                        } else {
                            navigateRight.setVisibility(View.GONE);
                        }
                    }
            }
            }

            @Override
            public void onFailure(Call<Photo> call, Throwable t) {

            }
        });
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    public static void launch(Context c, String photo) {
        Intent intent = new Intent(c, PhotoViewerActivity.class);
        intent.putExtra("photo", photo);
        c.startActivity(intent);
    }


}
