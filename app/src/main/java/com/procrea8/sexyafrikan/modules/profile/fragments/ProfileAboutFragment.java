package com.procrea8.sexyafrikan.modules.profile.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.adapter.InfoAdapter;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.model.Info;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.profile.ProfileApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileAboutFragment extends Fragment {

    static ProfileAboutFragment instance;
    static User user;
    static String userid;

    TextView title;
    ListView infoListView;
    ArrayList<Info> infoLists = new ArrayList<>();
    InfoAdapter infoAdapter;


    public ProfileAboutFragment() {
        // Required empty public constructor
    }

    public static ProfileAboutFragment getInstance(String userid1) {
        if (userid != null && !userid.equals(userid1)) {
            instance = null;
        }
        if (instance != null) return instance;
        instance = new ProfileAboutFragment();
        userid = userid1;
        return instance;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        infoListView = (ListView) view.findViewById(R.id.info);

        infoAdapter = new InfoAdapter(getContext(), infoLists);
        infoListView.setAdapter(infoAdapter);

        loadProfileDetails();

        return view;
    }

    public void loadProfileDetails() {
        Call<User> call = Api.getRetrofit().create(ProfileApiInterface.class).get(
                App.getInstance().getSession().getUserid(),
                userid
        );
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                user = response.body();
                infoLists.clear();
                infoLists.addAll(user.getProfileInfo());
                infoAdapter.notifyDataSetChanged();
                MyLog.e("Info has arrived " + user.getProfileInfo().size());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }


}