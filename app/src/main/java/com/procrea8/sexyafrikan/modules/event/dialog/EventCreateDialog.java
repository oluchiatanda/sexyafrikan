package com.procrea8.sexyafrikan.modules.event.dialog;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.modules.event.EventApiInterface;
import com.procrea8.sexyafrikan.modules.event.model.Event;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;
import com.satsuware.usefulviews.LabelledSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import retrofit2.Call;
import retrofit2.Callback;

public class EventCreateDialog implements Contants {
    String type = "create";
    Event event; //for edit type

    String privacy = "0";
    String category = "1";
    String start_day = "";
    String start_month = "";
    String start_year = "";
    String start_hour = "";
    String start_minute = "";
    String start_time_type = "am";

    String end_day = "";
    String end_month = "";
    String end_year = "";
    String end_hour = "";
    String end_minute = "";
    String end_time_type = "am";
    Context context;

    EditText title;
    EditText description;
    EditText address;
    Button startDate;
    Button endDate;
    EditText location;
    LabelledSpinner spinner;
    LabelledSpinner categorySpinner;


    ArrayList<MusicCategory> loadedCategories = new ArrayList<>();

    OnEventCreatedListener listener;

    FragmentManager fragmentManger;

    public EventCreateDialog(final Context context, FragmentManager manager,  final String type, final Event event) {
        this.type = type;
        this.context = context;
        this.fragmentManger = manager;
        this.event = event;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (type.equals("create")) {
            builder.setTitle(R.string.add_new_event);
        } else {
            builder.setTitle(R.string.edit_event);
        }
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.event_create, null);
        title = (EditText) convertView.findViewById(R.id.title);
        description = (EditText) convertView.findViewById(R.id.description);
        spinner = (LabelledSpinner) convertView.findViewById(R.id.privacy);
        address = (EditText) convertView.findViewById(R.id.address);
        location = (EditText) convertView.findViewById(R.id.location);
        startDate = (Button) convertView.findViewById(R.id.start_date);
        endDate = (Button) convertView.findViewById(R.id.end_date);
        spinner.setItemsArray(R.array.event_privacy_list);
        categorySpinner = (LabelledSpinner) convertView.findViewById(R.id.category);

        //lets public the category label
        Call<ArrayList<MusicCategory>> call = Api.getRetrofit().create(EventApiInterface.class).getCategories(App.getInstance().getSession().getUserid());
        call.enqueue(new Callback<ArrayList<MusicCategory>>() {
            @Override
            public void onResponse(Call<ArrayList<MusicCategory>> call, retrofit2.Response<ArrayList<MusicCategory>> response) {
                ArrayList<String> items = new ArrayList<String>();
                for (int i = 0; i < response.body().size(); i++) {
                    items.add(response.body().get(i).getTitle());
                }
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, items);
                categorySpinner.setCustomAdapter(arrayAdapter);
                categorySpinner.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
                    @Override
                    public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
                        if (loadedCategories.size() > 0) {
                            category = loadedCategories.get(position).getId();
                        }
                    }

                    @Override
                    public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onFailure(Call<ArrayList<MusicCategory>> call, Throwable t) {
                Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
            }
        });

        if (type.equals("edit")) {
            //chooseEvent.setVisibility(View.GONE);
            categorySpinner.setVisibility(View.GONE);
            title.setText(event.getTitle());
            description.setText(event.getDescription());
            address.setText(event.getAddress());
        }

        spinner.setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
            @Override
            public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
                switch (position) {
                    case 0:
                        privacy = "0";
                        break;
                    case 1:
                        privacy = "1";
                        break;
                }
            }

            @Override
            public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

            }
        });

        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SlideDateTimePicker.Builder(fragmentManger)
                        .setListener(startListener)
                        .setInitialDate(new Date())
                        .build()
                        .show();
            }
        });
        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SlideDateTimePicker.Builder(fragmentManger)
                        .setListener(endListener)
                        .setInitialDate(new Date())
                        .build()
                        .show();
            }
        });
        builder.setView(convertView);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok, null).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String mtitle = title.getText().toString();
                        String mDesc = description.getText().toString();
                        String mAddress = address.getText().toString();
                        String mLocation = location.getText().toString();

                        if (mtitle.isEmpty()) {
                            Toast.makeText(context, R.string.provide_title, Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (mLocation.isEmpty()) {
                            Toast.makeText(context, R.string.provide_location, Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (start_day.isEmpty()) {
                            Toast.makeText(context, R.string.provide_event_start_time, Toast.LENGTH_LONG).show();
                            return;
                        }

                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        if (type.equals("create")) {
                            progressDialog.setMessage(context.getResources().getString(R.string.adding));
                        } else {
                            progressDialog.setMessage(context.getResources().getString(R.string.saving));
                        }
                        progressDialog.setCancelable(false);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressDialog.show();

                        Call<Event> call1 = null;

                        if (type.equals("create")) {

                            call1 = Api.getRetrofit().create(EventApiInterface.class).create(
                                    App.getInstance().getSession().getUserid(),
                                    mtitle,
                                    mDesc,
                                    mLocation,
                                    mAddress,
                                    privacy,
                                    category,
                                    start_hour,
                                    start_minute,
                                    start_day,
                                    start_month,
                                    start_year,
                                    start_time_type,
                                    end_hour,
                                    end_minute,
                                    end_day,
                                    end_month,
                                    end_year,
                                    end_time_type
                            );
                            
                        } else {
                            call1 = Api.getRetrofit().create(EventApiInterface.class).edit(
                                    App.getInstance().getSession().getUserid(),
                                    event.getId(),
                                    mtitle,
                                    mDesc,
                                    mLocation,
                                    mAddress,
                                    privacy,
                                    category,
                                    start_hour,
                                    start_minute,
                                    start_day,
                                    start_month,
                                    start_year,
                                    start_time_type,
                                    end_hour,
                                    end_minute,
                                    end_day,
                                    end_month,
                                    end_year,
                                    end_time_type
                            );
                        }


                        if (call1 != null) {
                            call1.enqueue(new Callback<Event>() {
                                @Override
                                public void onResponse(Call<Event> call, retrofit2.Response<Event> response) {
                                    progressDialog.dismiss();
                                    if (response.body().getStatus()==1) {
                                        dialog.dismiss();
                                        ///MyLog.e("Item created - " + response.body().getTitle());
                                        if (listener != null)
                                            listener.onEventCreated(response.body());
                                    } else {
                                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Event> call, Throwable t) {
                                    progressDialog.dismiss();
                                    ///MyLog.e("Add Song error" + t.getMessage());
                                    Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                                }
                            });


                        }
                    }
                });
            }
        });
        dialog.show();
    }

    public void addListener(OnEventCreatedListener l) {
        listener = l;
    }

    public static class OnEventCreatedListener {
        public void onEventCreated(Event event) {}
    }

    public SlideDateTimeListener startListener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            Integer day = calendar.get(Calendar.DAY_OF_MONTH);
            Integer month = calendar.get(Calendar.MONTH);
            Integer year = calendar.get(Calendar.YEAR);
            Integer hour = calendar.get(Calendar.HOUR_OF_DAY);
            Integer minute = calendar.get(Calendar.MINUTE);
            Integer type = calendar.get(Calendar.AM_PM);
            start_day = day.toString();
            start_month = month.toString();
            start_year = year.toString();
            start_hour = hour.toString();
            start_minute = minute.toString();
            start_time_type = (type == 1) ? "pm" : "am";
        }
    };

    public SlideDateTimeListener endListener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            Integer day = calendar.get(Calendar.DAY_OF_MONTH);
            Integer month = calendar.get(Calendar.MONTH);
            Integer year = calendar.get(Calendar.YEAR);
            Integer hour = calendar.get(Calendar.HOUR_OF_DAY);
            Integer minute = calendar.get(Calendar.MINUTE);
            Integer type = calendar.get(Calendar.AM_PM);
            end_day = day.toString();
            end_month = month.toString();
            end_year = year.toString();
            end_hour = hour.toString();
            end_minute = minute.toString();
            end_time_type = (type == 1) ? "pm" : "am";
        }
    };
}
