package com.procrea8.sexyafrikan.modules.video.model;


import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;

import java.util.ArrayList;

public class VideoBrowseResponse {
    @SerializedName("categories")
    ArrayList<MusicCategory> categories;
    @SerializedName("videos")
    ArrayList<Video> videos;

    public ArrayList<MusicCategory> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<MusicCategory> categories) {
        this.categories = categories;
    }

    public ArrayList<Video> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<Video> videos) {
        this.videos = videos;
    }
}
