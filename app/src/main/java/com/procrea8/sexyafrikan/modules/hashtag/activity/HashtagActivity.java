package com.procrea8.sexyafrikan.modules.hashtag.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.feed.adapter.FeedAdapter;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;
import com.procrea8.sexyafrikan.modules.hashtag.HashtagApiInterface;
import com.procrea8.sexyafrikan.modules.hashtag.model.HashtagResponse;
import com.procrea8.sexyafrikan.modules.hashtag.model.Hashtags;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HashtagActivity extends ActivityBase {

    ListView listView;
    FeedAdapter feedAdapter;
    ArrayList<FeedResponse> feeds = new ArrayList<>();
    TextView topHashtags;

    AVLoadingIndicatorView loadingImage;
    TextView statusText;

    String hashtag = "";
    Integer limit = 10;
    Integer offset = 0;
    Boolean moreFeedsLoading = false;
    String loggedIn;

    EditText searchText;
    ImageView searchButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hashtag);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView) findViewById(R.id.list_view);
        loadingImage = (AVLoadingIndicatorView) findViewById(R.id.loading_image);
        statusText = (TextView) findViewById(R.id.status_text);

        loadingImage.show();

        loggedIn = App.getInstance().getSession().getUserid();
        feedAdapter = new FeedAdapter(this, feeds, "user", loggedIn, loggedIn, "user");
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        listView.addHeaderView(layoutInflater.inflate(R.layout.hashtag_header, null));
        topHashtags = (TextView) findViewById(R.id.top_hashtags);
        searchText = (EditText) findViewById(R.id.search_text);
        searchButton = (ImageView) findViewById(R.id.search_button);
        listView.setAdapter(feedAdapter);


        Intent intent = getIntent();
        if (intent.hasExtra("hashtag")) hashtag = intent.getStringExtra("hashtag");

        loadFeeds(false);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = searchText.getText().toString();
                if (!text.isEmpty()) {
                    feeds.clear();
                    offset = 0;
                    loadingImage.show();
                    hashtag = text;
                    loadFeeds(false);
                }
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                final int lastItem = i + i1;
                if (i2 > 5 && lastItem == i2) {
                    if (!moreFeedsLoading) {
                        offset = offset + limit;
                        loadFeeds(true);
                        moreFeedsLoading = true;
                    }
                }
            }
        });
    }

    public void loadFeeds(final Boolean more) {
        Call<HashtagResponse> call = Api.getRetrofit().create(HashtagApiInterface.class).getHashtags(
                loggedIn,
                hashtag,
                limit,
                offset
        );
        call.enqueue(new Callback<HashtagResponse>() {
            @Override
            public void onResponse(Call<HashtagResponse> call, Response<HashtagResponse> response) {
                hideLoading();
                if (!more) {
                    feeds.clear();
                    //Integer count = response.body().getFeeds().size();
                    //MyLog.e(count.toString());
                    if (response.body().getFeeds().size() < 1) {
                        statusText.setText(R.string.no_hashtags_found);
                        statusText.setVisibility(View.VISIBLE);
                    } else {
                        statusText.setVisibility(View.GONE);
                    }

                    if (response.body().getHashtags().size() > 0) {
                        CharSequence str = "";
                        ArrayList<Hashtags> hashtags = response.body().getHashtags();
                        for(int i = 0; i < hashtags.size(); i++) {
                            final String hash = hashtags.get(i).getTag();
                            SpannableStringBuilder b = new SpannableStringBuilder(hash);
                            b.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, hash.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                            ClickableSpan cS = new ClickableSpan() {
                                @Override
                                public void onClick(View view) {
                                    hashtag = hash;
                                    MyLog.e(hash);
                                    setLoading();
                                    loadFeeds(false);
                                }
                                @Override
                                public void updateDrawState(TextPaint t) {
                                    t.setUnderlineText(false);
                                }
                            };
                            b.setSpan(cS, 0, hash.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                            str = TextUtils.concat(str, " , ", b);

                        }
                        //MyLog.e("str - " + str.toString());
                        topHashtags.setText(str);
                        topHashtags.setMovementMethod(LinkMovementMethod.getInstance());
                    }
                }

                feeds.addAll(response.body().getFeeds());
                feedAdapter.notifyDataSetChanged();
                loadingImage.hide();
            }

            @Override
            public void onFailure(Call<HashtagResponse> call, Throwable t) {
                if (!more) {
                    statusText.setText(R.string.no_hashtags_found);
                    statusText.setVisibility(View.VISIBLE);
                }
                MyLog.e(t.getMessage());
                loadingImage.hide();
            }
        });
    }

    public static void launch(Context c, String hash) {
        Intent intent = new Intent(c, HashtagActivity.class);
        intent.putExtra("hashtag", hash);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        c.startActivity(intent);
    }

}
