package com.procrea8.sexyafrikan.modules.setting.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.modules.setting.SettingsApiInterface;
import com.procrea8.sexyafrikan.response.Response;

import retrofit2.Call;
import retrofit2.Callback;

public class PasswordSettingsActivity extends ActivityBase {

    EditText currentPassword;
    EditText newPassword;
    EditText confirmPassword;
    Button saveButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        currentPassword = (EditText) findViewById(R.id.current_password);
        newPassword = (EditText) findViewById(R.id.new_password);
        confirmPassword = (EditText) findViewById(R.id.confirm_new_password);
        saveButton = (Button) findViewById(R.id.save_button);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cPassword = currentPassword.getText().toString();
                String nPassword = newPassword.getText().toString();
                String confirmPass = confirmPassword.getText().toString();
                if (!cPassword.isEmpty() && !nPassword.isEmpty() && !confirmPass.isEmpty()) {
                    if (!nPassword.equals(confirmPass)) {
                        Toast.makeText(getApplicationContext(), R.string.new_password_not_same, Toast.LENGTH_LONG).show();
                    } else {
                        setLoading();
                        Call<Response> call = Api.getRetrofit().create(SettingsApiInterface.class).changePassword(
                                App.getInstance().getSession().getUserid(), cPassword, nPassword
                        );
                        call.enqueue(new Callback<Response>() {
                            @Override
                            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                hideLoading();
                                if (response.body().getStatus()==1) {
                                    Toast.makeText(getApplicationContext(), R.string.password_changed_success, Toast.LENGTH_LONG).show();
                                    onBackPressed();
                                    finish();
                                } else {
                                    Toast.makeText(getApplicationContext(), R.string.failed_password_changed, Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Response> call, Throwable t) {
                                hideLoading();
                                Toast.makeText(getApplicationContext(), R.string.failed_internet, Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                }


            }
        });

    }

}
