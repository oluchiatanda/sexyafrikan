package com.procrea8.sexyafrikan.modules.event.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.fragment.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class BirthdaysFragment extends BaseFragment {


    public BirthdaysFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_birthdays, container, false);
        return view;
    }

}
