package com.procrea8.sexyafrikan.modules.search;

/**
 * Created by pRocrea8 on 5/2/2017.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.ListUtils;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.helper.Session;
import com.procrea8.sexyafrikan.model.User;
import com.procrea8.sexyafrikan.modules.event.EventApiInterface;
import com.procrea8.sexyafrikan.modules.event.activity.EventActivity;
import com.procrea8.sexyafrikan.modules.event.adapter.EventsAdapter;
import com.procrea8.sexyafrikan.modules.event.model.Event;
import com.procrea8.sexyafrikan.modules.feed.FeedApiInterface;
import com.procrea8.sexyafrikan.modules.feed.activity.FeedActivity;
import com.procrea8.sexyafrikan.modules.feed.adapter.FeedAdapter;
import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;
import com.procrea8.sexyafrikan.modules.group.GroupApiInterface;
import com.procrea8.sexyafrikan.modules.group.activity.GroupActivity;
import com.procrea8.sexyafrikan.modules.group.adapter.GroupsAdapter;
import com.procrea8.sexyafrikan.modules.group.model.Group;
import com.procrea8.sexyafrikan.modules.marketplace.MarketplaceApiInterface;
import com.procrea8.sexyafrikan.modules.marketplace.activity.MarketplaceActivity;
import com.procrea8.sexyafrikan.modules.marketplace.adapter.MarketplaceAdapter;
import com.procrea8.sexyafrikan.modules.marketplace.model.Listing;
import com.procrea8.sexyafrikan.modules.music.MusicApiInterface;
import com.procrea8.sexyafrikan.modules.music.activity.MusicPlayerActivity;
import com.procrea8.sexyafrikan.modules.music.adapter.MusicAdapter;
import com.procrea8.sexyafrikan.modules.music.model.BrowseResponse;
import com.procrea8.sexyafrikan.modules.music.model.Music;
import com.procrea8.sexyafrikan.modules.page.PageApiInterface;
import com.procrea8.sexyafrikan.modules.page.activity.PageActivity;
import com.procrea8.sexyafrikan.modules.page.adapter.PagesAdapter;
import com.procrea8.sexyafrikan.modules.page.model.Page;
import com.procrea8.sexyafrikan.modules.page.model.PageBrowseResponse;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;
import com.procrea8.sexyafrikan.modules.relationship.RelationshipApiInterface;
import com.procrea8.sexyafrikan.modules.relationship.adapter.UsersAdapter;
import com.procrea8.sexyafrikan.modules.video.VideoApiInterface;
import com.procrea8.sexyafrikan.modules.video.activity.VideoActivity;
import com.procrea8.sexyafrikan.modules.video.adapter.VideosAdapter;
import com.procrea8.sexyafrikan.modules.video.model.Video;
import com.procrea8.sexyafrikan.modules.video.model.VideoBrowseResponse;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Shows a list that can be filtered in-place with a SearchView in non-iconified mode.
 */
public class SearchFragment extends Fragment {

    String loggedUserid;
    String category;
    String type;
    String filter, typeID, entityID, entityType;
    Integer limit;
    String term = "";
    Integer page = 1;

    EditText inputSearch;
    ImageView searchButton;

    ArrayList<Music> songs = new ArrayList<>();
    MusicAdapter musicadapter;

    ArrayList<User> usersList = new ArrayList<>();
    UsersAdapter usersAdapter;

    ListView listMusic, listVideo, listPage, listUser, listGroup, listMarket, listEvent, listFeed;

    LinearLayout music, videos, pageLayout, user, group, markets, event, feed;

    ArrayList<FeedResponse> feeds = new ArrayList<>();
    FeedAdapter feedadapter;

    ArrayList<Page> pages = new ArrayList<>();
    PagesAdapter pageadapter;

    GroupsAdapter groupadapter;
    ArrayList<Group> groups = new ArrayList<>();

    VideosAdapter videosAdapter;
    ArrayList<Video> video = new ArrayList<>();

    MarketplaceAdapter marketAdapter;
    ArrayList<Listing> market = new ArrayList<>();

    EventsAdapter eventAdapter;
    ArrayList<Event> events = new ArrayList<>();

    AVLoadingIndicatorView loadingImage;

    TabLayout mTabLayout;
    TextView text;
    FragmentActivity activity;
    Integer offset = 0;

//    Spinner categorySpinner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        activity = getActivity();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all, container, false);
        mTabLayout = (TabLayout) view.findViewById(R.id.tabs);

        loadingImage = (AVLoadingIndicatorView) view.findViewById(R.id.loading_image);
        listMusic = (ListView) view.findViewById(R.id.listMusic);
        listUser = (ListView) view.findViewById(R.id.listUser);
        listPage = (ListView) view.findViewById(R.id.listPage);
        listGroup = (ListView) view.findViewById(R.id.listGroup);
        listVideo = (ListView) view.findViewById(R.id.listVideo);
        listMarket = (ListView) view.findViewById(R.id.listMarket);
        listEvent = (ListView) view.findViewById(R.id.listEvent);
        listFeed = (ListView) view.findViewById(R.id.listPosts);
        videos = (LinearLayout) view.findViewById(R.id.video);
        music = (LinearLayout) view.findViewById(R.id.music);
        user = (LinearLayout) view.findViewById(R.id.people);
        pageLayout = (LinearLayout) view.findViewById(R.id.pages);
        feed = (LinearLayout) view.findViewById(R.id.posts);
        group = (LinearLayout) view.findViewById(R.id.groups);
        markets = (LinearLayout) view.findViewById(R.id.listings);
        event = (LinearLayout) view.findViewById(R.id.events);
        inputSearch = (EditText) view.findViewById(R.id.search_text);
        searchButton = (ImageView) view.findViewById(R.id.search_button);
        text = (TextView) view.findViewById(R.id.status_text);

        usersAdapter = new UsersAdapter(getContext(), usersList);
        musicadapter = new MusicAdapter(getContext(), songs);
        eventAdapter = new EventsAdapter(getContext(), events);
        pageadapter = new PagesAdapter(getContext(), pages);
        groupadapter = new GroupsAdapter(getContext(), groups);
        videosAdapter = new VideosAdapter(getContext(), video);
        marketAdapter = new MarketplaceAdapter(getContext(), market);

        feedadapter = new FeedAdapter(getContext(), feeds, type, typeID, entityID, entityType);
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            //        mTabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(pager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                loadingImage.show();
                onTabTapped(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                loadingImage.show();
                onTabTapped(tab.getPosition());

            }
        });

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String search = inputSearch.getText().toString();
                if (!search.isEmpty()) {
                    loadingImage.show();
                    all();
                    term = search;
                    type = "search";

                    int i = mTabLayout.getSelectedTabPosition();
                    onTabTapped(i);
                }
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String search = inputSearch.getText().toString();
                if (!search.isEmpty()) {
                    loadingImage.show();
                    all();
                    term = search;
                    type = "search";

                    int i = mTabLayout.getSelectedTabPosition();
                    onTabTapped(i);
                }
            }
        });

        loadingImage.hide();

        return view;
    }

    private void onTabTapped(int position) {
        switch (position) {

            case 0:
                loadingImage.show();
                all();
                posts();
                people();
                getPages();
                getListings();
                getSongs();
                getVideos();
                getEvent();
                getGroups();
                break;

            case 1:
                loadingImage.show();
                all();
                event.setVisibility(View.GONE);
                music.setVisibility(View.GONE);
                pageLayout.setVisibility(View.GONE);
                videos.setVisibility(View.GONE);
                markets.setVisibility(View.GONE);
                group.setVisibility(View.GONE);
                user.setVisibility(View.GONE);

                posts();
                break;
            case 2:
                loadingImage.show();
                all();
                event.setVisibility(View.GONE);
                music.setVisibility(View.GONE);
                pageLayout.setVisibility(View.GONE);
                videos.setVisibility(View.GONE);
                markets.setVisibility(View.GONE);
                group.setVisibility(View.GONE);
                feed.setVisibility(View.GONE);

                people();
                break;
            case 3:
                loadingImage.show();
                all();
                user.setVisibility(View.GONE);
                event.setVisibility(View.GONE);
                pageLayout.setVisibility(View.GONE);
                videos.setVisibility(View.GONE);
                markets.setVisibility(View.GONE);
                group.setVisibility(View.GONE);
                feed.setVisibility(View.GONE);

                getSongs();
                break;
            case 4:
                loadingImage.show();
                all();

                user.setVisibility(View.GONE);
                music.setVisibility(View.GONE);
                event.setVisibility(View.GONE);
                videos.setVisibility(View.GONE);
                markets.setVisibility(View.GONE);
                group.setVisibility(View.GONE);
                feed.setVisibility(View.GONE);

                getPages();
                break;
            case 5:
                loadingImage.show();
                all();

                user.setVisibility(View.GONE);
                music.setVisibility(View.GONE);
                pageLayout.setVisibility(View.GONE);
                videos.setVisibility(View.GONE);
                markets.setVisibility(View.GONE);
                group.setVisibility(View.GONE);
                feed.setVisibility(View.GONE);

                getEvent();
                break;
            case 6:
                loadingImage.show();
                all();
                user.setVisibility(View.GONE);
                music.setVisibility(View.GONE);
                pageLayout.setVisibility(View.GONE);
                event.setVisibility(View.GONE);
                markets.setVisibility(View.GONE);
                group.setVisibility(View.GONE);
                feed.setVisibility(View.GONE);

                getVideos();
                break;
            case 7:
                loadingImage.show();
                all();
                user.setVisibility(View.GONE);
                music.setVisibility(View.GONE);
                pageLayout.setVisibility(View.GONE);
                event.setVisibility(View.GONE);
                videos.setVisibility(View.GONE);
                markets.setVisibility(View.GONE);
                feed.setVisibility(View.GONE);

                getGroups();
                break;
            case 8:
                loadingImage.show();
                all();
                user.setVisibility(View.GONE);
                music.setVisibility(View.GONE);
                pageLayout.setVisibility(View.GONE);
                event.setVisibility(View.GONE);
                videos.setVisibility(View.GONE);
                group.setVisibility(View.GONE);
                feed.setVisibility(View.GONE);

                getListings();
                break;
        }
    }

    public void all() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                usersList.clear();
                usersAdapter.notifyDataSetChanged();

                feeds.clear();
                feedadapter.notifyDataSetChanged();

                songs.clear();
                musicadapter.notifyDataSetChanged();

                groups.clear();
                groupadapter.notifyDataSetChanged();

                pages.clear();
                pageadapter.notifyDataSetChanged();

                video.clear();
                videosAdapter.notifyDataSetChanged();

                market.clear();
                marketAdapter.notifyDataSetChanged();

                events.clear();
                eventAdapter.notifyDataSetChanged();
            }
        });
    }

    public void people() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final String userid = App.getInstance().getSession().getUserid();

                Call<ArrayList<User>> call = null;
                call = Api.getRetrofit().create(RelationshipApiInterface.class).suggestions(userid, page.toString(), term);
                call.enqueue(new Callback<ArrayList<User>>() {
                    @Override
                    public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                        MyLog.e(response.body().toString());
                        usersList.clear();
                        usersList.addAll(response.body());

                        if (response.body() != null && response.body().size() > 0) {
                            text.setVisibility(View.GONE);
                            user.setVisibility(View.VISIBLE);
                        } else {
                            text.setText("No results found");
                            text.setVisibility(View.VISIBLE);
                            user.setVisibility(View.GONE);
                        }
                        usersAdapter.notifyDataSetChanged();
                        loadingImage.hide();
                        listUser.setAdapter(new CustomListAdapter(getContext(), usersList));
                        listUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                User note = usersList.get(position);
                                ProfileActivity.load(getContext(), note.getId());
                            }
                        });
                        ListUtils.setDynamicHeight(listUser);
                    }

                    @Override
                    public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                        MyLog.e("Music fetch error - " + t.getMessage());
                        user.setVisibility(View.GONE);
                    }
                });
            }
        });

    }

    public void posts() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Call<ArrayList<FeedResponse>> call = Api.getRetrofit().create(FeedApiInterface.class).getFeeds(App.getInstance().getSession().getUserid(),
                        type, term, "10", "0");
                call.enqueue(new Callback<ArrayList<FeedResponse>>() {
                    @Override
                    public void onResponse(Call<ArrayList<FeedResponse>> call, Response<ArrayList<FeedResponse>> response) {
                        feeds.clear();
                        feeds.addAll(response.body());

                        if (response.body() != null && response.body().size() > 0) {
                            text.setVisibility(View.GONE);
                            feed.setVisibility(View.VISIBLE);
                        } else {
                            text.setText("No results found");
                            text.setVisibility(View.VISIBLE);
                            feed.setVisibility(View.GONE);
                        }
                        feedadapter.notifyDataSetChanged();
                        loadingImage.hide();
                        listFeed.setAdapter(new CustomListAdapter(getContext(), feeds));
                        listFeed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                FeedResponse feedResponse = feeds.get(position);
                                FeedActivity.load(getContext(), feedResponse);
                            }
                        });
                        ListUtils.setDynamicHeight(listFeed);
                    }

                    @Override
                    public void onFailure(Call<ArrayList<FeedResponse>> call, Throwable t) {
                        MyLog.e("Music fetch error - " + t.getMessage());
                        feed.setVisibility(View.GONE);
                    }
                });
            }
        });

    }

    public void getSongs() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Call<BrowseResponse> call = Api.getRetrofit().create(MusicApiInterface.class).browse(
                        loggedUserid, category, term, type, filter, limit, page
                );
                call.enqueue(new Callback<BrowseResponse>() {
                    @Override
                    public void onResponse(Call<BrowseResponse> call, Response<BrowseResponse> response) {
                        MyLog.e(response.body().getSongs().toString());
                        songs.clear();

                        songs.addAll(response.body().getSongs());

                        if (response.body() != null && response.body().getSongs().size() > 0) {
                            text.setVisibility(View.GONE);
                            music.setVisibility(View.VISIBLE);
                        } else {
                            text.setText("No results found");
                            text.setVisibility(View.VISIBLE);
                            music.setVisibility(View.GONE);
                        }

                        musicadapter.notifyDataSetChanged();
                        loadingImage.hide();
                        listMusic.setAdapter(new CustomListAdapter(getContext(), songs));
                        listMusic.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Music musicList = songs.get(position);
                                MusicPlayerActivity.launch(getContext(), musicList);
                            }
                        });
                        ListUtils.setDynamicHeight(listMusic);
                    }

                    @Override
                    public void onFailure(Call<BrowseResponse> call, Throwable t) {
                        MyLog.e("Music fetch error - " + t.getMessage());
                        music.setVisibility(View.GONE);
                    }
                });

            }
        });

    }

    public void getEvent() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Call<com.procrea8.sexyafrikan.modules.event.model.BrowseResponse> call = Api.getRetrofit().create(EventApiInterface.class).browse(
                        loggedUserid, category, term, type, limit, page
                );
                call.enqueue(new Callback<com.procrea8.sexyafrikan.modules.event.model.BrowseResponse>() {
                    @Override
                    public void onResponse(Call<com.procrea8.sexyafrikan.modules.event.model.BrowseResponse> call, Response<com.procrea8.sexyafrikan.modules.event.model.BrowseResponse> response) {

                        MyLog.e(response.body().getEvents().toString());
                        events.clear();
                        events.addAll(response.body().getEvents());

                        if (response.body() != null && response.body().getEvents().size() > 0) {
                            text.setVisibility(View.GONE);
                            event.setVisibility(View.VISIBLE);
                        } else {
                            text.setText("No results found");
                            text.setVisibility(View.VISIBLE);
                            event.setVisibility(View.GONE);
                        }
                        eventAdapter.notifyDataSetChanged();
                        loadingImage.hide();
                        listEvent.setAdapter(new CustomListAdapter(getContext(), events));
                        listEvent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Event eventList = events.get(position);
                                EventActivity.load(getContext(), eventList);

                            }
                        });
                        ListUtils.setDynamicHeight(listEvent);
                    }

                    @Override
                    public void onFailure(Call<com.procrea8.sexyafrikan.modules.event.model.BrowseResponse> call, Throwable t) {
                        MyLog.e("Music fetch error - " + t.getMessage());

                        event.setVisibility(View.GONE);

                    }
                });
            }
        });

    }

    public void getPages() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Call<PageBrowseResponse> call = Api.getRetrofit().create(PageApiInterface.class).browse(
                        loggedUserid, category, term, type, limit, page
                );
                call.enqueue(new Callback<PageBrowseResponse>() {
                    @Override
                    public void onResponse(Call<PageBrowseResponse> call, Response<PageBrowseResponse> response) {
                        MyLog.e(response.body().getPages().toString());
                        pages.clear();
                        pages.addAll(response.body().getPages());

                        if (response.body() != null && response.body().getPages().size() > 0) {
                            text.setVisibility(View.GONE);
                            pageLayout.setVisibility(View.VISIBLE);
                        } else {
                            text.setText("No results found");
                            text.setVisibility(View.VISIBLE);
                            event.setVisibility(View.GONE);
                        }

                        pageadapter.notifyDataSetChanged();
                        loadingImage.hide();
                        listPage.setAdapter(new CustomListAdapter(getContext(), pages));
                        listPage.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Page note = pages.get(position);
                                PageActivity.load(getContext(), note);
                            }
                        });
                        ListUtils.setDynamicHeight(listPage);
                    }

                    @Override
                    public void onFailure(Call<PageBrowseResponse> call, Throwable t) {
                        MyLog.e("Music fetch error - " + t.getMessage());
                        event.setVisibility(View.GONE);
                    }
                });
            }
        });

    }

    public void getGroups() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Call<ArrayList<Group>> call = Api.getRetrofit().create(GroupApiInterface.class).browse(
                        loggedUserid, term, type, limit, page
                );
                call.enqueue(new Callback<ArrayList<Group>>() {
                    @Override
                    public void onResponse(Call<ArrayList<Group>> call, Response<ArrayList<Group>> response) {
                        MyLog.e(response.body().toString());
                        groups.clear();
                        groups.addAll(response.body());

                        if (response.body() != null && response.body().size() > 0) {
                            text.setVisibility(View.GONE);
                            group.setVisibility(View.VISIBLE);
                        } else {
                            text.setText("No results found");
                            text.setVisibility(View.VISIBLE);
                            group.setVisibility(View.GONE);
                        }
                        groupadapter.notifyDataSetChanged();

                        loadingImage.hide();
                        listGroup.setAdapter(new CustomListAdapter(getContext(), groups));
                        listGroup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Group note = groups.get(position);
                                GroupActivity.load(getContext(), note);
                            }
                        });
                        ListUtils.setDynamicHeight(listGroup);
                    }

                    @Override
                    public void onFailure(Call<ArrayList<Group>> call, Throwable t) {
                        MyLog.e("Music fetch error - " + t.getMessage());
                        group.setVisibility(View.GONE);

                    }
                });
            }
        });

    }

    public void getVideos() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Call<VideoBrowseResponse> call = Api.getRetrofit().create(VideoApiInterface.class).browse(
                        loggedUserid, category, term, type, limit, page
                );
                call.enqueue(new Callback<VideoBrowseResponse>() {
                    @Override
                    public void onResponse(Call<VideoBrowseResponse> call, Response<VideoBrowseResponse> response) {
                        MyLog.e(response.body().getVideos().toString());
                        video.clear();
                        video.addAll(response.body().getVideos());

                        if (response.body() != null && response.body().getVideos().size() > 0) {
                            text.setVisibility(View.GONE);
                            videos.setVisibility(View.VISIBLE);

                        } else {
                            text.setText("No results found");
                            text.setVisibility(View.VISIBLE);
                            videos.setVisibility(View.GONE);
                        }
                        videosAdapter.notifyDataSetChanged();
                        loadingImage.hide();
                        listVideo.setAdapter(new CustomListAdapter(getContext(), video));
                        listVideo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Video note = video.get(position);
                                VideoActivity.load(getContext(), note);

                            }
                        });
                        ListUtils.setDynamicHeight(listVideo);
                    }

                    @Override
                    public void onFailure(Call<VideoBrowseResponse> call, Throwable t) {
                        MyLog.e("Music fetch error - " + t.getMessage());
                        videos.setVisibility(View.GONE);

                    }
                });
            }
        });

    }

    public void getListings() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Call<com.procrea8.sexyafrikan.modules.marketplace.model.BrowseResponse> call = Api.getRetrofit().create(MarketplaceApiInterface.class).browse(
                        loggedUserid, category, term, type, limit, page
                );
                call.enqueue(new Callback<com.procrea8.sexyafrikan.modules.marketplace.model.BrowseResponse>() {
                    @Override
                    public void onResponse(Call<com.procrea8.sexyafrikan.modules.marketplace.model.BrowseResponse> call, Response<com.procrea8.sexyafrikan.modules.marketplace.model.BrowseResponse> response) {
                        MyLog.e(response.body().getListings().toString());
                        market.clear();
                        market.addAll(response.body().getListings());

                        if (response.body() != null && response.body().getListings().size() > 0) {
                            text.setVisibility(View.GONE);
                            markets.setVisibility(View.VISIBLE);
                        } else {
                            text.setText("No results found");
                            text.setVisibility(View.VISIBLE);
                            markets.setVisibility(View.GONE);
                        }
                        marketAdapter.notifyDataSetChanged();
                        loadingImage.hide();
                        listMarket.setAdapter(new CustomListAdapter(getContext(), market));
                        listMarket.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Listing note = market.get(position);
                                MarketplaceActivity.load(getContext(), note);

                            }
                        });
                        ListUtils.setDynamicHeight(listMarket);
                    }

                    @Override
                    public void onFailure(Call<com.procrea8.sexyafrikan.modules.marketplace.model.BrowseResponse> call, Throwable t) {
                        MyLog.e("Music fetch error - " + t.getMessage());

                        markets.setVisibility(View.GONE);
                    }
                });
            }
        });

    }

    public class CustomListAdapter extends BaseAdapter {
        private ArrayList itemname;
        private LayoutInflater layoutInflater;
        Context context;
        Session session = App.getInstance().getSession();

        public CustomListAdapter(Context context, ArrayList itemname) {
            this.context = context;
            this.itemname = itemname;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return itemname.size();
        }

        @Override
        public Object getItem(int position) {
            return itemname.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.list_item, null);
                holder = new ViewHolder();

                holder.text = (TextView) convertView.findViewById(R.id.text);
                holder.image = (ImageView) convertView.findViewById(R.id.image);
                holder.description = (TextView) convertView.findViewById(R.id.description);
                holder.description2 = (TextView) convertView.findViewById(R.id.description2);
                holder.addfriend = (ImageButton) convertView.findViewById(R.id.add_friend);
                holder.requestSentButton = (ImageButton) convertView.findViewById(R.id.request_sent);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            try {
                for (int i = 0; i < usersList.size(); i++) {
                    final User userList = (User) itemname.get(position);
                    MyLog.e("Looking for country and gender" + userList.getEmail()
                            + userList.getBio() + session.getGender() + session.getCountry()
                            + usersList.get(i).getCity() + usersList.get(i).getState());
                    holder.text.setText(userList.getName());
                    holder.description2.setText(session.getGender());
                    holder.description.setText(session.getCountry());
                    String s = userList.getFriendStatus();
                    switch (s) {
                        case "0":
                            holder.addfriend.setVisibility(View.VISIBLE);
                            break;
                        case "1":
                            holder.requestSentButton.setVisibility(View.VISIBLE);
                            break;
                    }

                    final String userid = App.getInstance().getSession().getUserid();
                    holder.addfriend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            holder.addfriend.setVisibility(View.GONE);
                            holder.requestSentButton.setVisibility(View.VISIBLE);
                            userList.setFriendStatus("1");
                            MyLog.e("Adding user - " + userList.getId());
                            Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).add(
                                    userid,
                                    userList.getId()
                            );
                            call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                                @Override
                                public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, retrofit2.Response<com.procrea8.sexyafrikan.response.Response> response) {
                                    Toast.makeText(context, R.string.friend_requests_sent, Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                                    holder.addfriend.setVisibility(View.VISIBLE);
                                    holder.requestSentButton.setVisibility(View.GONE);
                                    Toast.makeText(context, R.string.failed_to_add_friend, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    });

                    holder.requestSentButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setMessage(R.string.are_you_sure_cancel_request)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                            holder.addfriend.setVisibility(View.VISIBLE);
                                            holder.requestSentButton.setVisibility(View.GONE);
                                            userList.setFriendStatus("0");
                                            Call<com.procrea8.sexyafrikan.response.Response> call = Api.getRetrofit().create(RelationshipApiInterface.class).remove(
                                                    userid,
                                                    userList.getId()
                                            );
                                            call.enqueue(new Callback<com.procrea8.sexyafrikan.response.Response>() {
                                                @Override
                                                public void onResponse(Call<com.procrea8.sexyafrikan.response.Response> call, retrofit2.Response<com.procrea8.sexyafrikan.response.Response> response) {

                                                }

                                                @Override
                                                public void onFailure(Call<com.procrea8.sexyafrikan.response.Response> call, Throwable t) {
                                                    holder.addfriend.setVisibility(View.GONE);
                                                    holder.requestSentButton.setVisibility(View.VISIBLE);
                                                    Toast.makeText(context, R.string.failed_internet, Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        }
                                    }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            builder.create().show();
                        }
                    });
                    if (holder.image != null) {
                        Glide.with(context).load(userList.getAvatar()).crossFade().into(holder.image);
                    }
                }
            } catch (Exception e) {
//                MyLog.e("user" + e.getMessage());
            }

            try {
                for (int i = 0; i < feeds.size(); i++) {
                    FeedResponse feedList = (FeedResponse) itemname.get(position);
                    holder.text.setText(feedList.getAuthorName());
                    holder.description2.setText(feedList.getFullMessage());
                    holder.description.setText(feedList.getTime());
                    if (holder.image != null) {
                        Glide.with(context).load(feedList.getAuthorAvatar()).crossFade().into(holder.image);
                    }
                }
            } catch (Exception e) {
//                MyLog.e("music" + e.getMessage());
            }


            try {
                for (int i = 0; i < songs.size(); i++) {
                    Music musicList = (Music) itemname.get(position);
                    holder.text.setText(musicList.getTitle());
                    holder.description2.setText(musicList.getPlayCount());
                    holder.description.setText(musicList.getArtist());
                    if (holder.image != null) {
                        Glide.with(context).load(musicList.getCover()).crossFade().into(holder.image);
                    }
                }
            } catch (Exception e) {
//                MyLog.e("music" + e.getMessage());
            }

            try {

                for (int i = 0; i < pages.size(); i++) {
                    Page pageList = (Page) itemname.get(position);
                    holder.text.setText(pageList.getTitle());
                    holder.description.setText(pageList.getDescription());
                    holder.description2.setText(pageList.getLikes());
                    if (holder.image != null) {
                        Glide.with(context).load(pageList.getCover()).crossFade().into(holder.image);
                    }
                }
            } catch (Exception e) {
//                MyLog.e("page" + e.getMessage());
            }
            try {

                for (int i = 0; i < groups.size(); i++) {
                    Group groupList = (Group) itemname.get(position);
                    holder.text.setText(groupList.getTitle());
                    holder.description.setText(groupList.getDescription());
//                    holder.description2.setText(groupList.);
                    if (holder.image != null) {
                        Glide.with(context).load(groupList.getCover()).crossFade().into(holder.image);
                    }
                }
            } catch (Exception e) {
//                MyLog.e("group" + e.getMessage());
            }
//
            try {
                for (int i = 0; i < video.size(); i++) {
                    Video videoList = (Video) itemname.get(position);
                    holder.text.setText(videoList.getTitle());
                    holder.description.setText(videoList.getDescription());
                    holder.description2.setText(videoList.getViewCount());
                    if (holder.image != null) {
                        Glide.with(context).load(videoList.getPhoto()).crossFade().into(holder.image);
                    }
                }
//                }
            } catch (Exception e) {
//                MyLog.e("video" + e.getMessage());
            }

            try {
                for (int i = 0; i < market.size(); i++) {
                    Listing marketList = (Listing) itemname.get(position);
                    holder.text.setText(marketList.getTitle());
                    holder.description.setText(marketList.getDescription());
                    holder.description2.setText(marketList.getLink());
                    if (holder.image != null) {
                        Glide.with(context).load(marketList.getImage()).crossFade().into(holder.image);
                    }
                }
            } catch (Exception e) {
//
            }

            try {
                for (int i = 0; i < events.size(); i++) {
                    Event eventList = (Event) itemname.get(position);
                    holder.text.setText(eventList.getTitle());
                    holder.description.setText(eventList.getDescription());
                    holder.description2.setText(eventList.getAddress());
                    if (holder.image != null) {
                        Glide.with(context).load(eventList.getImage()).crossFade().into(holder.image);
                    }
                }
            } catch (Exception e) {
//
            }

            return convertView;
        }

        class ViewHolder {
            TextView text;
            ImageView image;
            ImageButton addfriend, requestSentButton;
            TextView description, description2;

        }
    }
}



