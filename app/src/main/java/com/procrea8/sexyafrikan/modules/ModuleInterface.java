package com.procrea8.sexyafrikan.modules;


import android.content.Context;
import android.support.v4.app.Fragment;

import com.mikepenz.materialdrawer.Drawer;

public interface ModuleInterface {
    public void registerMenu(Context context, Drawer drawer, int i);
    public Fragment getFragment(Context context);
    public String getTitle(Context context);
    public Object invoke(Context context, String type, Object object);
    public Object invoke(Context context, String type);
}
