package com.procrea8.sexyafrikan.modules.chat.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.modules.chat.ChatApiInterface;
import com.procrea8.sexyafrikan.modules.chat.activity.ChatActivity;
import com.procrea8.sexyafrikan.modules.chat.adapter.ChatsAdapter;
import com.procrea8.sexyafrikan.modules.chat.dialog.SelectFriendsDialog;
import com.procrea8.sexyafrikan.modules.chat.model.Chat;
import com.procrea8.sexyafrikan.response.FCMResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessagesFragment extends Fragment implements Contants {

    TextView statusText;
    ListView listView;
    ArrayList<Chat> chats = new ArrayList<>();
    ChatsAdapter adapter;
    FloatingActionButton newButton;

    Gson gson;
    SharedPreferences pref;
    String cacheTag = "chat_lists";
    public MessagesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_messages, container, false);
        listView = (ListView) view.findViewById(R.id.chat_list);
        statusText = (TextView) view.findViewById(R.id.status_text);
        newButton = (FloatingActionButton) view.findViewById(R.id.new_message_button);
        adapter = new ChatsAdapter(getContext(), chats);
        listView.setAdapter(adapter);

        gson = new Gson();
        pref = getContext().getSharedPreferences(TAG, Context.MODE_PRIVATE);
        loadFromCache();
        loadConversations();

        listView.setClickable(true);
        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SelectFriendsDialog(getContext());
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Chat chat = chats.get(i);
                //launch the chat pane
                chat.setUnread(0);
                adapter.notifyDataSetChanged();
                ChatActivity.loadChat(getContext(),chat.getTitle(), chat.getCid(), chat.getUserid());
            }
        });
        return view;
    }

    public void loadConversations() {
        Call<ArrayList<Chat>> call = Api.getRetrofit().create(ChatApiInterface.class).getConversations(App.getInstance().getSession().getUserid());
        call.enqueue(new Callback<ArrayList<Chat>>() {
            @Override
            public void onResponse(Call<ArrayList<Chat>> call, Response<ArrayList<Chat>> response) {
                String arrayText = gson.toJson(response.body());
                pref.edit().putString(cacheTag, arrayText).apply();
                chats.clear();
                if (response.body().size() > 0 ) {
                    statusText.setVisibility(View.GONE);
                } else {
                    statusText.setVisibility(View.VISIBLE);
                }
                chats.addAll(response.body());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ArrayList<Chat>> call, Throwable t) {
                statusText.setVisibility(View.VISIBLE);
            }
        });
    }

    public void loadFromCache() {
        String cache = pref.getString(cacheTag, null);
        if (cache != null) {
            ArrayList<Chat> chatsList = gson.fromJson(cache, new TypeToken<ArrayList<Chat>>(){}.getType());
            chats.clear();
            chats.addAll(chatsList);
            adapter.notifyDataSetChanged();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFCMResponseEvent(FCMResponse event) {
        if (event.getType().equals("new-event")) {
            //lets check for new events
            loadConversations();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
