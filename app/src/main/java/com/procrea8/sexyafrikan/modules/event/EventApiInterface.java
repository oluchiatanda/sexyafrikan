package com.procrea8.sexyafrikan.modules.event;


import com.procrea8.sexyafrikan.modules.event.model.BirthdayResponse;
import com.procrea8.sexyafrikan.modules.event.model.BrowseResponse;
import com.procrea8.sexyafrikan.modules.event.model.Event;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface EventApiInterface {
    @GET("event/get/categories")
    Call<ArrayList<MusicCategory>> getCategories(@Query("userid") String userid);
    @GET("event/birthdays")
    Call<BirthdayResponse> getBirthdays(@Query("userid") String userid);
    @GET("event/browse")
    Call<BrowseResponse>  browse(@Query("userid") String userid, @Query("category_id") String category, @Query("term") String term,
                                 @Query("type") String type, @Query("limit") Integer limit,
                                 @Query("page") Integer page);
    @GET("event/delete")
    Call<Response> delete(@Query("userid") String userid, @Query("event_id") String eventId);

    @GET("event/rsvp")
    Call<Response> rsvp(@Query("userid") String userid, @Query("event_id") String eventId, @Query("rsvp") String rsvp);

    @Multipart
    @POST("event/cover")
    Call<Response> changeCover(@Query("userid") String userid, @Query("event_id") String eventId, @Part MultipartBody.Part photo);

    @POST("event/create")
    Call<Event> create(@Query("userid") String userid, @Query("title") String title, @Query("description") String description,
                         @Query("location") String location,@Query("address") String address,@Query("privacy") String privacy,
                         @Query("category_id") String categoryId, @Query("start_hour") String sHour, @Query("start_minute") String sMin,
                       @Query("start_day") String sDay, @Query("start_month") String sMonth, @Query("start_year") String sYear,
                       @Query("start_time_type") String sType,  @Query("end_hour") String eHour, @Query("end_minute") String eMin,
                       @Query("end_day") String eDay, @Query("end_month") String eMonth, @Query("end_year") String eYear,
                       @Query("end_time_type") String eType);

    @POST("event/create")
    Call<Event> edit(@Query("userid") String userid, @Query("event_id") String eventId,@Query("title") String title, @Query("description") String description,
                       @Query("location") String location,@Query("address") String address,@Query("privacy") String privacy,
                       @Query("category_id") String categoryId, @Query("start_hour") String sHour, @Query("start_minute") String sMin,
                       @Query("start_day") String sDay, @Query("start_month") String sMonth, @Query("start_year") String sYear,
                       @Query("start_time_type") String sType,  @Query("end_hour") String eHour, @Query("end_minute") String eMin,
                       @Query("end_day") String eDay, @Query("end_month") String eMonth, @Query("end_year") String eYear,
                       @Query("end_time_type") String eType);
}
