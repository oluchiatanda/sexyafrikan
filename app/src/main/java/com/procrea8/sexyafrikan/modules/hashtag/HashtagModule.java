package com.procrea8.sexyafrikan.modules.hashtag;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.hashtag.activity.HashtagActivity;


public class HashtagModule implements ModuleInterface {
    @Override
    public void registerMenu(final Context context, final Drawer drawer, int i) {
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_whatshot_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Intent intent = new Intent(context, HashtagActivity.class);
                        context.startActivity(intent);
                        drawer.closeDrawer();
                        return false;
                    }
                })
                .withName(context.getResources().getString(R.string.hash_discover)));
    }

    @Override
    public Fragment getFragment(Context context) {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type, Object object) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type) {
        return null;
    }
}
