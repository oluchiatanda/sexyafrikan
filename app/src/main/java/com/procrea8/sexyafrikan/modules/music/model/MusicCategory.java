package com.procrea8.sexyafrikan.modules.music.model;


import com.google.gson.annotations.SerializedName;

public class MusicCategory {
    @SerializedName("id")
    String id;
    @SerializedName("title")
    String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
