package com.procrea8.sexyafrikan.modules.blog.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.fragment.BaseFragment;
import com.procrea8.sexyafrikan.helper.GridSpacingItemDecoration;
import com.procrea8.sexyafrikan.modules.blog.BlogApiInterface;
import com.procrea8.sexyafrikan.modules.blog.adapter.BlogsAdapter;
import com.procrea8.sexyafrikan.modules.blog.model.Blog;
import com.procrea8.sexyafrikan.modules.blog.response.BrowseResponse;
import com.procrea8.sexyafrikan.modules.music.model.MusicCategory;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlogsFragment extends BaseFragment {

    String category = "all";
    String term = "";
    String filter = "all";
    String type = "browse";
    Integer limit = 10;
    Integer page = 1;

    ArrayList<MusicCategory> categories = new ArrayList<>();

    EditText searchText;
    ImageView searchButton;
    Spinner categorySpinner;
    RecyclerView recyclerView;
    LinearLayout header;

    AVLoadingIndicatorView loadingImage;
    TextView statusText;

    Boolean paginating = false;

    String loggedUserid;

    BlogsAdapter adapter;
    ArrayList<Blog> blogs = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;

    public BlogsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blogs, container, false);
        searchText = (EditText) view.findViewById(R.id.search_text);
        searchButton = (ImageView) view.findViewById(R.id.search_button);
        categorySpinner = (Spinner) view.findViewById(R.id.category);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleview);
        header = (LinearLayout) view.findViewById(R.id.header);
        loadingImage = (AVLoadingIndicatorView) view.findViewById(R.id.loading_image);
        statusText = (TextView)view.findViewById(R.id.status_text);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);

        loggedUserid = App.getInstance().getSession().getUserid();
        adapter = new BlogsAdapter(getContext(), blogs);
        final RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(5), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisibleItems = ((GridLayoutManager) layoutManager).findLastVisibleItemPosition();
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        //we are in the last item
                        if (!paginating) {
                            page++;
                            getBlogs(true);
                            paginating = true;
                        }
                    }
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getBlogs(false);
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String search = searchText.getText().toString();
                if (!search.isEmpty()) {
                    loadingImage.show();
                    blogs.clear();
                    adapter.notifyDataSetChanged();
                    term = search;
                    getBlogs(false);
                }
            }
        });

        loadingImage.show();
        getBlogs(false);
        return view;
    }

    public void getBlogs(final Boolean more) {
        //MyLog.e("Fetching musics");
        Call<BrowseResponse> call = Api.getRetrofit().create(BlogApiInterface.class).getBlogs(
                loggedUserid, category, term, type, filter, limit, page
        );
        call.enqueue(new Callback<BrowseResponse>() {
            @Override
            public void onResponse(Call<BrowseResponse> call, Response<BrowseResponse> response) {
                //MyLog.e("Musics Fetched");
                swipeRefreshLayout.setRefreshing(false);
                if (!more) {
                    header.setVisibility(View.VISIBLE);
                    if (categories.size() < 1) {
                        ArrayList<String> items = new ArrayList<String>();
                        categories = response.body().getCategories();

                        for(int i = 0; i < categories.size(); i++) {
                            items.add(response.body().getCategories().get(i).getTitle());
                        }
                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, items);
                        categorySpinner.setAdapter(arrayAdapter);
                        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                MusicCategory theCategory = categories.get(i);
                                category = theCategory.getId();
                                page = 1;
                                getBlogs(false);
                                //MyLog.e(category);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }

                    blogs.clear();
                }

                blogs.addAll(response.body().getBlogs());
                if (response.body().getBlogs().size() > 0) {
                    statusText.setVisibility(View.GONE);
                } else {
                    statusText.setText(R.string.no_music_found);
                    statusText.setVisibility(View.VISIBLE);
                }
                adapter.notifyDataSetChanged();
                loadingImage.hide();
                paginating = false;
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<BrowseResponse> call, Throwable t) {
                loadingImage.hide();
                if (!more) {
                    statusText.setText(R.string.no_music_found);
                    statusText.setVisibility(View.VISIBLE);
                }
                //MyLog.e("Music fetch error - " + t.getMessage());
            }
        });
    }

}
