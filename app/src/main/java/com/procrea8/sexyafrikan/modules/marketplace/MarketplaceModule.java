package com.procrea8.sexyafrikan.modules.marketplace;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.AppActivity;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.marketplace.activity.MarketplaceActivity;
import com.procrea8.sexyafrikan.modules.marketplace.fragments.MarketplaceFragment;
import com.procrea8.sexyafrikan.modules.marketplace.model.Listing;
import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;

public class MarketplaceModule implements ModuleInterface{
    @Override
    public void registerMenu(final Context context, final Drawer drawer, int i) {
        drawer.addItem(new PrimaryDrawerItem().withIdentifier(i).withIcon(R.drawable.ic_shopping_cart_grey)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = new MarketplaceFragment();
                        AppActivity.setFragment(fragment);
                        AppActivity.setCurrentTitle(context.getResources().getString(R.string.listings));
                        return false;
                    }
                })
                .withName(context.getResources().getString(R.string.marketplace)));
    }

    @Override
    public Fragment getFragment(Context context) {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type, Object object) {
        if (type.equals("notification-title")) {
            NotificationModel model = (NotificationModel) object;
            switch (model.getType()) {
                case "listing.comment":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.commented_on_listend));

            }
        }else if (type.equals("notification-action")) {
            NotificationModel model = (NotificationModel) object;
            String nType = model.getType();
            if (nType.equals("listing.comment")) {
                Listing listing = model.getListings().get(0);
                MarketplaceActivity.load(context, listing);
            }
        }
        return null;
    }

    @Override
    public Object invoke(Context context, String type) {
        return null;
    }
}
