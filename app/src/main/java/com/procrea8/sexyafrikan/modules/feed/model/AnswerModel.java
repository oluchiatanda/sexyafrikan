package com.procrea8.sexyafrikan.modules.feed.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AnswerModel {
    @SerializedName("percent")
    Integer percent;
    @SerializedName("text")
    String text;
    @SerializedName("answered")
    Boolean answered;

    @SerializedName("users")
    ArrayList<TagUserModel> users;

    @SerializedName("poll_id")
    String pollId;

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public ArrayList<TagUserModel> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<TagUserModel> users) {
        this.users = users;
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getAnswered() {
        return answered;
    }

    public void setAnswered(Boolean answered) {
        this.answered = answered;
    }

    @SerializedName("id")
    String answerId;
}
