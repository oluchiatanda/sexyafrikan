package com.procrea8.sexyafrikan.modules.feed.model;

import com.google.gson.annotations.SerializedName;

public class FileModel {
    @SerializedName("extension")
    String extension;
    @SerializedName("name")
    String name;
    @SerializedName("download")
    String download;

    public String getExtensionIcon() {
        return extensionIcon;
    }

    public void setExtensionIcon(String extensionIcon) {
        this.extensionIcon = extensionIcon;
    }

    @SerializedName("extension_icon")
    String extensionIcon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public String getExtension() {

        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
