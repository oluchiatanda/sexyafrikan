package com.procrea8.sexyafrikan.modules.marketplace.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.activity.AppActivity;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.modules.marketplace.MarketplaceApiInterface;
import com.procrea8.sexyafrikan.modules.marketplace.dialog.MarketplaceCreateDialog;
import com.procrea8.sexyafrikan.modules.marketplace.model.Listing;
import com.procrea8.sexyafrikan.response.Response;

import retrofit2.Call;
import retrofit2.Callback;

public class MarketplaceActivity extends ActivityBase {
    static Listing listing;
    ImageView largeCover;
    TextView title;
    TextView time;
    TextView description;
    TextView tags;
    TextView address;
    ImageView editButton;
    ImageView deleteButton;
    Context context;
    LinearLayout layout_share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marketplace);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        largeCover = (ImageView) findViewById(R.id.large_cover);
        title = (TextView) findViewById(R.id.title);
        time = (TextView) findViewById(R.id.time);
        description = (TextView) findViewById(R.id.description);
        tags = (TextView) findViewById(R.id.tags);
        address = (TextView) findViewById(R.id.address);
        editButton = (ImageView) findViewById(R.id.edit);
        deleteButton = (ImageView) findViewById(R.id.delete);
        layout_share = (LinearLayout) findViewById(R.id.ex_share_button);
        context = this;

        if (listing.getCanEdit()) {
            editButton.setVisibility(View.VISIBLE);
            deleteButton.setVisibility(View.VISIBLE);
        } else {
            editButton.setVisibility(View.GONE);
            deleteButton.setVisibility(View.GONE);
        }

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MarketplaceCreateDialog createDialog = new MarketplaceCreateDialog(context, "edit", listing);
                createDialog.addListener(new MarketplaceCreateDialog.OnListingCreatedListener() {
                    @Override
                    public void onListingCreated(Listing list) {
                        Glide.with(context).load(list.getImage()).into(largeCover);
                        title.setText(list.getTitle());
                        time.setText(list.getTime());
                        description.setText(list.getDescription());
                        tags.setText(list.getTags());
                        address.setText(list.getAddress());

                    }
                });
            }
        });

        layout_share.setVisibility(View.VISIBLE);
        layout_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/*");
                if (listing.getSludge().equals("")) {
                    String shareUrl = SITE_BASE_URL + "marketpace/" + "listing/" + listing.getId();
                    intent.putExtra(Intent.EXTRA_TEXT, shareUrl);
                } else {
                    String shareUrl = SITE_BASE_URL + "marketplace/" + "listing/" + listing.getSludge();
                    intent.putExtra(Intent.EXTRA_TEXT, shareUrl);

                }
                startActivity(Intent.createChooser(intent, "Share via"));
            }

        });


        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder confirmDialog = new AlertDialog.Builder(context);
                confirmDialog.setMessage(R.string.are_you_sure);
                confirmDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        setLoading(getResources().getString(R.string.deleting));
                        Call<Response> call = Api.getRetrofit().create(MarketplaceApiInterface.class).delete(
                                App.getInstance().getSession().getUserid(),
                                listing.getId()
                        );
                        call.enqueue(new Callback<Response>() {
                            @Override
                            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                hideLoading();
                                Intent intent = new Intent(context, AppActivity.class);
                                startActivity(intent);
                                finish();
                            }

                            @Override
                            public void onFailure(Call<Response> call, Throwable t) {

                            }
                        });
                    }
                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Glide.with(this).load(listing.getImage()).into(largeCover);
        title.setText(listing.getTitle());
        time.setText(listing.getTime());
        description.setText(listing.getDescription());
        tags.setText(listing.getTags());
        address.setText(listing.getAddress());

        initGlobalFooterComment(this, "listing", listing.getId(), listing.getComments());
    }

    public static void load(Context context, Listing l) {
        listing = l;
        Intent intent = new Intent(context, MarketplaceActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        context.startActivity(intent);
    }

}
