package com.procrea8.sexyafrikan.modules.mediachat.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.activity.ActivityBase;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.mediachat.CallApiInterface;
import com.procrea8.sexyafrikan.response.Response;
import com.twilio.video.AudioTrack;
import com.twilio.video.CameraCapturer;
import com.twilio.video.ConnectOptions;
import com.twilio.video.LocalAudioTrack;
import com.twilio.video.LocalMedia;
import com.twilio.video.LocalParticipant;
import com.twilio.video.LocalVideoTrack;
import com.twilio.video.Media;
import com.twilio.video.Participant;
import com.twilio.video.Room;
import com.twilio.video.VideoClient;
import com.twilio.video.VideoException;
import com.twilio.video.VideoTrack;
import com.twilio.video.VideoView;

import java.util.Iterator;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class CallActivity extends ActivityBase {

    String identity = "";
    String connectionId = "";
    String identityName = "";
    String identityAvatar = "";
    String which = "call";

    Integer callType = 1; // 1 = video 2 = audio

    VideoView remoteVideo,localVideo;
    LinearLayout identityContainer,endCallButton,switchCamera;
    CircleImageView vIdentityAvatar;
    TextView vIdentityName, callingText;

    boolean enable = true;

    LocalMedia localMedia;
    VideoClient videoClient;
    Room room;
    CameraCapturer cameraCapturer;
    Context context;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);

        context = this;
        remoteVideo = (VideoView) findViewById(R.id.remote_video);
        localVideo = (VideoView) findViewById(R.id.local_video);
        identityContainer = (LinearLayout) findViewById(R.id.identity_container);
        endCallButton = (LinearLayout) findViewById(R.id.end_call);
        vIdentityAvatar = (CircleImageView) findViewById(R.id.identity_avatar);
        vIdentityName = (TextView) findViewById(R.id.identity_name);
        callingText = (TextView) findViewById(R.id.calling_text);
        switchCamera = (LinearLayout) findViewById(R.id.switch_camera);

        localMedia = LocalMedia.create(this);
        activity = this;

        //get datas
        Intent intent = getIntent();
        if (intent.hasExtra("identity")) identity = intent.getStringExtra("identity");
        if (intent.hasExtra("connection_id")) connectionId =  intent.getStringExtra("connection_id");
        if (intent.hasExtra("identity_name")) identityName = intent.getStringExtra("identity_name");
        if (intent.hasExtra("identity_avatar")) identityAvatar = intent.getStringExtra("identity_avatar");
        if (intent.hasExtra("which")) which = intent.getStringExtra("which");
        if (intent.hasExtra("call_type")) callType =  intent.getIntExtra("call_type", 1);


        if (identity.isEmpty()) {
            finish(); return;
        }

        if (connectionId.isEmpty()) {
            connectionId  = identity + "-" + App.getInstance().getSession().getUserid() + "-connection-id";
        }

        LocalAudioTrack localAudioTrack = localMedia.addAudioTrack(enable);


        cameraCapturer = new CameraCapturer(this,
                CameraCapturer.CameraSource.FRONT_CAMERA);


        if (which.equals("call")) {

        } else {
            callingText.setVisibility(View.GONE);
        }

        if (!identityName.isEmpty()) {
            vIdentityName.setText(identityName);
        }

        if (!identityAvatar.isEmpty()) {
            Glide.with(this).load(identityAvatar).into(vIdentityAvatar);
        }

        if (callType.equals(1)) {
            LocalVideoTrack localVideoTrack = localMedia.addVideoTrack(enable, cameraCapturer);
            remoteVideo.setVisibility(View.VISIBLE);
            localVideo.setVisibility(View.VISIBLE);
            switchCamera.setVisibility(View.VISIBLE);
            setPermissionGranted(new OnPermissionGranted() {
                @Override
                public void onGranted() {
                    beginCall();
                }
            });
        } else {
            beginCall();
        }

        switchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraCapturer.switchCamera();
            }
        });



        endCallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                endCall();
            }
        });


    }

    public void beginCall() {
        Call<Response> call = Api.getRetrofit().create(CallApiInterface.class).init(
                App.getInstance().getSession().getUserid(),
                identity,
                callType,
                which,
                connectionId
        );
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                ConnectOptions connectOptions;
                if (response.body().getStatus()==1) {
                    String accessToken = response.body().getDataOne();
                    videoClient = new VideoClient(context, accessToken);
                    connectOptions = new ConnectOptions.Builder()
                            .roomName(connectionId)
                            .localMedia(localMedia)
                            .build();

                    //call is ringing here
                    callingText.setText(R.string.ringing);

                    Room.Listener roomListener = new Room.Listener() {
                        @Override
                        public void onConnected(Room room) {
                            // Notifies you that you are now connected to the Room
                            if (callType.equals(1)) {
                                LocalParticipant localParticipant = room.getLocalParticipant();
                                LocalMedia media = localParticipant.getLocalMedia();

                                VideoTrack videoTrack = media.getVideoTracks().get(0);
                                // videoTrack.addRenderer(remoteVideo);
                                videoTrack.addRenderer(localVideo);
                                identityContainer.setVisibility(View.GONE);

                                if (which.equals("receive") && room.getParticipants().size() > 0) {
                                    Iterator it = room.getParticipants().entrySet().iterator();
                                    while (it.hasNext()) {
                                        Map.Entry pair = (Map.Entry)it.next();
                                        Participant participant = (Participant) pair.getValue();
                                        Media m = participant.getMedia();

                                        m.setListener(new Media.Listener() {
                                            @Override
                                            public void onAudioTrackAdded(Media media, AudioTrack audioTrack) {

                                            }

                                            @Override
                                            public void onAudioTrackRemoved(Media media, AudioTrack audioTrack) {

                                            }

                                            @Override
                                            public void onVideoTrackAdded(Media media, VideoTrack videoTrack) {
                                                videoTrack.removeRenderer(remoteVideo);
                                                videoTrack.addRenderer(remoteVideo);
                                            }

                                            @Override
                                            public void onVideoTrackRemoved(Media media, VideoTrack videoTrack) {

                                            }

                                            @Override
                                            public void onAudioTrackEnabled(Media media, AudioTrack audioTrack) {

                                            }

                                            @Override
                                            public void onAudioTrackDisabled(Media media, AudioTrack audioTrack) {

                                            }

                                            @Override
                                            public void onVideoTrackEnabled(Media media, VideoTrack videoTrack) {

                                            }

                                            @Override
                                            public void onVideoTrackDisabled(Media media, VideoTrack videoTrack) {

                                            }
                                        });
                                        it.remove(); // avoids a ConcurrentModificationException
                                    }

                                }
                            }
                        }

                        @Override
                        public void onConnectFailure(Room room, VideoException e) {
                            // Notifies you that a failure occurred when attempting to the connect to the room
                            endCall();
                        }

                        @Override
                        public void onDisconnected(Room room, VideoException e) {
                            // Notifies you that you were disconnected from the Room
                            endCall();
                        }

                        @Override
                        public void onParticipantConnected(Room room, Participant participant) {
                            callingText.setVisibility(View.GONE); //user have picked call
                            MyLog.e("Participant added");
                            // A participant joined the Room
                            if (callType.equals(1)) {
                                identityContainer.setVisibility(View.GONE);
                                Media media = participant.getMedia();

                                //VideoTrack videoTrack = media.getVideoTracks().get(0);
                                //videoTrack.addRenderer(remoteVideo);
                                media.setListener(new Media.Listener() {
                                    @Override
                                    public void onAudioTrackAdded(Media media, AudioTrack audioTrack) {

                                    }

                                    @Override
                                    public void onAudioTrackRemoved(Media media, AudioTrack audioTrack) {

                                    }

                                    @Override
                                    public void onVideoTrackAdded(Media media, VideoTrack videoTrack) {
                                        videoTrack.removeRenderer(remoteVideo);
                                        videoTrack.addRenderer(remoteVideo);
                                        MyLog.e("Video tracked added");
                                    }

                                    @Override
                                    public void onVideoTrackRemoved(Media media, VideoTrack videoTrack) {

                                    }

                                    @Override
                                    public void onAudioTrackEnabled(Media media, AudioTrack audioTrack) {

                                    }

                                    @Override
                                    public void onAudioTrackDisabled(Media media, AudioTrack audioTrack) {

                                    }

                                    @Override
                                    public void onVideoTrackEnabled(Media media, VideoTrack videoTrack) {

                                    }

                                    @Override
                                    public void onVideoTrackDisabled(Media media, VideoTrack videoTrack) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onParticipantDisconnected(Room room, Participant participant) {
                            // A participant left the Room
                            MyLog.e("Participant disconnected");
                            endCall();
                        }
                    };


                    room = videoClient.connect(connectOptions, roomListener);

                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                endCall();
            }
        });
    }


    public static void initCall(Context context, Integer callType, String identity, String identityName, String identityAvatar, String which, String connId) {
        Intent intent = new Intent(context, CallActivity.class);
        intent.putExtra("identity", identity);
        intent.putExtra("identity_name", identityName);
        intent.putExtra("identity_avatar", identityAvatar);
        intent.putExtra("which", which);
        intent.putExtra("call_type", callType);
        intent.putExtra("connection_id", connId);
        context.startActivity(intent);
    }

    public void endCall() {
        Toast.makeText(this, R.string.call_ended, Toast.LENGTH_LONG).show();
        Call<Response> call = Api.getRetrofit().create(CallApiInterface.class).endCall(
                App.getInstance().getSession().getUserid(),
                connectionId
        );
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

            }
        });
        localMedia.release();
        if (room != null) room.disconnect();
        finish();
    }

}
