package com.procrea8.sexyafrikan.modules.feed.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.Contants;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.helper.MyLog;
import com.procrea8.sexyafrikan.modules.feed.FeedApiInterface;
import com.procrea8.sexyafrikan.modules.feed.adapter.FeedAdapter;
import com.procrea8.sexyafrikan.modules.feed.dialogs.EditorDialog;

import com.procrea8.sexyafrikan.modules.feed.response.FeedResponse;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.Collection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FeedFragment extends Fragment implements Contants {
    boolean restore = false;
    public ArrayList<FeedResponse> feedLists = new ArrayList<FeedResponse>();
    FloatingActionButton newPostButton;
    com.github.clans.fab.FloatingActionButton uploadPhoto,uploadVideo,addPoll,updateStatus;
    FeedAdapter feedAdapter;
    ListView feedListView;
    AVLoadingIndicatorView feedIndicatorImage;
    TextView feedIndicatorText;
    SwipeRefreshLayout swipeRefreshLayout;
    View footerLoading;


    String type,typeId,entityType,entityId;
    Integer limit = 10;
    Integer offset = 0;
    
    Boolean moreFeedsLoading = false;
    String cacheTag = "feeds-post";

    Gson gson;
    SharedPreferences pref;

    public FeedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString("type", "feed");
            typeId = getArguments().getString("typeId", App.getInstance().getSession().getUserid());
            entityType = getArguments().getString("entityType", "user");
            entityId = getArguments().getString("entityId", typeId);;
        } else {
            type  = "feed";
            typeId = App.getInstance().getSession().getUserid();
            entityType = "user";
            entityId = typeId;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("restore", true);
        outState.putParcelableArrayList("feed_items", feedLists);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_feed, container, false);
        newPostButton = (FloatingActionButton) rootView.findViewById(R.id.new_post_button);
        feedListView = (ListView) rootView.findViewById(R.id.feed_lists);
        feedIndicatorImage = (AVLoadingIndicatorView) rootView.findViewById(R.id.loading_image);
        feedIndicatorText = (TextView) rootView.findViewById(R.id.feed_indicator_text);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.feed_swipe_refresh);
        footerLoading = (View) inflater.inflate(R.layout.loading_indicator, null);
        uploadPhoto = (com.github.clans.fab.FloatingActionButton) rootView.findViewById(R.id.upload_photo);
        uploadVideo = (com.github.clans.fab.FloatingActionButton) rootView.findViewById(R.id.upload_video);
        addPoll  = (com.github.clans.fab.FloatingActionButton) rootView.findViewById(R.id.add_poll);
        updateStatus = (com.github.clans.fab.FloatingActionButton) rootView.findViewById(R.id.update_status);

        feedListView.addFooterView(footerLoading);
        showProgress();
        addDefaultPostEditor();

        feedAdapter = new FeedAdapter(getContext(), feedLists, this.type, this.typeId, this.entityId, this.entityType);

        feedListView.setAdapter(feedAdapter);
        feedListView.removeFooterView(footerLoading);
        gson = new Gson();
        pref = getContext().getSharedPreferences(TAG, Context.MODE_PRIVATE);

        loadPosts();

        newPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //lunch the new post activity
                //launchEditor();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                offset = 0;
                loadPosts();
            }
        });

        feedListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                final int lastItem = i + i1;
                if (feedLists.size() > 4 && i2 > 0 && lastItem == i2) {
                    if (!moreFeedsLoading) {
                        loadMoreFeeds();
                        moreFeedsLoading = true;
                    }
                }
            }
        });

        uploadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchEditor("photo");
            }
        });
        uploadVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchEditor("video");
            }
        });
        addPoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchEditor("poll");
            }
        });
        updateStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchEditor("");
            }
        });
        return rootView;
    }

    public void launchEditor(String launch) {
        EditorDialog dialog = new EditorDialog(getContext(), type, typeId, entityType, entityId, launch);
        dialog.setFeedAddedListener(new EditorDialog.FeedAddedListener() {
            @Override
            public void added(FeedResponse response) {
                if (feedLists.size() > 0) {
                    feedLists.add(1, response);
                    MyLog.e(response.getMessage() + " feedLists.size() > 0"+ " -feedfragment launch editor");
                } else {
                    feedLists.add(response);
                    MyLog.e(response.getMessage() + " feedLists.size() < 0"+ " -feedfragment launch editor");
                }
                feedAdapter.notifyDataSetChanged();
            }
        });
        dialog.show();
    }


    public void addDefaultPostEditor() {

        FeedResponse feedResponse = new FeedResponse();
        feedResponse.setIsFeedItem(false);
        feedLists.add(feedResponse);
    }
    public void loadPosts() {
        loadPosts(false);
    }
    public void loadPosts(final Boolean more) {
        if (feedLists.size() < 2 && !more && type.equals("feed")) {
            loadFromCache();
        }

        Call<ArrayList<FeedResponse>> call = Api.getRetrofit().create(FeedApiInterface.class).getFeeds(App.getInstance().getSession().getUserid(),
                type, typeId, limit.toString(), offset.toString());
        call.enqueue(new Callback<ArrayList<FeedResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<FeedResponse>> call, Response<ArrayList<FeedResponse>> response) {

                if (!more) {
//                    Toast.makeText(getContext(), "Response of not more Feed Fragment", Toast.LENGTH_SHORT).show();
                    feedLists.clear();
                    if (type.equals("feed")) {
                        String text = gson.toJson(response.body());
                        pref.edit().putString(cacheTag, text).apply();
                    }

                    addDefaultPostEditor();
//                    for(FeedResponse i : response.body() ){
//                        feedLists.addAll(response.body());
//                    }
                }
                swipeRefreshLayout.setRefreshing(false);
                hideProgress();

                if (response.body() != null && response.body().size() > 0) {
//                    Toast.makeText(getContext(), "Size is greater than zero so whatsup", Toast.LENGTH_SHORT).show();
                    feedLists.addAll(response.body());
                    feedIndicatorText.setVisibility(View.GONE);
                } else {
                    Toast.makeText(getContext(), "No New Posts found", Toast.LENGTH_SHORT).show();
                    if (!more) {
                        feedIndicatorText.setText("No New Posts found");
                        MyLog.e("not more- ");
                        feedIndicatorText.setVisibility(View.VISIBLE);
                    }
                }
                feedListView.removeFooterView(footerLoading);
                feedAdapter.notifyDataSetChanged();
                moreFeedsLoading = false;
            }

            @Override
            public void onFailure(Call<ArrayList<FeedResponse>> call, Throwable t) {
                MyLog.e("Error fetching feeds....." + t.getMessage());
                swipeRefreshLayout.setRefreshing(false);
                if (!more && feedLists.size() < 2) feedIndicatorText.setText("No New Posts found");
                feedListView.removeFooterView(footerLoading);
                moreFeedsLoading = false;
            }
        });
    }

    public void loadMoreFeeds() {
        offset = offset + limit;
        feedListView.addFooterView(footerLoading);
        MyLog.e("Paginating feeds - " + offset.toString());
        loadPosts(true);
    }

    public void loadFromCache() {
        String cache = pref.getString(cacheTag, null);
        if (cache != null) {
            ArrayList<FeedResponse> feeds = gson.fromJson(cache,new TypeToken<ArrayList<FeedResponse>>() {
            }.getType());
            if (feeds != null) {
                for(int i= 0; i <feeds.size();i++) {
                    FeedResponse f = feeds.get(i);
                    feedLists.add(f);
                }
                feedAdapter.notifyDataSetChanged();
                hideProgress();
                feedIndicatorText.setVisibility(View.GONE);
            }
        }
    }

    public void showProgress() {
        feedIndicatorImage.show();
    }

    public void hideProgress() {
        feedIndicatorImage.hide();
    }
}
