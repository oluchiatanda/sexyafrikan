package com.procrea8.sexyafrikan.modules.relationship;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.mikepenz.materialdrawer.Drawer;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;

public class RelationshipModule implements ModuleInterface {
    @Override
    public void registerMenu(final Context context, Drawer drawer, int i) {

    }

    @Override
    public Fragment getFragment(Context context) {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type, Object object) {
        if (type == "notification-title") {
            NotificationModel model = (NotificationModel) object;
            switch (model.getType()) {
                case "relationship.follow":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.started_following_you));
                case "relationship.confirm":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.accepted_your_friend_request));
            }
        } else if (type.equals("notification-action")) {
            NotificationModel model = (NotificationModel) object;
            String nType = model.getType();
            if (nType.equals("relationship.follow") || nType.equals("relationship.confirm")) {
                ProfileActivity.load(context, model.getUserid());
                //MyLog.e("Profile ID - " + model.getUserid());
            }
        }
        return null;
    }

    @Override
    public Object invoke(Context context, String type) {
        return null;
    }


}
