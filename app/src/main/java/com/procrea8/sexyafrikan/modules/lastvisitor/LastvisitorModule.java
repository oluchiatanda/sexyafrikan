package com.procrea8.sexyafrikan.modules.lastvisitor;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.mikepenz.materialdrawer.Drawer;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.ModuleInterface;
import com.procrea8.sexyafrikan.modules.notification.model.NotificationModel;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;

/**
 * Created by Tiamiyu waliu kola on 9/24/2016.
 */
public class LastvisitorModule implements ModuleInterface {
    @Override
    public void registerMenu(Context context, Drawer drawer, int i) {

    }

    @Override
    public Fragment getFragment(Context context) {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        return null;
    }

    @Override
    public Object invoke(Context context, String type, Object object) {
        if (type.equals("notification-title")) {
            NotificationModel model = (NotificationModel) object;
            switch (model.getType()) {
                case "profile.view":
                    return TextUtils.concat("", "", context.getResources().getString(R.string.view_your_profile));
            }
        }else if (type.equals("notification-action")) {
            NotificationModel model = (NotificationModel) object;
            String nType = model.getType();
            if (nType.equals("profile.view")) {
                ProfileActivity.load(context, model.getUserid());
            }
        }
        return null;
    }

    @Override
    public Object invoke(Context context, String type) {
        return null;
    }
}
