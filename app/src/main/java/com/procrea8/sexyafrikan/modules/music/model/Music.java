package com.procrea8.sexyafrikan.modules.music.model;

import com.google.gson.annotations.SerializedName;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

public class Music extends Response {

    @SerializedName("id")
    String id;
    @SerializedName("title")
    String title;

    @SerializedName("has_like")
    boolean hasLike = false;
    @SerializedName("has_dislike")
    boolean hasDislike = false;
    @SerializedName("has_react")
    boolean hasReact = false;
    @SerializedName("like_count")
    Integer likeCount = 0;
    @SerializedName("dislike_count")
    Integer dislikeCount = 0;

    @SerializedName("comments")
    Integer comments;

    @SerializedName("can_edit")
    Boolean canEdit;

    @SerializedName("slug")
    String sludge;

    public String getSludge(){return sludge;}
    public void setSludge(String sludge) {this.sludge = sludge;}

    public Boolean getCanEdit() {
        return canEdit;
    }

    public void setCanEdit(Boolean canEdit) {
        this.canEdit = canEdit;
    }

    @SerializedName("react_members")
    ArrayList<ArrayList<String>> reactMembers;

    @SerializedName("privacy")
    String privacy;

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public boolean isHasLike() {
        return hasLike;
    }

    public void setHasLike(boolean hasLike) {
        this.hasLike = hasLike;
    }

    public boolean isHasDislike() {
        return hasDislike;
    }

    public void setHasDislike(boolean hasDislike) {
        this.hasDislike = hasDislike;
    }

    public boolean isHasReact() {
        return hasReact;
    }

    public void setHasReact(boolean hasReact) {
        this.hasReact = hasReact;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getDislikeCount() {
        return dislikeCount;
    }

    public void setDislikeCount(Integer dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    public Integer getComments() {
        return comments;
    }

    public void setComments(Integer comments) {
        this.comments = comments;
    }

    public ArrayList<ArrayList<String>> getReactMembers() {
        return reactMembers;
    }

    public void setReactMembers(ArrayList<ArrayList<String>> reactMembers) {
        this.reactMembers = reactMembers;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getPlayCount() {
        return playCount;
    }

    public void setPlayCount(String playCount) {
        this.playCount = playCount;
    }

    public String getFeatured() {
        return featured;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }

    @SerializedName("artist")
    String artist;
    @SerializedName("album")
    String album;
    @SerializedName("file")
    String file;
    @SerializedName("cover")
    String cover;
    @SerializedName("play_count")
    String playCount;
    @SerializedName("featured")
    String featured;
    //@SerializedName("user")


}
