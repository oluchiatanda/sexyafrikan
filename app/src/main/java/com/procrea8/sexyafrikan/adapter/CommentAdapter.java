package com.procrea8.sexyafrikan.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.App;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.api.Api;
import com.procrea8.sexyafrikan.api.CommentApiInterface;
import com.procrea8.sexyafrikan.api.LikeApiInterface;
import com.procrea8.sexyafrikan.dialogs.CommentDialog;
import com.procrea8.sexyafrikan.helper.MyWebViewClient;
import com.procrea8.sexyafrikan.helper.TimeAgo;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;
import com.procrea8.sexyafrikan.response.CommentResponse;
import com.procrea8.sexyafrikan.response.LikeResponse;
import com.procrea8.sexyafrikan.response.Response;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;


public class CommentAdapter extends BaseAdapter {
    Context context;
    ArrayList<CommentResponse> comments;
    LayoutInflater layoutInflater;
    Boolean isReply = false;


    public CommentAdapter(Context c, ArrayList<CommentResponse> lists, Boolean reply) {
        this.context = c;
        this.comments = lists;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        isReply = reply;
    }

    public  CommentAdapter(Context c, ArrayList<CommentResponse> lists) {
        this.context = c;
        this.comments = lists;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        isReply = false;
    }
    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Object getItem(int i) {
        return comments.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHolder viewHolder;

        if (view == null) {
            view = (View) layoutInflater.inflate(R.layout.comment_item, null);
            viewHolder = new ViewHolder();
            viewHolder.avatar = (CircleImageView) view.findViewById(R.id.avatar);
            viewHolder.name = (TextView) view.findViewById(R.id.author_name);
            viewHolder.text = (WebView) view.findViewById(R.id.text);
            viewHolder.delete = (ImageView) view.findViewById(R.id.delete);
            viewHolder.likeButton = (ImageView) view.findViewById(R.id.like_button);
            viewHolder.dislikeButton = (ImageView) view.findViewById(R.id.dislike_button);
            viewHolder.likeCount = (TextView) view.findViewById(R.id.like_count);
            viewHolder.dislikeCount = (TextView) view.findViewById(R.id.dislike_count);
            viewHolder.time = (TextView) view.findViewById(R.id.comment_time);
            viewHolder.image = (ImageView) view.findViewById(R.id.image);
            viewHolder.replyButton = (TextView) view.findViewById(R.id.reply_button);
            viewHolder.replyCount = (TextView) view.findViewById(R.id.reply_count);
            viewHolder.replyIcon = (ImageView) view.findViewById(R.id.reply_icon);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }


        final CommentResponse comment = comments.get(i);
        viewHolder.name.setText(comment.getName());
        //viewHolder.text.setText(comment.getText());
        viewHolder.text.setWebViewClient(new MyWebViewClient());
        viewHolder.text.getSettings().setDefaultTextEncodingName("utf-8");
        viewHolder.text.loadData(comment.getText(), "text/html; charset=utf-8", "utf-8");
        viewHolder.likeCount.setText(comment.getLikeCount().toString());
        viewHolder.dislikeCount.setText(comment.getDislikeCount().toString());
        viewHolder.time.setText(TimeAgo.format(context, comment.getTime()));

        viewHolder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (comment.getEntityType().equals("user")) {
                    ProfileActivity.load(context, comment.getEntityId());
                }
            }
        });

        viewHolder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (comment.getEntityType().equals("user")) {
                    ProfileActivity.load(context, comment.getEntityId());
                }
            }
        });
        Glide.with(context).load(comment.getAvatar()).crossFade().into(viewHolder.avatar);
        if (!comment.getImage().equals("")) {
            Glide.with(context).load(comment.getImage()).crossFade().into(viewHolder.image);
            viewHolder.image.setVisibility(View.VISIBLE);
        } else {
            viewHolder.image.setVisibility(View.GONE);
        }
        if (comment.getReplies().equals("0")) {
            //hide it
            viewHolder.replyCount.setVisibility(View.GONE);
            viewHolder.replyIcon.setVisibility(View.GONE);
        } else {
            viewHolder.replyCount.setVisibility(View.VISIBLE);
            viewHolder.replyCount.setText(comment.getReplies());
            viewHolder.replyIcon.setVisibility(View.VISIBLE);
        }

        if (isReply) {
            viewHolder.replyCount.setVisibility(View.GONE);
            viewHolder.replyButton.setVisibility(View.GONE);
            viewHolder.replyIcon.setVisibility(View.GONE);
        }
        if (comment.getHasLike()) {
            viewHolder.likeButton.setImageResource(R.drawable.ic_thumb_up_done);
        } else {
            viewHolder.likeButton.setImageResource(R.drawable.ic_thumb_up);
        }

        if (comment.getHasDislike()) {
            viewHolder.dislikeButton.setImageResource(R.drawable.ic_thumb_down_done);
        } else {
            viewHolder.dislikeButton.setImageResource(R.drawable.ic_thumb_down);
        }

        if (comment.getCanEdit()) {
            viewHolder.delete.setVisibility(View.VISIBLE);
            viewHolder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(R.string.are_you_sure);
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            String userid = App.getInstance().getSession().getUserid();
                            String commentId = comment.getId();
                            Call<Response> call = Api.getRetrofit().create(CommentApiInterface.class).remove(userid, commentId);
                            call.enqueue(new Callback<Response>() {
                                @Override
                                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                    if (response.body().getStatus()==1) {
                                        comments.remove(i);
                                        notifyDataSetChanged();
                                    } else {
                                        Toast.makeText(context, context.getResources().getString(R.string.can_delete_comment), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Response> call, Throwable t) {
                                    Toast.makeText(context, context.getResources().getString(R.string.failed_internet), Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    })
                            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                    builder.show();
                }
            });
        } else {
            viewHolder.delete.setVisibility(View.GONE);
        }

        viewHolder.likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.likeButton.setImageResource(R.drawable.ic_thumb_up_done);
                String userid = App.getInstance().getSession().getUserid();
                String id = comment.getId();
                Call<LikeResponse> call = Api.getRetrofit().create(LikeApiInterface.class).likeItem(userid, "comment", id);
                call.enqueue(new Callback<LikeResponse>() {
                    @Override
                    public void onResponse(Call<LikeResponse> call, retrofit2.Response<LikeResponse> response) {
                        processLike("like", viewHolder, i, response.body());
                    }

                    @Override
                    public void onFailure(Call<LikeResponse> call, Throwable t) {
                        Toast.makeText(context, "Failed to complete that", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        viewHolder.dislikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.dislikeButton.setImageResource(R.drawable.ic_thumb_down_done);
                String userid = App.getInstance().getSession().getUserid();
                String id = comment.getId();
                Call<LikeResponse> call = Api.getRetrofit().create(LikeApiInterface.class).dislikeItem(userid, "comment", id);
                call.enqueue(new Callback<LikeResponse>() {
                    @Override
                    public void onResponse(Call<LikeResponse> call, retrofit2.Response<LikeResponse> response) {
                        processLike("dislike", viewHolder, i, response.body());
                    }

                    @Override
                    public void onFailure(Call<LikeResponse> call, Throwable t) {
                        Toast.makeText(context, "Failed to complete that", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        viewHolder.replyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommentDialog dialog = new CommentDialog(context, true);
                dialog.setType("comment");
                dialog.setTypeId(comment.getId());
                dialog.setEntityType("user");
                dialog.setEntityTypeId(App.getInstance().getSession().getUserid());
                dialog.setIsReply(true);
                dialog.setOnCommentsChangeListener(new CommentDialog.CommentsChangedListener() {
                    @Override
                    public void onChange(Integer count) {
                        comments.get(i).setReplies(count.toString());
                        if (count > 0) {
                            viewHolder.replyCount.setVisibility(View.VISIBLE);
                            viewHolder.replyIcon.setVisibility(View.VISIBLE);
                        }
                        notifyDataSetChanged();
                    }
                });
                dialog.show();
            }
        });
        return view;
    }

    public void processLike(String type, ViewHolder holder, int position, LikeResponse response) {
        CommentResponse commentResponse = comments.get(position);
        commentResponse.setLikeCount(response.getLikes());
        commentResponse.setDislikeCount(response.getDislikes());
        commentResponse.setHasLike(response.getHas_like());
        commentResponse.setHasDislike(response.getHas_dislike());
        notifyDataSetChanged();
    }

    static class  ViewHolder{
        CircleImageView avatar;
        TextView name;
        WebView text;
        ImageView delete;
        TextView time;
        ImageView likeButton;
        ImageView dislikeButton;
        TextView likeCount;
        TextView dislikeCount;
        ImageView image;
        TextView replyButton;
        TextView replyCount;
        ImageView replyIcon;

    }
}
