package com.procrea8.sexyafrikan.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.model.Info;

import java.util.ArrayList;


public class InfoAdapter extends BaseAdapter {
    ArrayList<Info> infos;
    Context context;
    LayoutInflater layoutInflater;

    public InfoAdapter(Context c, ArrayList<Info> lists) {
        this.context = c;
        this.infos = lists;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return infos.size();
    }

    @Override
    public Object getItem(int i) {
        return infos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MyViewHolder holder;
        if (view == null) {
            view = (View) layoutInflater.inflate(R.layout.info_item, null);
            holder = new MyViewHolder();
            holder.name = (TextView) view.findViewById(R.id.info_name);
            holder.value = (TextView) view.findViewById(R.id.info_value);
            view.setTag(holder);
        } else {
            holder = (MyViewHolder) view.getTag();
        }

        Info info = (Info) getItem(i);

        String text = info.getName();
        Resources r = context.getResources();
        switch (info.getName()) {
            case "online_time":
                text = r.getString(R.string.last_seen);
                break;
            case "city":
                text = r.getString(R.string.city);
                break;
            case "state":
                text = r.getString(R.string.state);
                break;
            case "country":
                text = r.getString(R.string.country);
                break;
            case "bio":
                text = r.getString(R.string.bio);
                break;
            case "gender":
                text = r.getString(R.string.gender);
                break;
            case "birth":
                text = r.getString(R.string.birthday);
                break;
        }
        holder.name.setText(text);
        holder.value.setText(info.getValue());
        return view;
    }

    static class MyViewHolder{
        TextView name;
        TextView value;
    }
}
