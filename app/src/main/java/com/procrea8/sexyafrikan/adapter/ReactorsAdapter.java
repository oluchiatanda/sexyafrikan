package com.procrea8.sexyafrikan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.procrea8.sexyafrikan.R;
import com.procrea8.sexyafrikan.modules.profile.activity.ProfileActivity;

import java.util.ArrayList;


public class ReactorsAdapter extends BaseAdapter {
    Context context;
    ArrayList<ArrayList<String>> members;
    LayoutInflater layoutInflater;

    public ReactorsAdapter(Context c, ArrayList<ArrayList<String>> members) {
        this.context = c;
        this.members = members;
    }

    @Override
    public int getCount() {
        return members.size();
    }

    @Override
    public Object getItem(int i) {
        return members.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (view == null) {
            view = layoutInflater.inflate(R.layout.reactors_item, null);
            viewHolder = new ViewHolder();
            viewHolder.avatar = (ImageView) view.findViewById(R.id.avatar);
            viewHolder.userTitle = (TextView) view.findViewById(R.id.user_title);
            viewHolder.reactType = (ImageView) view.findViewById(R.id.react_type);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        final ArrayList<String> member = members.get(i);
        viewHolder.userTitle.setText(member.get(2));
        String reactType = member.get(1);
        if (reactType.equals("1")) {
            viewHolder.reactType.setImageResource(R.drawable.like);
        } else if(reactType.equals("4")) {
            viewHolder.reactType.setImageResource(R.drawable.love);
        } else if (reactType.equals("7")) {
            viewHolder.reactType.setImageResource(R.drawable.wow);
        } else if(reactType.equals("5")) {
            viewHolder.reactType.setImageResource(R.drawable.haha);
        } else if(reactType.equals("8")) {
            viewHolder.reactType.setImageResource(R.drawable.sad);
        } else if(reactType.equals("9")) {
            viewHolder.reactType.setImageResource(R.drawable.angry);
        } else if(reactType.equals("6")) {
            viewHolder.reactType.setImageResource(R.drawable.yay);
        }

        Glide.with(context).load(member.get(0)).crossFade().into(viewHolder.avatar);
        viewHolder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfileActivity.load(context, member.get(3));
            }
        });
        return view;
    }

    public void addAll(ArrayList<ArrayList<String>> memb) {
        members.clear();
        members.addAll(memb);
        notifyDataSetChanged();
    }

    static class ViewHolder{
        ImageView avatar;
        TextView userTitle;
        ImageView reactType;
    }
}
